package pruebascrudrepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiaRepository;


public class AppCreate {
	
	public static void main(String[] args) {

		/*Noticia2 noticia = new Noticia2();
		noticia.setTitulo("Noticia Test");
		noticia.setDetalle("Detalle de la noticia test");
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		//Generamos la sentencia INSERT para guardar nuestra Noticia
		repo.save(noticia);
		context.close();*/
		
		Noticia noticia = new Noticia();
		noticia.setTitulo("Noticia Test");
		noticia.setDetalle("Detalle de la noticia test");
		noticia.setEstado("Activa");
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiaRepository repo = context.getBean("noticiaRepository", NoticiaRepository.class);
		//Generamos la sentencia INSERT para guardar nuestra Noticia
		repo.save(noticia);
		context.close();

	}
}
