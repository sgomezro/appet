package com.sgomezr.appet.controller;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Contacto;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IContactosService;

@Controller
@RequestMapping("/contacto")
public class ContactoController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(ContactoController.class);

	@Autowired
	IAnimalesService serviceAnimales;
	
	@Autowired
	IContactosService serviceContactos;
	
	@GetMapping("/listaContactos")
	public String listaContactos(Model model) {
		List<Contacto> contactos = serviceContactos.buscarTodosLosContactos();
		model.addAttribute("contactos", contactos);
		return "contacto/listaContactos";
	}

	@GetMapping(value = "/crear")
	public String crear(@ModelAttribute Contacto contacto, Model model) {
		model.addAttribute("tipos", serviceAnimales.buscarTipologias());
		model.addAttribute("tiposNotificaciones", tipoNotificaciones());
		return "contacto/formContacto";
	}

	@PostMapping(value = "/guardar")
	public String guardar(@ModelAttribute Contacto contacto, BindingResult result, RedirectAttributes atributos) {
		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "contacto/formContacto";
		}
		
		serviceContactos.insertar(contacto);
		atributos.addFlashAttribute("mensaje", "Su mensaje a sido enviado correctamente a los administradores de ApPet");
		return "redirect:/contacto/crear";
	}

	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idContacto") int idContacto, Model model,RedirectAttributes atributos) {
		serviceContactos.eliminar(idContacto);
		atributos.addFlashAttribute("mensaje", "El mensaje fue eliminado correctamente");
		return "redirect:/contacto/listaContactos";
	}
	
	private List<String> tipoNotificaciones() {
		List<String> tipos = new LinkedList<>();
		tipos.add("Noticias");
		tipos.add("Nuevos Animales");
		tipos.add("Animales adoptados");
		tipos.add("Nuevas protectoras");
		return tipos;
	}
}