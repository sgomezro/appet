package com.sgomezr.appet.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

	@Entity
	@Table(name = "animalesfavoritos")
	public class Favorito {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id_favorito")
		private int idFavorito;

		@Column(name = "Favoritoscol")
		private String favoritosCol;
		
		@Column(name = "id_usuario")
		private int idUsuario;
		
		@ManyToOne
		@JoinColumn(name = "Animales_id_animal")
		private Animal animal;
		
	    
		public int getIdUsuario() {
			return idUsuario;
		}

		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}

		public int getIdFavorito() {
			return idFavorito;
		}

		public void setIdFavorito(int idFavorito) {
			this.idFavorito = idFavorito;
		}

		public String getFavoritosCol() {
			return favoritosCol;
		}

		public void setFavoritosCol(String favoritosCol) {
			this.favoritosCol = favoritosCol;
		}

		public Animal getAnimal() {
			return animal;
		}

		public void setAnimal(Animal animal) {
			this.animal = animal;
		}

		@Override
		public String toString() {
			return "Favorito [idFavorito=" + idFavorito + ", favoritosCol=" + favoritosCol + ", idUsuario=" + idUsuario
					+ ", animal=" + animal + "]";
		}
}
