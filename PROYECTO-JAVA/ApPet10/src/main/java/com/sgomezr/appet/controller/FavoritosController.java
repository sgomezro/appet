package com.sgomezr.appet.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.repository.FavoritosRepository;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IFavoritosService;
import com.sgomezr.appet.service.IUsuariosService;

@Controller
@RequestMapping("/favoritos")
public class FavoritosController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(FavoritosController.class);

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IUsuariosService serviceUsuarios;

	@Autowired
	private IFavoritosService serviceFavoritos;
	
	@GetMapping("/listaAnimalesFavoritos")
	public String listaAnimalesFavoritos(Model model, Authentication authentication) {
		int idUsuario =(serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
		List<Favorito> listaFavoritos = serviceFavoritos.buscarFavoritosPorIdUsuario(idUsuario);
	
		model.addAttribute("favoritos", listaFavoritos);
		
		return "favoritos/listaAnimalesFavoritos";
	}

	@GetMapping("/listaProtectorasFavoritas")
	public String listaProtectorasFavoritas() {
		return "favoritos/listaProtectorasFavoritas";
	}

	@GetMapping("/agregar")
	@Transactional
	public String agregar(@RequestParam("idAnimal") int idAnimal, @RequestParam("username") String username,
			RedirectAttributes atributos) {
		
		Favorito favorito = new Favorito();
		favorito.setAnimal(serviceAnimales.buscarAnimalPorId(idAnimal));
		favorito.setFavoritosCol("rojo");
		favorito.setIdUsuario((serviceUsuarios.buscarUsuarioPorUsername(username)).getId());
		if (serviceFavoritos.existeFavorito(idAnimal,
				(serviceUsuarios.buscarUsuarioPorUsername(username)).getId()).equals(true)) {
			atributos.addFlashAttribute("mensaje", "El animal seleccionado ya se encuentra en tu listado de favoritos");
		} else {
			serviceFavoritos.agregar(favorito);
			atributos.addFlashAttribute("mensaje", "El animal ha sido a�adido a su lista de favoritos");
		}
		return "redirect:/favoritos/listaAnimalesFavoritos";
	}
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idFavorito") int idFavorito, Model model, RedirectAttributes atributos) {
		serviceFavoritos.eliminar(idFavorito);
		atributos.addFlashAttribute("mensaje", "El animal fue eliminado correctamente de su lista de favoritos");
		return "redirect:/favoritos/listaAnimalesFavoritos";
	}

}
