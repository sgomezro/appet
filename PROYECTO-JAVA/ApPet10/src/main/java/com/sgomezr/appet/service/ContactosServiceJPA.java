package com.sgomezr.appet.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Contacto;
import com.sgomezr.appet.repository.ContactosRepository;


@Service
public class ContactosServiceJPA implements IContactosService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(ContactosServiceJPA.class);
	
	@Autowired
	private ContactosRepository contactosRepository;
	
	@Override
	public void insertar(Contacto contacto) {
		contactosRepository.save(contacto);
	}

	@Override
	public List<Contacto> buscarTodosLosContactos() {
		return contactosRepository.findAll();
	}

	@Override
	public void eliminar(int idContacto) {
		contactosRepository.deleteById(idContacto);
		
	}
		

}
