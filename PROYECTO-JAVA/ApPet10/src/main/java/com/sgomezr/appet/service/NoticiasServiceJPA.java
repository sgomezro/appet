package com.sgomezr.appet.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.repository.NoticiaRepository;

@Service
public class NoticiasServiceJPA implements INoticiasService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(NoticiasServiceJPA.class);
		
	@Autowired
	private NoticiaRepository noticiasRepository;
	
	@Override
	public void guardarNoticia(Noticia noticia) {
		logger.info("Guadando el objeto " + noticia + " en la base de datos.");	
	}
	
	@Override
	public Noticia buscarNoticiaPorId(int idNoticia) {
		Optional<Noticia> noticia = noticiasRepository.findById(idNoticia);
		if(noticia.isPresent()) {
			return noticia.get();
		}
		return null;
	}

	@Override
	public void insertarNoticia(Noticia noticia) {
		noticiasRepository.save(noticia);	
	}

	@Override
	public List<Noticia> buscarTodasLasNoticias() {
		return noticiasRepository.findAll();
	}

	@Override
	public void eliminar(int idNoticia) {
		noticiasRepository.deleteById(idNoticia);
	}

	@Override
	public Page<Noticia> buscarTodasPaginacion(Pageable page) {
		return noticiasRepository.findAll(page);
	}

}
