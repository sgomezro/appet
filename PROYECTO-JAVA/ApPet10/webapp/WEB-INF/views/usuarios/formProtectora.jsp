<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Protectora - Formulario de registro</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/usuarios/guardar" var="urlForm"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="${urlPublic}/css/util.css">
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/mainformUsuario.css">
<!--===============================================================================================-->
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<!-- DatePicker script -->
<script>
	$(document).ready(
			function() {
				var date_input = $('input[name="fechaNacimiento"]'); //our date input has the name "date"
				var container = $('.bootstrap-iso form').length > 0 ? $(
						'.bootstrap-iso form').parent() : "body";
				var options = {
					format : 'dd/mm/yyyy',
					container : container,
					todayHighlight : true,
					autoclose : true,
				};
				date_input.datepicker(options);
			})
</script>
</head>
<body onload="desabilitarInput()">

	<div class="bg-contact2"
		style="background-image: url('${urlPublic}/images/fondos/general026.jpg');">
		<div class="container-contact2">
			<div class="wrap-contact2">

				<form:form action="${urlForm}" method="post"
					enctype="multipart/form-data" modelAttribute="usuario"
					class="contact100-form validate-form">
					<span class="login100-form-title"> <img style="width: 65%"
						src="${urlPublic}/images/ApPetLogo.JPG" alt="ApPetLogo">
					</span>
					<div class="text-center p-b-30">
						<span class="txt2"> "Mejora su mundo, mejora tu mundo" </span>
					</div>
					<span class="contact100-form-title"> �Tienes una protectora? �Registrala!
					</span>
					<spring:hasBindErrors name="usuario">
						<div class='alert alert-danger' role='alert'>
							Por favor corrija los siguientes errores:
							<ul>
								<c:forEach var="error" items="${errors.allErrors}">
									<li>&bull; <spring:message message="${error}" /></li>
								</c:forEach>
							</ul>
						</div>
					</spring:hasBindErrors>


					<input style="display: none" id="perfil" class="form-control"
						name="perfil" placeholder="Perfil" value="PROTECTORA-NA" />
					<span class="focus-input100"></span>
					<hr>
					<span style="background-color:#e0e0d1; font-size:20px" class="contact100-form-title">	
					DATOS GENERALES DE LA PROTECTORA</span>
					<hr>
					<div class="wrap-input100 validate-input"
						data-validate="El nombre de la protectora es un campo obligatorio">
						<label class="label-input100" for="username">Nombre la protectora*</label>
						<form:hidden path="id" />
						<form:input id="username" class="input100" type="text"
							path="username" required="required"
							placeholder="Nombre de la protectora" />
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="La localidad es un campo obligatorio">
						<label class="label-input100" for="localidad">Localidad en la que se encuentra la protectora*</label>
						<form:select id="localidad" class="form-control" path="localidad"
							required="required" placeholder="Seleccione su localidad"
							items="${localidades }">
						</form:select>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="El email debe de tener el formato correcto (example@example.com)">
						<label class="label-input100" for="nombre">Email de la protectora*</label>
						<form:input id="email" class="input100" type="text" path="email"
							required="required"
							placeholder="Formato: example@example.com" />
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="La contrase�a es un campo obligatorio">
						<label class="label-input100" for="nombre">Contrase�a*</label>
						<form:input id="password" class="input100" type="password"
							path="password" required="required"
							placeholder="Introduzca la contrase�a" />
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="La contrase�a es un campo obligatorio">
						<label class="label-input100" for="nombre">Repetir
							contrase�a*</label>
						<form:input id="comprobarPass" class="input100" type="password"
							path="comprobarPass" required="required"
							placeholder="Repita su contrase�a" />
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="La fecha de creaci�n debe de tener el formato correcto (dd/mm/yyyy)">
						<label class="label-input100" for="fechaNacimiento">Fecha
							de creaci�n de la protectora</label>
						<form:input id="fechaNacimiento" class="input100" type="text"
							path="fechaNacimiento" placeholder="Formato: dd/mm/yyyy" />
						<span class="focus-input100"></span>
					</div>
					
					<div class="wrap-input100 validate-input">
						<label class="label-input100" for="imagen">Imagen/Logo de la protectora</label>
						<form:hidden path="imagen" />
						<input type="file" id="archivoImagen" name="archivoImagen">
						<span class="focus-input100"></span>
					</div>
					<hr>
					<span style="background-color:#e0e0d1; font-size:17px" class="contact100-form-title">
					DATOS GENERALES DE SU ENCARGADO/RESPONSABLE</span>
					<hr>
					<div class="wrap-input100 validate-input"
						data-validate="El nombre es un campo obligatorio">
						<label class="label-input100" for="nombre">Nombre del encargado de la protectora*</label>
						<form:input id="nombre" class="input100" type="text" path="nombre"
							required="required" placeholder="Introduzca el nombre del encargado" />
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="Los apellidos son un campo obligatorio">
						<label class="label-input100" for="nombre">Apellidos del encargado de la protectora*</label>
						<form:input id="apellidos" class="input100" type="text"
							path="apellidos" required="required"
							placeholder="Introduzca los apellidos del encargado" />
						<span class="focus-input100"></span>
					</div>
					<c:choose>
					<c:when test="${prf.perfil =='ADMINISTRADOR'}">
						<div class="wrap-input100 validate-input"
						data-validate="El estado es requerido">
						<label class="label-input100" for="estado">Estado de la cuenta:</label>
						<form:select id="estado" class="form-control" path="estado"
							placeholder="Estado">
							<option value=1>Activa</option>
							<option value=0>Inactiva</option>
						</form:select>
						<span class="focus-input100"></span>
					</div>
					</c:when>
					</c:choose>
					
					

					<div>(*) Campos obligatorios</div>
					<div class="container-contact100-form-btn">
						<button type="submit" class="contact100-form-btn">ENVIAR</button>
					</div>

					<div class="contact100-form-social flex-c-m">
						<a href="#" class="contact100-form-social-item flex-c-m bg1 m-r-5">
							<i class="fa fa-facebook-f" aria-hidden="true"></i>
						</a> <a href="#"
							class="contact100-form-social-item flex-c-m bg2 m-r-5"> <i
							class="fa fa-twitter" aria-hidden="true"></i>
						</a> <a href="#" class="contact100-form-social-item flex-c-m bg3">
							<i class="fa fa-youtube-play" aria-hidden="true"></i>
						</a>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!--===============================================================================================-->
	<script src="${urlPublic}/bootstrap/js/popper.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/main.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/disableinput.js"></script>

</body>
</html>
