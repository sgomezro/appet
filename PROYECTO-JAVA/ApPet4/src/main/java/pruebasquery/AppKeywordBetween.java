package pruebasquery;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

public class AppKeywordBetween {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);

		// Ejemplo Keyword Or
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); //Formato en el que MySQL maneja las fechas
		List<Noticia2> lista = null;
		
		try {
			lista = repo.findByFechaPublicacionBetween(format.parse("2017-09-03"), format.parse("2017-09-06"));	
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		for (Noticia2 n : lista) {
			System.out.println(n);
		}

		context.close();

	}
}
