package com.sgomezr.appet.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="Animal")
public class Animal {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_animal")
	private int id;
	
	@Column(name="nombre_animal")
	private String nombre;
	
	@Column(name="tipo_animal")
	private String tipo;
	
	@Column(name="raza_animal")
	private String raza;
	
	@Column(name="sexo_animal")
	private String sexo;
	
	@Column(name="fechaNacimiento_animal")
	private Date fechaNacimiento;
	
	@Column(name="imagen_animal")
	private String imagen = "animal.jpg"; //Imagen por defecto
	
	//@Transient //ignorar este atributo durante la persistencia
	@ManyToOne
	@JoinColumn(name="Protectora_id_protectora")
	private Protectora protectora;
	
	@Column(name="tieneMicrochip_animal")
	private String tieneMicrochip;
	
	@Column(name="descripcion_animal")
	private String descripcion;
	
	@Column(name="fechaPublicacion_animal")
	private Date fechaPublicacion;
	
	@Column(name="estado_animal")
	private String estado = "En adopcion"; //Por defecto en adopci�n
	
	@Column(name="comportamiento_animal")
	private String comportamiento;
	
	@Column(name="envio_animal")
	private String envio;
	
	@Column(name="tasaAdopcion_animal")
	private int tasaAdopcion;
	
	@Column(name="urlVideo_animal")
	private String urlVideo = "Sin video";
	
	/**
	 * Constructor de la clase
	 */
	public Animal(){		
		this.fechaPublicacion = new Date(); // Por default, la fecha del sistema
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public Protectora getProtectora() {
		return protectora;
	}
	public void setProtectora(Protectora protectora) {
		this.protectora = protectora;
	}
	public String getTieneMicrochip() {
		return tieneMicrochip;
	}
	public void setTieneMicrochip(String tieneMicrochip) {
		this.tieneMicrochip = tieneMicrochip;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getComportamiento() {
		return comportamiento;
	}
	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}
	public String getEnvio() {
		return envio;
	}
	public void setEnvio(String envio) {
		this.envio = envio;
	}
	public int getTasaAdopcion() {
		return tasaAdopcion;
	}
	public void setTasaAdopcion(int tasaAdopcion) {
		this.tasaAdopcion = tasaAdopcion;
	}
	public String getUrlVideo() {
		return urlVideo;
	}
	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}
	@Override
	public String toString() {
		return "Animal [id=" + id + ", nombre=" + nombre + ", tipo=" + tipo + ", raza=" + raza + ", sexo=" + sexo
				+ ", fechaNacimiento=" + fechaNacimiento + ", imagen=" + imagen + ", protectora=" + protectora
				+ ", tieneMicrochip=" + tieneMicrochip + ", descripcion=" + descripcion + ", fechaPublicacion="
				+ fechaPublicacion + ", estado=" + estado + ", comportamiento=" + comportamiento + ", envio=" + envio
				+ ", tasaAdopcion=" + tasaAdopcion + ", urlVideo=" + urlVideo + "]";
	}
}
