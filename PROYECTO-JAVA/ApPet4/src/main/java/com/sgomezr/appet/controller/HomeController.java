package com.sgomezr.appet.controller;


import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.util.Utileria;

@Controller
public class HomeController {
	
	@Autowired
	private IAnimalesService serviceAnimales;
	
	@Autowired
	private IProtectorasService serviceProtectoras;
	
	private Protectora protectora = new Protectora();

	@RequestMapping(value = "/home", method=RequestMethod.GET)
	public String goHome() {
	return "home";
	}
	
	@RequestMapping(value = "/busqueda", method=RequestMethod.POST)
	public String buscarPorProtectora (@RequestParam ("protectora") String protectora, Model model) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		System.out.println(protectora);
		List<Animal> animales = serviceAnimales.buscarTodosLosAnimales();
		int idProtectora = serviceProtectoras.buscaIdProtectora(protectora);
		List<Animal> animalesPorProtectora = serviceAnimales.buscarAnimalPorProtectora(idProtectora);
		
		List <String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras",protectoras);

		model.addAttribute("protectoraBusqueda", protectora);
		
		if(protectora.equals("Mostrar todos")) {
			model.addAttribute("animales", animales);
		
		}else {
		model.addAttribute("animales", animalesPorProtectora);
		}
		
		System.out.println("Buscando todos los animales de la protectora: " + protectora);
		return "home";
	}
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public String mostrarAnimales(Model model) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();

		List<Animal> animales = serviceAnimales.buscarTodosLosAnimales();
		List <String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras",protectoras);
		model.addAttribute("protectoraBusqueda",protectora.getNombreProtectora());
		model.addAttribute("animales", animales);
		
	return "home";
	
	}
	
	@RequestMapping(value = "/detalleAnimal", method=RequestMethod.GET)
	public String mostrarDetalleAnimal(Model model, @RequestParam("idAnimal") int idAnimal,   @RequestParam("protectora") String protectora) {	
		System.out.println("El id del Animal es: " + idAnimal);
		System.out.println("La protectora en la que se encuentra ese animal es: " + protectora );
		model.addAttribute("animal", serviceAnimales.buscarAnimalPorId(idAnimal));
		//TODO: Buscar en la base de datos la protectora asociada
	
	return "detalleAnimal";
	}
	
	@RequestMapping(value = "/detalleProtectora", method=RequestMethod.GET)
	public String mostrarDetalleProtectora(Model model, @RequestParam("idProtectora") int idProtectora,@RequestParam("idAnimal") int idAnimal) {	
		System.out.println("El id de la protectora es: " + idProtectora);
		System.out.println("El id del Animal es: " + idAnimal);
		model.addAttribute("protectora", serviceProtectoras.buscarProtectoraPorId(idProtectora));
		//TODO: Buscar en la base de datos la protectora asociada
	
	return "detalleProtectora";
	}
	
	@RequestMapping(value="/formLogin", method=RequestMethod.GET)
	public String mostrarLogin() {
		return "formLogin";
	}
	
}
