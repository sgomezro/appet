package com.sgomezr.appet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;


public interface IProtectorasService {

	List<Protectora> buscarTodasLasProtectoras();
	
	void insertar (Protectora protectora);
	
	Protectora buscarProtectoraPorId (int idProtectora);
	
	int buscaIdProtectora(String nombreProtectora);
	
	List<String> buscarLocalidades();
	
	void eliminar(int idProtectora);
	
	Page<Protectora> buscarTodasPaginacion(Pageable page);
	

}
