<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>ApPet</title>
<!-- Tag de Spring para agregar recursos estaticos -->
<spring:url value="../resources" var="urlPublic"></spring:url>
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">


<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

</head>
<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div style="margin-left: 30px; margin-right: 30px;">


			<!-- Jumbotron Header -->
			<header class="jumbotron my-0"
				style="background-color: white; padding-bottom: 0px">
				<h1 class="display-3" style="color: black">Bienvenid@ a ApPet!</h1>
				<h3 class="lead" style="font-size: 2.25rem !important; color: black">Mejora
					su vida, mejora tu vida!</h3>

				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">

						<c:forEach items="${banners}" var="banner" varStatus="loop">
							<c:choose>
								<c:when test="${banner.estado eq 'Activo'}">
									<c:choose>
										<c:when test="${loop.index==0}">
											<li data-target="#myCarousel" data-slide-to="${loop.index}"
												class="active"></li>
										</c:when>
										<c:otherwise>
											<li data-target="#myCarousel" data-slide-to="${loop.index}"></li>
										</c:otherwise>
									</c:choose>
								</c:when>
							</c:choose>
						</c:forEach>
						<!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>-->

					</ol>
					<div class="carousel-inner"
						style="height: 300px; border-radius: .3rem;">

						<c:forEach items="${banners}" var="banner" varStatus="loop">
							<c:choose>
								<c:when test="${banner.estado eq 'Activo'}">
									<c:choose>
										<c:when test="${loop.index==0}">
											<div class="carousel-item active">
												<img class="img-responsive" style="filter: grayscale(100%); width: 100%"
													src="${urlPublic}/images/${banner.archivo}"
													alt="${banner.titulo}" title="${banner.titulo}">
												<div class="carousel-caption"
													style="position: sticky !important">
													<h4>${banner.titulo}</h4>
												</div>
											</div>
										</c:when>
										<c:otherwise>
											<div class="carousel-item">
												<img class="img-responsive" style="filter: grayscale(100%);width: 100%"
													src="${urlPublic}/images/${banner.archivo}"
													alt="${banner.titulo}" title="${banner.titulo}">
												<div class="carousel-caption"
													style="position: sticky !important">
													<h4>${banner.titulo}</h4>
												</div>
											</div>
										</c:otherwise>
									</c:choose>
								</c:when>
							</c:choose>
						</c:forEach>
					</div>
				</div>
			</header>

			<div class="col-lg-6 col-sm-6 col-md-6"
				style="text-align: center; margin-left: 25%; border-bottom: 2px solid black; margin-bottom: 20px">
				<h1 class="my-4">
					ApPet <small>�Qui�nes somos?</small>
				</h1>
			</div>

			<div class="container"
				style="position: relative; text-align: center; color: white;">
				<div class="row">
					<div style="padding-right: 2px; padding-left: 2px;"
						class="col-lg-6 co-md-6 col-sm-12">
						<img class="img-responsive" style="opacity: 0.3; width: 100%"
							src="${urlPublic}/images/Oto�o04.png" alt="" title="">
						<div class="centered"
							style="font-size: 1.5rem; color: black; position: absolute; top: 20%; left: 20%; transform: translate(-10%, -10%);">
							Plataforma para la <strong>adopci�n de animales
								procedentes de protectoras, asociaciones, rescatistas y
								administraciones p�blicas</strong>. Adopta a tu nuevo mejor amigo gracias
							a<strong> ApPet.</strong>
						</div>
					</div>

					<div class="col-lg-6 co-md-6 col-sm-12 effect"
						style="padding-right: 2px; padding-left: 2px;">
						<img class="img-responsive" style="width: 100%"
							src="${urlPublic}/images/Oto�o02.jpg" alt="" title="">
						<div class="overlay">
							<div class="text" style="font-size:3rem">Bienvenido a <strong>ApPET!</strong></div>
						</div>
					</div>
					<div class="col-lg-6 co-md-6 col-sm-12 effect"
						style="padding-right: 2px; padding-left: 2px;">
						<img class="img-responsive" style="width: 100%"
							src="${urlPublic}/images/Oto�o01.png" alt="" title="">
							<div class="overlay">
							<div class="text" style="font-size:3rem">Mejora <strong>SU</strong> vida, mejora <strong>TU</strong> vida</div>
						</div>
					</div>
					<div style="padding-right: 2px; padding-left: 2px;"
						class="col-lg-6 co-md-6 col-sm-12">
						<img class="img-responsive" style="opacity: 0.3; width: 100%"
							src="${urlPublic}/images/Oto�o03.png" alt="" title="">
						<div class="centered"
							style="font-size: 1.7rem; color: black; position: absolute; top: 20%; left: 20%; transform: translate(-10%, -10%);">
							�Te recomendamos mascotas que se adapten a tu forma y estilo de
							vida! �Estas pensando en adoptar? <strong>�Pues prueba
								ApPET!</strong>
						</div>
					</div>
				</div>
			</div>
			<!--  <div class="row">
				<img style="height: 100%; widht: 100%" class="card-img-top"
					src="${urlPublic}/images/fondo2.png" alt="">
			</div>-->
			<div class="col-lg-6 col-sm-6 col-md-6"
				style="text-align: center; margin-left: 25%; border-bottom: 2px solid black;margin-bottom: 20px">
				<h1 class="my-4">
					ApPet <small>Ultimas noticias</small>
				</h1>
			</div>

			<div class="row">
				<c:forEach items="${ noticias }" var="noticia">
					<c:choose>
						<c:when test="${noticia.estado eq 'Activa'}">
							<div class="col-sm-12 col-md-12 col-lg-6"
								style="padding-top: 5px; padding-bottom: 60px">
								<div class="card h-100"
									style="border-width: 1px; border-color: #565656; padding-bottom: 15px;">

									<div class="card-text"
										style="margin: 15px; margin-bottom: 0px;">
										<h2>${noticia.titulo }</h2>
										<a href="#"><img class="card-img-top"
											style="height: 300px;"
											src=${urlPublic}/images/${noticia.imagen } alt=""></a>
										<p>${noticia.fechaPublicacion }</p>
										<p>${noticia.detalle}</p>
										<div style="text-align: right">
											<a href="#" class="btn-default"
												style="background-color: #76323f; color: white !important; padding-bottom: 2px; padding-top: 2px; border: 1px solid black; padding-left: 80px; padding-right: 80px;"><i
												class="fa-mouse-pointer fas button-icon-left"
												style="padding-right: 10px;"></i><span>M�S
													INFORMACI�N</span></a>
										</div>
									</div>
								</div>
							</div>
						</c:when>
					</c:choose>
				</c:forEach>
			</div>
		</div>
	</div>



	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>

	<!-- Bootstrap core JavaScript -->
	<script
		src="https://ajax.googleapis.com/Ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>