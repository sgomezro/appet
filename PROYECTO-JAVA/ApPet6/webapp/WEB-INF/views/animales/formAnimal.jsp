<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>ApPet - Nuevo animal</title>

<!-- Tag de Spring para agregar recursos estaticos -->
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/animales/guardar" var="urlForm"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<!-- DatePicker script -->
<script>
	$(document).ready(
			function() {
				var date_input = $('input[name="fechaNacimiento"]'); //our date input has the name "date"
				var container = $('.bootstrap-iso form').length > 0 ? $(
						'.bootstrap-iso form').parent() : "body";
				var options = {
					format : 'dd-mm-yyyy',
					container : container,
					todayHighlight : true,
					autoclose : true,
				};
				date_input.datepicker(options);
			})
</script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/Banner3.jpg)">

				<h1 class="display-3" style="color: white">Nuevo animal</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">
				<span class="label label-success">Datos del animal</span>
			</h3>
			<div class="separator"
				style="border: 0 solid #2F4F4F; clear: both; position: relative; z-index: 11; border-color: #2F4F4F; border-top-width: 3px; margin-top: 0px; margin-bottom: 20px; width: 100%; max-width: 260px;"></div>


			<spring:hasBindErrors name="animal">
				<div class='alert alert-danger' role='alert'>
					Por favor corrija los siguientes errores:
					<ul>
						<c:forEach var="error" items="${errors.allErrors}">
							<li><spring:message message="${error}" /></li>
						</c:forEach>
					</ul>
				</div>
			</spring:hasBindErrors>
			<form:form action="${urlForm}" method="post"
				enctype="multipart/form-data" modelAttribute="animal">
				<div class="row">

					<div class="col-sm-4">
						<div class="form-group">
							<img style="width: 250px; height: 175px;" class="card-img-top"
								src=${urlPublic}/images/${animal.imagen
								} title="Imagen actual del animal">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="nombre">Nombre:</label>
							<form:hidden path="id" />
							<form:input placeholder="Nombre del animal" type="text"
								class="form-control" path="nombre" id="nombre"
								required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="tipo">Tipologia:</label>
							<form:select
								placeholder="Tipologia del animal. Ej: Perro, Gato..."
								class="form-control" path="tipo" items="${tipos }" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="raza">Raza:</label>
							<form:input placeholder="Raza del animal" type="text"
								class="form-control" path="raza" id="raza" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="sexo">Sexo:</label>
							<form:select class="form-control" path="sexo" id="sexo" required="required">
								<form:option value="Macho">Macho</form:option>
								<form:option value="Hembra">Hembra</form:option>
							</form:select>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="badgetForm" for="imagen">Fecha de
							nacimiento:</label>
						<div class="form-group has-feedback">
							<i class="far fa-calendar form-control-feedback"></i>
							<form:input type="text" class="form-controlIcon"
								id="fechaNacimiento" path="fechaNacimiento"
								placeholder="dd-mm-yyyy" required="required"/>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="tieneMicrochip">�Tiene
								microchip?:</label>
							<div class="form-check">
								<label><form:radiobutton path="tieneMicrochip"
										id="tieneMicrochip" value="Si" />S�</label> <label><form:radiobutton
										path="tieneMicrochip" id="tieneMicrochip" required="required" value="No" />No</label>

							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="estado">Estado:</label>
							<form:select id="estado" path="estado" class="form-control" required="required">
								<form:option value="En adopcion">En adopcion</form:option>
								<form:option value="En apadrinamiento">En apadrinamiento</form:option>
								<form:option value="Apadrinado">Apadrinado</form:option>
								<form:option value="Adoptado">Adoptado</form:option>
							</form:select>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="descripcion">Descripci�n
								del animal</label>
							<form:textarea class="form-control" path="descripcion"
								id="descripcion" rows="10" required="required"></form:textarea>
						</div>
					</div>
					<div class="col-sm-4">

						<label class="badgetForm" for="comportamiento">Comportamiento</label>
						<div>
							Es bueno con otros animales<label class="switch"
								style="margin-left: 30px"> <input name="comportamiento"
								id="comportamiento" type="checkbox"
								value="Es bueno con otros animales"> <span
								class="slider"></span>
							</label>
						</div>
						<div>
							Es bueno con ni�os<label class="switch" style="margin-left: 30px">
								<input name="comportamiento" id="comportamiento" type="checkbox"
								value="Es bueno con ni�os"> <span class="slider"></span>
							</label>
						</div>
						<div>
							Es cari�oso<label class="switch" style="margin-left: 30px">
								<input name="comportamiento" id="comportamiento" type="checkbox"
								value="Es cari�oso"> <span class="slider"></span>
							</label>
						</div>
						<div>
							Es amigable<label class="switch" style="margin-left: 30px">
								<input name="comportamiento" id="comportamiento" type="checkbox"
								value="Es amigable"> <span class="slider"></span>
							</label>
						</div>
						<div>
							Muerde<label class="switch" style="margin-left: 30px"> <input
								name="comportamiento" id="comportamiento" type="checkbox"
								value="Muerde"> <span class="slider"></span>
							</label>
						</div>
						<div>
							Ara�a<label class="switch" style="margin-left: 30px"> <input
								name="comportamiento" id="comportamiento" type="checkbox"
								value="Ara�a"> <span class="slider"></span>
							</label>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="envio">Se envia a:</label>
							<form:select placeholder="Localidad de envio."
								class="form-control" path="envio" required="required"
								items="${localidades }" />
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="tasaAdopcion">Tasa de
								adopci�n (en euros)</label>
							<form:input placeholder="Tasa de adopcion en euros" tyspe="text"
								class="form-control" path="tasaAdopcion" id="tasaAdopcion"
								required="required" />
						</div>
					</div>
					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="imagen">Imagen:</label>
							<form:hidden path="imagen" />
							<input type="file" id="archivoImagen" name="archivoImagen"/>
						</div>
					</div>
					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="urlVideo">URL del video
								(youtube)</label>
							<form:input placeholder="URL del video (youtube)" type="text"
								class="form-control" path="urlVideo" id="urlVideo"
								required="required" />
						</div>
					</div>

					<h3 class="blog-title col-sm-12 col-lg-12 col-md-12"
						style="margin-bottom: 40px; margin-top: 40px;">
						<span class="label label-success"
							style="border-bottom: 2px solid black;">Datos de la
							protectora a la que pertenece</span>
					</h3>

					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="idProtectora">Id
								de la protectora a la que pertenece:</label> 
								<form:select class="form-control mr-3"
								path="protectora.idProtectora" id="idProtectora">

								<c:forEach items="${protectoras}" var="idProtectora">
									<c:choose>
										<c:when test="${idProtectora eq protectora}">
											<option value="${idProtectora}" selected>${idProtectora}</option>
										</c:when>
										<c:otherwise>
											<option value="${idProtectora}">${idProtectora}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</form:select> 
							
						</div>
					</div>


				</div>
				<div class="container" style="text-align: right">

					<button type="submit" class="btn btn-primary"
						style="background-color: #0F1626; margin-top: 50px; margin-bottom: 50px;">Guardar</button>
				</div>
			</form:form>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
	<script>
		// Configuracion de la barra de heramientas
		// https://www.tinymce.com/docs/get-started/basic-setup/
		tinymce
				.init({
					selector : '#detalle',
					plugins : "textcolor, table lists code",
					toolbar : " undo redo | bold italic | alignleft aligncenter alignright alignjustify \n\
                    | bullist numlist outdent indent | forecolor backcolor table code"
				});
	</script>
</body>