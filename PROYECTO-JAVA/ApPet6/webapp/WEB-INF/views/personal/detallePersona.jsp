<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Mi perfil</title>

<spring:url value="/resources" var="urlPublic"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->


		<!-- Page Content -->
		<div class="container">

			<div class="row" style="margin-top: 70px; margin-bottom: 50px">

				<!-- Post Content Column -->

				<div class="col-lg-8">
					<!-- Title -->
					<h3>Mi perfil</h3>


					<hr>
					<div
						style="box-shadow: 1px 1px 10px #999; text-align: center; padding-top: 10px; padding-bottom: 10px; background-color: #F3EDE8">
						<a style="text-decoration: none; color: black" href="#video">
							<p>�Quieres saber un poco mas sobre mi?</p>

						</a>
					</div>
					<hr>


					<hr>

					<!-- Post Content -->
					<h3 class="mt-4" id="descripcion" style="margin-bottom: 15px">Descripci�n</h3>
					<p class="lead"></p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Ut, tenetur natus doloremque laborum quos iste ipsum rerum
						obcaecati impedit odit illo dolorum ab tempora nihil dicta earum
						fugiat. Temporibus, voluptatibus.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Eos, doloribus, dolorem iusto blanditiis unde eius illum
						consequuntur neque dicta incidunt ullam ea hic porro optio ratione
						repellat perspiciatis. Enim, iure!</p>


					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora
						commodi nihil ullam alias modi dicta saepe minima ab quo
						voluptatem obcaecati?</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione
						tempore quidem voluptates cupiditate voluptas illo saepe quaerat
						numquam recusandae? Qui, necessitatibus, est!</p>

					<hr>

				</div>

				<!-- Sidebar Widgets Column -->
				<div class="col-md-4">

					<!-- Categories Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Mis datos</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Nombre de usuario:</strong> </a></li>
										<li><a><strong>Nombre:</strong></a></li>
										<li><a><strong>Apellidos:</strong> </a></li>
										<li><a><strong>Email:</strong> </a></li>
										<li><a><strong>G�nero:</strong> </a></li>
										<li><a><strong>Fecha de nacimiento:</strong> </a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<!-- Side Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							de mi cuenta:</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">

										<li><a><strong>Fecha de creaci�n de la
													cuenta:</strong> </a></li>
										<li><a><strong>Estado de la cuenta:</strong> </a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					
					
						<h5 class="card-header"
							style="color: black">�Quieres editar tus datos?</h5>
						
									
									<a href="">
										<button type="button" style="margin-top: 10px;"
											class="btn btn-success">Editar cuenta</button>
									</a>
						
					
					
					<hr>
					<hr>
					<h5 class="card-header"
							style="color: black">Accede a tus animales favoritos de ApPet!</h5>
					<button type="button" style="margin-top: 10px;" class="btn btn-primary">Mis favoritos</button>
					<p></p>
					<hr>
					<hr>
					<h5 class="card-header"
							style="color: black">Accede a toda la informaci�n de tu actividad en ApPet!</h5>
				
					
					<button type="button" style="margin-top: 10px;" class="btn btn-info">Mis adopciones</button>
				
					<button type="button" style="margin-top: 10px;" class="btn btn-info">Mis apadrinamientos</button>
					<hr>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>

	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/jquery/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!--JS -->
	<script type="text/javascript" src="${urlPublic}/js/slider.js"></script>