package pruebasrelaciones;

import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.repository.ProtectorasRepository;

public class AppGetAnimalesProtectora {
	public static void main(String[] args) {
	
	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
	ProtectorasRepository protectorasRepository = context.getBean("protectorasRepository", ProtectorasRepository.class);
	
	Optional<Protectora> optional = protectorasRepository.findById(1);
	System.out.println(optional.get().getAnimales().size());
	context.close();
}
}
