package com.sgomezr.appet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IPerfilesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.service.IUsuariosService;
import com.sgomezr.appet.util.Utileria;

@Controller
@RequestMapping("/protectoras")
public class ProtectorasController {

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IProtectorasService serviceProtectoras;
	
	@Autowired
	private IUsuariosService serviceUsuarios;
	
	@Autowired
	private IPerfilesService servicePerfiles;


	@GetMapping("/listaProtectoras")
	public String mostrarIndex(Model model) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		model.addAttribute("protectoras", listaProtectoras);
		return "protectoras/listaProtectoras";
	}

	@GetMapping("/crear")
	public String crear(Model model, @ModelAttribute Protectora protectora) {
		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());

		return "protectoras/formProtectora";

	}

	@PostMapping("/guardar")
	public String guardar(Model model, @ModelAttribute Protectora protectora, BindingResult result,
			RedirectAttributes atributos, @RequestParam("archivoImagen") MultipartFile multiPart,
			@RequestParam("archivoImagenLogo") MultipartFile multiPartLogo, HttpServletRequest request) {

		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());
		
		// Validacion de campos/errores del formulario
		if(serviceProtectoras.buscarProtectoraPorId(protectora.getIdProtectora()) == null) {
			serviceProtectoras.validarCampos(result, protectora);
		}
		

		if (result.hasErrors()) {
			System.out.println("Errores");
			return "protectoras/formProtectora";
		}
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request);
			protectora.setImagenProtectora(nombreImagen);

		}
		if (!multiPartLogo.isEmpty()) {
			String nombreLogo = Utileria.guardarImagen(multiPartLogo, request);
			protectora.setLogoProtectora(nombreLogo);
		}

		System.out.println("Recibiendo objeto protectora" + protectora);
		// System.out.println("Elementos en la lista antes de la inserci�n: " +
		// serviceProtectoras.buscarTodasLasProtectoras().size());
		
		//A�adimos la protectora a la tabla "Protectoras" de BBDD
		serviceProtectoras.insertar(protectora);
		
		//A�adimos la protectora a la tabla "Usuarios" de BBDD
		int idUsuario = 0;
		int idPerfil= 0;
		Usuario usuarioProtectora = new Usuario();
		Perfil perfilProtectora = new Perfil();
		if(serviceUsuarios.buscarUsuarioPorUsername(protectora.getNombreProtectora()) == null) {
			usuarioProtectora = serviceUsuarios.generarObjetoUsuario(protectora, idUsuario);
			serviceUsuarios.guardar(usuarioProtectora);
			
			//A�adimos el perfil de esa protectora a la tabla "perfiles" de BBDD
			Perfil perfilTmp = new Perfil();
			perfilTmp.setUsername(usuarioProtectora.getUsername());
			perfilTmp.setPerfil("PROTECTORA");
			servicePerfiles.guardar(perfilTmp);
		}else {
			idUsuario = serviceUsuarios.buscarUsuarioPorUsername(protectora.getNombreProtectora()).getId();
			idPerfil = servicePerfiles.buscarIdPerfilPorUsername(protectora.getNombreProtectora()).getIdPerfil();
			usuarioProtectora = serviceUsuarios.generarObjetoUsuario(protectora, idUsuario);
			perfilProtectora = servicePerfiles.generarObjetoPerfil(protectora, idPerfil);
			serviceUsuarios.guardar(usuarioProtectora);
			servicePerfiles.guardar(perfilProtectora);
		}

		// System.out.println("Elementos en la lista despues de la inserci�n: " +
		// serviceProtectoras.buscarTodasLasProtectoras().size());
		atributos.addFlashAttribute("mensaje",
				"La protectora ha sido creada y guardada correctamente con el rol y los permisos de PROTECTORA, a su vez a sido a�adida al listado de USUARIOS");
		return "redirect:/protectoras/listaProtectoras";
	}

	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idProtectora") int idProtectora, Model model) {
		Protectora protectora = serviceProtectoras.buscarProtectoraPorId(idProtectora);
		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());
		protectora.setPassword(null);
		protectora.setComprobarPass(null);
		model.addAttribute("protectora", protectora);
		return "protectoras/formProtectora";
	}

	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idProtectora") int idProtectora, Model model, RedirectAttributes atributos) {

		// Buscamos el id de los animales corresponientes a la protectora a eliminar y
		// los eliminamos de BBDD
		List<Animal> listaAnimalesEliminar = serviceAnimales.buscarAnimalPorProtectora(idProtectora);
		for (Animal animalEliminar : listaAnimalesEliminar) {
			serviceAnimales.eliminar(animalEliminar.getId());
		}
		Usuario usuarioEliminar = serviceUsuarios.buscarUsuarioPorUsername((serviceProtectoras.buscarProtectoraPorId(idProtectora).getNombreProtectora()));
		Perfil perfilEliminar = servicePerfiles.buscarIdPerfilPorUsername((serviceProtectoras.buscarProtectoraPorId(idProtectora).getNombreProtectora()));
		// Eliminamos el perfil de l a protectora de BBDD de perfiles
		servicePerfiles.eliminar(perfilEliminar.getIdPerfil());
		// Eliminamos la protectora de BBDD de usuarios
		serviceUsuarios.eliminar(usuarioEliminar.getId());
		
		// Eliminamos la protectora de BBDD de protectoras
		serviceProtectoras.eliminar(idProtectora);
		atributos.addFlashAttribute("mensaje", "La protectora fue eliminada correctamente tanto de PROTECTORAS como de USUARIOS");
		return "redirect:/protectoras/listaProtectoras";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

}
