package com.sgomezr.appet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.service.IBannersService;
import com.sgomezr.appet.service.INoticiasService;

@Controller
@RequestMapping("/index")
public class IndexController {
	@Autowired
	private INoticiasService serviceNoticias;
	@Autowired
	private IBannersService serviceBanners;
	

	@GetMapping(value = "/welcome")
	public String mostrarNoticias(Model model) {
		List<Noticia> noticias = serviceNoticias.buscarTodasLasNoticias();
		List<Banner> banners = serviceBanners.buscarTodos();
		model.addAttribute("noticias",noticias);
		model.addAttribute("banners",banners);
		
	return "index/indexPage";
	
	}

}
