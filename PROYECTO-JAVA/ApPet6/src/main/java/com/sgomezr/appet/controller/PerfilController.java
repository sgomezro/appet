package com.sgomezr.appet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/miPerfil")
public class PerfilController {
	
	@GetMapping("/detalle")
	public String miPerfil() {
	
		return "personal/detallePersona";
	}

	

}
