package com.sgomezr.appet.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.repository.BannersRepository;

@Service
public class BannersServiceJPA implements IBannersService {
	
	@Autowired
	private BannersRepository bannersRepository;

	@Override
	public void insertar(Banner banner) {
		bannersRepository.save(banner);
		
	}
	@Override
	public Banner buscarBannerPorId(int idBanner) {
		Optional<Banner> banner = bannersRepository.findById(idBanner);
		if(banner.isPresent()) {
			return banner.get();
		}
		return null;
	}

	@Override
	public List<Banner> buscarTodos() {
		return bannersRepository.findAll();
	}

	@Override
	public void eliminar(int idBanner) {
		bannersRepository.deleteById(idBanner);
	}
	
	@Override
	public Page<Banner> buscarTodasPaginacion(Pageable page) {
	return bannersRepository.findAll(page);
	}

}
