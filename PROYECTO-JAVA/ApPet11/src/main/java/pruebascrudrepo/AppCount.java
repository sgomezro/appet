package pruebascrudrepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

public class AppCount {
	
	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		
		//Contar numero de registros en una tabla (metodo count() del repositorio)
		long num = repo.count(); //El metodo count devuelve un long
		System.out.println("Se encontraron " + num + " registros");
		context.close();

	}
}
