package com.sgomezr.appet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sgomezr.appet.model.Contacto;

public interface ContactosRepository extends JpaRepository<Contacto, Integer> {

}
