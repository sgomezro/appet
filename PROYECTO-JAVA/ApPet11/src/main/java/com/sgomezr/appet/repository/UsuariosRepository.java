package com.sgomezr.appet.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import com.sgomezr.appet.model.Usuario;

public interface UsuariosRepository extends JpaRepository<Usuario, Integer> {
	Usuario findByUsername(String username);
	Usuario findByEmail(String email);
}
