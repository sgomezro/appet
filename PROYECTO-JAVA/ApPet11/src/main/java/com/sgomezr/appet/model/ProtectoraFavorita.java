package com.sgomezr.appet.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

	@Entity
	@Table(name = "protectorasfavoritas")
	public class ProtectoraFavorita {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id_favorito")
		private int idFavorito;
		
		@Column(name = "Favoritoscol")
		private String favoritosCol;
		
		@Column(name = "id_usuario")
		private int idUsuario;
		
		@ManyToOne
		@JoinColumn(name = "Protectoras_id_protectora")
		private Protectora protectoraFav;

		public int getIdFavorito() {
			return idFavorito;
		}

		public void setIdFavorito(int idFavorito) {
			this.idFavorito = idFavorito;
		}

		public String getFavoritosCol() {
			return favoritosCol;
		}

		public void setFavoritosCol(String favoritosCol) {
			this.favoritosCol = favoritosCol;
		}

		public int getIdUsuario() {
			return idUsuario;
		}

		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}

		
		public Protectora getProtectoraFav() {
			return protectoraFav;
		}

		public void setProtectoraFav(Protectora protectoraFav) {
			this.protectoraFav = protectoraFav;
		}

		@Override
		public String toString() {
			return "ProtectoraFavorita [idFavorito=" + idFavorito + ", favoritosCol=" + favoritosCol + ", idUsuario="
					+ idUsuario + ", protectoraFav=" + protectoraFav + "]";
		}
 
}
