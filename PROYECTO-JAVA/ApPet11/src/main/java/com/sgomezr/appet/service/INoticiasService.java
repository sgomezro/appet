package com.sgomezr.appet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.sgomezr.appet.model.Noticia;

public interface INoticiasService {

	void guardarNoticia(Noticia noticia);

	void insertarNoticia(Noticia noticia);

	Noticia buscarNoticiaPorId(int idNoticia);

	List<Noticia> buscarTodasLasNoticias();

	void eliminar(int idNoticia);

	Page<Noticia> buscarTodasPaginacion(Pageable page);
}
