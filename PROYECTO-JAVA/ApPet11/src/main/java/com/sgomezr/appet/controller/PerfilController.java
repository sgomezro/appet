package com.sgomezr.appet.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IPerfilesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.service.IUsuariosService;

@Controller
@RequestMapping("/miPerfil")
public class PerfilController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(PerfilController.class);

	@Autowired
	private IProtectorasService serviceProtectoras;

	@Autowired
	private IUsuariosService serviceUsuarios;

	@Autowired
	private IPerfilesService servicePerfiles;

	@GetMapping("/detalle")
	public String miPerfil(Model model, Authentication authentication) {
		if ((authentication.getAuthorities()).equals("PROTECTORA")) {
			Usuario usuario = serviceUsuarios.buscarUsuarioPorUsername(authentication.getName());
			System.out.println(serviceProtectoras.buscaIdProtectora(usuario.getUsername()));
			model.addAttribute("protectora", serviceProtectoras.buscarProtectoraPorNombre(usuario.getUsername()));
		}
		return "personal/detallePersona";
	}

	@GetMapping(value = "/inactivar")
	public String inactivar(@RequestParam("id") int idUsuario, @RequestParam("username") String username, Model model,
			RedirectAttributes atributos) {
		// editamos su estado de activo a inactivo de BBDD
		Usuario usuario = serviceUsuarios.buscarUsuarioPorId(idUsuario);
		usuario.setEstado(0);
		servicePerfiles.guardar(servicePerfiles.buscarIdPerfilPorUsername(username));
		serviceUsuarios.guardar(usuario);

		// Si se trata de una protectora, debemos cambiar el estado tambien de la tabla
		// de PROTECTORAS
		if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("PROTECTORA")) {
			Protectora protectora = serviceProtectoras.buscarProtectoraPorNombre(username);
			protectora.setEstado("Inactiva");
			serviceProtectoras.insertar(protectora);
		}
		atributos.addFlashAttribute("mensaje",
				"La cuenta ha sido INACTIVADA, una vez que cierre sesi�n no podr� volver a"
						+ "iniciar sesi�n en ApPet, hasta contactar con un administrador que vuelva a activarla.");
		return "redirect:/miPerfil/detalle";
	}
}
