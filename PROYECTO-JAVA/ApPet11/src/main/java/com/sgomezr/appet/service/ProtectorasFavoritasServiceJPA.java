package com.sgomezr.appet.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.ProtectoraFavorita;
import com.sgomezr.appet.repository.AnimalesRepository;
import com.sgomezr.appet.repository.FavoritosRepository;
import com.sgomezr.appet.repository.NoticiaRepository;
import com.sgomezr.appet.repository.ProtectorasFavoritasRepository;
import com.sgomezr.appet.repository.ProtectorasRepository;

@Service
public class ProtectorasFavoritasServiceJPA implements IProtectorasFavoritasService{

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(ProtectorasFavoritasServiceJPA.class);

	@Autowired
	private ProtectorasFavoritasRepository protectorasFavoritasRepository;
	@Autowired
	private ProtectorasRepository protectorasRepository;

	@Override
	public void agregar(ProtectoraFavorita protectoraFavorita) {
		protectorasFavoritasRepository.save(protectoraFavorita);
	}

	@Override
	public Boolean existeProtectoraFavorita(int idProtectora, int idUsuario) {
		Optional<Protectora> protectora = protectorasRepository.findById(idProtectora);
		ProtectoraFavorita protectoraFavorita = protectorasFavoritasRepository.findByProtectoraFavAndIdUsuario(protectora, idUsuario);
		if (protectoraFavorita == null) {
			return false;
		}
		if (((protectoraFavorita.getProtectoraFav()).getIdProtectora() == (idProtectora)) && (protectoraFavorita.getIdUsuario() == idUsuario)) {
			return true;
		}
		return false;
	}

	@Override
	public List<ProtectoraFavorita> buscarProtectorasFavoritasPorIdUsuario(int idUsuario) {
		return protectorasFavoritasRepository.findByIdUsuario(idUsuario);
	}
	
	@Override
	public void eliminarProtectoraFavorita(int idProtectora) {
		protectorasFavoritasRepository.deleteById(idProtectora);
	}

}
