package com.sgomezr.appet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IBannersService;
import com.sgomezr.appet.service.INoticiasService;
import com.sgomezr.appet.service.IPerfilesService;
import com.sgomezr.appet.service.IUsuariosService;

@Controller
@RequestMapping("/index")
public class IndexController {
	@Autowired
	private INoticiasService serviceNoticias;
	
	@Autowired
	private IBannersService serviceBanners;
	
	@Autowired
	private IUsuariosService serviceUsuarios;
	
	@Autowired
	private IPerfilesService servicePerfiles;
	
	//Implementamos el logger
	final static Logger logger = Logger.getLogger(IndexController.class);

	@GetMapping(value = "/welcome")
	public String mostrarNoticias(Model model, Authentication authentication,HttpServletRequest request) {
		List<Noticia> noticias = serviceNoticias.buscarTodasLasNoticias();
		List<Banner> banners = serviceBanners.buscarTodos();
		model.addAttribute("noticias",noticias);
		model.addAttribute("banners",banners);
		
		//Recuperamos el nombre de usuario y el rol del usuario en el caso de que un usuario se acabe de logear
		if(authentication != null) {
		logger.info("Username: " + authentication.getName());
		for(GrantedAuthority rol: authentication.getAuthorities()) {
		logger.info("Rol: " + rol.getAuthority());
		}
		Usuario usuario = serviceUsuarios.buscarUsuarioPorUsername(authentication.getName());
		Perfil perfilUsuario = servicePerfiles.buscarIdPerfilPorUsername(usuario.getUsername());
		HttpSession session = request.getSession();
		session.setAttribute("usr", usuario);
		session.setAttribute("prf", perfilUsuario);
		}
		return "index/indexPage";
	}
}
