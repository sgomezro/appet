<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>ApPet</title>
<!-- Tag de Spring para agregar recursos estaticos -->
<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">


<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">

			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/banners/banner.jpg)">

				<h1 class="display-3" style="color: white">Bienvenido a ApPet!</h1>
				<h3 class="lead" style="font-size: 2.25rem !important; color: white">Mejora
					su vida, mejora tu vida!</h3>
			</header>

			<!-- Page Heading -->
			<h4>Filtros:</h4>
			<form class="form-group" action="${urlRoot}busqueda" method="post">

				<label class="badgetForm" for="exampleFormControlSelect1">Seleccione
					la protectora por la que quiera filtrar</label> <input type="hidden"
					name="${_csrf.parameterName}" value="${_csrf.token}" />
				<div class="form-inline">
					<select class="form-control mr-3" id="protectora" name="protectora">
						<option>Mostrar todos</option>
						<c:forEach items="${protectoras}" var="protectora">
							<c:choose>
								<c:when test="${protectoraBusqueda eq protectora}">
									<option value="${protectora}" selected>${protectora}</option>
								</c:when>
								<c:otherwise>
									<option value="${protectora}">${protectora}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>

					<button type="submit" class="btn btn-primary"
						style="background-color: #0F1626">Filtrar</button>
				</div>
			</form>
			<h1 class="my-4">
				ApPet <small>Lista de Animales en adopcion</small>
			</h1>
			<div class="separator"
				style="border: 0 solid #2F4F4F; clear: both; position: relative; z-index: 11; border-color: #2F4F4F; border-top-width: 3px; margin-top: 0px; margin-bottom: 71px; width: 100%; max-width: 700px;"></div>
			<div class="container" style="background-color: #f7f7f7">
				<div class="row">
					<c:forEach items="${ animales }" var="animal">
						<div class="col-lg-3 col-md-6 col-sm-12 portfolio-item"
							style="padding-top: 7.5px; padding-bottom: 7.5px">
							<div class="card h-100">
								<h4 class="card-title"
									style="padding: 8px; color: white; background-color: #76323F; text-align: center">

									${animal.nombre}</h4>
								<a><img style="height: 175px;" class="card-img-top"
									src=${urlPublic}/images/animales/${animal.imagen } alt=""></a>
								<div class="card-text" style="margin: 15px; margin-bottom: 0px;">
									<p class="card-text">
										<strong>Raza:</strong> ${animal.raza }
									</p>
									<p class="card-text">
										<strong>Sexo:</strong> ${animal.sexo }
									</p>
									<hr>
									<p class="card-text">
										<strong>Localidad:</strong> ${animal.protectora.localidad }
									</p>
								</div>
								<hr>
								<div
									style="display: flex; justify-content: space-between; text-aligh: right; padding: 10px; padding-top: 0px;">

									<c:forEach items="${favoritos}" var="favorito">
										<c:choose>
											<c:when test="${favorito.animal.nombre eq animal.nombre}">
												<i class="fa fa-heart ml-2 mr-5 align-middle"
													style="font-size: 30px; color: red"></i>
											</c:when>
										</c:choose>
									</c:forEach>
									<a class="btn btn-info" style="color: white !important"
										href="detalleAnimal?idAnimal=${animal.id }&protectora=${animal.protectora.nombreProtectora }"
										role="button">+ info</a>
								</div>
								<c:choose>
									<c:when test="${animal.estado=='En adopcion'}">
										<div class="adopcion">${animal.estado }</div>
									</c:when>
									<c:when test="${animal.estado=='Adoptado'}">
										<div class="adoptado">${animal.estado }</div>
									</c:when>
									<c:otherwise>
										<div class="apadrinamiento">${animal.estado }</div>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>


			<!-- Pagination -->
			<ul class="pagination justify-content-center">
				<li class="page-item"><a class="page-link" href="#"
					aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						<span class="sr-only">Previous</span>
				</a></li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#"
					aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span
						class="sr-only">Next</span>
				</a></li>
			</ul>
		</div>


	</div>
	<!-- Footer -->
	<jsp:include page="includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/jquery/jquery.min.js"></script>
	<script src="${urlPublic}/js/botonFavoritos.js"></script>


	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>