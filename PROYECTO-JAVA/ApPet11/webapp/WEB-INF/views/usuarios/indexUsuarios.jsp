<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Bienvenido a ApPet!</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="../" var="urlRoot"></spring:url>
<spring:url value="/usuarios/editar" var="urlEdit"></spring:url>
<spring:url value="/usuarios/eliminar" var="urlDelete"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container" style="margin-bottom: 40px;">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/banners/banner17.jpg)">

				<h1 class="display-3" style="color: white">Bienvenido a ApPet!</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>
			<h2 style="text-align: center; font-size: 2.0rem !important; color: black">Inicia sesi�n para empezar a navegar por ApPet y encontrar a tu nuevo mejor amigo</h2>
			<div
					style="text-align: center; margin-top: 40px">
					<a href="${urlRoot}formLogin">
						<button style="padding-top:8px; padding-bottom:8px;border:none;color:white;width: 20%; background: #76323F;border-radius: 10px;text-transform: none;"
							class="contact100-form-btn">Iniciar sesi�n</button>
					</a>
				</div>
			
			
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>

</body>
</html>