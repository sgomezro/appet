<!-- Modal Eliminar -->
<div id="dataConfirmModal" class="modal fade" tabindex="-1"
	role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header"
				style="text-align: center; color: white; background-color: #17a2b8">
				<h5 class="modal-title" id="exampleModalLabel">ELIMINAR</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<p style="margin: 10px">
				<strong>�Est�s seguro de querer eliminarlo?</strong> </br> <small>El
					elemento seleccionado ser� eliminado de la base de datos de ApPet,
					y esta acci�n no se podr� revertir.</small>
			</p>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button class="btn btn-outline-secondary" data-dismiss="modal"
					aria-hidden="true">No, volver atr�s</button>
				<a class="btn btn-info" style="color: white !important;"
					id="dataConfirmOK">Si, estoy seguro</a>
			</div>
		</div>
	</div>
</div>