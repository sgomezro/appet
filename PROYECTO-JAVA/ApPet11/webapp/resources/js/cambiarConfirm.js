$(document).ready(function() {
		$('a[data-confirm]').click(function(ev) {
			var href = $(this).attr('href');
			if (!$('#dataConfirmModal').length) {
				$('body').append('<div id="dataConfirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header" style="text-align: center; color: white; background-color: #17a2b8"><h5 class="modal-title" id="exampleModalLabel">GESTIÓN DE CUENTA | APPET</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><p style="margin:10px"><small>Esta acción no se podrá revertir</small></p><div class="modal-body"></div><div class="modal-footer"><button class="btn btn-outline-secondary" data-dismiss="modal" aria-hidden="true">No, volver atrás</button><a class="btn btn-info" style="color:white !important;" id="dataConfirmOK">Si, estoy seguro</a></div></div></div></div>');
			} 
			$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
			$('#dataConfirmOK').attr('href', href);
			$('#dataConfirmModal').modal({show:true});
			return false;
		});
	});