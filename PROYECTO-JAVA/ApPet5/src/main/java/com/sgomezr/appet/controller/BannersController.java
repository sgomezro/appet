package com.sgomezr.appet.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.service.IBannersService;
import com.sgomezr.appet.util.Utileria;

@Controller
@RequestMapping("/banners")
public class BannersController {

	@Autowired
	private IBannersService serviceBanners;
	
	/**
	 * Metodo para mostrar el listado de banners
	 * @param model
	 * @return
	 */
	@GetMapping("/listaBanners")
	public String mostrarIndex(Model model) {
		List<Banner> banners = serviceBanners.buscarTodos();
		model.addAttribute("banners", banners);
		return "banners/listaBanners";
	}
	
	/**
	 * Metodo para mostrar el formulario para crear un nuevo Banner
	 * @return
	 */
	@GetMapping("/crear")
	public String crear(@ModelAttribute Banner banner) {
		return "banners/formBanner";
	}
	
	/**
	 * Metodo para guardar el objeto de modelo de tipo Banner
	 * @param banner
	 * @param result
	 * @param attributes
	 * @param multiPart
	 * @param request
	 * @return
	 */
	@PostMapping("/guardar")
	public String guardar(@ModelAttribute Banner banner, BindingResult result, RedirectAttributes atributos,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request
			) {
		
		if (result.hasErrors()) {
			System.out.println("Existieron errores");
			return "banners/formBanner";
		}
		
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart,request);
			banner.setArchivo(nombreImagen);
		}
		System.out.println("Recibiendo objeto banner" + banner);
		System.out.println("Elementos en la lista antes de la inserción: " + serviceBanners.buscarTodos().size());
		serviceBanners.insertar(banner);
		System.out.println("Elementos en la lista despues de la inserción: " + serviceBanners.buscarTodos().size());
    	atributos.addFlashAttribute("mensaje", "El banner ha sido creado y guardado correctamente");		
		return "redirect:/banners/listaBanners";
	}
	
	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idBanner") int idBanner, Model model) {
		Banner banner = serviceBanners.buscarBannerPorId(idBanner);
		model.addAttribute("banner", banner);
		return "banners/formBanner";
	}

	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idBanner") int idBanner, Model model,RedirectAttributes atributos) {
		serviceBanners.eliminar(idBanner);
		atributos.addFlashAttribute("mensaje", "El banner fue eliminado correctamente");
		return "redirect:/banners/listaBanners";
	}
}
