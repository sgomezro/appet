package com.sgomezr.appet.service;

import java.util.List;

import com.sgomezr.appet.model.Usuario;

public interface IUsuariosService {
	
	List<Usuario> buscarTodosLosUsuarios();
	void guardar (Usuario usuario);
	List<String> buscarLocalidades();
	void eliminar(int idUsuario);
	Usuario buscarUsuarioPorId (int idUsuario);
	Usuario buscarUsuarioPorUsername (String username);
	Usuario buscarUsuarioPorEmail (String email);
}
