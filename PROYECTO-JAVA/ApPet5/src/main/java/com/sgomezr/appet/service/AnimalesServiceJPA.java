package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.repository.AnimalesRepository;
import com.sgomezr.appet.repository.ProtectorasRepository;

@Service
public class AnimalesServiceJPA implements IAnimalesService {

	@Autowired
	private AnimalesRepository animalesRepository;
	
	@Autowired
	private ProtectorasRepository protectorasRepository;

	@Override
	public void insertar(Animal animal) {
		animalesRepository.save(animal);

	}

	@Override
	public List<Animal> buscarTodosLosAnimales() {
		return animalesRepository.findAll();
	}

	@Override
	public Animal buscarAnimalPorId(int idAnimal) {
		Optional<Animal> animal = animalesRepository.findById(idAnimal);
		if(animal.isPresent()) {
			return animal.get();
		}
		return null;
	}

	@Override
	public List<Animal> buscarAnimalPorProtectora(int idProtectoraAnimal) {
		Optional<Protectora> optional = protectorasRepository.findById(idProtectoraAnimal);
		System.out.println(optional.get().getAnimales().size());
		List<Animal> listaAnimalesPorProtectora = optional.get().getAnimales();
		return listaAnimalesPorProtectora;
		
	}

	@Override
	public List<String> buscarTipologias() {
		List<String> tipos = new LinkedList<>();
		tipos.add("Perro");
		tipos.add("Gato");
		tipos.add("Pajaro");
		tipos.add("Tortuga");
		tipos.add("Hamster");
		tipos.add("Huron");
		tipos.add("Cerdo");
		tipos.add("Reptil");
		tipos.add("Equino");
		return tipos;
	}

	@Override
	public void eliminar(int idAnimal) {
		animalesRepository.deleteById(idAnimal);
	}
	
	@Override
	public Page<Animal> buscarTodasPaginacionAnimales(Pageable page) {
	return animalesRepository.findAll(page);
	}


}
