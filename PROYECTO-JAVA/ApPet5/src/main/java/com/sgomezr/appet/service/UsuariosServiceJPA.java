package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.repository.UsuariosRepository;

@Service
public class UsuariosServiceJPA implements IUsuariosService{
	
	@Autowired
	private UsuariosRepository usuariosRepository;

	@Override
	public List<Usuario> buscarTodosLosUsuarios() {
		return usuariosRepository.findAll();
	}
	@Override
	public void guardar(Usuario usuario) {
		usuariosRepository.save(usuario);
		
	}
	
	@Override
	public List<String> buscarLocalidades() {
		List<String> localidades = new LinkedList<>();

		localidades.add("A Coru�a");
		localidades.add("�lava");
		localidades.add("Albacete");
		localidades.add("Alicante");
		localidades.add("Almer�a");
		localidades.add("Asturias");
		localidades.add("�vila");
		localidades.add("Badajoz");
		localidades.add("Islas Baleares");
		localidades.add("Barcelona");
		localidades.add("Burgos");
		localidades.add("C�ceres");
		localidades.add("C�diz");
		localidades.add("Cantabria");
		localidades.add("Castell�n");
		localidades.add("Ciudad Real");
		localidades.add("C�rdoba");
		localidades.add("Cuenca");
		localidades.add("Girona");
		localidades.add("Granada");
		localidades.add("Guadalajara");
		localidades.add("Guip�zcoa");
		localidades.add("Huelva");
		localidades.add("Huesca");
		localidades.add("Ja�n");
		localidades.add("La Rioja");
		localidades.add("Las Palmas");
		localidades.add("Le�n");
		localidades.add("Lleida");
		localidades.add("Lugo");
		localidades.add("Madrid");
		localidades.add("M�laga");
		localidades.add("Murcia");
		localidades.add("Navarra");
		localidades.add("Orense");
		localidades.add("Palencia");
		localidades.add("Pontevedra");
		localidades.add("Salamanca");
		localidades.add("Segovia");
		localidades.add("Sevilla");
		localidades.add("Soria");
		localidades.add("Tarragona");
		localidades.add("Santa Cruz de Tenerife");
		localidades.add("Teruel");
		localidades.add("Toledo");
		localidades.add("Valencia");
		localidades.add("Valladolid");
		localidades.add("Vizcaya");
		localidades.add("Zamora");
		localidades.add("Zaragoza");

		return localidades;
	}
	@Override
	public void eliminar(int idUsuario) {
		usuariosRepository.deleteById(idUsuario);
		
	}
	
	@Override
	public Usuario buscarUsuarioPorId(int idUsuario) {
		Optional<Usuario> usuario = usuariosRepository.findById(idUsuario);
		if(usuario.isPresent()) {
			return usuario.get();
		}
		return null;
	}

	@Override
	public Usuario buscarUsuarioPorUsername(String username) {
		Usuario usuario = usuariosRepository.findByUsername(username);

		return usuario;
	}
	
	@Override
	public Usuario buscarUsuarioPorEmail(String email) {
		Usuario usuario = usuariosRepository.findByEmail(email);

		return usuario;
	}

}
