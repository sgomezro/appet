package com.sgomezr.appet.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;

//@Service
public class ProtectorasServiceImpl implements IProtectorasService {
	
	private List<Protectora> listaProtectoras = null;
	
	public  ProtectorasServiceImpl() {
		
		System.out.println("Creando una instancia de la clase ProtectorasServiceImpl");
		
		listaProtectoras = new LinkedList<>();

		Protectora protectora1 = new Protectora();
		protectora1.setIdProtectora(1);
		protectora1.setNombreProtectora("BurgosProtect");
		protectora1.setEmail("burgos@gmail.com");
		protectora1.setPassword("12345222");
		protectora1.setTelefonoProtectora("616812788");
		protectora1.setLocalidad("Burgos");
		protectora1.setDireccionProtectora("Ctra. Quintanadue�as, 2, 09001 Burgos");
		protectora1.setImagenProtectora("detalleProtectora01.jpg");
		protectora1.setLogoProtectora("ProBurgos.jpg");
		//protectora1.setFechaCreacionProtectora(fechaCreacionProtectora);
		protectora1.setEstado("Activa");
		//protectora1.setPerfilProtectora(perfilProtectora);
		protectora1.setDescripcionProtectora("Protectora de animales de Burgos");
		protectora1.setUrlVideoProtectora("https://www.youtube.com/embed/6_QyMOiYr78");
		protectora1.setDescripcionProcedimientoProtectora("El procedimiento para adoptar es...");
		protectora1.setLatitudProtectora("42.3602324");
		protectora1.setAltitudProtectora("-3.7108882572561415");
		
	
		Protectora protectora2 = new Protectora();
		protectora2.setIdProtectora(2);
		protectora2.setNombreProtectora("AguilarProtect");
		protectora2.setEmail("aguilar@gmail.com");
		protectora2.setPassword("12345333");
		protectora2.setTelefonoProtectora("616812787");
		protectora2.setLocalidad("Aguilar de la frontera");
		protectora2.setDireccionProtectora("Calle de Aguilar - C�rdoba");
		protectora2.setImagenProtectora("detalleProtectora02.jpg");
		//protectora2.setLogoProtectora("");
		//protectora2.setFechaCreacionProtectora(fechaCreacionProtectora);
		protectora2.setEstado("Activa");
		//protectora2.setPerfilProtectora(perfilProtectora);
		protectora2.setDescripcionProtectora("Protectora de animales de Aguilar");
		protectora2.setDescripcionProcedimientoProtectora("El procedimiento para adoptar es...");
		protectora1.setLatitudProtectora("42.3602324");
		protectora1.setAltitudProtectora("-3.7108882572561415");
	
		Protectora protectora3 = new Protectora();
		protectora3.setIdProtectora(3);
		protectora3.setNombreProtectora("BenidormProtect");
		protectora3.setEmail("benidorm@gmail.com");
		protectora3.setPassword("12345444");
		protectora3.setTelefonoProtectora("616812786");
		protectora3.setLocalidad("Benidorm");
		protectora3.setDireccionProtectora("Partida Salto del Agua, 60, 03503 Benidorm, Alicante");
		protectora3.setImagenProtectora("detalleProtectora03.jpg");
		//protectora3.setLogoProtectora("");
		//protectora3.setFechaCreacionProtectora(fechaCreacionProtectora);
		protectora3.setEstado("Activa");
		//protectora3.setPerfilProtectora(perfilProtectora);
		protectora3.setDescripcionProtectora("Protectora de animales de Benidorm");
		protectora3.setDescripcionProcedimientoProtectora("El procedimiento para adoptar es...");
		protectora1.setLatitudProtectora("42.3602324");
		protectora1.setAltitudProtectora("-3.7108882572561415");
	
		Protectora protectora4 = new Protectora();
		protectora4.setIdProtectora(4);
		protectora4.setNombreProtectora("FinestratProtect");
		protectora4.setEmail("finestrat@gmail.com");
		protectora4.setPassword("12345555");
		protectora4.setTelefonoProtectora("616812785");
		protectora4.setLocalidad("Finestrat");
		protectora4.setDireccionProtectora("Partida Salto del Agua, 60, 03503 Finestrat, Alicante");
		protectora4.setImagenProtectora("detalleProtectora04.jpg");
		//protectora4.setLogoProtectora("");
		//protectora4.setFechaCreacionProtectora(fechaCreacionProtectora);
		protectora4.setEstado("Activa");
		//protectora4.setPerfilProtectora(perfilProtectora);
		protectora4.setDescripcionProtectora("Protectora de animales de Finestrat");
		protectora4.setDescripcionProcedimientoProtectora("El procedimiento para adoptar es...");
		protectora1.setLatitudProtectora("42.3602324");
		protectora1.setAltitudProtectora("-3.7108882572561415");
		
		// Agregamos los objetos Protectora a la lista
		listaProtectoras.add(protectora1);
		listaProtectoras.add(protectora2);
		listaProtectoras.add(protectora3);
		listaProtectoras.add(protectora4);

	}

	@Override
	public List<Protectora> buscarTodasLasProtectoras() {

		return listaProtectoras;
	}

	@Override
	public void insertar(Protectora protectora) {
		listaProtectoras.add(protectora);
		
	}

	@Override
	public Protectora buscarProtectoraPorId(int idProtectora) {
		
		for(Protectora protectora: listaProtectoras) {
			if((protectora.getIdProtectora()) == idProtectora) {
				return protectora;
			}
		}
		return null;
	}
	
	@Override
	public List<String> buscarLocalidades(){
		List<String>localidades = new LinkedList<>();

		localidades.add("A Coru�a");
		localidades.add("�lava");
		localidades.add("Albacete");
		localidades.add("Alicante");
		localidades.add("Almer�a");
		localidades.add("Asturias");
		localidades.add("�vila");
		localidades.add("Badajoz");
		localidades.add("Islas Baleares");
		localidades.add("Barcelona");
		localidades.add("Burgos");
		localidades.add("C�ceres");
		localidades.add("C�diz");
		localidades.add("Cantabria");
		localidades.add("Castell�n");
		localidades.add("Ciudad Real");
		localidades.add("C�rdoba");
		localidades.add("Cuenca");
		localidades.add("Girona");
		localidades.add("Granada");
		localidades.add("Guadalajara");
		localidades.add("Guip�zcoa");
		localidades.add("Huelva");
		localidades.add("Huesca");
		localidades.add("Ja�n");
		localidades.add("La Rioja");
		localidades.add("Las Palmas");
		localidades.add("Le�n");
		localidades.add("Lleida");
		localidades.add("Lugo");
		localidades.add("Madrid");
		localidades.add("M�laga");
		localidades.add("Murcia");
		localidades.add("Navarra");
		localidades.add("Orense");
		localidades.add("Palencia");
		localidades.add("Pontevedra");
		localidades.add("Salamanca");
		localidades.add("Segovia");
		localidades.add("Sevilla");
		localidades.add("Soria");
		localidades.add("Tarragona");
		localidades.add("Santa Cruz de Tenerife");
		localidades.add("Teruel");
		localidades.add("Toledo");
		localidades.add("Valencia");
		localidades.add("Valladolid");
		localidades.add("Vizcaya");
		localidades.add("Zamora");
		localidades.add("Zaragoza");
		
		return localidades;
	}

	@Override
	public int buscaIdProtectora(String nombreProtectora) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void eliminar(int idProtectora) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Page<Protectora> buscarTodasPaginacion(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Protectora generarObjetoProtectora(Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
