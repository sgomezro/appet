package com.sgomezr.appet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.util.Utileria;


@Controller
@RequestMapping("/protectoras")
public class ProtectorasController {
	
	@Autowired
	private IAnimalesService serviceAnimales;
	
	@Autowired
	private IProtectorasService serviceProtectoras;
	
	@GetMapping("/listaProtectoras")
	public String mostrarIndex(Model model) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		model.addAttribute("protectoras",listaProtectoras);
		return "protectoras/listaProtectoras";
	}
	
	@GetMapping("/crear")
	public String crear(Model model, @ModelAttribute Protectora protectora) {
		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());

		return "protectoras/formProtectora";
		
	}
	
	@PostMapping("/guardar")
	public String guardar(Model model,  @ModelAttribute Protectora protectora, BindingResult result, RedirectAttributes atributos,
		 @RequestParam ("archivoImagen") MultipartFile multiPart, @RequestParam ("archivoImagenLogo") MultipartFile multiPartLogo, HttpServletRequest request ) {
		
		
		if(result.hasErrors()) {
			System.out.println("Errores");
			return "animales/formAnimal";
		}
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart,request);
			protectora.setImagenProtectora(nombreImagen);
			
		}
		if (!multiPartLogo.isEmpty()) {
			
			String nombreLogo = Utileria.guardarImagen(multiPartLogo,request);
			protectora.setLogoProtectora(nombreLogo);
		}
		
		System.out.println("Recibiendo objeto protectora" + protectora);
		//System.out.println("Elementos en la lista antes de la inserción: " + serviceProtectoras.buscarTodasLasProtectoras().size());
		serviceProtectoras.insertar(protectora);
		
		//System.out.println("Elementos en la lista despues de la inserción: " + serviceProtectoras.buscarTodasLasProtectoras().size());
		atributos.addFlashAttribute("mensaje","La protectora ha sido creada y guardada correctamente con el rol y los permisos de PROTECTORA");
		return "redirect:/protectoras/listaProtectoras";
	}
	
	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idProtectora") int idProtectora, Model model) {
		Protectora protectora = serviceProtectoras.buscarProtectoraPorId(idProtectora);
		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());
		model.addAttribute("protectora", protectora);
		return "protectoras/formProtectora";
	}

	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idProtectora") int idProtectora, Model model,RedirectAttributes atributos) {
		serviceProtectoras.eliminar(idProtectora);
		atributos.addFlashAttribute("mensaje", "La protectora fue eliminada correctamente");
		return "redirect:/protectoras/listaProtectoras";
	}
	
		
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat ("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	




}

