package com.sgomezr.appet.service;

import org.springframework.data.domain.Pageable;
import java.util.List;

import org.springframework.data.domain.Page;

import com.sgomezr.appet.model.Animal;


public interface IAnimalesService {
	
	void insertar (Animal animal);
	
	List<Animal> buscarTodosLosAnimales();

	Animal buscarAnimalPorId (int idAnimal);
	
	List<Animal> buscarAnimalPorProtectora (int idProtectoraAnimal);
	
	List<String> buscarTipologias();
	
	void eliminar(int idAnimal);
	
	Page<Animal> buscarTodasPaginacionAnimales(Pageable page);

	
}
