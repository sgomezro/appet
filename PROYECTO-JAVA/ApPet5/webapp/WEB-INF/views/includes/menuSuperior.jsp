<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<head>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
	integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
	crossorigin="anonymous">
<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>

</head>

<!-- Menu cualquier usuario sin registrar-->
<sec:authorize access="isAnonymous()">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
		<a class="navbar-brand mr-3" href="${urlRoot}index/welcome">ApPet Application</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}usuarios/formUsuarioProtectora">Registrate</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}formLogin">Iniciar sesi�n</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}admin/logout">Cerrar sesi�n</a>
				<li>
			</ul>
		</div>
	</nav>
</sec:authorize>

<!-- Menu usuario registrado-->
<sec:authorize access="hasAnyAuthority('USUARIO')">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
		<a class="navbar-brand mr-3" href="${urlRoot}index/welcome">ApPet Application |
			Usuario</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}formLogin">Iniciar sesi�n</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}admin/logout">Cerrar sesi�n</a>
				<li>
			</ul>
		</div>
	</nav>
</sec:authorize>
<!-- Menu protectora-->
<sec:authorize access="hasAnyAuthority('PROTECTORA')">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
		<a class="navbar-brand mr-3" href="${urlRoot}index/welcome">ApPet Application |
			Protectora</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}formLogin">Iniciar sesi�n</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}admin/logout">Cerrar sesi�n</a>
				<li>
			</ul>
		</div>
	</nav>
</sec:authorize>

<!-- Menu administrador-->
<sec:authorize access="hasAnyAuthority('ADMINISTRADOR')">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
		<i style="font-size: 23px; margin-right:10px;color:white" class="fas fa-cog"></i><a class="navbar-brand mr-3" href="${urlRoot}index/welcome">ApPet Application |
			Administrador</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}formLogin">Iniciar sesi�n</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}admin/logout">Cerrar sesi�n</a>
				<li>
			</ul>
		</div>
	</nav>
</sec:authorize>





