package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.repository.PerfilesRepository;

@Service
public class PerfilesServiceJPA implements IPerfilesService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(PerfilesServiceJPA.class);

	@Autowired
	private PerfilesRepository perfilesRepository;

	@Override
	public void guardar(Perfil perfil) {
		perfilesRepository.save(perfil);
	}

	@Override
	public Perfil buscarIdPerfilPorUsername(String username) {
		Perfil perfil = perfilesRepository.findByUsername(username);
		return perfil;
	}

	@Override
	public void eliminar(int idPerfil) {
		perfilesRepository.deleteById(idPerfil);
	}

	@Override
	public List<Perfil> buscarPerfilPorRol(String perfil) {
		List<Perfil> listaPerfiles = new LinkedList<>();
		listaPerfiles = perfilesRepository.findByPerfil(perfil);
		return listaPerfiles;
	}

	@Override
	public Perfil generarObjetoPerfil(Protectora protectora, int idPerfil) {

		Perfil perfilGenerado = new Perfil();
		perfilGenerado.setIdPerfil(idPerfil);
		perfilGenerado.setPerfil("PROTECTORA");
		perfilGenerado.setUsername(protectora.getNombreProtectora());
		return perfilGenerado;
	}
}