package com.sgomezr.appet.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.springframework.lang.NonNull;

	
	@Entity
	@Table(name = "animalesfavoritos")
	public class Favorito {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id_favorito")
		private int idFavorito;

		@Column(name = "Favoritoscol")
		private String favoritosCol;
		
		@ManyToOne
		@JoinColumn(name = "Animales_id_animal")
		private Animal animal;
		
		@NotEmpty
	    @ManyToMany(fetch = FetchType.LAZY)
	    @JoinTable(name = "AnimalesFavoritos_has_Usuario", 
	             joinColumns = { @JoinColumn(name = "id_favorito") }, 
	             inverseJoinColumns = { @JoinColumn(name = "username") })
	    private Set<Usuario> usuarios = new HashSet<Usuario>();

		public int getIdFavorito() {
			return idFavorito;
		}

		public void setIdFavorito(int idFavorito) {
			this.idFavorito = idFavorito;
		}

		public String getFavoritosCol() {
			return favoritosCol;
		}

		public void setFavoritosCol(String favoritosCol) {
			this.favoritosCol = favoritosCol;
		}

		public Animal getAnimal() {
			return animal;
		}

		public void setAnimal(Animal animal) {
			this.animal = animal;
		}

		public Set<Usuario> getUsuarios() {
			return usuarios;
		}

		public void setUsuarios(Set<Usuario> usuarios) {
			this.usuarios = usuarios;
		}

		@Override
		public String toString() {
			return "Favorito [idFavorito=" + idFavorito + ", favoritosCol=" + favoritosCol + ", animal=" + animal
					+ ", usuarios=" + usuarios + "]";
		}
}
