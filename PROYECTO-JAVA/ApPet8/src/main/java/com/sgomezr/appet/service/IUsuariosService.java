package com.sgomezr.appet.service;

import java.util.List;

import org.springframework.validation.BindingResult;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;

public interface IUsuariosService {

	List<Usuario> buscarTodosLosUsuarios();

	void guardar(Usuario usuario);

	List<String> buscarLocalidades();

	void eliminar(int idUsuario);

	Usuario buscarUsuarioPorId(int idUsuario);

	Usuario buscarUsuarioPorUsername(String username);

	Usuario buscarUsuarioPorEmail(String email);

	void validarCampos(BindingResult result, Usuario usuario);

	Usuario generarObjetoUsuario(Protectora protectora, int idUsuario);
}
