package com.sgomezr.appet.controller;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IProtectorasService;

@Controller
public class HomeController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(HomeController.class);

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IProtectorasService serviceProtectoras;

	private Protectora protectora = new Protectora();

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String goHome() {
		return "home";
	}

	@RequestMapping(value = "/busqueda", method = RequestMethod.POST)
	public String buscarPorProtectora(@RequestParam("protectora") String protectora, Model model) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		
		logger.info("Protectora de busqueda: " + protectora);
		List<Animal> animales = serviceAnimales.buscarTodosLosAnimales();
		List<String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras", protectoras);
		model.addAttribute("protectoraBusqueda", protectora);

		if (protectora.equals("Mostrar todos")) {
			model.addAttribute("animales", animales);

		} else {
			int idProtectora = serviceProtectoras.buscaIdProtectora(protectora);
			List<Animal> animalesPorProtectora = serviceAnimales.buscarAnimalPorProtectora(idProtectora);
			model.addAttribute("animales", animalesPorProtectora);
		}

		logger.info("Buscando todos los animales de la protectora: " + protectora);
		return "home";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String mostrarAnimales(Model model) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();

		List<Animal> animales = serviceAnimales.buscarTodosLosAnimales();
		List<String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras", protectoras);
		model.addAttribute("protectoraBusqueda", protectora.getNombreProtectora());
		model.addAttribute("animales", animales);

		return "home";
	}

	@RequestMapping(value = "/detalleAnimal", method = RequestMethod.GET)
	public String mostrarDetalleAnimal(Model model, @RequestParam("idAnimal") int idAnimal,
			@RequestParam("protectora") String protectora) {
		logger.info("El id del Animal es: " + idAnimal);
		logger.info("La protectora en la que se encuentra ese animal es: " + protectora);
		model.addAttribute("animal", serviceAnimales.buscarAnimalPorId(idAnimal));

		return "detalleAnimal";
	}

	@RequestMapping(value = "/detalleProtectora", method = RequestMethod.GET)
	public String mostrarDetalleProtectora(Model model, @RequestParam("idProtectora") int idProtectora,
			@RequestParam("idAnimal") int idAnimal) {
		logger.info("El id de la protectora es: " + idProtectora);
		logger.info("El id del Animal es: " + idAnimal);
		model.addAttribute("protectora", serviceProtectoras.buscarProtectoraPorId(idProtectora));

		return "detalleProtectora";
	}

	@RequestMapping(value = "/formLogin", method = RequestMethod.GET)
	public String mostrarLogin() {
		return "formLogin";
	}

}