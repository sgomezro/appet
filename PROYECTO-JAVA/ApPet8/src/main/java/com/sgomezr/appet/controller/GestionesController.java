package com.sgomezr.appet.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gestiones")
public class GestionesController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(GestionesController.class);

	@GetMapping("/listaAdopciones")
	public String listaAdopciones() {
		return "gestiones/listaAdopciones";
	}

	@GetMapping("/listaApadrinamientos")
	public String listaApadrinamientos() {
		return "gestiones/listaApadrinamientos";
	}
}
