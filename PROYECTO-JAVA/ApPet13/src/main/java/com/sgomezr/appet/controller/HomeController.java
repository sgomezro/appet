package com.sgomezr.appet.controller;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.ProtectoraFavorita;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IFavoritosService;
import com.sgomezr.appet.service.IProtectorasFavoritasService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.service.IUsuariosService;

/**
 * Home Controller: Controlador encargado de la gesti�n de la funcionalidad de
 * la p�gina Home o p�gina donde se muestran los animales publicados de ApPet.
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
public class HomeController {

	final static Logger logger = Logger.getLogger(HomeController.class);

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IProtectorasService serviceProtectoras;

	@Autowired
	private IFavoritosService serviceFavoritos;

	@Autowired
	private IProtectorasFavoritasService serviceProtectorasFavoritas;

	@Autowired
	private IUsuariosService serviceUsuarios;

	private Protectora protectora = new Protectora();

	/**
	 * M�todo encargado de renderizar la vista de la home, o vista donde se muestran
	 * los animales publicados de ApPet.
	 * 
	 * @return String ruta
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String goHome() {
		return "home";
	}

	/**
	 * M�todo encargado de renderizar la vista de la home o vista donde se muestran
	 * los animales publicados de ApPet, filtrando por la protectora seleccionada en
	 * el filtro.
	 * 
	 * @param protectora:
	 *            Nombre de la protectora por la que se quiere filtrar.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @return String ruta.
	 */
	@RequestMapping(value = "/busqueda", method = RequestMethod.POST)
	public String buscarPorProtectora(@RequestParam("protectora") String protectora, Model model,
			Authentication authentication) {
		if (authentication != null) {
			int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
			List<Favorito> favoritos = serviceFavoritos.buscarFavoritosPorIdUsuario(idUsuario);
			model.addAttribute("favoritos", favoritos);
		}
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();

		logger.info("Protectora de busqueda: " + protectora);
		List<Animal> animales = serviceAnimales.buscarTodosLosAnimales();

		List<String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras", protectoras);
		model.addAttribute("protectoraBusqueda", protectora);

		if (protectora.equals("Mostrar todos")) {
			model.addAttribute("animales", animales);

		} else {
			int idProtectora = serviceProtectoras.buscaIdProtectora(protectora);
			List<Animal> animalesPorProtectora = serviceAnimales.buscarAnimalPorProtectora(idProtectora);
			model.addAttribute("animales", animalesPorProtectora);
		}
		logger.info("Buscando todos los animales de la protectora: " + protectora);
		return "home";
	}

	/**
	 * M�todo encargado de renderizar la vista de la home, o vista donde se muestran
	 * los animales publicados de ApPet, diferenciando si existe o no un usuario
	 * autenticado, de tal forma que si existe usuario autenticado, en dicha vista
	 * se mostrar�n con un corazon los animales que dicho usuario tiene en su lista
	 * de favoritos, por el contrario, si no existe usuario autenticado la vista
	 * ser� la definida por defecto.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @return String ruta.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String mostrarAnimales(Model model, Authentication authentication) {
		if (authentication != null) {
			int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
			List<Favorito> favoritos = serviceFavoritos.buscarFavoritosPorIdUsuario(idUsuario);
			model.addAttribute("favoritos", favoritos);
		}
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();

		List<Animal> animales = serviceAnimales.buscarTodosLosAnimales();

		List<String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras", protectoras);
		model.addAttribute("protectoraBusqueda", protectora.getNombreProtectora());
		model.addAttribute("animales", animales);
		return "home";
	}

	/**
	 * M�todo encargado de renderizar la vista en la cual se muestra la hoja de
	 * detalle de un determinado animal,diferenciando si existe o no un usuario
	 * autenticado, de tal forma que si existe usuario autenticado, en dicha vista
	 * se mostrar� un corazon en los animales que dicho usuario tiene en su lista de
	 * animales favoritos, por el contrario, si no existe usuario autenticado la
	 * vista ser� la definida por defecto.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param idAnimal:
	 *            Id del animal del cual se mostrar� el detalle
	 * @param protectora:
	 *            Nombre de la protectora a la que pertenece dicho animal.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @return String ruta.
	 */
	@RequestMapping(value = "/detalleAnimal", method = RequestMethod.GET)
	public String mostrarDetalleAnimal(Model model, @RequestParam("idAnimal") int idAnimal,
			@RequestParam("protectora") String protectora, Authentication authentication) {
		Boolean animalEnFavorito = null;
		if (authentication != null) {
			int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
			List<Favorito> favoritos = serviceFavoritos.buscarFavoritosPorIdUsuario(idUsuario);
			for (Favorito favorito : favoritos) {
				animalEnFavorito = false;
				System.out.println();
				if ((favorito.getAnimal().getNombre())
						.equalsIgnoreCase(serviceAnimales.buscarAnimalPorId(idAnimal).getNombre())) {
					animalEnFavorito = true;
					break;
				}
			}
			model.addAttribute("animalEnFavorito", animalEnFavorito);
		}
		logger.info("El id del Animal es: " + idAnimal);
		logger.info("La protectora en la que se encuentra ese animal es: " + protectora);
		model.addAttribute("animal", serviceAnimales.buscarAnimalPorId(idAnimal));
		return "detalleAnimal";
	}

	/**
	 * M�todo encargado de renderizar la vista en la cual se muestra la hoja de
	 * detalle de una determinada protectora,diferenciando si existe o no un usuario
	 * autenticado, de tal forma que si existe usuario autenticado, en dicha vista
	 * se mostrar� un corazon enlas protectoras que dicho usuario tiene en su lista
	 * de protectoras favoritas, por el contrario, si no existe usuario autenticado
	 * la vista ser� la definida por defecto.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param idProtectora:
	 *            Id de la protectora de la cual se mostrar� el detalle.
	 * @param idAnimal:
	 *            Id del animal del cual se proviene para la muestra del detalle de
	 *            la protectora a la que pertenece.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @return String ruta.
	 */
	@RequestMapping(value = "/detalleProtectora", method = RequestMethod.GET)
	public String mostrarDetalleProtectora(Model model, @RequestParam("idProtectora") int idProtectora,
			@RequestParam("idAnimal") int idAnimal, Authentication authentication) {
		Boolean protectoraEnFavorito = null;
		if (authentication != null) {
			int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
			List<ProtectoraFavorita> protectorasFavoritas = serviceProtectorasFavoritas
					.buscarProtectorasFavoritasPorIdUsuario(idUsuario);
			for (ProtectoraFavorita protectoraFavorita : protectorasFavoritas) {
				protectoraEnFavorito = false;
				System.out.println();
				if ((protectoraFavorita.getProtectoraFav().getNombre())
						.equalsIgnoreCase(serviceProtectoras.buscarProtectoraPorId(idProtectora).getNombre())) {
					protectoraEnFavorito = true;
					break;
				}
			}
			model.addAttribute("protectoraEnFavorito", protectoraEnFavorito);
		}
		logger.info("El id de la protectora es: " + idProtectora);
		logger.info("El id del Animal es: " + idAnimal);
		model.addAttribute("protectora", serviceProtectoras.buscarProtectoraPorId(idProtectora));
		return "detalleProtectora";
	}

	/**
	 * M�todo encargado demostrar el formulario de acceso a la aplicaci�n.
	 * 
	 * @return String ruta.
	 */
	@RequestMapping(value = "/formLogin", method = RequestMethod.GET)
	public String mostrarLogin() {
		return "formLogin";
	}
}