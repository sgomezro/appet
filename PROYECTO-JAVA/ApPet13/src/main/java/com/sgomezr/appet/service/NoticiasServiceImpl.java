package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Noticia;

//@Service
public class NoticiasServiceImpl implements INoticiasService {

	private List<Noticia> lista = null;

	// Constructor

	/**
	 * En el constructor creamos una lista enlazada con objetos de tipo Noticia
	 */
	public NoticiasServiceImpl() {
		System.out.println("Creando una instancia de la clase NoticiasServiceImpl");

		lista = new LinkedList<>();

		Noticia noticia1 = new Noticia();
		noticia1.setTitulo("Recogida de alimentos para nuestros peludos");
		noticia1.setDetalle("Os presentamos una nueva iniciativa solidaria de la mano de ApPet. "
				+ "Desde el 1 de Enero de 2019 y hasta el dia 31 de Febrero se ha puesto en marcha una "
				+ "campa�a de donaciones de comida para perros y gatos que ir�n destinadas a nuestra Protectoras"
				+ "registradas en la aplicacion");
		noticia1.setEstado("Activa");

		Noticia noticia2 = new Noticia();
		noticia2.setTitulo("�LLEVAMOS EL GORDO!");
		noticia2.setDetalle("Hola a todos!"
				+ "Ya tenemos disponible nuestra LOTER�A DE NAVIDAD. Pod�is adquirirla los S�bados y Domingos "
				+ "de 10 a 14h aprox en nuestro albergue o en los puntos de venta que os indicamos aqui abajo:"
				+ "Paseo Marques de Zafra n�11 (Madrid)"
				+ "Paseo Marques de Zafra n�35 (Madrid)."
				+ "Precio por papeleta: 5 euros, de los cuales se juegan 4 euros y 1 euro sera donado a nuestras asociaciones");
		noticia2.setEstado("Activa");

		Noticia noticia3 = new Noticia();
		noticia3.setTitulo("EUKANUBA, empresa solidaria");
		noticia3.setDetalle("�Buenas noticias para ApPet!"
				+ "La Empresa de comida animal EUKANUBA ha donado a La Madrile�a 11.000 kilos de pienso para nuestros peludos.");
		noticia3.setEstado("Activa");

		// Agregamos los objetos Banner a la lista
		lista.add(noticia1);
		lista.add(noticia2);
		lista.add(noticia3);

	}

	@Override
	public void guardarNoticia(Noticia noticia) {
		System.out.println("Guadando el objeto " + noticia + " en la base de datos.");
	}

	/**
	 * Insertamos un objeto de tipo Noticia a la lista
	 */
	@Override
	public void insertarNoticia(Noticia noticia) {
		lista.add(noticia);
	}

	/**
	 * Regresamos la lista de objetos Noticia
	 */
	@Override
	public List<Noticia> buscarTodasLasNoticias() {
		return lista;
	}

	@Override
	public void eliminar(int idNoticia) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Page<Noticia> buscarTodasPaginacion(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Noticia buscarNoticiaPorId(int idNoticia) {
		// TODO Auto-generated method stub
		return null;
	}

}
