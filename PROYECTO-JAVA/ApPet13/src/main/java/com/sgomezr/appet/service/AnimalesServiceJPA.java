package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.repository.AnimalesRepository;
import com.sgomezr.appet.repository.ProtectorasRepository;
import com.sgomezr.appet.util.Utileria;

/**
 * Servicio Animales
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class AnimalesServiceJPA implements IAnimalesService {

	final static Logger logger = Logger.getLogger(AnimalesServiceJPA.class);

	@Autowired
	private AnimalesRepository animalesRepository;

	@Autowired
	private ProtectorasRepository protectorasRepository;

	/**
	 * M�todo encargado de insertar un animal el BD.
	 * 
	 * @param animal:
	 *            Objeto animal a insertar en BD.
	 */
	@Override
	public void insertar(Animal animal) {
		animal.setEdad(animal.getFechaNacimiento());
		animalesRepository.save(animal);
	}

	/**
	 * M�todo encargado de buscar todos los animales guardados en BD.
	 * 
	 * @return lista de todos los animale guardados.
	 */
	@Override
	public List<Animal> buscarTodosLosAnimales() {
		return animalesRepository.findAll();
	}

	/**
	 * M�todo encargado de buscar un animal por su id.
	 * 
	 * @param idAnimal:
	 *            Id del animal que se esta buscando.
	 * @return Objeto animal con un determinado id.
	 */
	@Override
	public Animal buscarAnimalPorId(int idAnimal) {
		Optional<Animal> animal = animalesRepository.findById(idAnimal);
		if (animal.isPresent()) {
			return animal.get();
		}
		return null;
	}

	/*
	 * M�todo encargado de buscar la lista de animales por su protectora
	 * 
	 * @param: Id de la protectora de la cual se busca su lista de animales
	 * 
	 * @return: Lista de animales asociados a una protectora.
	 */
	@Override
	public List<Animal> buscarAnimalPorProtectora(int idProtectoraAnimal) {
		Optional<Protectora> optional = protectorasRepository.findById(idProtectoraAnimal);
		List<Animal> listaAnimalesPorProtectora = animalesRepository.findByProtectora(optional.get());
		return listaAnimalesPorProtectora;
	}

	/**
	 * M�todo encargado de devolver la lista de tipos de animales
	 * 
	 * @return Lista de tipolog�as de animales.
	 */
	@Override
	public List<String> buscarTipologias() {
		List<String> tipos = new LinkedList<>();
		tipos.add("Perro");
		tipos.add("Gato");
		tipos.add("Pajaro");
		tipos.add("Tortuga");
		tipos.add("Hamster");
		tipos.add("Huron");
		tipos.add("Cerdo");
		tipos.add("Reptil");
		tipos.add("Equino");
		return tipos;
	}

	/**
	 * M�todo encargado de devolver la lista de localidades de Espa�a.
	 * 
	 * @return Lista de localidades de Espa�a.
	 */
	@Override
	public List<String> buscarLocalidades() {
		List<String> localidades = new LinkedList<>();
		localidades.add("A toda Espa�a");
		localidades.add("A Coru�a");
		localidades.add("�lava");
		localidades.add("Albacete");
		localidades.add("Alicante");
		localidades.add("Almer�a");
		localidades.add("Asturias");
		localidades.add("�vila");
		localidades.add("Badajoz");
		localidades.add("Islas Baleares");
		localidades.add("Barcelona");
		localidades.add("Burgos");
		localidades.add("C�ceres");
		localidades.add("C�diz");
		localidades.add("Cantabria");
		localidades.add("Castell�n");
		localidades.add("Ciudad Real");
		localidades.add("C�rdoba");
		localidades.add("Cuenca");
		localidades.add("Girona");
		localidades.add("Granada");
		localidades.add("Guadalajara");
		localidades.add("Guip�zcoa");
		localidades.add("Huelva");
		localidades.add("Huesca");
		localidades.add("Ja�n");
		localidades.add("La Rioja");
		localidades.add("Las Palmas");
		localidades.add("Le�n");
		localidades.add("Lleida");
		localidades.add("Lugo");
		localidades.add("Madrid");
		localidades.add("M�laga");
		localidades.add("Murcia");
		localidades.add("Navarra");
		localidades.add("Orense");
		localidades.add("Palencia");
		localidades.add("Pontevedra");
		localidades.add("Salamanca");
		localidades.add("Segovia");
		localidades.add("Sevilla");
		localidades.add("Soria");
		localidades.add("Tarragona");
		localidades.add("Santa Cruz de Tenerife");
		localidades.add("Teruel");
		localidades.add("Toledo");
		localidades.add("Valencia");
		localidades.add("Valladolid");
		localidades.add("Vizcaya");
		localidades.add("Zamora");
		localidades.add("Zaragoza");

		return localidades;
	}

	/**
	 * M�todo encargado de eliminar un animal de BD.
	 * 
	 * @param idAnimal:
	 *            Id del animal que se quiere eliminar de BD.
	 */
	@Override
	public void eliminar(int idAnimal) {
		animalesRepository.deleteById(idAnimal);
	}

	/**
	 * M�todo encargado de buscar los animales en una determinada p�gina
	 * 
	 * @param page:
	 *            Objeto pageable.
	 * @return conjunto de animales por p�gina.
	 */
	@Override
	public Page<Animal> buscarTodasPaginacionAnimales(Pageable page) {
		return animalesRepository.findAll(page);
	}

	/**
	 * M�todo encargado de validar algunos de los campos del formulario de creaci�n
	 * de nuevo animal.
	 * 
	 * @param result:
	 *            Objeto BindingResult.
	 * @param animal:
	 *            Objeto Animal.
	 */
	@Override
	public void validarCampos(BindingResult result, Animal animal) {
		if (Utileria.isNumeric(animal.getRaza()) == true) {
			logger.info("Error raza");
			ObjectError errorRaza = result.getFieldError("raza");
			errorRaza = new ObjectError("raza",
					"Raza: Debe introducir una raza correcta, la raza del animal no puede ser un valor numerico");
			result.addError(errorRaza);
		}
	}
}
