package com.sgomezr.appet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.util.Utileria;

/**
 * Animales Controller: Controlador encargado de la gesti�n del m�dulo de
 * animales de ApPet
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
@RequestMapping("/animales")
public class AnimalesController {

	final static Logger logger = Logger.getLogger(AnimalesController.class);

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IProtectorasService serviceProtectoras;

	/**
	 * M�todo encargado de renderizar la vista donde se muestra la lista de todos
	 * los animales publicados en ApPet por las diferentes protectoras.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @return String ruta
	 */
	@GetMapping("/listaAnimales")
	public String mostrarIndex(Model model) {
		List<Animal> listaAnimales = serviceAnimales.buscarTodosLosAnimales();
		model.addAttribute("animales", listaAnimales);
		return "animales/listaAnimales";
	}

	/**
	 * Metodo encargado de renderizar la vista donde se muestran los animales
	 * asociados a una determinada protectora.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @param idProtectora:
	 *            id de la protectora de la cual se van a mostrar los animales
	 * @return String ruta
	 */
	@GetMapping("/listaAnimalesProtectora")
	public String listaAnimalesProtectora(Model model, @RequestParam("idProtectora") int idProtectora) {
		List<Animal> listaAnimalesPorProtectora = serviceAnimales.buscarAnimalPorProtectora(idProtectora);
		model.addAttribute("animales", listaAnimalesPorProtectora);
		model.addAttribute("protectora", serviceProtectoras.buscarProtectoraPorId(idProtectora));
		return "animales/listaAnimalesProtectora";
	}

	/**
	 * M�todo encargado de renderiar el formulario para crear un nuevo animal en
	 * ApPET
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @param animal:
	 *            Objeto animal, modelAttribute nos permite realizar un binding de
	 *            los datos que tenemos en el formulario de "crear nuevo animal" con
	 *            la capa de backend, es decir /crear asignar� un objeto Animal al
	 *            modelo.
	 * @return String ruta
	 */
	@GetMapping("/crear")
	public String crear(Model model, @ModelAttribute Animal animal) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		List<String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras", listaProtectoras);
		model.addAttribute("localidades", serviceAnimales.buscarLocalidades());
		return "animales/formAnimal";
	}

	/**
	 * M�todo cuya funci�n principal es guardar los datos introudcidos en el
	 * formulario de nuevo animal en la tabla de animales de la base de datos. A su
	 * vez, dicho m�todo se encarga de realizar la validaci�n de los campos del
	 * formularo, si estos pasan la validaci�n se proceder� al guardado de los datos
	 * de dicho animal en BD, si esto no ocurriese, se volver�a a renderizar la
	 * pagina de formulario indicando los errores encontrados y los campos a los que
	 * corresponden. Por otro lado, este m�todo se encarga de setear un nombre a la
	 * imagen, el cual es generado con la combinaci�n de un conjunto de caracteres
	 * aleatorios + el nombre del archivo de imagen subido y guardarla en su carpeta
	 * correspondiente.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @param animal:
	 *            Objeto animal
	 * @param result:
	 *            Interfaz BindingResult, define los resultados del dataBinding para
	 *            el objeto animal que se intenta crear (valida que los campos del
	 *            formulario sean del tipo de dato que deben ser o esten definidos
	 *            de forma correcta)
	 * @param protectora:
	 *            Objeto protectora asociada al animal que se esta intentando
	 *            guardar
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @param multiPart:
	 *            Objeto MultipartFile, se trata de un archivo de imagen, en este
	 *            caso la imagen del animal.
	 * @param request:
	 *            objeto HttpServletRequest.
	 * @return String ruta
	 */

	@PostMapping("/guardar")
	public String guardar(Model model, @ModelAttribute Animal animal, BindingResult result,
			@ModelAttribute Protectora protectora, RedirectAttributes atributos,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request) {
		String folder = "animales";
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		List<Integer> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getIdProtectora());
		}
		model.addAttribute("protectoras", listaProtectoras);
		model.addAttribute("localidades", serviceAnimales.buscarLocalidades());
		serviceAnimales.validarCampos(result, animal);

		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "animales/formAnimal";
		}
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request, folder);
			animal.setImagen(nombreImagen);
		}
		logger.info("Recibiendo objeto animal" + animal);
		logger.info("Recibiendo objeto animal de protectora: " + protectora.getIdProtectora());
		logger.info("Elementos en la lista antes de la inserci�n: " + serviceAnimales.buscarTodosLosAnimales().size());
		serviceAnimales.insertar(animal);
		logger.info(
				"Elementos en la lista despues de la inserci�n: " + serviceAnimales.buscarTodosLosAnimales().size());
		atributos.addFlashAttribute("mensaje", "El animal ha sido creado y guardado correctamente");
		return "redirect:/animales/listaAnimales";
	}

	/**
	 * M�todo encargado de editar un determinado animal de la base de datos (se
	 * encarga de hacer un update en BD)
	 * 
	 * @param idAnimal:
	 *            id del animal del que se quieren editar sus datos
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @return String ruta
	 */
	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idAnimal") int idAnimal, Model model) {
		Animal animal = serviceAnimales.buscarAnimalPorId(idAnimal);
		System.out.println(animal.getId());
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		List<String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("animal", animal);
		model.addAttribute("protectoras", listaProtectoras);
		model.addAttribute("localidades", serviceAnimales.buscarLocalidades());
		return "animales/formAnimal";
	}

	/**
	 * M�todo encargado de eliminar un animal de la BD
	 * 
	 * @param idAnimal:
	 *            id del animal que se quiere eliminar.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @param atributos:
	 *            atributos, objeto RedirectAttributes para indicar mensajes de
	 *            inter�s al usuario.
	 * @return String ruta
	 */
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idAnimal") int idAnimal, Model model, RedirectAttributes atributos) {
		serviceAnimales.eliminar(idAnimal);
		atributos.addFlashAttribute("mensaje", "El animal fue eliminado correctamente");
		return "redirect:/animales/listaAnimales";
	}

	/**
	 * M�todo encargado de mostrar un listado de las distintas tipologias de
	 * animales en la vista del formulario de creaci�n de nuevo animal.
	 * 
	 * @return lista de strings con todas las tipolog�as de animales disponibles en la
	 *         aplicaci�n
	 */
	@ModelAttribute("tipos")
	public List<String> getTipologias() {
		return serviceAnimales.buscarTipologias();
	}

	/**
	 * M�todo encargado de que se realice un correcto binding con el formato de
	 * fecha del formulario
	 * 
	 * @param binder:
	 *            Objeto WebDataBinder, encargado de hacer un correcto "binding" de
	 *            los datos
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
}