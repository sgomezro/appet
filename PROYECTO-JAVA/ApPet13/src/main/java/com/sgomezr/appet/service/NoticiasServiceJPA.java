package com.sgomezr.appet.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.repository.NoticiaRepository;

/**
 * Servicio de Noticias
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class NoticiasServiceJPA implements INoticiasService {

	final static Logger logger = Logger.getLogger(NoticiasServiceJPA.class);

	@Autowired
	private NoticiaRepository noticiasRepository;

	/**
	 * M�todo informativo de comprobaci�n.
	 * 
	 * @param noticia:
	 *            Objeto noticia.
	 */
	@Override
	public void guardarNoticia(Noticia noticia) {
		logger.info("Guadando el objeto " + noticia + " en la base de datos.");
	}

	/**
	 * M�todo encargado de buscar una noticia en BD por su id.
	 * 
	 * @param idNoticia:
	 *            Id de la noticia que se quiere buscar en BD.
	 * @return Objeto noticia con un id determinado si existe o null si no existe en
	 *         BD.
	 */
	@Override
	public Noticia buscarNoticiaPorId(int idNoticia) {
		Optional<Noticia> noticia = noticiasRepository.findById(idNoticia);
		if (noticia.isPresent()) {
			return noticia.get();
		}
		return null;
	}

	/**
	 * M�todo encargado de insertar un objeto noticia en BD.
	 * 
	 * @param noticia:
	 *            Objeto noticia que se va a insertar en BD.
	 */
	@Override
	public void insertarNoticia(Noticia noticia) {
		noticiasRepository.save(noticia);
	}

	/**
	 * M�todo encargado de buscar todas las noticias que existen en la tabla de
	 * noticias de BD.
	 * 
	 * @return: Listado de todas las noticias existentes en BD.
	 */
	@Override
	public List<Noticia> buscarTodasLasNoticias() {
		return noticiasRepository.findAll();
	}

	/**
	 * M�todo encargado de eliminar una noticia con un id determinado de la tabla de
	 * noticias de BD.
	 * 
	 * @param idNoticia:
	 *            Id de la noticia que se quiere eliminar.
	 */
	@Override
	public void eliminar(int idNoticia) {
		noticiasRepository.deleteById(idNoticia);
	}

	/**
	 * M�todo encargado de buscar todas las noticias de una determinada p�gina.
	 * 
	 * @param page:
	 *            Objeto pageable
	 * @return Noticias d una determinada p�gina.
	 */
	@Override
	public Page<Noticia> buscarTodasPaginacion(Pageable page) {
		return noticiasRepository.findAll(page);
	}

}
