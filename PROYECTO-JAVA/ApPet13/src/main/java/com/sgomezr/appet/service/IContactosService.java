package com.sgomezr.appet.service;

import java.util.List;

import com.sgomezr.appet.model.Contacto;


public interface IContactosService {
	
	void insertar (Contacto contacto);
	
	List<Contacto> buscarTodosLosContactos();
	
	void eliminar(int idContacto);
	
}
