package com.sgomezr.appet.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;
import com.sgomezr.appet.repository.AnimalesRepository;
import com.sgomezr.appet.repository.FavoritosRepository;

/**
 * Servicio de animales favoritos
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class FavoritosServiceJPA implements IFavoritosService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(FavoritosServiceJPA.class);

	@Autowired
	private FavoritosRepository favoritosRepository;
	@Autowired
	private AnimalesRepository animalesRepository;

	/**
	 * M�todo encargado de a�adir un objeto favorito a la BD.
	 * 
	 * @param favorito:
	 *            Objeto favorito que estamos intentando a�adir a la BD.
	 */
	@Override
	public void agregar(Favorito favorito) {
		favoritosRepository.save(favorito);
	}

	/**
	 * M�todo encargado de determinar si un animal ya existe en la lista de animales
	 * favoritos de un determinado usuario.
	 * 
	 * @param idAnimal:
	 *            Id del animal del que queremos saber si pertenece o no al listado
	 *            de animales favoritos de un determinado usuario.
	 * @param idUsuario:
	 *            Id del usuario del que estamos tomando la lista de animales
	 *            favoritos.
	 * @return resultado en booleano.
	 */
	@Override
	public Boolean existeFavorito(int idAnimal, int idUsuario) {
		Optional<Animal> animal = animalesRepository.findById(idAnimal);
		Favorito favorito = favoritosRepository.findByAnimalAndIdUsuario(animal, idUsuario);
		if (favorito == null) {
			return false;
		}
		if (((favorito.getAnimal()).getId() == (idAnimal)) && (favorito.getIdUsuario() == idUsuario)) {
			return true;
		}
		return false;
	}

	/**
	 * M�todo que se encarga de buscar el listado de animales favoritos de un
	 * determinado usuario.
	 * 
	 * @param idUsuario:
	 *            Id del usuario del cual queremos recuperar su listado de animales
	 *            favoritos.
	 * @return Listado de animales favoritos de un usuario.
	 */
	@Override
	public List<Favorito> buscarFavoritosPorIdUsuario(int idUsuario) {
		return favoritosRepository.findByIdUsuario(idUsuario);
	}

	/**
	 * M�todo encargado de eliminar un animal favorito de la tabla de "Animales
	 * favoritos" de BD.
	 * 
	 * @param idAnimal:
	 *            Id del Animal del cual se quiere eliminar el favorito.
	 */
	@Override
	public void eliminar(int idAnimal) {
		favoritosRepository.deleteById(idAnimal);
	}

}
