package com.sgomezr.appet.controller;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Contacto;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IContactosService;

/**
 * Contacto Controller: Controlador encargado de la gesti�n del m�dulo de
 * contacto de ApPet.
 * 
 * @author Sandra Gomez Roman
 */
@Controller
@RequestMapping("/contacto")
public class ContactoController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(ContactoController.class);

	@Autowired
	IAnimalesService serviceAnimales;

	@Autowired
	IContactosService serviceContactos;

	/**
	 * M�todo encargado de renderizar la vista donde se muestra la lista de todos
	 * los contactos que los usuarios han realizado en ApPet.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @return String ruta: contacto/listaContactos
	 */
	@GetMapping("/listaContactos")
	public String listaContactos(Model model) {
		List<Contacto> contactos = serviceContactos.buscarTodosLosContactos();
		model.addAttribute("contactos", contactos);
		return "contacto/listaContactos";
	}

	/**
	 * M�todo encargado de renderiar el formulario de contacto de la aplicaci�n
	 * 
	 * @param contacto:
	 *            Objeto Contacto, modelAttribute nos permite realizar un binding de
	 *            los datos que tenemos en el formulario de contacto con la capa de
	 *            backend, es decir /crear asignar� un objeto Contacto al modelo.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @return String ruta: contacto/formContacto
	 */
	@GetMapping(value = "/crear")
	public String crear(@ModelAttribute Contacto contacto, Model model) {
		model.addAttribute("tipos", serviceAnimales.buscarTipologias());
		model.addAttribute("tiposNotificaciones", tipoNotificaciones());
		return "contacto/formContacto";
	}

	/**
	 * M�todo cuya funci�n principal es guardar los datos introudcidos en el
	 * formulario de contacto en la tabla de contactos de la base de datos. A su
	 * vez, dicho m�todo se encarga de realizar la validaci�n de los campos del
	 * formularo, si estos pasan la validaci�n se proceder� al guardado de los datos
	 * de dicho contacto en BD, si esto no ocurriese, se volver�a a renderizar la
	 * pagina de formulario indicando los errores encontrados y los campos a los que
	 * corresponden.
	 * 
	 * @param contacto:
	 *            Objeto Contacto
	 * @param result:
	 *            Interfaz BindingResult, define los resultados del dataBinding para
	 *            el objeto contacto que se esta creando (valida que los campos del
	 *            formulario sean del tipo de dato que deben ser o esten definidos
	 *            de forma correcta)
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta
	 */
	@PostMapping(value = "/guardar")
	public String guardar(@ModelAttribute Contacto contacto, BindingResult result, RedirectAttributes atributos) {
		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "contacto/formContacto";
		}
		serviceContactos.insertar(contacto);
		atributos.addFlashAttribute("mensaje",
				"Su mensaje a sido enviado correctamente a los administradores de ApPet");
		return "redirect:/contacto/crear";
	}

	/**
	 * M�todo encargado de eliminar un contacto o "mensaje" de la BD.
	 * 
	 * @param idContacto:
	 *            id del contacto que se quiere eliminar.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @param atributos:
	 *            objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta
	 */
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idContacto") int idContacto, Model model, RedirectAttributes atributos) {
		serviceContactos.eliminar(idContacto);
		atributos.addFlashAttribute("mensaje", "El mensaje fue eliminado correctamente");
		return "redirect:/contacto/listaContactos";
	}

	/**
	 * M�todo encargado de mostrar un listado de los distintos tipos de
	 * notificaciones que un usuario puede desear en la vista del formulario de
	 * contacto.
	 * 
	 * @return listado de strings con los tipos de notificaciones
	 */
	private List<String> tipoNotificaciones() {
		List<String> tipos = new LinkedList<>();
		tipos.add("Noticias");
		tipos.add("Nuevos Animales");
		tipos.add("Animales adoptados");
		tipos.add("Nuevas protectoras");
		return tipos;
	}
}