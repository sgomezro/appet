package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.repository.PerfilesRepository;

/**
 * Servicio de perfiles
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class PerfilesServiceJPA implements IPerfilesService {

	final static Logger logger = Logger.getLogger(PerfilesServiceJPA.class);

	@Autowired
	private PerfilesRepository perfilesRepository;

	/**
	 * M�todo encargado de guardar un Objeto perfil en su tabla correspondiente de
	 * BD.
	 * 
	 * @param perfil:
	 *            Objeto perfil que queremos guardar en BD.
	 */
	@Override
	public void guardar(Perfil perfil) {
		perfilesRepository.save(perfil);
	}

	/**
	 * M�todo encargado de buscar un determinado perfil en BD por su nombre de
	 * usuario
	 * 
	 * @param username:
	 *            nombre de usuario
	 * @return Objeto perfil que posea un determinado nombre de usuario.
	 */
	@Override
	public Perfil buscarIdPerfilPorUsername(String username) {
		Perfil perfil = perfilesRepository.findByUsername(username);
		return perfil;
	}

	/**
	 * M�todo encargado de elimnar un determinado perfil de su tabla en la BD.
	 * 
	 * @param idPerfil:
	 *            Id del perfil que se quiere eliminar de la BD.
	 */
	@Override
	public void eliminar(int idPerfil) {
		perfilesRepository.deleteById(idPerfil);
	}

	/**
	 * M�todo encargado de buscar y devolver la lista de perfiles en BD con un
	 * determinado rol de usuario.
	 * 
	 * @param perfil:
	 *            rol de usuario por el cual hacemos la b�squeda.
	 * @return Lista de perfiles en BD con un determinado rol de usuario.
	 */
	@Override
	public List<Perfil> buscarPerfilPorRol(String perfil) {
		List<Perfil> listaPerfiles = new LinkedList<>();
		listaPerfiles = perfilesRepository.findByPerfil(perfil);
		return listaPerfiles;
	}

	/**
	 * M�todo que se encarga de generar un objeto perfil a partir de un objeto
	 * protectora y el id de perfil
	 * 
	 * @param protectora:
	 *            Objeto protectora de la cual se quiere crear el objeto perfil
	 * @param idPerfil:
	 *            id del perfil que estamos creando para la protectora.
	 * @return Objeto perfil que generamos, con los datos de la protectora.
	 */
	@Override
	public Perfil generarObjetoPerfil(Protectora protectora, int idPerfil) {

		Perfil perfilGenerado = new Perfil();
		perfilGenerado.setIdPerfil(idPerfil);
		perfilGenerado.setPerfil("PROTECTORA");
		perfilGenerado.setUsername(protectora.getNombreProtectora());
		return perfilGenerado;
	}
}