package com.sgomezr.appet.service;

import java.util.List;

import com.sgomezr.appet.model.ProtectoraFavorita;

public interface IProtectorasFavoritasService {

	void agregar(ProtectoraFavorita protectoraFavorita);
	List<ProtectoraFavorita> buscarProtectorasFavoritasPorIdUsuario (int idUsuario);
	Boolean existeProtectoraFavorita(int idProtectora, int idUsuario);
	public void eliminarProtectoraFavorita(int idFavorito);
}
