package com.sgomezr.appet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="perfiles")
public class Perfil {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_perfil")
	private int idPerfil;
	
	@Column(name="usuarios_username")
	private String username;
	
	@Column(name="perfil")
	private String perfil;

	public int getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(int id) {
		this.idPerfil = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	@Override
	public String toString() {
		return "Perfil [id=" + idPerfil + ", username=" + username + ", perfil=" + perfil + "]";
	}
	
	

}
