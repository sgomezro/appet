package com.sgomezr.appet.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Contacto;
import com.sgomezr.appet.repository.ContactosRepository;

/**
 * Servicio de Contactos
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class ContactosServiceJPA implements IContactosService {

	final static Logger logger = Logger.getLogger(ContactosServiceJPA.class);

	@Autowired
	private ContactosRepository contactosRepository;

	/**
	 * M�todo que se encarga de insertar un determinado objeto contacto en BD.
	 * 
	 * @param contacto:
	 *            Objeto contacto que se quiere guardar.
	 */
	@Override
	public void insertar(Contacto contacto) {
		contactosRepository.save(contacto);
	}

	/**
	 * M�todo encargado de buscar todos los contactos guardados en BD.
	 * 
	 * @return Listado de tdos los contactos.
	 */
	@Override
	public List<Contacto> buscarTodosLosContactos() {
		return contactosRepository.findAll();
	}

	/**
	 * M�todo que se encarga de eliminar un determinado contacto de la BD por su id.
	 * 
	 * @param idContacto:
	 *            Id del contacto que se quiere eliminar.
	 */
	@Override
	public void eliminar(int idContacto) {
		contactosRepository.deleteById(idContacto);
	}
}
