package com.sgomezr.appet.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.parameters.P;

@Entity
@Table(name="protectoras")
public class Protectora {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_protectora")
	int idProtectora;
	
	@Column(name="nombre_protectora")
	private String nombreProtectora;
	
	@Column(name="nombre_usuario")
	private String nombre;
	
	@Column(name="apellidos_usuario")
	private String apellidos;
	
	@Column(name="email_usuario")
	private String email;
	
	@Column(name="password_usuario")
	private String password;
	
	@Column(name="comprobarPass_usuario")
	private String comprobarPass;
	
	@Column(name="fechaNacimiento_usuario")
	private Date fechaNacimiento;
	
	@Column(name="localidad_usuario")
	private String localidad;

	@Column(name="logo_protectora")
	private String logoProtectora;
	
	@Column(name="fechaCreacion_usuario")
	private Date fechaCreacion;
	
	@Column(name="estado_usuario")
	private String estado; 
	
	@Column(name="telefono_protectora")
	private int telefonoProtectora;
	
	@Column(name="direccion_protectora")
	private String direccionProtectora;
	
	@Column(name="imagen_protectora")
	private String imagenProtectora;

	@Column(name="perfil_protectora")
	private String perfilProtectora;
	
	@Column(name="descripcion_protectora")
	private String descripcionProtectora;
	
	@Column(name="urlVideo_protectora")
	private String urlVideoProtectora;
	
	@Column(name="descripcionProcedimiento_protectora")
	private String descripcionProcedimientoProtectora;
	
	@Column(name="latitud_protectora")
	private String latitudProtectora;
	
	@Column(name="altitud_protectora")
	private String altitudProtectora;
	
	@OneToMany(mappedBy="protectora", fetch= FetchType.EAGER)
	private List<Animal> animales;
	
	public Protectora(int idProtectora, String nombreProtectora,String nombre,String apellidos, String email, String password, String comprobarPass,
			Date fechaNacimiento,
			int telefonoProtectora, String localidad, String direccionProtectora, String imagenProtectora,
			String logoProtectora, Date fechaCreacion, String estado, String perfilProtectora,
			String descripcionProtectora, String urlVideoProtectora, String descripcionProcedimientoProtectora,
			String latitudProtectora, String altitudProtectora) {
		super();
		this.idProtectora = idProtectora;
		this.nombreProtectora = nombreProtectora;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = email;
		this.password = password;
		this.comprobarPass = comprobarPass;
		this.fechaNacimiento = fechaNacimiento;
		this.telefonoProtectora = telefonoProtectora;
		this.localidad = localidad;
		this.direccionProtectora = direccionProtectora;
		this.imagenProtectora = imagenProtectora;
		this.logoProtectora = logoProtectora;
		this.fechaCreacion = fechaCreacion;
		this.estado = estado;
		this.perfilProtectora = perfilProtectora;
		this.descripcionProtectora = descripcionProtectora;
		this.urlVideoProtectora = urlVideoProtectora;
		this.descripcionProcedimientoProtectora = descripcionProcedimientoProtectora;
		this.latitudProtectora = latitudProtectora;
		this.altitudProtectora = altitudProtectora;
	}

	public Protectora() {
		this.fechaCreacion = new Date(); // Por default, la fecha del sistema
		this.imagenProtectora = "Sin imagen";
		this.latitudProtectora = "Sin latitud";
		this.altitudProtectora = "Sin altitud";
		this.estado = "Activa";
		this.logoProtectora = "protectoraImg.png";
		this.urlVideoProtectora = "Sin video";
		this.perfilProtectora = "Protectora";

	}

	public int getIdProtectora() {
		return idProtectora;
	}

	public void setIdProtectora(int idProtectora) {
		this.idProtectora = idProtectora;
	}

	public String getNombreProtectora() {
		return nombreProtectora;
	}

	public void setNombreProtectora(String nombreProtectora) {
		this.nombreProtectora = nombreProtectora;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getComprobarPass() {
		return comprobarPass;
	}

	public void setComprobarPass(String comprobarPass) {
		this.comprobarPass =comprobarPass;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public int getTelefonoProtectora() {
		return telefonoProtectora;
	}

	public void setTelefonoProtectora(int telefonoProtectora) {
		this.telefonoProtectora = telefonoProtectora;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getDireccionProtectora() {
		return direccionProtectora;
	}

	public void setDireccionProtectora(String direccionProtectora) {
		this.direccionProtectora = direccionProtectora;
	}

	public String getImagenProtectora() {
		return imagenProtectora;
	}

	public void setImagenProtectora(String imagenProtectora) {
		this.imagenProtectora = imagenProtectora;
	}
	
	public String getLogoProtectora() {
		return logoProtectora;
	}

	public void setLogoProtectora(String logoProtectora) {
		this.logoProtectora = logoProtectora;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPerfilProtectora() {
		return perfilProtectora;
	}

	public void setPerfilProtectora(String perfilProtectora) {
		this.perfilProtectora = perfilProtectora;
	}

	public String getDescripcionProtectora() {
		return descripcionProtectora;
	}

	public void setDescripcionProtectora(String descripcionProtectora) {
		this.descripcionProtectora = descripcionProtectora;
	}

	public String getUrlVideoProtectora() {
		return urlVideoProtectora;
	}

	public void setUrlVideoProtectora(String urlVideoProtectora) {
		this.urlVideoProtectora = urlVideoProtectora;
	}

	public String getDescripcionProcedimientoProtectora() {
		return descripcionProcedimientoProtectora;
	}

	public void setDescripcionProcedimientoProtectora(String descripcionProcedimientoProtectora) {
		this.descripcionProcedimientoProtectora = descripcionProcedimientoProtectora;
	}

	public String getLatitudProtectora() {
		return latitudProtectora;
	}

	public void setLatitudProtectora(String latitudProtectora) {
		this.latitudProtectora = latitudProtectora;
	}

	public String getAltitudProtectora() {
		return altitudProtectora;
	}

	public void setAltitudProtectora(String altitudProtectora) {
		this.altitudProtectora = altitudProtectora;
	}
	
	public List<Animal> getAnimales() {
		return animales;
	}

	public void setAnimales(List<Animal> animales) {
		this.animales = animales;
	}

	@Override
	public String toString() {
		return "Protectora [idProtectora=" + idProtectora + ", nombreProtectora=" + nombreProtectora + ", nombre=" + nombre + ", apellidos=" + apellidos
				+ ", email=" + email + ", password=" + password + ", comprobarPass=" + comprobarPass+ ", fechaNacimiento=" + fechaNacimiento
				+ ", telefonoProtectora=" + telefonoProtectora + ", localidad=" + localidad
				+ ", direccionProtectora=" + direccionProtectora + ", imagenProtectora=" + imagenProtectora
				+ ", logoProtectora=" + logoProtectora + ", fechaCreacion=" + fechaCreacion
				+ ", estado=" + estado + ", perfilProtectora=" + perfilProtectora
				+ ", descripcionProtectora=" + descripcionProtectora + ", urlVideoProtectora=" + urlVideoProtectora
				+ ", descripcionProcedimientoProtectora=" + descripcionProcedimientoProtectora + ", latitudProtectora="
				+ latitudProtectora + ", altitudProtectora=" + altitudProtectora + "]";
	}

	

}
