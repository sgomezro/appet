package com.sgomezr.appet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.ProtectoraFavorita;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IPerfilesService;
import com.sgomezr.appet.service.IProtectorasFavoritasService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.service.IUsuariosService;
import com.sgomezr.appet.util.Utileria;

/**
 * Protectoras Controller: Controlador encargado de la gesti�n del m�dulo de
 * protectoras de ApPet.
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
@RequestMapping("/protectoras")
public class ProtectorasController {

	final static Logger logger = Logger.getLogger(ProtectorasController.class);

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IProtectorasService serviceProtectoras;

	@Autowired
	private IProtectorasFavoritasService serviceProtectorasFavoritas;

	@Autowired
	private IUsuariosService serviceUsuarios;

	@Autowired
	private IPerfilesService servicePerfiles;

	/**
	 * M�todo encargado de renderizar la vista donde se muestra la lista de todas
	 * las protectoras disponibles en ApPet.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @return String ruta.
	 */
	@GetMapping("/listaProtectoras")
	public String mostrarIndex(Model model) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		model.addAttribute("protectoras", listaProtectoras);
		return "protectoras/listaProtectoras";
	}

	/**
	 * Metodo encargado de renderizar la vista donde se muestran todas las
	 * protectoras disponibles en ApPet, diferenciando si existe o no un usuario
	 * autenticado, de tal forma que si existe usuario autenticado, en dicha vista
	 * se mostrar�n con un corazon las protectoras que dicho usuario tiene en su
	 * lista de protectoras favoritoa, por el contrario, si no existe usuario
	 * autenticado la vista ser� la definida por defecto.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder a
	 *            determinados datos del usuario autenticado.
	 * @return String ruta.
	 */
	@GetMapping("/protectorasDisponibles")
	public String protectorasDisponibles(Model model, Authentication authentication) {
		if (authentication != null) {
			int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
			List<ProtectoraFavorita> protectorasFavoritas = serviceProtectorasFavoritas
					.buscarProtectorasFavoritasPorIdUsuario(idUsuario);
			model.addAttribute("protectorasFavoritas", protectorasFavoritas);
		}
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		List<String> localidades = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			localidades.add(p.getLocalidad());
		}
		model.addAttribute("localidades", localidades);
		model.addAttribute("protectoras", listaProtectoras);
		return "protectoras/protectorasDisponibles";
	}

	/**
	 * M�todo encargado de renderizar la vista en la cual se muestra la hoja de
	 * detalle de una determinada protectora, diferenciando si existe o no un
	 * usuario autenticado, de tal forma que si existe usuario autenticado,en dicha
	 * vista se mostrar� un corazon enlas protectoras que dicho usuario tiene en su
	 * lista de protectoras favoritas, por el contrario, si no existe usuario
	 * autenticado la vista ser� la definida por defecto.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param idProtectora:
	 *            Id de la protectora de la cual se va a mostrar la hoja de detalle.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder a
	 *            determinados datos del usuario autenticado.
	 * @return String ruta.
	 */
	@RequestMapping(value = "/detalleProtectora", method = RequestMethod.GET)
	public String mostrarDetalleProtectora(Model model, @RequestParam("idProtectora") int idProtectora,
			Authentication authentication) {
		Boolean protectoraEnFavorito = null;
		if (authentication != null) {
			int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
			List<ProtectoraFavorita> protectorasFavoritas = serviceProtectorasFavoritas
					.buscarProtectorasFavoritasPorIdUsuario(idUsuario);
			for (ProtectoraFavorita protectoraFavorita : protectorasFavoritas) {
				protectoraEnFavorito = false;
				if ((protectoraFavorita.getProtectoraFav().getNombre())
						.equalsIgnoreCase(serviceProtectoras.buscarProtectoraPorId(idProtectora).getNombre())) {
					protectoraEnFavorito = true;
					break;
				}
			}
			model.addAttribute("protectoraEnFavorito", protectoraEnFavorito);
		}
		logger.info("El id de la protectora es: " + idProtectora);
		model.addAttribute("protectora", serviceProtectoras.buscarProtectoraPorId(idProtectora));
		return "detalleProtectora";
	}

	/**
	 * M�todo encargado de renderizar la vista en la cual se muestran las
	 * protectoras disponibles de una determinada localidad, tras indicar por que
	 * localidad se quiere filtrar.
	 * 
	 * @param localidad:
	 *            Nombre de la localidad por la cual se quiere hacer el filtro de
	 *            protectoras.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder a
	 *            determinados datos del usuario autenticado.
	 * @return String ruta.
	 */
	@RequestMapping(value = "/busqueda", method = RequestMethod.POST)
	public String buscarProtectoraPorLocalidad(@RequestParam("localidad") String localidad, Model model,
			Authentication authentication) {
		if (authentication != null) {
			int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
			List<ProtectoraFavorita> protectorasFavoritas = serviceProtectorasFavoritas
					.buscarProtectorasFavoritasPorIdUsuario(idUsuario);
			model.addAttribute("protectorasFavoritas", protectorasFavoritas);
		}
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();

		logger.info("Localidad de busqueda: " + localidad);
		List<String> localidades = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			localidades.add(p.getLocalidad());
		}
		model.addAttribute("localidades", localidades);
		model.addAttribute("localidadBusqueda", localidad);
		if (localidad.equals("Mostrar todas")) {
			model.addAttribute("protectoras", listaProtectoras);
		} else {
			List<Protectora> protectorasPorLocalidad = serviceProtectoras.buscarProtectoraPorLocalidad(localidad);
			model.addAttribute("protectoras", protectorasPorLocalidad);
		}
		return "protectoras/protectorasDisponibles";
	}

	/**
	 * M�todo encargado de renderiar el formulario para crear.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param protectora:
	 *            Objeto protectora, modelAttribute nos permite realizar un binding
	 *            de los datos que tenemos en el formulario de "crear nueva
	 *            protectora" con la capa de backend, es decir /crear asignar� un
	 *            objeto Protectora al modelo.
	 * @return String ruta.
	 */
	@GetMapping("/crear")
	public String crear(Model model, @ModelAttribute Protectora protectora) {
		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());
		return "protectoras/formProtectora";

	}

	/**
	 * M�todo cuya funci�n principal es guardar los datos introudcidos en el
	 * formulario de nueva animal en la tabla de protectoras de la base de datos. A
	 * su vez, dicho m�todo se encarga de realizar la validaci�n de los campos del
	 * formularo, si estos pasan la validaci�n se proceder� al guardado de los datos
	 * de dicha protectora en BD, si esto no ocurriese, se volver�a a renderizar la
	 * pagina de formulario indicando los errores encontrados y los campos a los que
	 * corresponden. Por otro lado, este m�todo se encarga de setear un nombre a las
	 * imagenes, el cual es generado con la combinaci�n de un conjunto de caracteres
	 * aleatorios + el nombre del archivo de imagen subido y guardarla en su carpeta
	 * correspondiente.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param protectora:
	 *            Objeto Protectora
	 * @param result:
	 *            Interfaz BindingResult, define los resultados del dataBinding para
	 *            el objeto protectora que se intenta crear(valida que los campos
	 *            del formulario sean del tipo de dato que deben ser o esten
	 *            definidos de forma correcta).
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @param multiPart:
	 *            Objeto MultipartFile, se trata de un archivo de imagen, en este
	 *            caso la imagen de la protectora.
	 * @param multiPartLogo:
	 *            Objeto MultipartFile, se trata de un archivo de imagen, en este
	 *            caso el logo de la protectora.
	 * @param request:
	 *            Objeto HttpServletRequest.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder a
	 *            determinados datos del usuario autenticado.
	 * @return String ruta.
	 */
	@PostMapping("/guardar")
	public String guardar(Model model, @ModelAttribute Protectora protectora, BindingResult result,
			RedirectAttributes atributos, @RequestParam("archivoImagen") MultipartFile multiPart,
			@RequestParam("archivoImagenLogo") MultipartFile multiPartLogo, HttpServletRequest request,
			Authentication authentication) {

		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());

		// Validacion de campos/errores del formulario
		if (serviceProtectoras.buscarProtectoraPorId(protectora.getIdProtectora()) == null) {
			serviceProtectoras.validarCampos(result, protectora);
		}

		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "protectoras/formProtectora";
		}
		String folder = "protectoras";
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request, folder);
			protectora.setImagenProtectora(nombreImagen);

		}
		if (!multiPartLogo.isEmpty()) {
			String nombreLogo = Utileria.guardarImagen(multiPartLogo, request, folder);
			protectora.setLogoProtectora(nombreLogo);
		}

		logger.info("Recibiendo objeto protectora" + protectora);

		// A�adimos la protectora a la tabla "Protectoras" de BBDD
		serviceProtectoras.insertar(protectora);

		// A�adimos la protectora a la tabla "Usuarios" de BBDD
		int idUsuario = 0;
		int idPerfil = 0;
		Usuario usuarioProtectora = new Usuario();
		Perfil perfilProtectora = new Perfil();
		if (serviceUsuarios.buscarUsuarioPorUsername(protectora.getNombreProtectora()) == null) {
			usuarioProtectora = serviceUsuarios.generarObjetoUsuario(protectora, idUsuario);
			serviceUsuarios.guardar(usuarioProtectora);

			// A�adimos el perfil de esa protectora a la tabla "perfiles" de BBDD
			Perfil perfilTmp = new Perfil();
			perfilTmp.setUsername(usuarioProtectora.getUsername());
			perfilTmp.setPerfil("PROTECTORA");
			servicePerfiles.guardar(perfilTmp);
		} else {
			idUsuario = serviceUsuarios.buscarUsuarioPorUsername(protectora.getNombreProtectora()).getId();
			idPerfil = servicePerfiles.buscarIdPerfilPorUsername(protectora.getNombreProtectora()).getIdPerfil();
			usuarioProtectora = serviceUsuarios.generarObjetoUsuario(protectora, idUsuario);
			perfilProtectora = servicePerfiles.generarObjetoPerfil(protectora, idPerfil);
			serviceUsuarios.guardar(usuarioProtectora);
			servicePerfiles.guardar(perfilProtectora);
		}
		String redirection = "redirect:/protectoras/listaProtectoras";
		for (GrantedAuthority rol : authentication.getAuthorities()) {
			if (rol.getAuthority().equals("PROTECTORA")) {
				redirection = "redirect:/miPerfil/detalle";
				atributos.addFlashAttribute("mensaje",
						"Los datos de su PROTECTORA han sido editados correctamente, vuelva a inciar sesi�n si quiere ver sus datos actualizados");
			} else {
				atributos.addFlashAttribute("mensaje",
						"La protectora ha sido guardada correctamente con el rol y los permisos de PROTECTORA, a su vez, se encuentra en el listado de USUARIOS");
			}
		}
		return redirection;
	}

	/**
	 * M�todo encargado de editar una determinada protectora de la base de datos (se
	 * encarga de hacer un update en BD)
	 * 
	 * @param idProtectora:
	 *            Id de la protectora de la cual se quieren editar sus datos.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @return String ruta.
	 */
	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idProtectora") int idProtectora, Model model) {
		Protectora protectora = serviceProtectoras.buscarProtectoraPorId(idProtectora);
		model.addAttribute("localidades", serviceProtectoras.buscarLocalidades());
		protectora.setPassword(null);
		protectora.setComprobarPass(null);
		model.addAttribute("protectora", protectora);
		return "protectoras/formProtectora";
	}

	/**
	 * M�todo encargado de eliminar una protectora de la BD.
	 * 
	 * @param idProtectora:
	 *            Id de la protectora que se quiere eliminar.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta.
	 */
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idProtectora") int idProtectora, Model model, RedirectAttributes atributos) {

		// Buscamos el id de los animales corresponientes a la protectora a eliminar y
		// los eliminamos de BBDD
		List<Animal> listaAnimalesEliminar = serviceAnimales.buscarAnimalPorProtectora(idProtectora);
		for (Animal animalEliminar : listaAnimalesEliminar) {
			serviceAnimales.eliminar(animalEliminar.getId());
		}
		Usuario usuarioEliminar = serviceUsuarios.buscarUsuarioPorUsername(
				(serviceProtectoras.buscarProtectoraPorId(idProtectora).getNombreProtectora()));
		Perfil perfilEliminar = servicePerfiles.buscarIdPerfilPorUsername(
				(serviceProtectoras.buscarProtectoraPorId(idProtectora).getNombreProtectora()));
		if (perfilEliminar != null) {
			// Eliminamos el perfil de l a protectora de BBDD de perfiles si existen
			servicePerfiles.eliminar(perfilEliminar.getIdPerfil());
			// Eliminamos la protectora de BBDD de usuarios si existen
			serviceUsuarios.eliminar(usuarioEliminar.getId());
		}
		// Eliminamos la protectora de BBDD de protectoras
		serviceProtectoras.eliminar(idProtectora);
		atributos.addFlashAttribute("mensaje",
				"La protectora fue eliminada correctamente tanto de PROTECTORAS como de USUARIOS");
		return "redirect:/protectoras/listaProtectoras";
	}

	/**
	 * M�todo encargado de que se realice un correcto binding con el formato de
	 * fecha del formulario.
	 * 
	 * @param binder:
	 *            Objeto WebDataBinder, encargado de hacer un correcto "binding" de
	 *            los datos.
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

}