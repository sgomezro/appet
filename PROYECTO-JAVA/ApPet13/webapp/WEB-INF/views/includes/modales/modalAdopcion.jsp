<!-- Modal Adopcion -->
<div class="modal fade" id="ModalAdopcion" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header"
				style="text-align: center; color: white; background-color: #17a2b8">
				<h5 style="color: white;" class="modal-title" id="exampleModalLabel">APPET
					ADOPCIONES Y APADRINAMIENTOS FORMULARIO</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<small>Con la mayor brevedad posible el equipo de ApPet se
						pondra en contacto con usted v�a correo electr�nico y email para <strong>indicarle
							los pasos a seguir para la adopci�n.</strong>
					</small>
					<hr>
					<p class="text-center" style="color: red"><strong>FUNCIONALIDAD EN CONSTRUCCI�N</strong></p>
					<p style="text-align: center;">
						<strong>FORMULARIO DE ADOPCI�N O APADRINAMIENTO</strong> </br> <small>Por
							favor, rellene los siguientes campos:</small>
					</p>
					<hr>
					<div class="form-group">

						<label for="exampleInputEmail1"><strong>Email: </strong></label> <input
							type="email" class="form-control" id="InputEmail1"
							aria-describedby="emailHelp" placeholder="Introduce tu email">

					</div>
					<div class="form-group">
						<label for="exampleInputEmail1"><strong>N�mero de
								tel�fono</strong></label> <input type="text" class="form-control"
							id="InputTelefono" aria-describedby="telefonoHelp"
							placeholder="N�mero de tel�fono">
					</div>
					<div class="form-group">
						<label for="descripcion"><strong>Indicanos una
								breve descripci�n de tu inter�s en Sayker:</strong></label> <input type="text"
							class="form-control" id="descripcion" placeholder="Descripci�n">
					</div>
					<div style="text-align: right;">
						<button type="submit" class="btn btn-info">Enviar</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-secondary "
					data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>