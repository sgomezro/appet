<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<head>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
	integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
	crossorigin="anonymous">
<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>

</head>
<!-- Menu cualquier usuario sin registrar-->
<sec:authorize access="isAnonymous()">
	<div style="text-align: center" class="pt-5 sidebar navbar-nav">
		<img style="height: 130px; border: 4px solid white;"
			class="img-fluid img-profile rounded-circle mx-auto mb-2 img-responsive"
			src=${urlPublic}/images/usuarios/usuario.png alt="Imagen Perfil">
		<ul style="list-style: none">

			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}index/welcome"> <i class="fas fa-dog"></i> <span>Inicio</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				ANIMALES</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link" href="${urlRoot}">
					<i class="fas fa-paw"></i> <span>Animales en adopcion</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				PROTECTORAS</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}protectoras/protectorasDisponibles"> <i
					class="fas fa-home"></i> <span>Protectoras disponibles</span>
			</a></li>
		</ul>
	</div>
</sec:authorize>

<!-- Menu para usuarios y protectorasNA registrados -->
<sec:authorize access="hasAnyAuthority('USUARIO','PROTECTORA-NA')">
	<div style="text-align: center" class="pt-5 sidebar navbar-nav">
		<img style="height: 190px; width: 200px; border: 2px solid white;"
			class="img-fluid img-profile rounded-circle mx-auto mb-2 img-responsive"
			src=${urlPublic}/images/usuarios/${usr.imagen} alt="Imagen Perfil">
		<ul style="list-style: none">

			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}index/welcome"> <i class="fas fa-dog"></i> <span>Inicio</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">AREA
				PERSONAL</div>
			<hr style="background-color: #708090;">
			<li class="nav-item active"><a class="nav-link"
				href="${urlRoot}miPerfil/detalle"> <i class="fas fa-user"></i> <span>Mi
						perfil</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				ANIMALES</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link" href="${urlRoot}">
					<i class="fas fa-paw"></i> <span>Animales en adopci�n</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				PROTECTORAS</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}protectoras/protectorasDisponibles"> <i
					class="fas fa-home"></i> <span>Protectoras disponibles</span>
			</a></li>

			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">MIS
				GESTIONES</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}favoritos/listaAnimalesFavoritos"> <i
					class="fas fa-heart"></i> <span>Mis favoritos</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}gestiones/listaAdopciones"> <i
					class="fas fa-home"></i> <span>Mis adopciones</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}gestiones/listaApadrinamientos"> <i
					class="fas fa-child"></i> <span>Mis apadrinamientos</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}gestiones/listaDonaciones"> <i
					class="fas fa-donate"></i> <span>Donaciones</span>
			</a></li>

		</ul>
	</div>
</sec:authorize>


<!-- Menu para protectoras -->
<sec:authorize access="hasAnyAuthority('PROTECTORA')">
	<div style="text-align: center" class="pt-5 sidebar navbar-nav">
		<img style="height: 190px; width: 200px; border: 2px solid white;"
			class="img-fluid img-profile rounded-circle mx-auto mb-2 img-responsive"
			src=${urlPublic}/images/protectoras/${usr.imagen} alt="Imagen Perfil">
		<ul style="list-style: none">

			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}index/welcome"> <i class="fas fa-dog"></i> <span>Inicio</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">AREA
				PERSONAL</div>
			<hr style="background-color: #708090;">
			<li class="nav-item active"><a class="nav-link"
				href="${urlRoot}miPerfil/detalle"> <i class="fas fa-user"></i> <span>Mi
						perfil</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				ANIMALES</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link" href="${urlRoot}">
					<i class="fas fa-paw"></i> <span>Animales en adopci�n</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}animales/crear"> <i class="fas fa-bone "></i> <span>Nuevo
						animal</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}animales/listaAnimales"> <i
					class="fas fa-list-ul "></i> <span>Listado de animales</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				PROTECTORAS</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}protectoras/protectorasDisponibles"> <i
					class="fas fa-home"></i> <span>Protectoras disponibles</span>
			</a></li>
		</ul>
	</div>
</sec:authorize>

<!-- Menu para administrador-->
<sec:authorize access="hasAnyAuthority('ADMINISTRADOR')">
	<div style="text-align: center" class="pt-5 sidebar navbar-nav">
		<img style="height: 190px; width: 200px; border: 2px solid white;"
			class="img-fluid img-profile rounded-circle mx-auto mb-2 img-responsive"
			src=${urlPublic}/images/usuarios/${usr.imagen} alt="Imagen Perfil">
		<ul style="list-style: none">

			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}index/welcome"> <i class="fas fa-dog"></i> <span>Inicio</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">AREA
				PERSONAL</div>
			<hr style="background-color: #708090;">
			<li class="nav-item active"><a class="nav-link"
				href="${urlRoot}miPerfil/detalle"> <i class="fas fa-user"></i> <span>Mi
						perfil</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				USUARIOS</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}usuarios/listaUsuarios"> <i class="fas fa-user"></i>
					<span>Gesti�n de usuarios</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				ANIMALES</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link" href="${urlRoot}">
					<i class="fas fa-paw"></i> <span>Animales en adopci�n</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}animales/crear"> <i class="fas fa-bone "></i> <span>Nuevo
						animal</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}animales/listaAnimales"> <i
					class="fas fa-list-ul "></i> <span>Listado de animales</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				PROTECTORAS</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}protectoras/protectorasDisponibles"> <i
					class="fas fa-home"></i> <span>Protectoras disponibles</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}protectoras/crear"> <i class="fas fa-plus "></i>
					<span>Nueva protectora</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}protectoras/listaProtectoras"> <i
					class="fas fa-hospital-alt"></i> <span>Listado de
						Protectoras</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				NOTICIAS</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}noticias/crear"> <i class="fas fa-book"></i> <span>Nueva
						noticia</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}noticias/listaNoticias"> <i
					class="fas fa-list-ul"></i> <span>Listado de noticias</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				BANNERS</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}banners/crear"> <i class="fas fa-images"></i> <span>Gesti�n
						de banners</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}banners/listaBanners"> <i class="fas fa-list-ul"></i>
					<span>Listado de banners</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				GESTI�N</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}favoritos/listaAnimalesFavoritos"> <i
					class="fas fa-heart"></i> <span>Mis favoritos</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}gestiones/listaAdopciones"> <i
					class="fas fa-home"></i> <span>Mis adopciones</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}gestiones/listaApadrinamientos"> <i
					class="fas fa-child"></i> <span>Mis apadrinamientos</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}gestiones/listaDonaciones"> <i
					class="fas fa-donate"></i> <span>Donaciones</span>
			</a></li>
			<hr style="background-color: #708090;">
			<div
				style="padding-left: 20px; text-align: left; background-color: black; color: white;">SECCI�N
				CONTACTO</div>
			<hr style="background-color: #708090;">
			<li class="nav-item"><a class="nav-link"
				href="${urlRoot}contacto/listaContactos"> <i
					class="fas fa-envelope"></i> <span>Mensajes recibidos</span>
			</a></li>
		</ul>
	</div>
</sec:authorize>