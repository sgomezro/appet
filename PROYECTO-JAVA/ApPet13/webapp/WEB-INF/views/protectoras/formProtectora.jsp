<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;">
<title>ApPet - Protectora</title>
<!-- Tag de Spring para agregar recursos estaticos -->
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/protectoras/guardar" var="urlForm"></spring:url>
<!-- Favicon -->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<!-- DatePicker script -->
<script>
	$(document).ready(
			function() {
				var date_input = $('input[name="fechaNacimiento"]'); //our date input has the name "date"
				var container = $('.bootstrap-iso form').length > 0 ? $(
						'.bootstrap-iso form').parent() : "body";
				var options = {
					format : 'dd-mm-yyyy',
					container : container,
					todayHighlight : true,
					autoclose : true,
				};
				date_input.datepicker(options);
			})
</script>
</head>

<body id="page-top" onload="desabilitarInput()">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/banners/banner17.jpg)">

				<h1 class="display-3" style="color: white">Protectora</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<div class="text-center">
			<hr>
			<h3 class="blog-title">
				<img style="margin-bottom: 15px; width: 60px; height: 40px;" class="card-img-top mr-2"
								src=${urlPublic}/images/ApPetHuella.JPG title="huella"> <span class="label label-success">Datos de la protectora</span> <img style="margin-bottom: 15px; width: 60px; height: 40px;" class="card-img-top mr-2"
								src=${urlPublic}/images/ApPetHuella.JPG title="huella">
			</h3>
			<hr>
			</div>
			<spring:hasBindErrors name="protectora">
				<div class='alert alert-danger' role='alert'>
					Por favor corrija los siguientes errores:
					<ul>
						<c:forEach var="error" items="${errors.allErrors}">
							<li><spring:message message="${error}" /></li>
						</c:forEach>
					</ul>
				</div>
			</spring:hasBindErrors>
			<div class="container pt-3" style="background-color: #f7f7f7">
			<form:form action="${urlForm}" method="post"
				enctype="multipart/form-data" modelAttribute="protectora">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="nombreProtectora">Nombre:</label>
							<form:hidden path="idProtectora" />
							<form:input placeholder="Nombre de la protectora" type="text"
								class="form-control" path="nombreProtectora"
								id="nombreProtectora" required="required" />
						
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="nombre">Nombre del
								responsable de la protectora:</label>
							<form:input
								placeholder="Nombre del responsable de la protectora:"
								type="text" class="form-control" path="nombre" id="nombre"
								required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="apellidos">Apellidos del
								responsable de la protectora:</label>
							<form:input
								placeholder="Apellidos del responsable de la protectora:"
								type="text" class="form-control" path="apellidos" id="apellidos"
								required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="email">Email:</label>
							<form:input placeholder="Email de la protectora" type="text"
								class="form-control" path="email" id="email" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="password">Contrase�a:</label>
							<form:input placeholder="Contrase�a de la protectora" type="password"
								class="form-control" path="password" id="password"
								required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="comprobarPass">Repita la
								contrase�a:</label>
							<form:input placeholder="Repita la contrase�a de la protectora"
								type="password" class="form-control" path="comprobarPass"
								id="comprobarPass" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<label class="badgetForm" for="fechaNacimiento">Fecha de
							fundaci�n de la protectora:</label>
						<div class="form-group has-feedback">
							<i class="far fa-calendar form-control-feedback"></i>
							<form:input required="required" type="text"
								class="form-controlIcon" id="fechaNacimiento"
								path="fechaNacimiento" placeholder="dd-mm-yyyy" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="telefonoProtectora">Tel�fono:</label>
							<form:input placeholder="Telefono de la protectora" type="text"
								class="form-control" path="telefonoProtectora"
								id="telefonoProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="localidad">Localidad:</label>
							<form:select placeholder="Localidad de la protectora."
								class="form-control" path="localidad" required="required"
								items="${localidades }" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="direccionProtectora">Direcci�n:</label>
							<form:input placeholder="Direcci�n de la protectora" type="text"
								class="form-control" path="direccionProtectora"
								id="direccionProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="coordenadasProtectora">Coordenadas:</label>
							A�ada su posici�n si desea que aparezca un mapa de situacion:
							<form:input placeholder="Latitud de la protectora" type="text"
								class="form-control mt-3" path="latitudProtectora"
								id="latitudProtectora" />

							<form:input placeholder="Altitud de la protectora" type="text"
								class="form-control" path="altitudProtectora"
								id="altitudProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="descripcionProtectora">Descripci�n
								de la protectora</label>
							<form:textarea class="form-control" path="descripcionProtectora"
								id="descripcionProtectora" rows="10"></form:textarea>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm"
								for="descripcionProcedimientoProtectora">Descripci�n del
								procedimiento de adopci�n de la protectora</label>
							<form:textarea class="form-control"
								path="descripcionProcedimientoProtectora"
								id="descripcionProcedimientoProtectora" rows="10"></form:textarea>
						</div>
					</div>
					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="logoProtectora">Logo de la
								protectora:</label>
							<form:hidden path="logoProtectora" />
							<input type="file" id="archivoImagenLogo"
								name="archivoImagenLogo">
						</div>
						Logo actual de la protectora:
						<div class="form-group mt-3">
							<img style="width: 130px; height: 100px;" class="card-img-top"
								src=${urlPublic}/images/protectoras/${protectora.logoProtectora} title="Imagen actual de la protectora">

						</div>
						

					</div>
					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="imagenProtectora">Imagen:</label>
							<form:hidden path="imagenProtectora" />
							<input type="file" id="archivoImagen" name="archivoImagen">
						</div>
					</div>

					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="urlVideoProtectora">URL
								del video (youtube)</label>
							<form:input placeholder="URL del video (youtube)" type="text"
								class="form-control" path="urlVideoProtectora"
								id="urlVideoProtectora" required="required" />
						</div>
					</div>
					
					<c:choose>
					<c:when test="${prf.perfil =='ADMINISTRADOR'}">
					
						<div class="wrap-input100 validate-input"
						data-validate="El estado es requerido">
						<label class="badgetForm" for="estado">Estado de la cuenta:</label>
						<form:select id="estado" class="form-control" path="estado"
							placeholder="Estado">
							<option value="Activa">Activa</option>
							<option value="Inactiva">Inactiva</option>
						</form:select>
						<span class="focus-input100"></span>
					</div>
					</c:when>
					</c:choose>
				</div>
				<div class="container" style="text-align: right">

					<button type="submit" class="btn btn-info"
						style="margin-top: 50px; margin-bottom: 50px;">Guardar</button>
				</div>
			</form:form>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>

<script>

</script>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
	<script src="${urlPublic}/js/disableinput.js"></script>
	<script>
		// Configuracion de la barra de heramientas
		// https://www.tinymce.com/docs/get-started/basic-setup/
		tinymce
				.init({
					selector : '#descripcionProtectora, #descripcionProcedimientoProtectora',
					plugins : "textcolor, table lists code",
					toolbar : " undo redo | bold italic | alignleft aligncenter alignright alignjustify \n\
                    | bullist numlist outdent indent | forecolor backcolor table code"
				});
	</script>
</body>