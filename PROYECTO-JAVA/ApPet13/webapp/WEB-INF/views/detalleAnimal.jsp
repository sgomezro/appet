<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Detalle animal</title>

<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/favoritos/agregar" var="urlFavoritos"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
</head>

<body id="page-top">
	<!-- Modales -->
	<jsp:include page="includes/modales/modalAdopcion.jsp" />
	<!-- Menu superior -->
	<jsp:include page="includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="includes/menuLateral.jsp" />
		<!-- Page Content -->


		<!-- Page Content -->
		<div class="container">

			<div class="row" style="margin-top: 70px; margin-bottom: 50px">

				<!-- Post Content Column -->

				<div class="col-lg-8">
					<!-- Title -->
					<h3>Detalle del animal</h3>

					<!-- Author -->
					<p class="lead">
						<strong>${animal.nombre}: <a
							href="detalleProtectora?idProtectora=${animal.protectora.idProtectora}&idAnimal=${animal.id }">
								${animal.protectora.nombreProtectora}</a></strong>
					</p>

					<hr>
					<div
						style="box-shadow: 1px 1px 10px #999; text-align: center; padding-top: 10px; padding-bottom: 10px; background-color: #F3EDE8">
						<a style="text-decoration: none; color: black" href="#video">
							<p>�Quieres saber un poco mas sobre mi?</p>
							<h3 style="text-align: center">Conoce mejor a
								${animal.nombre}!</h3>
						</a>
					</div>
					<hr>

					<!-- Container for the image gallery -->


					<div class="container">
						<div class="mySlides">
							<div class="numbertext">1 / 6</div>
							<img src="${urlPublic}/images/animales/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">2 / 6</div>
							<img src="${urlPublic}/images/animales/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">3 / 6</div>
							<img src="${urlPublic}/images/animales/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">4 / 6</div>
							<img src="${urlPublic}/images/animales/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">5 / 6</div>
							<img src="${urlPublic}/images/animales/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">6 / 6</div>
							<img src="${urlPublic}/images/animales/${animal.imagen}"
								style="width: 100%">
						</div>


						<a class="prev" onclick="plusSlides(-1)">&#60;</a> <a class="next"
							onclick="plusSlides(1)">&#62;</a>

						<div class="caption-container">
							<p id="caption"></p>
						</div>

						<div class="row" style="margin-left: 0px; margin-right: 0px">
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/animales/${animal.imagen}"
									style="width: 100%" onclick="currentSlide(1)" alt="Imagen 1">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/animales/${animal.imagen}"
									style="width: 100%" onclick="currentSlide(2)" alt="Imagen 2">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/animales/${animal.imagen}"
									style="width: 100%" onclick="currentSlide(3)" alt="Imagen 3">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/animales/${animal.imagen}"
									style="width: 100%" onclick="currentSlide(4)" alt="Imagen 4">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/animales/${animal.imagen}"
									style="width: 100%" onclick="currentSlide(5)" alt="Imagen 5 ">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/animales/${animal.imagen}"
									style="width: 100%" onclick="currentSlide(6)" alt="Imagen 6">
							</div>
						</div>
					</div>
					<hr>

					<!-- Post Content -->
					<h3 class="mt-4" id="descripcion" style="margin-bottom: 15px">Descripci�n</h3>
					<p class="lead">${animal.descripcion}</p>
					<hr>
					<c:choose>
						<c:when test="${animal.urlVideo !='Sin video'}">
							<h3 class="mt-4" style="margin-bottom: 15px">As� soy yo:</h3>
							<!-- 16:9 aspect ratio -->
							<div id="video" class="embed-responsive embed-responsive-16by9"
								style="margin-bottom: 35px">
								<iframe class="embed-responsive-item" src="${animal.urlVideo}"></iframe>
							</div>
						</c:when>

					</c:choose>
				</div>

				<!-- Sidebar Widgets Column -->
				<div class="col-md-4">

					<!-- Categories Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							de ${animal.nombre}</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Tipolog�a:</strong> ${animal.tipo}</a></li>
										<li><a><strong>Raza:</strong> ${animal.raza}</a></li>
										<li><a><strong>Sexo:</strong> ${animal.sexo}</a></li>
										<li><a><strong>Fecha de nacimiento:</strong> <fmt:formatDate
													pattern="dd-MM-yyyy" value="${animal.fechaNacimiento}" /></a></li>
										<li><a><strong>Edad:</strong> ${animal.edad} <span>a�os</span></a></li>
										<li><a><strong>�Tiene microchip?:</strong>
												${animal.tieneMicrochip}</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<!-- Side Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Comportamiento</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a>${animal.comportamiento}</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- Side Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							de la protectora</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Nombre: </strong>${animal.protectora.nombreProtectora}</a>
										</li>
										<li><a><strong>Localidad: </strong>${animal.protectora.localidad}</a></li>
									</ul>
									<a
										href="detalleProtectora?idProtectora=${animal.protectora.idProtectora}&idAnimal=${animal.id }">
										<button type="button" style="margin-top: 10px;"
											class="btn btn-success">M�s informaci�n sobre la
											protectora</button>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Side Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							de adopciones</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Estado:</strong> ${animal.estado}</a></li>
										<li><a><strong>Envio:</strong> ${animal.envio}</a></li>
										<li><a><strong>Tasa de adopci�n:</strong>
												${animal.tasaAdopcion} euros</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<c:choose>
						<c:when
							test="${(prf.perfil == 'ADMINISTRADOR') or (prf.perfil == 'PROTECTORA-NA')or (prf.perfil == 'USUARIO') }">
							<p>
								�Quieres adoptar a ${animal.nombre}? <strong>Haz click!</strong>
							</p>
							<button type="button" class="btn btn-primary" data-toggle="modal"
								data-target="#ModalAdopcion">Ad�ptame</button>
							<p></p>
							<hr>
							<p>
								�Quieres ayudar a ${animal.nombre} pero no puedes adoptarlo? <strong>Apadrinalo!</strong>
							</p>
							<button type="button" class="btn btn-info" data-toggle="modal"
								data-target="#ModalAdopcion">Apadriname</button>
							<hr>
						</c:when>
						<c:when
							test="${(prf.perfil == 'PROTECTORA') or (prf.perfil == 'PROTECTORA-NA')or (prf.perfil == 'USUARIO') }">
						</c:when>
						<c:otherwise>
							<p>
								Registrate para<strong> ADOPTAR, APADRINAR </strong>y poder
								acceder a todas las funcionalidades de<strong> ApPet!</strong>
							</p>
							<a href="formLogin" style="color: white !important"
								class="btn btn-success" role="button" title="A�adir"><span>Registrate
									en ApPet!</span></a>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${animalEnFavorito eq true}">
							<p>
								${animal.nombre}<strong> se encuentra en su lista de
									animales favoritos</strong>
							</p>
							<i class="fa fa-heart ml-2 mr-5 align-middle"
								style="font-size: 30px; color: red"></i>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when
									test="${(prf.perfil == 'ADMINISTRADOR') or (prf.perfil == 'PROTECTORA-NA')or (prf.perfil == 'USUARIO') }">
									<p>
										�Quieres a�adir a ${animal.nombre} a tu <strong>
											lista de favoritos?</strong>
									</p>
									<a
										href="${urlFavoritos}?idAnimal=${animal.id}&username=${usr.username}"
										style="color: white !important" class="btn btn-success"
										role="button" title="A�adir"><span>A�adir a
											favoritos</span></a>
								</c:when>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="includes/footer.jsp"></jsp:include>

	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/jquery/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!--JS -->
	<script type="text/javascript" src="${urlPublic}/js/slider.js"></script>