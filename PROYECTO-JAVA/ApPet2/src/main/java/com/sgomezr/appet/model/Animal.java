package com.sgomezr.appet.model;

import java.util.Date;

public class Animal {
	private int id;
	private String nombre;
	private String tipo;
	private String raza;
	private String sexo;
	private Date fechaNacimiento;
	private int edad;
	private String imagen = "animal.jpg"; //Imagen por defecto
	private String protectora;
	private String tieneMicrochip;
	private String descripcion;
	private Date fechaPublicacion;
	private String estado = "En adopci�n"; //Por defecto en adopci�n
	private String comportamiento;
	private String envio;
	private int tasaAdopcion;
	
	/**
	 * Constructor de la clase
	 */
	public Animal(){		
		this.fechaPublicacion = new Date(); // Por default, la fecha del sistema
	}
	public int getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getProtectora() {
		return protectora;
	}
	public void setProtectora(String protectora) {
		this.protectora = protectora;
	}
	public String getTieneMicrochip() {
		return tieneMicrochip;
	}
	public void setTieneMicrochip(String tieneMicrochip) {
		this.tieneMicrochip = tieneMicrochip;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getComportamiento() {
		return comportamiento;
	}
	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}
	public String getEnvio() {
		return envio;
	}
	public void setEnvio(String envio) {
		this.envio = envio;
	}
	public int getTasaAdopcion() {
		return tasaAdopcion;
	}
	public void setTasaAdopcion(int tasaAdopcion) {
		this.tasaAdopcion = tasaAdopcion;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Animal [id=" + id + ", nombre=" + nombre + ", tipo=" + tipo + ", raza=" + raza + ", sexo=" + sexo
				+ ", fechaNacimiento=" + fechaNacimiento + ", edad=" + edad + ", imagen=" + imagen + ", protectora="
				+ protectora + ", tieneMicrochip=" + tieneMicrochip + ", descripcion=" + descripcion
				+ ", fechaPublicacion=" + fechaPublicacion + ", estado=" + estado + ", comportamiento=" + comportamiento
				+ ", envio=" + envio + ", tasaAdopcion=" + tasaAdopcion + "]";
	}
	
	
}
