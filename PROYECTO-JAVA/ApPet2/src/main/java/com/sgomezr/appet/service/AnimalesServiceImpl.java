package com.sgomezr.appet.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Animal;

@Service
public class AnimalesServiceImpl implements IAnimalesService {

	private List<Animal> listaAnimales = null;
	
	public  AnimalesServiceImpl() {
		System.out.println("Creando una instancia de la clase AnimalesServiceImpl");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			listaAnimales = new LinkedList<>();
			Animal animal1 = new Animal();
			animal1.setId(1);
			animal1.setNombre("Sayker");
			animal1.setTipo("Perro");
			animal1.setRaza("Labrador Retreier");
			animal1.setSexo("Macho");
			animal1.setFechaNacimiento(formatter.parse("11-09-2001"));
			animal1.setEdad(15);
			animal1.setImagen("Perros012.jpg"); 
			animal1.setProtectora("Aguilar de la Frontera");
			animal1.setTieneMicrochip("Si");
			animal1.setDescripcion("Un perrito muy bonito");
			animal1.setFechaPublicacion(formatter.parse("16-09-2018"));
			animal1.setEstado("En adopcion");
			animal1.setComportamiento("Muy bueno");
			animal1.setEnvio("A Burgos");
			animal1.setTasaAdopcion(10000000);
			
			Animal animal2 = new Animal();
			animal2.setId(2);
			animal2.setNombre("Rabin");
			animal2.setTipo("Perro");
			animal2.setRaza("Sin Datos");
			animal2.setSexo("Macho");
			animal2.setFechaNacimiento(formatter.parse("03-06-2011"));
			animal2.setEdad(10);
			animal2.setImagen("Perros013.jpg");
			animal2.setProtectora("Finestrat");
			animal2.setTieneMicrochip("Si");
			animal2.setDescripcion("Se trata de un perro muy cari�oso y amigable. Ha tenido una infncia muy dura, sus primeros due�os"
					+ "lo abandonaron pero esta dispuesto a rehacer su vida, con una familia que lo cuide para siempre.");
			animal2.setFechaPublicacion(formatter.parse("16-09-2018"));
			animal2.setEstado("Adoptado");
			animal2.setComportamiento("Muy bueno");
			animal2.setEnvio("A Burgos");
			animal2.setTasaAdopcion(10000);

			Animal animal3 = new Animal();
			animal3.setId(3);
			animal3.setNombre("Lolita");
			animal3.setTipo("Tortuga");
			animal3.setRaza("Tortuga enana");
			animal3.setSexo("Hembra");
			animal3.setFechaNacimiento(formatter.parse("12-06-2012"));
			animal3.setEdad(6);
			animal3.setImagen("Tortugas001.jpg");
			animal3.setProtectora("Burgos");
			animal3.setTieneMicrochip("Si");
			animal3.setDescripcion("Una tortuga muy bonita");
			animal3.setFechaPublicacion(formatter.parse("16-09-2018"));
			animal3.setEstado("Apadrinado");
			animal3.setComportamiento("A veces muerde");
			animal3.setEnvio("A Burgos");
			animal3.setTasaAdopcion(10);
			
			Animal animal4 = new Animal();
			animal4.setId(4);
			animal4.setNombre("Miau Miau");
			animal4.setTipo("Gato");
			animal4.setRaza("Sin identificar");
			animal4.setSexo("Hembra");
			animal4.setFechaNacimiento(formatter.parse("12-06-2012"));
			animal4.setEdad(6);
			animal4.setImagen("Gatos001.jpg");
			animal4.setProtectora("Aguilar de la Frontera");
			animal4.setTieneMicrochip("No");
			animal4.setDescripcion("Una gatita muy bonita");
			animal4.setFechaPublicacion(formatter.parse("16-09-2018"));
			animal4.setEstado("Apadrinado");
			animal4.setComportamiento("A veces muerde");
			animal4.setEnvio("A Burgos");
			animal4.setTasaAdopcion(10);
			
			Animal animal5 = new Animal();
			animal5.setId(5);
			animal5.setNombre("Pepa");
			animal5.setTipo("Perro");
			animal5.setRaza("Sin identificar");
			animal5.setSexo("Hembra");
			animal5.setFechaNacimiento(formatter.parse("12-06-2012"));
			animal5.setEdad(6);
			animal5.setImagen("Perros011.jpg");
			animal5.setProtectora("Benidorm");
			animal5.setTieneMicrochip("No");
			animal5.setDescripcion("Una perrita muy bonita");
			animal5.setFechaPublicacion(formatter.parse("16-09-2018"));
			animal5.setEstado("En adopcion");
			animal5.setComportamiento("A veces muerde");
			animal5.setEnvio("A Burgos");
			animal5.setTasaAdopcion(10);
			
			Animal animal6 = new Animal();
			animal6.setId(6);
			animal6.setNombre("Moro");
			animal6.setTipo("Perro");
			animal6.setRaza("Pastor belga");
			animal6.setSexo("Macho");
			animal6.setFechaNacimiento(formatter.parse("12-06-2012"));
			animal6.setEdad(6);
			animal6.setImagen("Perros007.jpg");
			animal6.setProtectora("Burgos");
			animal6.setTieneMicrochip("No");
			animal6.setDescripcion("Se trata de un perro muy cari�oso y amigable. Ha tenido una infancia muy dura, sus primeros due�os"
					+ " lo abandonaron pero esta dispuesto a rehacer su vida, con una familia que lo cuide para siempre.");
			animal6.setFechaPublicacion(formatter.parse("16-09-2018"));
			animal6.setEstado("En adopcion");
			animal6.setComportamiento("A veces muerde");
			animal6.setEnvio("A Burgos");
			animal6.setTasaAdopcion(10);

			// Agregamos los objetos Animal a la lista
			listaAnimales.add(animal1);
			listaAnimales.add(animal2);
			listaAnimales.add(animal3);
			listaAnimales.add(animal4);
			listaAnimales.add(animal5);
			listaAnimales.add(animal6);
			
		} catch (ParseException e) {
			System.out.println("Error: " + e.getMessage());	
		}
	}
	
	@Override
	public List<Animal> buscarTodosLosAnimales() {
		return listaAnimales;
	}

	@Override
	public Animal buscarAnimalPorId(int idAnimal) {
		for(Animal animal : listaAnimales) {
			if(animal.getId() == idAnimal) {
				return animal;
			}
		}
		return null;
	}

	@Override
	public List<Animal> buscarAnimalPorProtectora(String protectoraAnimal) {
		List<Animal> listaAnimalesPorProtectora = new LinkedList<>();
		for(Animal animal : listaAnimales) {
			if(animal.getProtectora().equals(protectoraAnimal)) {
				listaAnimalesPorProtectora.add(animal);
			}
		}
		return listaAnimalesPorProtectora;
	}

	@Override
	public void insertar(Animal animal) {
		listaAnimales.add(animal);
		
		
	}

}
