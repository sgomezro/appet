package com.sgomezr.appet.model;


public class Protectora {
	
	String nombre;

	public Protectora() {
	}

	public Protectora(String nombre) {
		this.nombre = nombre;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
