package com.sgomezr.appet.service;

import java.util.List;


import com.sgomezr.appet.model.Noticia;

public interface INoticiasService {

	void guardarNoticia (Noticia noticia);
	void insertarNoticia(Noticia noticia); 
	List<Noticia> buscarTodasLasNoticias();
}
