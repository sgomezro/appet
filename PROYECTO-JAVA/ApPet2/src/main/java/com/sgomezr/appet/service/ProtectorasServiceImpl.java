package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Protectora;

@Service
public class ProtectorasServiceImpl implements IProtectorasService {
	
	private List<String> listaProtectoras = null;
	
	public  ProtectorasServiceImpl() {
		
		System.out.println("Creando una instancia de la clase ProtectorasServiceImpl");
		
		listaProtectoras = new LinkedList<>();

		Protectora protectora1 = new Protectora();
		protectora1.setNombre("Burgos");
	
		Protectora protectora2 = new Protectora();
		protectora2.setNombre("Aguilar de la Frontera");
	
		Protectora protectora3 = new Protectora();
		protectora3.setNombre("Benidorm");
	
		Protectora protectora4 = new Protectora();
		protectora4.setNombre("Finestrat");

		
		// Agregamos los objetos Protectora a la lista
		listaProtectoras.add(protectora1.getNombre());
		listaProtectoras.add(protectora2.getNombre());
		listaProtectoras.add(protectora3.getNombre());
		listaProtectoras.add(protectora4.getNombre());

	}

	@Override
	public List<String> buscarTodasLasProtectoras() {

		return listaProtectoras;
	}
	

}
