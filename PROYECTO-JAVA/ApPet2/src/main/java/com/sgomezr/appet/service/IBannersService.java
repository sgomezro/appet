package com.sgomezr.appet.service;

import java.util.List;
import com.sgomezr.appet.model.Banner;

public interface IBannersService {

	void insertar(Banner banner); 
	List<Banner> buscarTodos();
	
}
