package com.sgomezr.appet.service;

import java.util.List;


public interface IProtectorasService {

	List<String> buscarTodasLasProtectoras();
}
