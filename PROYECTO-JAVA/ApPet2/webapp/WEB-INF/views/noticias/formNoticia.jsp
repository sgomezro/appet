<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Creacion de Noticias</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/noticias/guardar" var="urlForm"></spring:url>
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
	<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
</head>
<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />

		<div class="container">

			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/Banner2.jpg)">

			<h1 class="display-3" style="color: white">Noticias</h1>
			<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">
				<span class="label label-success">Datos de la Noticia</span>
			</h3>


			<spring:hasBindErrors name="noticia">
				<div class='alert alert-danger' role='alert'>
				Por favor corrija los siguientes errores:
				<ul>
					<c:forEach var="error" items="${errors.allErrors}">
						<li><spring:message message="${error}" /></li>
					</c:forEach>
				</ul>
				</div>
			</spring:hasBindErrors>
			<form action="${urlForm}" method="post"  enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="badgetForm" for="titulo">Titulo:</label> <input type="text"
								class="form-control" name="titulo" id="titulo"
								required="required" />
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label class="badgetForm" for="estado">Estatus:</label> <select id="estado"
								name="estado" class="form-control">
								<option value="Activa">Activa</option>
								<option value="Inactiva">Inactiva</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="detalle">Descripción de la noticia:</label>
							<textarea class="form-control" name="detalle" id="detalle"
								rows="10"></textarea>
						</div>
					</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="badgetForm" for="imagen">Imagen:</label><input
									type="file" id="archivoImagen" name="archivoImagen">
							</div>
						</div>
				</div>

				<button type="submit" class="btn btn-primary" style="background-color:#0F1626; margin-top: 30px;margin-bottom: 50px;" >Guardar</button>
			</form>
		</div>
		<!-- /container -->
	</div>
	
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script
		src="https://ajax.googleapis.com/Ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
	<script>
		// Configuracion de la barra de heramientas
		// https://www.tinymce.com/docs/get-started/basic-setup/
		tinymce
				.init({
					selector : '#detalle',
					plugins : "textcolor, table lists code",
					toolbar : " undo redo | bold italic | alignleft aligncenter alignright alignjustify \n\
                    | bullist numlist outdent indent | forecolor backcolor table code"
				});
	</script>
</body>
</html>