<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Listado de banners disponibles</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/banners/crear" var="urlCreate"></spring:url>
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />

		<div class="container" style="margin-bottom: 40px;">

			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/Banner10.jpg)">

			<h1 class="display-3" style="color: white">Listado de <br/> banners</h1>
			<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">Banners disponibles</h3>
			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>

			<a href="${urlCreate}" style="background-color: #0F1626;"
				class="btn btn-success" role="button" title="Nuevo banner">Nuevo
				banner</a><br>
			<br>

		  <div class="table-responsive">
        <table class="table table-hover table-striped table-bordered">
            <tr>
                <th>Id</th>
                <th>Titulo</th>                           
                <th>Fecha Publicacion</th>              
                <th>Nombre Archivo</th>
                <th>Estado</th>
                <th>Opciones</th>              
            </tr>
            
            <c:forEach items="${banners}" var="banner">            
	            <tr>
	                <td>${banner.id}</td>
	                <td>${banner.titulo}</td>
	                 <td><fmt:formatDate pattern="dd-MM-yyyy" value="${banner.fecha}" /></td>    
	                <td>${banner.archivo}</td>                         	                        
                   <c:choose>
								<c:when test="${banner.estado eq 'Activo'}">
									<td><span style="color: #8FC33A; font-weight: 600;" class="label label-success">${banner.estado}</span></td>
								</c:when>
								<c:otherwise>
									<td><span style="color: #E24E42; font-weight: 600;" class="label label-danger">${banner.estado}</span></td>
								</c:otherwise>
						 </c:choose>	                
	             
	                <td>
	                    <a href="#" class="btn btn-success btn-sm" role="button" title="Edit" ><span class="fas fa-pencil-alt"></span></a>
	                    <a href="#" class="btn btn-danger btn-sm" role="button" title="Eliminar" ><span class="fas fa-trash-alt"></span></a>
	                </td>
            	</tr>
            </c:forEach>
                 
        </table>
      </div>

		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>

</body>
</html>