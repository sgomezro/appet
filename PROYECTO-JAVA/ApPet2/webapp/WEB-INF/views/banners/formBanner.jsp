<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Nuevo banner</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/banners/guardar" var="urlForm"></spring:url>
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />

		<div class="container">

			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/Banner8.jpg)">

			<h1 class="display-3" style="color: white">Nuevo <br/> banner</h1>
			<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">
				<span class="label label-success">Datos del banner</span>
			</h3>

			<spring:hasBindErrors name="banner">
				<div class='alert alert-danger' role='alert'>
				Por favor corrija los siguientes errores:
				<ul>
					<c:forEach var="error" items="${errors.allErrors}">
						<li><spring:message message="${error}" /></li>
					</c:forEach>
				</ul>
				</div>
			</spring:hasBindErrors>

				<form action="${urlForm}" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label class="badgetForm" for="nombre">Titulo:</label> <input
									placeholder="Titulo del banner" type="text"
									class="form-control" name="titulo" id="titulo"
									required="required" />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="badgetForm" for="imagen">Imagen:</label><input
									type="file" id="archivoImagen" name="archivoImagen" required="required">
							</div>
						</div>
							<div class="col-sm-4">
							<div class="form-group">
								<label class="badgetForm" for="estado">Estado:</label> <select
									id="estado" name="estado" class="form-control">
									<option value="Activo">Activo</option>
									<option value="Inactivo">Inactivo</option>
								</select>
							</div>
						</div>
					</div>
					<div class="container" style="text-align: right">
						<button type="submit" class="btn btn-primary"
							style="background-color: #0F1626; margin-top: 50px; margin-bottom: 50px;">Guardar</button>
					</div>
				</form>
		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>