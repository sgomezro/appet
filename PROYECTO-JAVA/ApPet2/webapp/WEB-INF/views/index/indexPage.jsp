<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Creacion de Noticias</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />

		<div class="container">

			<!-- Jumbotron Header -->

			<header class="jumbotron my-0" style="background-color:white">
			<h1 class="display-3" style="color: black">Bienvenido a ApPet!</h1>
			<h3 class="lead" style="font-size: 2.25rem !important; color: black">Mejora
				su vida, mejora tu vida!</h3>
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">

					<c:forEach items="${banners}" var="banner" varStatus="loop">
						<c:choose>
							<c:when test="${banner.estado eq 'Activo'}">
								<c:choose>
									<c:when test="${loop.index==0}">
										<li data-target="#myCarousel" data-slide-to="${loop.index}"
											class="active"></li>
									</c:when>
									<c:otherwise>
										<li data-target="#myCarousel" data-slide-to="${loop.index}"></li>
									</c:otherwise>
								</c:choose>
							</c:when>
						</c:choose>
					</c:forEach>
					<!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>-->

				</ol>
				<div class="carousel-inner"
					style="height: 320px; border-radius: .3rem;">

					<c:forEach items="${banners}" var="banner" varStatus="loop">
						<c:choose>
							<c:when test="${banner.estado eq 'Activo'}">
								<c:choose>
									<c:when test="${loop.index==0}">
										<div class="carousel-item active">
											<img src="${urlPublic}/images/${banner.archivo}"
												alt="${banner.titulo}" title="${banner.titulo}">
											<div class="carousel-caption"
												style="position: sticky !important">
												<h4>${banner.titulo}</h4>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="carousel-item">
											<img src="${urlPublic}/images/${banner.archivo}"
												alt="${banner.titulo}" title="${banner.titulo}">
											<div class="carousel-caption"
												style="position: sticky !important">
												<h4>${banner.titulo}</h4>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
							</c:when>
						</c:choose>
					</c:forEach>
					<!-- <div class="carousel-item active">
						<img class="d-block w-100" src="${urlPublic}/images/Banner9.jpg"
							alt="First slide">
						<div class="carousel-caption" style="position: sticky !important">
							<h3>Carousel 1</h3>
							<p>Text 1</p>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="${urlPublic}/images/Banner3.jpg"
							alt="Second slide">
						<div class="carousel-caption" style="position: sticky !important;">
							<h3>Carousel 2</h3>
							<p>Text 2</p>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="${urlPublic}/images/Banner10.jpg"
							alt="Third slide">
						<div class="carousel-caption" style="position: sticky !important">
							<h3>Carousel 3</h3>
							<p>Text 3</p>
						</div>
					</div>-->
				</div>

			</div>
			</header>
			<h1 class="my-4">
				ApPet <small>Ultimas noticias</small>
			</h1>
			<div class="separator"
				style="border: 0 solid #2F4F4F; clear: both; position: relative; z-index: 11; border-color: #2F4F4F; border-top-width: 3px; margin-top: 0px; margin-bottom: 71px; width: 100%; max-width: 415px;"></div>
			<div></div>
			<div class="row">
				<c:forEach items="${ noticias }" var="noticia">
					<c:choose>
						<c:when test="${noticia.estado eq 'Activa'}">
							<div class="col-sm-6 col-md-6 col-l-6"
								style="padding-top: 5px; padding-bottom: 60px">
								<div class="card h-100"
									style="border-width: 1px; border-color: #565656; padding-bottom: 15px;">

									<div class="card-text"
										style="margin: 15px; margin-bottom: 0px;">
										<h2>${noticia.titulo }</h6>
											<a href="#"><img class="card-img-top"
												style="height: 300px;"
												src=${urlPublic}/images/${noticia.imagen } alt=""></a>
											<p>${noticia.fechaPublicacion }</p>
											<p>${noticia.detalle}</p>
											<div style="text-align: right">
												<a href="#" class="btn-default"
													style="background-color: #76323f; color: white; padding-bottom: 2px; padding-top: 2px; border: 1px solid black; padding-left: 80px; padding-right: 80px;"><i
													class="fa-mouse-pointer fas button-icon-left"
													style="padding-right: 10px;"></i><span>M�S
														INFORMACI�N</span></a>
											</div>
									</div>
								</div>
							</div>
						</c:when>
					</c:choose>
				</c:forEach>
			</div>



		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>

</body>
</html>