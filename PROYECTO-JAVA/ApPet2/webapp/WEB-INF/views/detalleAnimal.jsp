<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Detalle del animal</title>
<spring:url value="/resources" var="urlPublic"></spring:url>
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
</head>
<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="includes/menuLateral.jsp" />

		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 row"
				style="margin-right: 0px; margin-left: 0px; margin-top: 30px">
				<!-- Heading Row -->

				<div class="col-lg-6 col-md-6 col-sm-12"
					style="padding-left: 0px; padding-right: 0px;">
					<img style="width: inherit;" class="img-fluid rounded"
						src="${urlPublic}/images/${animal.imagen}" alt="">
					<div class="col-lg-12 col-md-12"
						style="margin-top: 15px; padding-left: 0px; padding-right: 0px;text-align: center;">
						<img class="img-fluid rounded col-lg-3 col-md-3"
							style="max-width: 22%; padding-left: 0px; padding-right: 0px;"
							src="${urlPublic}/images/${animal.imagen}" alt=""> <img
							class="img-fluid rounded col-lg-3 col-md-3"
							style="max-width: 22%; padding-left: 0px; padding-right: 0px;"
							src="${urlPublic}/images/${animal.imagen}" alt=""> <img
							class="img-fluid rounded col-lg-3 col-md-3"
							style="max-width: 22%; padding-left: 0px; padding-right: 0px;"
							src="${urlPublic}/images/${animal.imagen}" alt=""> <img
							class="img-fluid rounded col-lg-3 col-md-3"
							style="max-width: 22%; padding-left: 0px; padding-right: 0px;"
							src="${urlPublic}/images/${animal.imagen}" alt="">
					</div>
				</div>
				<!-- /.col-lg-8 -->
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="row col-lg-12 col-md-12">
						<h3>${animal.nombre}</h3>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<label class="badgetForm" for="exampleFormControlSelect1">${animal.tipo}</label>
							<label class="badgetForm" for="exampleFormControlSelect1">${animal.sexo}</label>
							<p class="mt-0 mb-0">Raza:</p>
							<label class="badgetForm" for="exampleFormControlSelect1">${animal.raza}</label>
							<p class="mt-0 mb-0">Fecha de nacimiento:</p>
							<label class="badgetForm" for="exampleFormControlSelect1">${animal.fechaNacimiento}</label>
							<p class="mt-0 mb-0">�Tengo microchip?</p>
							<c:choose>
								<c:when test="${animal.tieneMicrochip=='Si'}">
									<label class="badgetForm" style="background-color: #8FC33A"
										for="exampleFormControlSelect1">${animal.tieneMicrochip}</label>
								</c:when>
								<c:otherwise>
									<label class="badgetForm" style="background-color: #E24E42"
										for="exampleFormControlSelect1">${animal.tieneMicrochip}</label>

								</c:otherwise>
							</c:choose>
							<p>Comportamiento:</p>
							<label class="badgetForm" for="exampleFormControlSelect1">${animal.comportamiento}</label>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">


							<p class="mt-0 mb-0">Estado</p>
							<c:choose>
								<c:when test="${animal.estado=='En adopcion'}">
									<label class="badgetForm" style="background-color: #8FC33A"
										for="exampleFormControlSelect1">${animal.estado}</label>
								</c:when>
								<c:when test="${animal.estado=='Adoptado'}">
									<label class="badgetForm" style="background-color: #E24E42"
										for="exampleFormControlSelect1">${animal.estado}</label>

								</c:when>
								<c:otherwise>
									<label class="badgetForm" style="background-color: #F7B733"
										for="exampleFormControlSelect1">${animal.estado}</label>

								</c:otherwise>
							</c:choose>
							<p class="mt-0 mb-0">Se envia a:</p>
							<label class="badgetForm" for="exampleFormControlSelect1">${animal.envio}</label>
							<p class="mt-0 mb-0">Tasa de adopci�n:</p>
							<label class="badgetForm" for="exampleFormControlSelect1">${animal.tasaAdopcion}
								euros</label>
						</div>
					</div>
				</div>
				<div class="container" style="text-align: right">

					<button type="submit" class="btn btn-primary"
						style="background-color: #0F1626">Adoptame</button>
				</div>

				<!-- /.row -->

				<!-- Call to Action Well -->
				<div class="card text-white  my-4 text-center col-lg-12"
					style="background-color: #D7CEC7">
					<div class="card-body" style="background-color: #0F1626">
						<p class="text-white m-0">${animal.descripcion}</p>
					</div>
				</div>


				<!-- Content Row -->
				<div class="row">
					<div class="col-md-4 mb-4">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title">Card One</h2>
								<p class="card-text">Lorem ipsum dolor sit amet, consectetur
									adipisicing elit. Rem magni quas ex numquam, maxime minus quam
									molestias corporis quod, ea minima accusamus.</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->
					<div class="col-md-4 mb-4">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title">Card Two</h2>
								<p class="card-text">Lorem ipsum dolor sit amet, consectetur
									adipisicing elit. Quod tenetur ex natus at dolorem enim!
									Nesciunt pariatur voluptatem sunt quam eaque, vel, non in id
									dolore voluptates quos eligendi labore.</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->
					<div class="col-md-4 mb-4">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title">Card Three</h2>
								<p class="card-text">Lorem ipsum dolor sit amet, consectetur
									adipisicing elit. Rem magni quas ex numquam, maxime minus quam
									molestias corporis quod, ea minima accusamus.</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->

				</div>
				<!-- /.row -->

			</div>

		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script
		src="https://ajax.googleapis.com/Ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>

</body>
</html>