package com.sgomezr.appet.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sgomez.appet.util.Utileria;
import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;

@Controller
public class HomeController {
	
	private Protectora protectora = new Protectora("protectora1");

	@RequestMapping(value = "/home", method=RequestMethod.GET)
	public String goHome() {
	return "home";
	}
	
	@RequestMapping(value = "/busqueda", method=RequestMethod.POST)
	public String buscarPorProtectora (@RequestParam ("protectora") String protectora, Model model) {
		List<String> protectoras = Utileria.getProtectoras();
		System.out.println(protectora);
		List<Animal> animales = Utileria.getListaAnimales();
		List<Animal> animalesPorProtectora = Utileria.getListaAnimalesPorProtectora(protectora);
		model.addAttribute("protectoras",protectoras);
		model.addAttribute("protectoraBusqueda", protectora);
		if(protectora.equals("Mostrar todos")) {
			model.addAttribute("animales", animales);
		
		}else {
		model.addAttribute("animales", animalesPorProtectora);
		}
		
		System.out.println("Buscando todos los animales de la protectora: " + protectora);
		return "home";
	}
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public String mostrarAnimales(Model model) {
		List<String> protectoras = Utileria.getProtectoras();

		List<Animal> animales = Utileria.getListaAnimales();
		model.addAttribute("protectoras",protectoras);
		model.addAttribute("protectoraBusqueda",protectora.getNombre());
		model.addAttribute("animales", animales);
		
	return "home";
	
	}
	//@RequestMapping(value = "/detalleAnimal/{id}/{protectora}", method=RequestMethod.GET)
	@RequestMapping(value = "/detalleAnimal", method=RequestMethod.GET)
	//public String mostrarDetalleAnimal(Model model, @PathVariable("id") int idAnimal,  @PathVariable("protectora") String protectora) {
	public String mostrarDetalleAnimal(Model model, @RequestParam("idAnimal") int idAnimal,   @RequestParam("protectora") String protectora) {	
		System.out.println("El id del Animal es: " + idAnimal);
		System.out.println("La protectora en la que se encuentra ese animal es: " + protectora );
		
		//TODO: Buscar en la base de datos la protectora asociada
		
		/*String nombreAnimal ="Sayker";
		String tipoAnimal ="Perro";
		String razaAnimal ="Labrador Retrevier";
		
		model.addAttribute("nombre", nombreAnimal);
		model.addAttribute("tipo", tipoAnimal);
		model.addAttribute("raza", razaAnimal);*/
		
	return "detalleAnimal";
	}
	
	// Metodo para generar una lista de Objetos de Modelo (Animal)
		/*private List<Animal> getListaAnimales() {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			List<Animal> listaAnimales = null;
			try {
				
				listaAnimales = new LinkedList<>();

				Animal animal1 = new Animal();
				animal1.setId(1);
				animal1.setNombre("Sayker");
				animal1.setTipo("Perro");
				animal1.setRaza("Labrador Retreier");
				animal1.setSexo("Macho");
				animal1.setFechaNacimiento(formatter.parse("11-09-2001"));
				animal1.setEdad(15);
				animal1.setImagen("Perros004.jpg"); 
				animal1.setTieneMicrochip(true);
				animal1.setDescripcion("Un perrito muy bonito");
				animal1.setFechaPublicacion(formatter.parse("16-09-2018"));
				animal1.setEstado("En adopcion");
				animal1.setComportamiento("Muy bueno");
				animal1.setEnvio("A Burgos");
				animal1.setTasaAdopcion(10000000);
				

				Animal animal2 = new Animal();
				animal2.setId(2);
				animal2.setNombre("Rabin");
				animal2.setTipo("Perro");
				animal2.setRaza("Sin Datos");
				animal2.setSexo("Macho");
				animal2.setFechaNacimiento(formatter.parse("03-06-20011"));
				animal2.setEdad(10);
				animal2.setImagen("Perros006.jpg");
				animal2.setTieneMicrochip(false);
				animal2.setDescripcion("Un perrito muy bonito tambien");
				animal2.setFechaPublicacion(formatter.parse("16-09-2018"));
				animal2.setEstado("Adoptado");
				animal2.setComportamiento("Muy bueno");
				animal2.setEnvio("A Burgos");
				animal2.setTasaAdopcion(10000);

				Animal animal3 = new Animal();
				animal3.setId(3);
				animal3.setNombre("Lolita");
				animal3.setTipo("Tortuga");
				animal3.setRaza("Tortuga enana");
				animal3.setSexo("Hembra");
				animal3.setFechaNacimiento(formatter.parse("12-06-2012"));
				animal3.setEdad(6);
				animal3.setImagen("Tortugas001.jpg");
				animal3.setTieneMicrochip(false);
				animal3.setDescripcion("Una tortuga muy bonita");
				animal3.setFechaPublicacion(formatter.parse("16-09-2018"));
				animal3.setEstado("Apadrinado");
				animal3.setComportamiento("A veces muerde");
				animal3.setEnvio("A Burgos");
				animal3.setTasaAdopcion(10);
				
				Animal animal4 = new Animal();
				animal4.setId(3);
				animal4.setNombre("Miau Miau");
				animal4.setTipo("Gato");
				animal4.setRaza("Sin identificar");
				animal4.setSexo("Hembra");
				animal4.setFechaNacimiento(formatter.parse("12-06-2012"));
				animal4.setEdad(6);
				animal4.setImagen("Gatos001.jpg");
				animal4.setTieneMicrochip(false);
				animal4.setDescripcion("Una gatita muy bonita");
				animal4.setFechaPublicacion(formatter.parse("16-09-2018"));
				animal4.setEstado("Apadrinado");
				animal4.setComportamiento("A veces muerde");
				animal4.setEnvio("A Burgos");
				animal4.setTasaAdopcion(10);
				
				Animal animal5 = new Animal();
				animal5.setId(3);
				animal5.setNombre("Pepa");
				animal5.setTipo("Perro");
				animal5.setRaza("Sin identificar");
				animal5.setSexo("Hembra");
				animal5.setFechaNacimiento(formatter.parse("12-06-2012"));
				animal5.setEdad(6);
				animal5.setImagen("Perros010.jpg");
				animal5.setTieneMicrochip(false);
				animal5.setDescripcion("Una perrita muy bonita");
				animal5.setFechaPublicacion(formatter.parse("16-09-2018"));
				animal5.setEstado("En adopcion");
				animal5.setComportamiento("A veces muerde");
				animal5.setEnvio("A Burgos");
				animal5.setTasaAdopcion(10);
				
				Animal animal6 = new Animal();
				animal6.setId(3);
				animal6.setNombre("Moro");
				animal6.setTipo("Perro");
				animal6.setRaza("Pastor belga");
				animal6.setSexo("Macho");
				animal6.setFechaNacimiento(formatter.parse("12-06-2012"));
				animal6.setEdad(6);
				animal6.setImagen("Perros007.jpg");
				animal6.setTieneMicrochip(false);
				animal6.setDescripcion("Un perrito muy bonito");
				animal6.setFechaPublicacion(formatter.parse("16-09-2018"));
				animal6.setEstado("En adopcion");
				animal6.setComportamiento("A veces muerde");
				animal6.setEnvio("A Burgos");
				animal6.setTasaAdopcion(10);

				
				// Agregamos los objetos Animal a la lista
				listaAnimales.add(animal1);
				listaAnimales.add(animal2);
				listaAnimales.add(animal3);
				listaAnimales.add(animal4);
				listaAnimales.add(animal5);
				listaAnimales.add(animal6);

				return listaAnimales;
				
			} catch (ParseException e) {
				System.out.println("Error: " + e.getMessage());
				return null;
			}
		}*/
}
