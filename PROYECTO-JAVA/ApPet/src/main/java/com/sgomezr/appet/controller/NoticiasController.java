package com.sgomezr.appet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sgomezr.appet.model.Noticia;
@Controller
@RequestMapping("/noticias")

public class NoticiasController {
	
	@GetMapping(value="/crear")
	public String crearNoticia(){
		return "noticias/formNoticia";
	}
	
	@PostMapping(value="/guardar")
	public String guardar(@RequestParam("titulo") String titulo, @RequestParam("estatus") String estatus,  
			@RequestParam("detalle") String detalle) {
		
		Noticia noticia = new Noticia();
		noticia.setTitulo(titulo);
		noticia.setEstatus(estatus);
		noticia.setDetalle(detalle);
		
		//TODO: Guardar el objeto noticia en base de datos
		
		System.out.println(noticia);
		return "noticias/formNoticia";
	}
}
