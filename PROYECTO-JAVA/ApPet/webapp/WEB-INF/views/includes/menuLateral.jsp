<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<head>
<spring:url value="/resources" var="urlPublic"></spring:url>
</head>
<!-- Sidebar -->
		<div  style="text-align: center" class="pt-5 sidebar navbar-nav">
		<img style="height: 130px; margin-top:20px" class="img-fluid img-profile rounded-circle mx-auto mb-2"
							src=${urlPublic}/images/usuario.png alt="Imagen Perfil">
		<ul style="list-style:none">
		
		
			<li class="nav-item active"><a class="nav-link"
				href=""> <i class="fas fa-fw fa-tachometer-alt"></i> <span>Mi perfil</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-fw fa-chart-area"></i> <span>Animales en adopcion</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-fw fa-table"></i> <span>Protectoras</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-fw fa-table"></i> <span>Mis favoritos</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-fw fa-table"></i> <span>Mis adopciones</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-fw fa-table"></i> <span>Mis apadrinamientos</span>
			</a></li>
				<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-fw fa-table"></i> <span>Donaciones</span>
			</a></li>
		</ul>
</div>