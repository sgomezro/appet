/*
 * Funcion js para desabilitar campos del formulario
 */
function desabilitarInput() {
	if ((location.href).includes('/protectoras/editar')) {
		$('#nombreProtectora').attr('readonly', true);
	} else if ((location.href).includes('/usuarios/editar')) {
		$('#username').attr('readonly', true);
		$('#nombreProtectora').attr('readonly', true);
	}
}