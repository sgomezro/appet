<!-- Footer -->
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<head>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
	integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
	crossorigin="anonymous">
<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>

</head>
<footer class="py-3 bg-dark" style="background-color:#212529 !important">
	<div class="container" style="text-align:right;">

	<ul class="navbar-nav ml-auto">
	<li class="nav-item" id="footer">
	<a class="nav-link" style="font-size:15px;" href="${urlRoot}contacto/crear">Contacta con nosotros
		</a>
		<a class="nav-link" style="font-size:15px;" href="${urlRoot}index/welcome"> ¿Quienes somos?
		</a>
		</li>
		</ul>
		</div>
	<div div class="container" style="text-align:center; font-size:12px">
	<img style="width: 205px; height: 60px;" class="card-img-top mb-2"
								src=${urlPublic}/images/ApPetLogoBlanco.JPG title="logo">
		<p class="m-0 text-white">Copyright &copy; ApPet 2018</p>
		</div>

</footer>