<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Usuarios / Protectoras</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/usuarios/guardar" var="urlForm"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="${urlPublic}/css/util.css">
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/mainformUsuario.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="bg-contact2"
		style="background-image: url('${urlPublic}/images/fondos/general022.jpg');">
		<div class="container-contact2">
			<div class="wrap-contact2" style="width: 50%">
				<span class="login100-form-title"> <img style="width: 65%"
					src="${urlPublic}/images/ApPetLogo.JPG" alt="ApPetLogo">
				</span>
				<div class="text-center p-b-30">
					<span class="txt2"> "Mejora su mundo, mejora tu mundo" </span>
				</div>
				<span class="contact100-form-title"> REGISTRATE COMO: </span>
				<div
					style="padding-left: 25%; text-align: center; margin-bottom: 20px">
					<a href="${urlRoot}usuarios/crearUsuario">
						<button style="width: 65%; text-transform: none;"
							class="contact100-form-btn">Usuario</button>
					</a>
				</div>
				<div
					style="padding-left: 25%; text-align: center; margin-bottom: 20px">
					<a href="${urlRoot}usuarios/crearProtectora">
						<button
							style="width: 65%; background: #708090; text-transform: none;"
							class="contact100-form-btn">Protectora</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!--===============================================================================================-->
	<script src="${urlPublic}/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/bootstrap/js/popper.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/main.js"></script>

</body>
</html>
