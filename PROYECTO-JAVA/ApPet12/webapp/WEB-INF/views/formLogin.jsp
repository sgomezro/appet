<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ApPet - Inicio Sesion</title>
<!-- Tag de Spring para agregar recursos estaticos -->
<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png" href="images/iconos/favicon.ico" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${urlPublic}/css/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="${urlPublic}/css/util.css">
<link rel="stylesheet" type="text/css" href="${urlPublic}/css/main.css">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form">
					<form class="validate-form" action="${urlRoot}login" method="post">
						<a href="${urlRoot}" ><span class="login100-form-title"> <img style="width: 65%"
							src="${urlPublic}/images/Logo.JPG" alt="ApPetLogo">
						</span></a>
						<div class="text-center p-b-30">
							<span class="txt2"> "Mejora SU vida, mejora T� vida" </span>
						</div>
						<span class="login100-form-title p-b-25"> Iniciar sesion </span>
						<c:if test="${param.error!=null }">
							<h5 class="form-signin-heading"
								style="margin-bottom: 10px; color: #76323F">Acceso
								denegado, usuario o contrase�a incorrectos, int�ntelo de nuevo</h5>
						</c:if>

						<div class="wrap-input100 validate-input"
							data-validate="Introduzca un nombre de usuario valido">
							<input class="input100" type="text" id="username" name="username">
							<span class="focus-input100"></span> <span class="label-input100">Nombre/nick
								de usuario o protectora</span>
						</div>

						<div class="wrap-input100 validate-input"
							data-validate="Introduzca una contrase�a valida">
							<input class="input100" for="password" type="password"
								id="password" name="password"> <input type="hidden"
								name="${_csrf.parameterName}" value="${_csrf.token}" /> <span
								class="focus-input100"></span> <span class="label-input100">Contrase�a</span>
						</div>

						<div class="flex-sb-m w-full p-t-3 p-b-32">
							<div class="contact100-form-checkbox">
								<input class="input-checkbox100" id="ckb1" type="checkbox"
									name="remember-me"> <label class="label-checkbox100"
									for="ckb1"> Recuerdame </label>
							</div>

							<div>
								<a href="#" class="txt1"> �Olvidaste la contrase�a? </a>
							</div>
						</div>

						<div class="container-login100-form-btn">
							<button class="login100-form-btn" type="submit">Iniciar
								sesi�n</button>
						</div>
					</form>
					<hr>
					<span class="login100-form-title p-b-25" style="margin-top: 30px">
						�A�n no te has registrado? </span> <a class="container-login100-form-btn"
						href="${urlRoot}usuarios/crearUsuario">
						<button class="login100-form-btn2">Registrarse como
							usuario</button>
					</a> <a class="container-login100-form-btn"
						href="${urlRoot}usuarios/crearProtectora">
						<button class="login100-form-btn2">�Eres una protectora?
							�Registrala!</button>
					</a>
					<hr>
					<hr>
					<a class="container-login100-form-btn"
						href="${urlRoot}">
						<button class="login100-form-btn">Accede como invitado</button>
</a>
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2"> Accede a trav�s de: </span>
					</div>

					<div class="login100-form-social flex-c-m">
						<a href="#" class="login100-form-social-item flex-c-m bg1 m-r-5">
							<i class="fa fa-facebook-f" aria-hidden="true"></i>
						</a> <a href="#" class="login100-form-social-item flex-c-m bg2 m-r-5">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</div>

				</div>
				<div class="login100-more"
					style="background-image: url('${urlPublic}/images/fondos/general01.jpg');">
				</div>
			</div>
		</div>
	</div>

	<!--===============================================================================================-->
	<script src="${urlPublic}/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/bootstrap/js/popper.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/moment.min.js"></script>
	<script src="${urlPublic}/js/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="${urlPublic}/js/main.js"></script>

</body>
</html>
