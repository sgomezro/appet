<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Mi perfil</title>

<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/usuarios/editar" var="urlEdit"></spring:url>
<spring:url value="/usuarios/eliminar" var="urlDelete"></spring:url>
<spring:url value="/miPerfil/inactivar" var="urlInactivar"></spring:url>
<spring:url value="/animales" var="urlAnimales"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<!-- Modal eliminar-->
<script src="${urlPublic}/js/cambiarConfirm.js"></script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<div style="margin-top: 3rem; position: relative; left: 0; top: 0;">
				<img style="border-radius: .3rem;" class="img-fluid"
					src="${urlPublic}/images/banners/banner16.jpg" alt="Card image cap" />
				<c:choose>
					<c:when test="${prf.perfil =='PROTECTORA'}">
						<img
							style="border: 4px solid white; border-radius: 50%; position: absolute; top: 52%; left: 5%; z-index: 1; height: 70%; width: 20%;"
							src="${urlPublic}/images/protectoras/${usr.imagen }"
							alt="profile-image" class="profile" />
					</c:when>
					<c:otherwise>
						<img
							style="border: 4px solid white; border-radius: 50%; position: absolute; top: 52%; left: 5%; z-index: 1; height: 70%; width: 20%;"
							src="${urlPublic}/images/usuarios/${usr.imagen }"
							alt="profile-image" class="profile" />
					</c:otherwise>
				</c:choose>
			</div>
			<div style="text-align: center">
				<hr>
				<c:choose>
					<c:when test="${prf.perfil =='PROTECTORA'}">
						<h3>Perfil de ${usr.username } | PROTECTORA</h3>
					</c:when>
					<c:when test="${prf.perfil =='PROTECTORA-NA'}">
						<h3 class="mb-4">Perfil de ${usr.username } | PROTECTORA</h3>
						<h6 class="pt-1 pb-1" style="text-transform: none; color: white;background-color: #A65A4A;">(Su cuenta a�n no posee los accesos de PROTECTORA, debido a que
						 no ha sido aprobada por un administrador de ApPet.)</h6>
					</c:when>
					<c:when test="${prf.perfil =='ADMINISTRADOR'}">
						<h3>Mi perfil | ADMINISTRADOR DE APPET</h3>
					</c:when>
					<c:otherwise>
						<h3>Mi perfil de usuario:</h3>
					</c:otherwise>
				</c:choose>
				<hr>
			</div>
			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>
			<div class="container" style="background-color: #f7f7f7">
				<div class="row">
					<c:choose>
						<c:when test="${prf.perfil =='PROTECTORA'}">
							<div class="col-lg-6 col-sm-12 portfolio-item mt-3 mb-1">
								<div class="card h-100">
									<h4 class="card-title"
										style="padding: 8px; color: white; background-color: #17a2b8; text-align: center">
										Datos personales de ${usr.username }</h4>
									<div class="card-body">
										<p class="card-text">
											<strong>Nombre del encargado de ${usr.username }: </strong>
											${usr.nombre }
										</p>
										<p class="card-text">
											<strong>Apellidos del encargado de ${usr.username }:
											</strong> ${usr.apellidos }
										</p>
										<p class="card-text">
											<strong>G�nero:</strong> Protectora
										</p>
										<p class="card-text">
											<strong>Fecha de fundaci�n:</strong>
											<fmt:formatDate pattern="dd-MM-yyyy"
												value="${usr.fechaNacimiento}" />
										</p>
										<p class="card-text">
											<strong>Antiguedad:</strong> ${usr.edad} a�os
										</p>
										<p class="card-text">
											<strong>Localidad:</strong> ${usr.localidad}
										</p>
										<p class="card-text">
											<strong>Email:</strong> ${usr.email}
										</p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-sm-12 portfolio-item mt-3 mb-1">
								<div class="card h-100">
									<h4 class="card-title"
										style="padding: 8px; color: white; background-color: #17a2b8; text-align: center">
										${usr.username} en ApPet</h4>
									<div class="card-body">
										<p class="card-text">
											<strong>Nombre de usuario/protectora:</strong>
											${usr.username}
										</p>
										<p class="card-text">
											<strong>Fecha de creaci�n la cuenta ${usr.username}
												en ApPet:</strong>
											<fmt:formatDate pattern="dd-MM-yyyy"
												value="${usr.fechaCreacion}" />
										</p>
										<hr>
										<a href="${urlAnimales}/listaAnimalesProtectora?idProtectora=${protectora.idProtectora }"><button class="btn btn-outline-secondary">
											Animales publicados en ApPet</button></a>
										<hr>
										<p>
											<strong>Gestiones de ${usr.username} en ApPet:</strong>
										</p>
										<button href="" class="btn btn-outline-info">
											adopciones/apadrinamientos realizados</button>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-sm-12 portfolio-item mt-3 mb-3">
								<div class="card h-100">
									<h4 class="card-title"
										style="padding: 8px; color: white; background-color: #17a2b8; text-align: center">
										Gesti�n de la cuenta ${usr.username} de ApPet</h4>
									<div class="card-body">
										<a href="${urlEdit}?id=${usr.id}&username=${usr.username}">
											<button class="btn btn-outline-secondary mr-3">Editar
												datos de la protectora</button>
										</a>
											<a href="${urlInactivar}?id=${usr.id}&username=${usr.username}"
											data-confirm="�Est� seguro de querer INACTIVAR su cuenta de ${usr.username} en ApPet? Esta opci�n no
											podr� ser revertida por usted, deber� contactar con un ADMINISTRADOR de ApPet para volver a activarla."
											style="color: white !important" class="btn btn-danger"
											role="button" title="Inactivar"><span>�Quieres inactivar la cuenta de ${usr.username}?
												</span></a>
												 <a href="${urlDelete}?id=${usr.id}&username=${usr.username}"
											data-confirm="�Est� seguro de querer ELIMINAR su cuenta de ApPet? "
											style="color: white !important" class="btn btn-danger"
											role="button" title="Eliminar"><span>Eliminar la
												cuenta de ${usr.username}</span></a>
									</div>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="col-lg-6 col-sm-12 portfolio-item mt-3 mb-1">
								<div class="card h-100">
									<h4 class="card-title"
										style="padding: 8px; color: white; background-color: #17a2b8; text-align: center">
										Mis datos personales</h4>
									<div class="card-body">
										<p class="card-text">
											<strong>Nombre:</strong> ${usr.nombre }
										</p>
										<p class="card-text">
											<strong>Apellidos:</strong> ${usr.apellidos }
										</p>
										<p class="card-text">
											<strong>G�nero:</strong> ${usr.genero}
										</p>
										<p class="card-text">
											<strong>Fecha de nacimiento:</strong>
											<fmt:formatDate pattern="dd-MM-yyyy"
												value="${usr.fechaNacimiento}" />
										</p>
										<p class="card-text">
											<strong>Edad:</strong> ${usr.edad} a�os
										</p>
										<p class="card-text">
											<strong>Localidad:</strong> ${usr.localidad}
										</p>
										<p class="card-text">
											<strong>Email:</strong> ${usr.email}
										</p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-sm-12 portfolio-item mt-3 mb-1">
								<div class="card h-100">
									<h4 class="card-title"
										style="padding: 8px; color: white; background-color: #17a2b8; text-align: center">
										Mi cuenta de ApPet</h4>
									<div class="card-body">
										<p class="card-text">
											<strong>Nombre de usuario:</strong> ${usr.username}
										</p>
										<p class="card-text">
											<strong>Fecha de creaci�n de mi cuenta:</strong>
											<fmt:formatDate pattern="dd-MM-yyyy"
												value="${usr.fechaCreacion}" />
										</p>
										<hr>
										<a href="../favoritos/listaAnimalesFavoritos">
											<button class="btn btn-outline-secondary">Mis
											animales favoritos</button>
										</a>
										<a  href="../favoritos/listaProtectorasFavoritas">
										<button class="btn btn-outline-secondary">Mis
											protectoras favoritas</button></a>
										<hr>
										<p>
											<strong>Mis gestiones en ApPet:</strong>
										</p>
										<button href="" class="btn btn-outline-info">Mis
											adopciones/apadrinamientos</button>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-sm-12 portfolio-item mt-3 mb-3">
								<div class="card h-100">
									<h4 class="card-title"
										style="padding: 8px; color: white; background-color: #17a2b8; text-align: center">
										Gesti�n de mi cuenta de ApPet</h4>
									<div class="card-body">
										<a href="${urlEdit}?id=${usr.id}&username=${usr.username}">
											<button class="btn btn-outline-secondary mr-3">Editar
												mis datos</button>
										</a> 
										<a href="${urlInactivar}?id=${usr.id}&username=${usr.username}"
											data-confirm="�Est� seguro de querer INACTIVAR su cuenta de ApPet?. Esta opci�n no
											podr� ser revertida por usted, deber� contactar con un ADMINISTRADOR de ApPet para volver a activarla."
											style="color: white !important" class="btn btn-danger"
											role="button" title="Inactivar"><span>�Quieres inactivar tu cuenta?
												</span></a>
												<a href="${urlDelete}?id=${usr.id}&username=${usr.username}"
											data-confirm="�Est� seguro de querer ELIMINAR su cuenta de ApPet? "
											style="color: white !important" class="btn btn-danger"
											role="button" title="Eliminar"><span>Eliminar mi
												cuenta</span></a>
									</div>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->


	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>

	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/jquery/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>