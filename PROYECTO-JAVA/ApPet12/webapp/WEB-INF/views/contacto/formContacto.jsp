<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">
<title>Contacto</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/contacto/guardar" var="urlForm"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/banners/banner14.jpg)">

				<h1 class="display-3" style="color: #2F4F4F">
					Formulario<br /> de contacto
				</h1>

				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>
			<div class="text-center">
				<hr>
				<h3 class="blog-title">
					<img style="margin-bottom: 15px; width: 60px; height: 40px;" class="card-img-top mr-2"
								src=${urlPublic}/images/ApPetHuella.JPG title="huella"> <span class="label label-success">Envianos tu opini�n</span> <img style="margin-bottom: 15px; width: 60px; height: 40px;" class="card-img-top mr-2"
								src=${urlPublic}/images/ApPetHuella.JPG title="huella">
				</h3>
				<hr>
			</div>
			<spring:hasBindErrors name="contacto">
				<div class='alert alert-danger' role='alert'>
					Por favor corrija los siguientes errores:
					<ul>
						<c:forEach var="error" items="${errors.allErrors}">
							<li><spring:message message="${error}" /></li>
						</c:forEach>
					</ul>
				</div>
			</spring:hasBindErrors>
			<div class="container pt-3" style="background-color: #f7f7f7">
				<form:form action="${urlForm}" method="post"
					modelAttribute="contacto">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="badgetForm" for="nombre">Nombre</label>
								<form:hidden path="id" />
								<form:input type="text" class="form-control" path="nombre"
									id="nombre" required="required"
									placeholder="Introduce tu nombre" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="badgetForm" for="apellidos">Apellidos</label>
								<form:input type="text" class="form-control" path="apellidos"
									placeholder="Introduce tus apellidos" id="apellidos" />
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="badgetForm" for="email">Email</label>
								<form:input type="text" class="form-control" path="email"
									id="email" placeholder="Introduce tu email" />
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="badgetForm" for="tipo">�Qu� tipologia de
									animal le gustaria adoptar?</label>
								<form:select id="tipo" path="tipos" multiple="multiple"
									class="form-control" items="${tipos }" required="required" />
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group">
								<label class="badgetForm" for="experiencia">�Como ha
									sido tu experiencia en ApPet?</label>
								<div class="col-sm-10">
									<label><form:radiobutton path="experiencia"
											value="Muy buena" />Muy buena</label> <label><form:radiobutton
											path="experiencia" value="Buena" />Buena</label> <label><form:radiobutton
											path="experiencia" value="Regular" />Regular</label> <label><form:radiobutton
											path="experiencia" value="Mala" />Mala</label> <label><form:radiobutton
											path="experiencia" value="Muy mala" />Muy mala</label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="badgetForm" for="notificaciones">�Te
									gustaria recibir notificaciones? Si es as�, indicanos de cual</label>
								<div class="col-sm-10">
									<form:checkboxes items="${tiposNotificaciones }"
										path="notificaciones" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="badgetForm" for="comentarios">Comentarios</label>
								<form:textarea class="form-control" path="comentarios"
									id="comentarios" rows="10"
									placeholder="Envianos tu comentario. �Como ha sido su experiencia en ApPet?, �Que mejoraria? �T� opinion nos ayuda a mejorar!"></form:textarea>
							</div>
						</div>
					</div>
					<div class="container" style="text-align: right">
						<button type="submit" class="btn btn-info"
							style="margin-top: 30px; margin-bottom: 50px;">Enviar</button>
					</div>
				</form:form>
			</div>

		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
	<script>
		// Configuracion de la barra de heramientas
		// https://www.tinymce.com/docs/get-started/basic-setup/
		tinymce
				.init({
					selector : '#detalle',
					plugins : "textcolor, table lists code",
					toolbar : " undo redo | bold italic | alignleft aligncenter alignright alignjustify \n\
                    | bullist numlist outdent indent | forecolor backcolor table code"
				});
	</script>
</body>
</html>