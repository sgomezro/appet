package com.sgomezr.appet.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sgomezr.appet.model.Perfil;

public interface PerfilesRepository extends JpaRepository<Perfil, Integer> {
	Perfil findByUsername(String username);
	List<Perfil> findByPerfil(String perfil);
}
