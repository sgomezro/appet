package com.sgomezr.appet.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;
import com.sgomezr.appet.model.Protectora;


@Repository
public interface AnimalesRepository extends JpaRepository<Animal, Integer> {
	List <Animal> findByProtectora(Protectora protectora);

}
