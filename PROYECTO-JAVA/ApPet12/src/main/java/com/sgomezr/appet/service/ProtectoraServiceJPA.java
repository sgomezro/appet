package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.repository.ProtectorasRepository;

@Service
public class ProtectoraServiceJPA implements IProtectorasService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(ProtectoraServiceJPA.class);

	@Autowired
	private ProtectorasRepository protectorasRepository;

	@Override
	public List<Protectora> buscarTodasLasProtectoras() {
		return protectorasRepository.findAll();
	}

	@Override
	public void insertar(Protectora protectora) {
		protectorasRepository.save(protectora);
	}

	@Override
	public Protectora buscarProtectoraPorId(int idProtectora) {
		Optional<Protectora> protectora = protectorasRepository.findById(idProtectora);
		if (protectora.isPresent()) {
			return protectora.get();
		}
		return null;
	}

	@Override
	public Protectora buscarProtectoraPorNombre(String nombreProtectora) {
		Protectora protectora = protectorasRepository.findByNombreProtectora(nombreProtectora);
		return protectora;
	}
	@Override
	public int buscaIdProtectora(String nombreProtectora) {
		int idProtectora = 0;
		Protectora protectora = protectorasRepository.findByNombreProtectora(nombreProtectora);
		idProtectora = protectora.getIdProtectora();
		return idProtectora;
	}
	
	
	@Override
	public List<String> buscarLocalidades() {

		List<String> localidades = new LinkedList<>();
		localidades.add("A Coru�a");
		localidades.add("�lava");
		localidades.add("Albacete");
		localidades.add("Alicante");
		localidades.add("Almer�a");
		localidades.add("Asturias");
		localidades.add("�vila");
		localidades.add("Badajoz");
		localidades.add("Islas Baleares");
		localidades.add("Barcelona");
		localidades.add("Burgos");
		localidades.add("C�ceres");
		localidades.add("C�diz");
		localidades.add("Cantabria");
		localidades.add("Castell�n");
		localidades.add("Ciudad Real");
		localidades.add("C�rdoba");
		localidades.add("Cuenca");
		localidades.add("Girona");
		localidades.add("Granada");
		localidades.add("Guadalajara");
		localidades.add("Guip�zcoa");
		localidades.add("Huelva");
		localidades.add("Huesca");
		localidades.add("Ja�n");
		localidades.add("La Rioja");
		localidades.add("Las Palmas");
		localidades.add("Le�n");
		localidades.add("Lleida");
		localidades.add("Lugo");
		localidades.add("Madrid");
		localidades.add("M�laga");
		localidades.add("Murcia");
		localidades.add("Navarra");
		localidades.add("Orense");
		localidades.add("Palencia");
		localidades.add("Pontevedra");
		localidades.add("Salamanca");
		localidades.add("Segovia");
		localidades.add("Sevilla");
		localidades.add("Soria");
		localidades.add("Tarragona");
		localidades.add("Santa Cruz de Tenerife");
		localidades.add("Teruel");
		localidades.add("Toledo");
		localidades.add("Valencia");
		localidades.add("Valladolid");
		localidades.add("Vizcaya");
		localidades.add("Zamora");
		localidades.add("Zaragoza");
		return localidades;
	}

	@Override
	public void eliminar(int idProtectora) {
		protectorasRepository.deleteById(idProtectora);
	}

	@Override
	public Page<Protectora> buscarTodasPaginacion(Pageable page) {
		return protectorasRepository.findAll(page);
	}

	@Override
	public Protectora generarObjetoProtectora(Usuario usuario) {

		Protectora protectoraGenerada = new Protectora();
		protectoraGenerada.setNombreProtectora(usuario.getUsername());
		protectoraGenerada.setNombre(usuario.getNombre());
		protectoraGenerada.setApellidos(usuario.getApellidos());
		protectoraGenerada.setEmail(usuario.getEmail());
		protectoraGenerada.setPassword(usuario.getPassword());
		protectoraGenerada.setComprobarPass(usuario.getComprobarPass());
		protectoraGenerada.setFechaNacimiento(usuario.getFechaNacimiento());
		protectoraGenerada.setLocalidad(usuario.getLocalidad());
		protectoraGenerada.setLogoProtectora(usuario.getImagen());
		protectoraGenerada.setFechaCreacion(usuario.getFechaCreacion());
		if (usuario.getEstado() == 1) {
			protectoraGenerada.setEstado("Activa");
		} else {
			protectoraGenerada.setEstado("Inactiva");
		}
		return protectoraGenerada;
	}

	@Override
	public void validarCampos(BindingResult result, Protectora protectora) {
		if (protectora.getPassword().equals(protectora.getComprobarPass())) {
			logger.info("Sin errores en contrase�as");
		} else {
			logger.info("Con errores en contrase�as");
			ObjectError error = new ObjectError("password", "Contrase�a: Las contrase�as no son iguales");
			result.addError(error);
		}
		if ((protectorasRepository.findByNombreProtectora(protectora.getNombreProtectora())) == null) {
			logger.info("Sin errores en nombreProtectora");
		} else {
			if ((protectora.getNombreProtectora()).equalsIgnoreCase(protectorasRepository
					.findByNombreProtectora(protectora.getNombreProtectora()).getNombreProtectora())) {
				logger.info("errores en nombreProtectora");
				ObjectError error = new ObjectError("nombreProtectora",
						"Nombre de la protectora: Ya existe una protectora con el mismo nombre registrada en ApPet");
				result.addError(error);
			}
		}
		if ((protectorasRepository.findByEmail(protectora.getEmail())) == null) {
			logger.info("Sin errores en email");
		} else {
			if ((protectora.getEmail())
					.equalsIgnoreCase(protectorasRepository.findByEmail(protectora.getEmail()).getEmail())) {
				logger.info("errores en email");
				ObjectError error = new ObjectError("email",
						"Email: El email de la protectora ya se encuentra registrado en ApPet, pruebe con otro");
				result.addError(error);
			}
		}
	}

	@Override
	public List<Protectora> buscarProtectoraPorLocalidad(String localidad) {
		List<Protectora> listaProtectorasPorLocalidad = protectorasRepository.findByLocalidad(localidad);
		
		return listaProtectorasPorLocalidad;
	}


}
