package com.sgomezr.appet.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.repository.AnimalesRepository;
import com.sgomezr.appet.repository.FavoritosRepository;
import com.sgomezr.appet.repository.NoticiaRepository;

@Service
public class FavoritosServiceJPA implements IFavoritosService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(FavoritosServiceJPA.class);

	@Autowired
	private FavoritosRepository favoritosRepository;
	@Autowired
	private AnimalesRepository animalesRepository;

	@Override
	public void agregar(Favorito favorito) {
		favoritosRepository.save(favorito);
	}

	@Override
	public Boolean existeFavorito(int idAnimal, int idUsuario) {
		Optional<Animal> animal = animalesRepository.findById(idAnimal);
		Favorito favorito = favoritosRepository.findByAnimalAndIdUsuario(animal, idUsuario);
		if (favorito == null) {
			return false;
		}
		if (((favorito.getAnimal()).getId() == (idAnimal)) && (favorito.getIdUsuario() == idUsuario)) {
			return true;
		}
		return false;
	}

	@Override
	public List<Favorito> buscarFavoritosPorIdUsuario(int idUsuario) {
		return favoritosRepository.findByIdUsuario(idUsuario);
	}
	
	@Override
	public void eliminar(int idAnimal) {
		favoritosRepository.deleteById(idAnimal);
	}

}
