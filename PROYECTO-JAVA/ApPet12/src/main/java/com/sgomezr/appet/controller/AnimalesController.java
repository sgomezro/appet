package com.sgomezr.appet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.util.Utileria;

@Controller
@RequestMapping("/animales")
public class AnimalesController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(AnimalesController.class);

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IProtectorasService serviceProtectoras;

	@GetMapping("/listaAnimales")
	public String mostrarIndex(Model model) {
		List<Animal> listaAnimales = serviceAnimales.buscarTodosLosAnimales();
		model.addAttribute("animales", listaAnimales);
		return "animales/listaAnimales";
	}

	@GetMapping("/listaAnimalesProtectora")
	public String listaAnimalesProtectora(Model model, @RequestParam("idProtectora") int idProtectora) {
		List<Animal> listaAnimalesPorProtectora = serviceAnimales.buscarAnimalPorProtectora(idProtectora);
		model.addAttribute("animales", listaAnimalesPorProtectora);
		model.addAttribute("protectora", serviceProtectoras.buscarProtectoraPorId(idProtectora));

		return "animales/listaAnimalesProtectora";
	}

	@GetMapping("/crear")
	public String crear(Model model, @ModelAttribute Animal animal) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();

		List<String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras", listaProtectoras);
		model.addAttribute("localidades", serviceAnimales.buscarLocalidades());

		return "animales/formAnimal";

	}

	@PostMapping("/guardar")
	public String guardar(Model model, @ModelAttribute Animal animal, BindingResult result,
			@ModelAttribute Protectora protectora, RedirectAttributes atributos,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request) {
		String folder = "animales";
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		List<Integer> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getIdProtectora());
		}
		model.addAttribute("protectoras", listaProtectoras);
		model.addAttribute("localidades", serviceAnimales.buscarLocalidades());
		// Validacion de campos/errores del formulario
		serviceAnimales.validarCampos(result, animal);

		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "animales/formAnimal";
		}
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request, folder);
			animal.setImagen(nombreImagen);
		}

		logger.info("Recibiendo objeto animal" + animal);
		logger.info("Recibiendo objeto animal de protectora: " + protectora.getIdProtectora());
		logger.info("Elementos en la lista antes de la inserción: " + serviceAnimales.buscarTodosLosAnimales().size());
		serviceAnimales.insertar(animal);
		logger.info(
				"Elementos en la lista despues de la inserción: " + serviceAnimales.buscarTodosLosAnimales().size());
		atributos.addFlashAttribute("mensaje", "El animal ha sido creado y guardado correctamente");
		return "redirect:/animales/listaAnimales";
	}

	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idAnimal") int idAnimal, Model model) {
		Animal animal = serviceAnimales.buscarAnimalPorId(idAnimal);
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();

		List<Integer> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getIdProtectora());
		}
		model.addAttribute("animal", animal);
		model.addAttribute("protectoras", protectoras);
		model.addAttribute("localidades", serviceAnimales.buscarLocalidades());
		return "animales/formAnimal";
	}

	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idAnimal") int idAnimal, Model model, RedirectAttributes atributos) {
		serviceAnimales.eliminar(idAnimal);
		atributos.addFlashAttribute("mensaje", "El animal fue eliminado correctamente");
		return "redirect:/animales/listaAnimales";
	}

	@ModelAttribute("tipos")
	public List<String> getTipologias() {
		return serviceAnimales.buscarTipologias();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

}