package pruebascrudrepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.repository.NoticiasRespository;

public class AppExists {
	
	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		
		//Metodo para verificar si una entidad existe en la base de datos (metodo existsById)
		
		int idNoticia = 1;
		
		//Si regresa true el registro existe en la tabla, si no regresa false
		System.out.println(repo.existsById(idNoticia));
		context.close();

	}
}
