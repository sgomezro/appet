package pruebasrelaciones;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.repository.AnimalesRepository;

public interface AppRepoAnimales extends JpaRepository<Animal, Integer> {
public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		
		AnimalesRepository repositorioAnimales = context.getBean("animalesRepository", AnimalesRepository.class);
		List<Animal> listaAnimales = repositorioAnimales.findAll();
		
		for(Animal animal : listaAnimales) {
			System.out.println(animal);
		}
		context.close();

	}
	
}
