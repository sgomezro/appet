package com.sgomezr.appet.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

	@Entity
	@Table(name = "protectorasfavoritas")
	public class ProtectoraFavorita {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id_favorito")
		private int idFavorito;
		
		@Column(name = "id_usuario")
		private int idUsuario;
		
		@ManyToOne
		@JoinColumn(name = "Protectoras_id_protectora")
		private Protectora protectorafav;

		public int getIdFavorito() {
			return idFavorito;
		}

		public void setIdFavorito(int idFavorito) {
			this.idFavorito = idFavorito;
		}

		public int getIdUsuario() {
			return idUsuario;
		}

		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}

		public Protectora getProtectorafav() {
			return protectorafav;
		}

		public void setProtectorafav(Protectora protectorafav) {
			this.protectorafav = protectorafav;
		}

		@Override
		public String toString() {
			return "ProtectoraFavorita [idFavorito=" + idFavorito + ", idUsuario=" + idUsuario + ", protectorafav="
					+ protectorafav + "]";
		}


	
		
	   
}
