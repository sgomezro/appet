package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.repository.UsuariosRepository;

@Service
public class UsuariosServiceJPA implements IUsuariosService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(UsuariosServiceJPA.class);

	@Autowired
	private UsuariosRepository usuariosRepository;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public List<Usuario> buscarTodosLosUsuarios() {
		return usuariosRepository.findAll();
	}

	@Override
	public void guardar(Usuario usuario) {
		usuario.setEdad(usuario.getFechaNacimiento());
		usuariosRepository.save(usuario);
	}

	@Override
	public List<String> buscarLocalidades() {

		List<String> localidades = new LinkedList<>();
		localidades.add("A Coru�a");
		localidades.add("�lava");
		localidades.add("Albacete");
		localidades.add("Alicante");
		localidades.add("Almer�a");
		localidades.add("Asturias");
		localidades.add("�vila");
		localidades.add("Badajoz");
		localidades.add("Islas Baleares");
		localidades.add("Barcelona");
		localidades.add("Burgos");
		localidades.add("C�ceres");
		localidades.add("C�diz");
		localidades.add("Cantabria");
		localidades.add("Castell�n");
		localidades.add("Ciudad Real");
		localidades.add("C�rdoba");
		localidades.add("Cuenca");
		localidades.add("Girona");
		localidades.add("Granada");
		localidades.add("Guadalajara");
		localidades.add("Guip�zcoa");
		localidades.add("Huelva");
		localidades.add("Huesca");
		localidades.add("Ja�n");
		localidades.add("La Rioja");
		localidades.add("Las Palmas");
		localidades.add("Le�n");
		localidades.add("Lleida");
		localidades.add("Lugo");
		localidades.add("Madrid");
		localidades.add("M�laga");
		localidades.add("Murcia");
		localidades.add("Navarra");
		localidades.add("Orense");
		localidades.add("Palencia");
		localidades.add("Pontevedra");
		localidades.add("Salamanca");
		localidades.add("Segovia");
		localidades.add("Sevilla");
		localidades.add("Soria");
		localidades.add("Tarragona");
		localidades.add("Santa Cruz de Tenerife");
		localidades.add("Teruel");
		localidades.add("Toledo");
		localidades.add("Valencia");
		localidades.add("Valladolid");
		localidades.add("Vizcaya");
		localidades.add("Zamora");
		localidades.add("Zaragoza");

		return localidades;
	}

	@Override
	public void eliminar(int idUsuario) {
		usuariosRepository.deleteById(idUsuario);
	}

	@Override
	public Usuario buscarUsuarioPorId(int idUsuario) {
		Optional<Usuario> usuario = usuariosRepository.findById(idUsuario);
		if (usuario.isPresent()) {
			return usuario.get();
		}
		return null;
	}

	@Override
	public Usuario buscarUsuarioPorUsername(String username) {
		Usuario usuario = usuariosRepository.findByUsername(username);
		return usuario;
	}

	@Override
	public Usuario buscarUsuarioPorEmail(String email) {
		Usuario usuario = usuariosRepository.findByEmail(email);
		return usuario;
	}

	@Override
	public void validarCampos(BindingResult result, Usuario usuario) {
		if (usuario.getPassword().equals(usuario.getComprobarPass())) {
			logger.info("Sin errores en contrase�a");
		} else {
			logger.info("Con errores en contrase�a");
			ObjectError error = new ObjectError("password", "Las contrase�as no son iguales");
			result.addError(error);
		}
		if ((usuariosRepository.findByUsername(usuario.getUsername())) == null) {
			logger.info("Sin errores en username");
		} else {
			if ((usuario.getUsername())
					.equalsIgnoreCase(usuariosRepository.findByUsername(usuario.getUsername()).getUsername())) {
				logger.info("errores en username");
				ObjectError error = new ObjectError("username",
						"El usuario ya se encuentra registrado en ApPet, pruebe con otro");
				result.addError(error);
			}
		}
		if ((usuariosRepository.findByEmail(usuario.getEmail())) == null) {
			logger.info("Sin errores en email");
		} else {
			if ((usuario.getEmail()).equalsIgnoreCase(usuariosRepository.findByEmail(usuario.getEmail()).getEmail())) {
				logger.info("errores en email");
				ObjectError error = new ObjectError("email",
						"El email ya se encuentra registrado en ApPet, pruebe con otro");
				result.addError(error);
			}
		}
	}

	@Override
	public Usuario generarObjetoUsuario(Protectora protectora, int idUsuario) {

		Usuario usuarioGenerado = new Usuario();
		usuarioGenerado.setId(idUsuario);
		usuarioGenerado.setUsername(protectora.getNombreProtectora());
		usuarioGenerado.setNombre(protectora.getNombre());
		usuarioGenerado.setApellidos(protectora.getApellidos());
		usuarioGenerado.setEmail(protectora.getEmail());

		usuarioGenerado.setPassword(protectora.getPassword());
		String encriptado = encoder.encode(usuarioGenerado.getPassword());
		usuarioGenerado.setPassword(encriptado);

		usuarioGenerado.setComprobarPass(protectora.getComprobarPass());
		String comprobarEncriptado = encoder.encode(usuarioGenerado.getComprobarPass());
		usuarioGenerado.setComprobarPass(comprobarEncriptado);

		usuarioGenerado.setFechaNacimiento(protectora.getFechaNacimiento());
		usuarioGenerado.setLocalidad(protectora.getLocalidad());
		usuarioGenerado.setImagen(protectora.getLogoProtectora());
		usuarioGenerado.setFechaCreacion(protectora.getFechaCreacion());

		if ((protectora.getEstado()).equals("Activa")) {
			usuarioGenerado.setEstado(1);
		} else {
			usuarioGenerado.setEstado(0);
		}
		return usuarioGenerado;
	}
}
