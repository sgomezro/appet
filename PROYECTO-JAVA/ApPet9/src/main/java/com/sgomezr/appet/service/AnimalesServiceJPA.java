package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.repository.AnimalesRepository;
import com.sgomezr.appet.repository.ProtectorasRepository;
import com.sgomezr.appet.util.Utileria;

@Service
public class AnimalesServiceJPA implements IAnimalesService {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(AnimalesServiceJPA.class);
		
	@Autowired
	private AnimalesRepository animalesRepository;
	
	@Autowired
	private ProtectorasRepository protectorasRepository;

	@Override
	public void insertar(Animal animal) {
		animal.setEdad(animal.getFechaNacimiento());
		animalesRepository.save(animal);
	}

	@Override
	public List<Animal> buscarTodosLosAnimales() {
		return animalesRepository.findAll();
	}

	@Override
	public Animal buscarAnimalPorId(int idAnimal) {
		Optional<Animal> animal = animalesRepository.findById(idAnimal);
		if(animal.isPresent()) {
			return animal.get();
		}
		return null;
	}

	@Override
	public List<Animal> buscarAnimalPorProtectora(int idProtectoraAnimal) {
		Optional<Protectora> optional = protectorasRepository.findById(idProtectoraAnimal);
		logger.info(optional.get().getAnimales().size());
		List<Animal> listaAnimalesPorProtectora = optional.get().getAnimales();
		return listaAnimalesPorProtectora;
	}

	@Override
	public List<String> buscarTipologias() {
		List<String> tipos = new LinkedList<>();
		tipos.add("Perro");
		tipos.add("Gato");
		tipos.add("Pajaro");
		tipos.add("Tortuga");
		tipos.add("Hamster");
		tipos.add("Huron");
		tipos.add("Cerdo");
		tipos.add("Reptil");
		tipos.add("Equino");
		return tipos;
	}
	@Override
	public List<String> buscarLocalidades() {
		List<String> localidades = new LinkedList<>();
		localidades.add("A toda Espa�a");
		localidades.add("A Coru�a");
		localidades.add("�lava");
		localidades.add("Albacete");
		localidades.add("Alicante");
		localidades.add("Almer�a");
		localidades.add("Asturias");
		localidades.add("�vila");
		localidades.add("Badajoz");
		localidades.add("Islas Baleares");
		localidades.add("Barcelona");
		localidades.add("Burgos");
		localidades.add("C�ceres");
		localidades.add("C�diz");
		localidades.add("Cantabria");
		localidades.add("Castell�n");
		localidades.add("Ciudad Real");
		localidades.add("C�rdoba");
		localidades.add("Cuenca");
		localidades.add("Girona");
		localidades.add("Granada");
		localidades.add("Guadalajara");
		localidades.add("Guip�zcoa");
		localidades.add("Huelva");
		localidades.add("Huesca");
		localidades.add("Ja�n");
		localidades.add("La Rioja");
		localidades.add("Las Palmas");
		localidades.add("Le�n");
		localidades.add("Lleida");
		localidades.add("Lugo");
		localidades.add("Madrid");
		localidades.add("M�laga");
		localidades.add("Murcia");
		localidades.add("Navarra");
		localidades.add("Orense");
		localidades.add("Palencia");
		localidades.add("Pontevedra");
		localidades.add("Salamanca");
		localidades.add("Segovia");
		localidades.add("Sevilla");
		localidades.add("Soria");
		localidades.add("Tarragona");
		localidades.add("Santa Cruz de Tenerife");
		localidades.add("Teruel");
		localidades.add("Toledo");
		localidades.add("Valencia");
		localidades.add("Valladolid");
		localidades.add("Vizcaya");
		localidades.add("Zamora");
		localidades.add("Zaragoza");

		return localidades;
	}


	@Override
	public void eliminar(int idAnimal) {
		animalesRepository.deleteById(idAnimal);
	}
	
	@Override
	public Page<Animal> buscarTodasPaginacionAnimales(Pageable page) {
	return animalesRepository.findAll(page);
	}

	@Override
	public void validarCampos(BindingResult result, Animal animal) {
		if(Utileria.isNumeric(animal.getRaza())==true) {
			logger.info("Error raza");
			ObjectError errorRaza = result.getFieldError("raza");
			errorRaza = new ObjectError("raza", "Raza: Debe introducir una raza correcta, la raza del animal no puede ser un valor numerico");
			 result.addError(errorRaza);
		}
	}
}
