package com.sgomezr.appet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.service.INoticiasService;
import com.sgomezr.appet.util.Utileria;

@Controller
@RequestMapping("/noticias")
public class NoticiasController {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(NoticiasController.class);

	@Autowired
	private INoticiasService serviceNoticias;

	/**
	 * Metodo para mostrar el listado de noticias
	 * 
	 * @param model
	 * @return
	 */

	@GetMapping("/listaNoticias")
	public String listaNoticias(Model model) {
		List<Noticia> noticias = serviceNoticias.buscarTodasLasNoticias();
		model.addAttribute("noticias", noticias);
		return "noticias/listaNoticias";
	}

	@GetMapping(value = "/crear")
	public String crear(@ModelAttribute Noticia noticia) {
		return "noticias/formNoticia";
	}

	@PostMapping(value = "/guardar")
	public String guardar(@ModelAttribute Noticia noticia, BindingResult result, RedirectAttributes atributos,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request) {

		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "noticias/formNoticia";
		}
		String folder = "noticias";
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request,folder);
			noticia.setImagen(nombreImagen);
		}
		logger.info("Recibiendo objeto noticia" + noticia);
		logger.info("Elementos en la lista antes de la inserción: " + serviceNoticias.buscarTodasLasNoticias().size());
		serviceNoticias.insertarNoticia(noticia);
		logger.info(
				"Elementos en la lista despues de la inserción: " + serviceNoticias.buscarTodasLasNoticias().size());
		atributos.addFlashAttribute("mensaje", "El noticia ha sido creado y guardado correctamente");
		return "redirect:/noticias/listaNoticias";
	}

	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idNoticia") int idNoticia, Model model) {
		Noticia noticia = serviceNoticias.buscarNoticiaPorId(idNoticia);
		model.addAttribute("noticia", noticia);
		return "noticias/formNoticia";
	}

	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idNoticia") int idNoticia, Model model, RedirectAttributes atributos) {
		serviceNoticias.eliminar(idNoticia);
		atributos.addFlashAttribute("mensaje", "La noticia fue eliminada correctamente");
		return "redirect:/noticias/listaNoticias";
	}

}