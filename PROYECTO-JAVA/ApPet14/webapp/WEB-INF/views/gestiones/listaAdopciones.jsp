<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Mis adopciones</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/gestiones" var="urlRoot"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
</head>

<body id="page-top">
	<!-- Modales -->
	<jsp:include page="../includes/modales/modalExperiencia.jsp" />
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<!-- Page Heading -->
			<hr>
			<h1 class="my-4 text-center" style="color: red">P�GINA EN
				CONSTRUCCI�N</h1>
			<hr>
			<hr>
			<h1 class="my-4 text-center">
				<img style="margin-bottom: 15px; width: 60px; height: 40px;"
					class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
					title="huella"> Mis adopciones <small>en ApPet</small> <img
					style="margin-bottom: 15px; width: 60px; height: 40px;"
					class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
					title="huella">
			</h1>
			<hr>
			<div style="text-align: center;">
				<a href="${urlRoot}/listaAdopciones"><button type="button"
						class="btn btn-outline-secondary mb-3">Mis adopciones</button></a> <a
					href="${urlRoot}/listaApadrinamientos"><button type="button"
						class="btn btn-outline-secondary mb-3">Mis
						apadrinamientos</button></a>
			</div>
			<!-- Project One -->

			<div class="row" style="border: 2px solid #f7f7f7;">
				<div class="col-md-6">
					<div style="position: relative; left: 0; top: 0;">
						<a href="#"> <img
							class="img-fluid rounded mb-3 pt-3 pb-3 mb-md-0"
							src="${urlPublic}/images/animales/perros012.jpg" alt=""></a> <a
							href="#"> <img class="img-fluid rounded mb-3 mb-md-0"
							style="position: absolute; top: 25px; left: 80%; z-index: 1; height: 21%; width: 16%;"
							src="${urlPublic}/images/protectoras/proBurgos.jpg" alt=""></a>
					</div>
				</div>
				<div class="col-md-6 mt-2">
					<h3>
						Sayker: <small><a href="#" class="badge badge-light">Accede
								a su ficha completa</a></small>
					</h3>
					<hr>
					<p>
						<strong>Nombre:</strong> Sayker
					</p>
					<p>
						<strong>Tipologia:</strong> Perro
					</p>
					<p>
						<strong>Raza:</strong> Labrador Retrevier
					</p>
					<p>
						<strong>Edad:</strong> 8 a�os
					</p>
					<hr>
					<p>
						<strong>Cu�ntanos </strong>
					</p>
					<button type="button" class="btn btn-outline-secondary mb-3"
						data-toggle="modal" data-target="#ModalExperiencia">Tu
						experiencia con Sayker</button>
					</br>
					<button type="button" class="btn btn-outline-info mr-3"
						data-toggle="modal" data-target="#ModalExperiencia">Tu
						experiencia con BurgosProtect</button>
				</div>
			</div>
			<!-- /.row -->
			<hr>
			<!-- Project One -->
			<div class="row" style="border: 2px solid #f7f7f7;">
				<div class="col-md-6 mt-2">
					<h3>
						Miau Miau: <small><a href="#" class="badge badge-light">Accede
								a su ficha completa</a></small>
					</h3>
					<hr>
					<p>
						<strong>Nombre:</strong> Miau Miau
					</p>
					<p>
						<strong>Tipologia:</strong> Gato
					</p>
					<p>
						<strong>Raza:</strong> Sin determinar
					</p>
					<p>
						<strong>Edad:</strong> 2 a�os
					</p>
					<hr>
					<p>
						<strong>Cu�ntanos </strong>
					</p>
					<button type="button" class="btn btn-outline-secondary mb-3"
						data-toggle="modal" data-target="#ModalExperiencia">Tu
						experiencia con Miau Miau:</button>
					</br>
					<button type="button" class="btn btn-outline-info mr-3"
						data-toggle="modal" data-target="#ModalExperiencia">Tu
						experiencia con CordobaProtect</button>
				</div>
				<div class="col-md-6">
					<div style="position: relative; left: 0; top: 0;">
						<a href="#"> <img
							class="img-fluid rounded mb-3 pt-3 pb-3 mb-md-0"
							src="${urlPublic}/images/animales/gatos003.jpg" alt=""></a> <a
							href="#"> <img class="img-fluid rounded mb-3 mb-md-0"
							style="position: absolute; top: 25px; left: 80%; z-index: 1; height: 21%; width: 16%;"
							src="${urlPublic}/images/protectoras/protectoraImg.png" alt=""></a>
					</div>
				</div>
			</div>
			<hr>

			<!-- Pagination -->
			<ul class="pagination justify-content-center">
				<li class="page-item"><a class="page-link" href="#"
					aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						<span class="sr-only">Previous</span>
				</a></li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#"
					aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span
						class="sr-only">Next</span>
				</a></li>
			</ul>
		</div>
		<!-- /.container -->
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
</body>
</html>