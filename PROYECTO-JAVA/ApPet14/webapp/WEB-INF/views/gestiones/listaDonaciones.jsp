<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Mis donaciones</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/gestiones" var="urlRoot"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
</head>

<body id="page-top">
	<!-- Modales -->
	<jsp:include page="../includes/modales/modalExperiencia.jsp" />
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<!-- Page Heading -->
			<hr>
			<h1 class="my-4 text-center" style="color: red">PÁGINA EN
				CONSTRUCCIÓN</h1>
			<hr>
			<hr>
			<h1 class="my-4 text-center">
				<img style="margin-bottom: 15px; width: 60px; height: 40px;"
					class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
					title="huella"> Mis Donaciones <small>en ApPet</small> <img
					style="margin-bottom: 15px; width: 60px; height: 40px;"
					class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
					title="huella">
			</h1>
			<hr>
			<h3>Donaciones pendientes:</h3>
			<table style="font-size: 0.8rem;"
				class="table table-hover table-striped table-bordered">
				<thead class="thead-light ">
					<tr>
						<th>Cantidad:</th>
						<th>Protectora de destino:</th>
						<th>Fecha de realización:</th>
						<th>Estado:</th>
					</tr>
				</thead>
			</table>
			<h3>Donaciones realizadas:</h3>
			<table style="font-size: 0.8rem;"
				class="table table-hover table-striped table-bordered">
				<thead class="thead-light ">
					<tr>
						<th>Cantidad:</th>
						<th>Protectora de destino:</th>
						<th>Fecha de realización:</th>
						<th>Fecha de aprobación:</th>
						<th>Estado:</th>
						<th>Volver a realizar la donación:</th>
					</tr>
				</thead>
			</table>
			<hr>

			<!-- Pagination -->
			<ul class="pagination justify-content-center">
				<li class="page-item"><a class="page-link" href="#"
					aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						<span class="sr-only">Previous</span>
				</a></li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#"
					aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span
						class="sr-only">Next</span>
				</a></li>
			</ul>
		</div>
		<!-- /.container -->
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
</body>
</html>