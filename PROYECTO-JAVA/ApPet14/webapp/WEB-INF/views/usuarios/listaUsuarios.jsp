<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Listado de usuarios</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/usuarios/crearUsuario" var="urlCreateUser"></spring:url>
<spring:url value="/usuarios/crearProtectora" var="urlCreateShelterNA"></spring:url>
<spring:url value="/protectoras/crear" var="urlCreateShelterA"></spring:url>
<spring:url value="/usuarios/crearAdministrador" var="urlCreateAdmin"></spring:url>
<spring:url value="/usuarios/editar" var="urlEdit"></spring:url>
<spring:url value="/usuarios/eliminar" var="urlDelete"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<!-- Modal eliminar-->
<script src="${urlPublic}/js/cambiarConfirm.js"></script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container" style="margin-bottom: 40px;">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/banners/banner7.jpg)">

				<h1 class="display-3" style="color: white">Listado de usuarios
					y roles</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>
			<div class="row" style="margin: 10px;">
				<a href="${urlCreateUser}"
					style="margin-right: 10px; color: white !important; background-color: #0F1626;"
					class="btn btn-success" role="button" title="Nuevo usuario">Nuevo
					usuario</a> <a href="${urlCreateShelterNA}"
					style="margin-right: 10px; color: white !important; background-color: #0F1626;"
					class="btn btn-success" role="button" title="Nueva protectora">Nueva
					protectora (NA) <span style="font-size: 12px">No aprobada</span>
				</a> <a href="${urlCreateShelterA}"
					style="margin-right: 10px; color: white !important; background-color: #0F1626;"
					class="btn btn-success" role="button" title="Nueva protectora">Nueva
					protectora (A)<span style="font-size: 12px"> Aprobada</span>
				</a> <a href="${urlCreateAdmin}"
					style="margin-right: 10px; color: white !important; background-color: #0F1626;"
					class="btn btn-success" role="button" title="Nueva protectora">Nuevo
					administrador</a>
			</div>
			<hr>
			<h3 class="blog-title">Usuarios disponibles con rol de USUARIO</h3>
			<hr>
			<div class="table-responsive">
				<table style="font-size: 0.8rem;"
					class="table table-hover table-striped table-bordered">
					<thead class="thead-light ">
						<tr>
							<th>Nick</th>
							<th>Nombre</th>
							<th>Apellidos</th>
							<th>Email</th>
							<th>G�nero</th>
							<th>Localidad</th>
							<th>Imagen</th>
							<th>Fecha de nacimiento <span style="font-size: 10px">(dd-MM-yyyy)</span></th>
							<th>Fecha de creaci�n <span style="font-size: 9px">(dd-MM-yyyy
									HH:mm:ss)</span></th>
							<th>Estado</th>
							<th>Opciones</th>
						</tr>
					</thead>

					<c:forEach var="usuario" items="${usuarios}">
						<tr>
							<th scope="row">${usuario.username}</th>
							<td>${usuario.nombre}</td>
							<td>${usuario.apellidos}</td>
							<td>${usuario.email}</td>
							<td>${usuario.genero}</td>
							<td>${usuario.localidad}</td>
							<td>${usuario.imagen}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${usuario.fechaNacimiento}" /></td>
							<!-- <td>${usuario.fechaNacimiento}</td> -->
							<td><fmt:formatDate pattern="dd-MM-yyyy hh:mm:ss"
									value="${usuario.fechaCreacion}" /></td>
							<c:choose>
								<c:when test="${usuario.estado==1}">
									<td><span style="color: #8FC33A; font-weight: 600;"
										class="label label-success">Activo</span></td>
								</c:when>
								<c:otherwise>
									<td><span style="color: #E24E42; font-weight: 600;"
										style="background-color: #E24E42" class="label label-danger">Inactivo</span></td>
								</c:otherwise>
							</c:choose>
							<td><a
								href="${urlEdit}?id=${usuario.id}&username=${usuario.username}"
								class="btn btn-success btn-sm" role="button" title="Edit"><span
									style="color: white !important;" class="fas fa-pencil-alt"></span></a>
								<a
								href="${urlDelete}?id=${usuario.id}&username=${usuario.username}"
								data-confirm="�Est�s seguro de querer eliminar aL USUARIO: ${usuario.username}?"
								class="btn btn-danger btn-sm" role="button" title="Eliminar"><span
									style="color: white !important;" class="fas fa-trash-alt"></span></a></td>
						</tr>
					</c:forEach>
				</table>

			</div>
			<hr>
			<hr>
			<h3 class="blog-title">Usuarios disponibles con rol de
				PROTECTORA-NA (Not aproved yet)</h3>
			<hr>
			<label class="badgetForm" for="exampleFormControlSelect1"><strong>(*)
					NOTA:</strong> Al editar y guardar una protectora no aprobada, esta se
				aprobar� automaticamente, cambiando su rol de USUARIO a PROTECTORA</label>
			<hr>
			<div class="table-responsive">
				<table style="font-size: 0.8rem;"
					class="table table-hover table-striped table-bordered">
					<thead class="thead-light ">
						<tr>
							<th>Nick</th>
							<th>Nombre</th>
							<th>Apellidos</th>
							<th>Email</th>
							<th>Localidad</th>
							<th>Imagen</th>
							<th>Fecha de nacimiento <span style="font-size: 10px">(dd-MM-yyyy)</span></th>
							<th>Fecha de creaci�n <span style="font-size: 10px">(dd-MM-yyyy
									HH:mm:ss)</span></th>
							<th>Estado</th>
							<th>Opciones</th>
						</tr>
					</thead>

					<c:forEach var="protectoraNA" items="${protectorasNA}">
						<tr>
							<th scope="row">${protectoraNA.username}</th>
							<td>${protectoraNA.nombre}</td>
							<td>${protectoraNA.apellidos}</td>
							<td>${protectoraNA.email}</td>
							<td>${protectoraNA.localidad}</td>
							<td>${protectoraNA.imagen}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${protectoraNA.fechaNacimiento}" /></td>
							<td><fmt:formatDate pattern="dd-MM-yyyy hh:mm:ss"
									value="${protectoraNA.fechaCreacion}" /></td>
							<c:choose>
								<c:when test="${protectoraNA.estado==1}">
									<td><span style="color: #8FC33A; font-weight: 600;"
										class="label label-success">Activo</span></td>
								</c:when>
								<c:otherwise>
									<td><span style="color: #E24E42; font-weight: 600;"
										style="background-color: #E24E42" class="label label-danger">Inactivo</span></td>
								</c:otherwise>
							</c:choose>
							<td><a
								href="${urlEdit}?id=${protectoraNA.id}&username=${protectoraNA.username}"
								class="btn btn-success btn-sm" role="button" title="Edit"><span
									style="color: white !important;" class="fas fa-pencil-alt"></span></a>
								<a
								href="${urlDelete}?id=${protectoraNA.id}&username=${protectoraNA.username}"
								data-confirm="�Est�s seguro de querer eliminar a la PROTECTORA-NA: ${protectoraNA.username}?"
								class="btn btn-danger btn-sm" role="button" title="Eliminar"><span
									style="color: white !important;" class="fas fa-trash-alt"></span></a></td>
						</tr>
					</c:forEach>
				</table>

			</div>
			<h3 class="blog-title">Usuarios disponibles con rol de
				PROTECTORA-A (Aproved)</h3>
			<hr>
			<div class="table-responsive">
				<table style="font-size: 0.8rem;"
					class="table table-hover table-striped table-bordered">
					<thead class="thead-light ">
						<tr>
							<th>Nick</th>
							<th>Nombre</th>
							<th>Apellidos</th>
							<th>Email</th>
							<th>Localidad</th>
							<th>Imagen</th>
							<th>Fecha de nacimiento <span style="font-size: 10px">(dd-MM-yyyy)</span></th>
							<th>Fecha de creaci�n <span style="font-size: 10px">(dd-MM-yyyy
									HH:mm:ss)</span></th>
							<th>Estado</th>
							<!-- <th>Opciones</th>-->
						</tr>
					</thead>

					<c:forEach var="protectora" items="${protectoras}">
						<tr>
							<th scope="row">${protectora.username}</th>
							<td>${protectora.nombre}</td>
							<td>${protectora.apellidos}</td>
							<td>${protectora.email}</td>
							<td>${protectora.localidad}</td>
							<td>${protectora.imagen}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${protectora.fechaNacimiento}" /></td>
							<td><fmt:formatDate pattern="dd-MM-yyyy hh:mm:ss"
									value="${protectora.fechaCreacion}" /></td>
							<c:choose>
								<c:when test="${protectora.estado==1}">
									<td><span style="color: #8FC33A; font-weight: 600;"
										class="label label-success">Activo</span></td>
								</c:when>
								<c:otherwise>
									<td><span style="color: #E24E42; font-weight: 600;"
										style="background-color: #E24E42" class="label label-danger">Inactivo</span></td>
								</c:otherwise>
							</c:choose>
							<!--  <td><a href="${urlEdit}?id=${protectora.id}&username=${protectora.username}"
								class="btn btn-success btn-sm" role="button" title="Edit"><span
									style="color: white !important;" class="fas fa-pencil-alt"></span></a>
								<a href="${urlDelete}?id=${protectora.id}&username=${protectora.username}"
								data-confirm=""
								class="btn btn-danger btn-sm" role="button" title="Eliminar"><span
									style="color: white !important;" class="fas fa-trash-alt"></span></a></td>-->
						</tr>
					</c:forEach>
				</table>

			</div>
			<hr>
			<h3 class="blog-title">Usuarios disponibles con rol de
				ADMINISTRADOR</h3>
			<hr>
			<div class="table-responsive">
				<table style="font-size: 0.8rem;"
					class="table table-hover table-striped table-bordered">
					<thead class="thead-light ">
						<tr>
							<th>Nick</th>
							<th>Nombre</th>
							<th>Apellidos</th>
							<th>Email</th>
							<th>G�nero</th>
							<th>Localidad</th>
							<th>Imagen</th>
							<th>Fecha de nacimiento <span style="font-size: 10px">(dd-MM-yyyy)</span></th>
							<th>Fecha de creaci�n <span style="font-size: 10px">(dd-MM-yyyy
									HH:mm:ss)</span></th>
							<th>Estado</th>
							<th>Opciones</th>
						</tr>
					</thead>

					<c:forEach var="administrador" items="${administradores}">
						<tr>
							<th scope="row">${administrador.username}</th>
							<td>${administrador.nombre}</td>
							<td>${administrador.apellidos}</td>
							<td>${administrador.email}</td>
							<td>${administrador.genero}</td>
							<td>${administrador.localidad}</td>
							<td>${administrador.imagen}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${administrador.fechaNacimiento}" /></td>
							<td><fmt:formatDate pattern="dd-MM-yyyy hh:mm:ss"
									value="${administrador.fechaCreacion}" /></td>
							<c:choose>
								<c:when test="${administrador.estado==1}">
									<td><span style="color: #8FC33A; font-weight: 600;"
										class="label label-success">Activo</span></td>
								</c:when>
								<c:otherwise>
									<td><span style="color: #E24E42; font-weight: 600;"
										style="background-color: #E24E42" class="label label-danger">Inactivo</span></td>
								</c:otherwise>
							</c:choose>
							<td><a
								href="${urlEdit}?id=${administrador.id}&username=${administrador.username}"
								class="btn btn-success btn-sm" role="button" title="Edit"><span
									style="color: white !important;" class="fas fa-pencil-alt"></span></a>
								<a
								href="${urlDelete}?id=${administrador.id}&username=${administrador.username}"
								data-confirm="�Est�s seguro de querer eliminar al ADMINISTRADOR: ${administrador.username}?"
								class="btn btn-danger btn-sm" role="button" title="Eliminar"><span
									style="color: white !important;" class="fas fa-trash-alt"></span></a></td>
						</tr>
					</c:forEach>
				</table>

			</div>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>

</body>
</html>