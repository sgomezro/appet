<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Mensajes disponibles</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/contacto/eliminar" var="urlDelete"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!-- Modal eliminar-->
<script src="${urlPublic}/js/cambiarConfirm.js"></script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container" style="margin-bottom: 40px;">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/banners/banner14.jpg)">
				<h1 class="display-3" style="font-size: 3.9rem;">
					Listado de <br /> mensajes de <br /> usuarios
				</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<div class="text-center">
				<hr>
				<h3>
					<img style="margin-bottom: 15px; width: 60px; height: 40px;"
						class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
						title="huella"> Mensajes disponibles: <img
						style="margin-bottom: 15px; width: 60px; height: 40px;"
						class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
						title="huella">
				</h3>
				<hr>
			</div>
			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>
			<c:forEach items="${ contactos }" var="contacto">
				<div class="row mb-5" style="border: 2px solid #f7f7f7;">
					<div class="col-md-12 mt-2">
						<h4 class="card-title"
							style="padding: 8px; color: white; background-color: #17a2b8; text-align: center">
							Mensaje de: ${contacto.nombre } ${contacto.apellidos }</h4>
						<hr>
						<p>
							<strong>Email:</strong> ${contacto.email }
						</p>
						<p>
							<strong>Tipologia de animales en los que se encuentra
								interesado:</strong> ${contacto.tipos }
						</p>
						<p>
							<strong>Experiencia en ApPet:</strong> ${contacto.experiencia }
						</p>
						<p>
							<strong>Desea recibir notificaciones de:</strong>
							${contacto.notificaciones }
						</p>
						<hr>
						<p>
							<strong>Comentario: </strong> ${contacto.comentarios }
						</p>
						<button type="button" class="btn btn-outline-secondary mb-3">Contactar
							con ${contacto.nombre }</button>
						<a href="${urlDelete}?idContacto=${contacto.id}"
							data-confirm="�Est�s seguro de querer eliminar el mesaneje de ${contacto.nombre }?"
							class="btn btn-danger mb-3" style="color: white !important"
							role="button" title="Eliminar">Eliminar mensaje</a>
					</div>
				</div>
			</c:forEach>


		</div>
		<!-- /container -->
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
</body>
</html>