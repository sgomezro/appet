<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<head>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
	integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
	crossorigin="anonymous">
<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>

</head>

<!-- Menu cualquier usuario sin registrar-->
<sec:authorize access="isAnonymous()">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top"
		style="background-color: #212529 !important">
		<a class="navbar-brand mr-3" href="${urlRoot}index/welcome"><img
			style="width: 205px; height: 55px;" class="card-img-top"
			src=${urlPublic}/images/ApPetLogoBlanco.JPG title="logo">
			APPLICATION</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}usuarios/formUsuarioProtectora">Registrate</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlRoot}formLogin">Iniciar sesi�n</a></li>
			</ul>
		</div>
	</nav>
</sec:authorize>

<!-- Menu usuario registrado y protectoras NA-->
<sec:authorize access="hasAnyAuthority('USUARIO','PROTECTORA-NA')">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top"
		style="background-color: #212529 !important">
		<a class="navbar-brand mr-3" href="${urlRoot}index/welcome"><img
			style="width: 205px; height: 55px;" class="card-img-top"
			src=${urlPublic}/images/ApPetLogoBlanco.JPG title="logo"> |
			USUARIO</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<p class="ml-auto navbar-nav">
				<a class="nav-link text-right" href="${urlRoot}miPerfil/detalle"><img
					style="height: 100%; width: 45px;"
					class="mr-1 img-fluid img-profile rounded-circle img-responsive"
					src=${urlPublic}/images/usuarios/${usr.imagen} alt="Imagen Perfil"></a>
				<a href="${urlRoot}admin/logout" class="nav-link mt-1"><button
						type="button" class="btn btn-info"
						onclick=' return confirm("�Est�s seguro de querer cerrar sesi�n en ApPet?") '>Cerrar
						sesi�n</button></a>
			</p>
		</div>
	</nav>
</sec:authorize>
<!-- Menu protectora-->
<sec:authorize access="hasAnyAuthority('PROTECTORA')">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top"
		style="background-color: #212529 !important">
		<a class="navbar-brand mr-3" href="${urlRoot}index/welcome"><img
			style="width: 205px; height: 55px;" class="card-img-top"
			src=${urlPublic}/images/ApPetLogoBlanco.JPG title="logo"> |
			PROTECTORA</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<p class="ml-auto navbar-nav">
				<a class="nav-link text-right" href="${urlRoot}miPerfil/detalle"><img
					style="height: 100%; width: 45px;"
					class="mr-1 img-fluid img-profile rounded-circle img-responsive"
					src=${urlPublic}/images/protectoras/${usr.imagen}
					alt="Imagen Perfil"></a> <a href="${urlRoot}admin/logout"
					class="nav-link mt-1"><button type="button"
						class="btn btn-info"
						onclick=' return confirm("�Est�s seguro de querer cerrar sesi�n en ApPet?") '>Cerrar
						sesi�n</button></a>

			</p>
		</div>
	</nav>
</sec:authorize>

<!-- Menu administrador-->
<sec:authorize access="hasAnyAuthority('ADMINISTRADOR')">
	<nav class="navbar navbar-expand navbar-dark bg-dark static-top"
		style="background-color: #212529 !important">
		<a class="navbar-brand mr-3" href="${urlRoot}index/welcome"><img
			style="width: 205px; height: 55px;" class="card-img-top"
			src=${urlPublic}/images/ApPetLogoBlanco.JPG title="logo"> |
			ADMINISTRAD<i
			style="margin-right: 2px; margin-left: 2px; font-size: 18px; color: white"
			class="fas fa-cog"></i>R</a>

		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<p class="ml-auto navbar-nav">
				<a class="nav-link text-right" href="${urlRoot}miPerfil/detalle"><img
					style="height: 100%; width: 45px;"
					class="mr-1 img-fluid img-profile rounded-circle img-responsive"
					src=${urlPublic}/images/usuarios/${usr.imagen} alt="Imagen Perfil"></a>
				<a href="${urlRoot}admin/logout" class="nav-link mt-1"><button
						type="button" class="btn btn-info"
						onclick=' return confirm("�Est�s seguro de querer cerrar sesi�n en ApPet?") '>Cerrar
						sesi�n</button></a>
			</p>
		</div>
	</nav>
</sec:authorize>





