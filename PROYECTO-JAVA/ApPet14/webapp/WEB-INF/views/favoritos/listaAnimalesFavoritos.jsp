<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Mis animales favoritos</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/favoritos" var="urlRoot"></spring:url>
<spring:url value="/animales" var="urlAnimales"></spring:url>
<spring:url value="/" var="urlDetalle"></spring:url>
<spring:url value="/favoritos/eliminar" var="urlDelete"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<!-- Modal eliminar-->
<script src="${urlPublic}/js/cambiarConfirm.js"></script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container" style="margin-bottom: 40px;">

			<!-- Page Heading -->
			<div class="text-center">
				<hr>
				<h1 class="my-4">
					Mis favortios <a href="${urlRoot}/listaAnimalesFavoritos"><small>Animales
							|</small></a><a href="${urlRoot}/listaProtectorasFavoritas"><small>
							Protectoras</small></a>
				</h1>
				<hr>
				<img class="card-img-top rounded mx-auto d-block"
					src="${urlPublic}/images/banners/banner12.jpg" alt="">

				<hr>
				<h3>
					<img style="margin-bottom: 15px; width: 60px; height: 40px;"
						class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
						title="huella"> Mis animales favoritos: <img
						style="margin-bottom: 15px; width: 60px; height: 40px;"
						class="card-img-top mr-2" src=${urlPublic}/images/ApPetHuella.JPG
						title="huella">
				</h3>
				<hr>
			</div>
			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>
			<div class="container" style="background-color: #f7f7f7">
				<div class="row">
					<c:forEach items="${favoritos }" var="favorito">
						<div class="col-lg-4 col-sm-6 portfolio-item">
							<div class="card h-100">
								<h4 class="card-title"
									style="padding: 8px; color: white; background-color: #76323F; text-align: center">
									${favorito.animal.nombre }</h4>
								<a class="text-center"><img
									style="height: 175px; width: 280px;" class="card-img-top"
									src=${urlPublic}/images/animales/${favorito.animal.imagen
									} alt=""></a>
								<div class="card-body" style="line-height: 15px">


									<p class="card-text">
										<strong>Tipologia:</strong> ${favorito.animal.tipo }
									</p>
									<p class="card-text">
										<strong>Raza:</strong> ${favorito.animal.raza }
									</p>
									<p class="card-text">
										<strong>Sexo:</strong> ${favorito.animal.sexo }
									</p>
									<p class="card-text">
										<strong>Edad:</strong> ${favorito.animal.edad } a�os
									</p>
									<a
										href="${urlDetalle}detalleAnimal?idAnimal=${favorito.animal.id }&protectora=${favorito.animal.protectora.nombreProtectora }"><button
											class="btn btn-info card-text">+ info de
											${favorito.animal.nombre }</button></a>
									<hr>
									<p class="card-text">
										<strong>Pertenece a:</strong>
										${favorito.animal.protectora.nombreProtectora}
									</p>
									<a
										href="${urlDetalle}detalleProtectora?idProtectora=${favorito.animal.protectora.idProtectora}&idAnimal=${favorito.animal.id }"><button
											class="btn btn-outline-secondary">+ info de
											${favorito.animal.protectora.nombreProtectora}</button></a>
									<hr>
									<a href="${urlDelete}?idFavorito=${favorito.idFavorito}"
										data-confirm="�Est�s seguro de querer eliminar a ${favorito.animal.nombre} de tus favoritos?"><button
											class="btn btn-danger">Eliminar de: "Mis animales
											favoritos"</button></a>
								</div>
							</div>
						</div>
					</c:forEach>

				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>

</body>
</html>