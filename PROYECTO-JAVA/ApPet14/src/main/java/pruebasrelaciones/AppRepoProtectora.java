package pruebasrelaciones;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.repository.ProtectorasRepository;

public class AppRepoProtectora {
	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		// Recuperar todas las entidades de tipo protectora

		ProtectorasRepository repositorioProtectoras = context.getBean("protectorasRepository",
				ProtectorasRepository.class);
		List<Protectora> listaProtectoras = repositorioProtectoras.findAll();

		for (Protectora protectora : listaProtectoras) {
			System.out.println(protectora);
		}
		context.close();
	}
}
