package pruebasrelaciones;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.repository.AnimalesRepository;

public class AppFindAll {
	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		// Recuperar todas las entidades de tipo animal
		AnimalesRepository repositorioAnimales = context.getBean("animalesRepository", AnimalesRepository.class);

		List<Animal> listaAnimales = repositorioAnimales.findAll();
		for (Animal animal : listaAnimales) {
			System.out.println(animal);
		}

		context.close();

	}
}
