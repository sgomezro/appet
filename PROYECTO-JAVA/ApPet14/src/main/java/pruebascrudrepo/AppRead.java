package pruebascrudrepo;

import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

public class AppRead {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		// Operacion CRUD - Read (Metodo findbyId del repositorio)
		// Optional evita la excepcion de null en caso de que no se encuentre el
		// registro

		Optional<Noticia2> noticia = repo.findById(1);
		// Si no lo encuentra devuelve Optiona empty, como en el siguiente caso:
		// Optional<Noticia2> noticia = repo.findById(2);
		// System.out.println(noticia);
		// Si queremos obtener solo el objeto noticia, si lo hacemos sin el get()
		// obtenemos el contendor Optional de noticia
		System.out.println(noticia.get());
		context.close();

	}
}
