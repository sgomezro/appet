package pruebascrudrepo;

import java.util.LinkedList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

public class AppFindAllById {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);

		// Recuperar varios registros por Id (metodo findAllById del repositorio)
		List<Integer> ids = new LinkedList<Integer>();
		ids.add(2);
		ids.add(5);
		ids.add(8);

		// La interfaz List internamente extiende de la interfaz Iterable, por lo que es
		// valido
		Iterable<Noticia2> it = repo.findAllById(ids);
		// Asi estaremos buscando las entidades con id 2,5 y 8
		for (Noticia2 n : it) {
			System.out.println(n);
		}
		context.close();

	}
}
