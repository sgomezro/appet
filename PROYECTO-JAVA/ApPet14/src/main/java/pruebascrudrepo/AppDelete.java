package pruebascrudrepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.repository.NoticiasRespository;

public class AppDelete {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);

		// Operacion CRUD - Delete (metodo deleteById del reposiorio)
		// Si le pasamos un id que no eiste nos lanza la excepcion
		// "EmptyResultDataAccessException"
		int idNoticia = 1;
		// repo.deleteById(idNoticia);

		// Comprobamos que el id existe antes de intentar eliminarlo, para que no salte
		// la excepcion
		if (repo.existsById(idNoticia)) {
			repo.deleteById(idNoticia);
		}
		context.close();

	}
}
