package pruebascrudrepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.repository.NoticiasRespository;

public class AppDeleteAll {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);

		// Borrar todos los registros (Metodo deleteAll del repositorio)
		// ALERTA ES UN METODO MUY PELIGROSO!!!!!!!!
		repo.deleteAll();
		context.close();

	}
}
