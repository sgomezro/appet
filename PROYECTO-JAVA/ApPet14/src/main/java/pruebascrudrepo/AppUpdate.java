package pruebascrudrepo;

import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

public class AppUpdate {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		// Operacion CRUD - Update (metodo save del repositorio)

		// 1. Primero buscamos la entidad que vamos a modificar(findbyId)
		Optional<Noticia2> optional = repo.findById(1);

		// 2. Modificamos la entidad y a guardamos
		if (optional.isPresent()) {
			Noticia2 noticia = optional.get();
			noticia.setEstado("Inactiva");

			// Spring diferencia si va a hacer un INSERT o un UPDATE por el id,si el id es
			// distinto de 0 realiza un update
			repo.save(noticia);
		}
		context.close();

	}
}
