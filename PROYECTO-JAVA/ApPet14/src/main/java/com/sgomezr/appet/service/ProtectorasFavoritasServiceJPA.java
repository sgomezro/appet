package com.sgomezr.appet.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.ProtectoraFavorita;
import com.sgomezr.appet.repository.ProtectorasFavoritasRepository;
import com.sgomezr.appet.repository.ProtectorasRepository;

/**
 * Servicio de protectoras favoritas
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class ProtectorasFavoritasServiceJPA implements IProtectorasFavoritasService {

	final static Logger logger = Logger.getLogger(ProtectorasFavoritasServiceJPA.class);

	@Autowired
	private ProtectorasFavoritasRepository protectorasFavoritasRepository;

	@Autowired
	private ProtectorasRepository protectorasRepository;

	/**
	 * M�todo encargado de agregar una nueva protectora favorita a la tabla de
	 * protectoras favoritas de BD.
	 * 
	 * @param protectoraFavorita:
	 *            Objeto protectora favorita que se quiere guardar en BD.
	 */
	@Override
	public void agregar(ProtectoraFavorita protectoraFavorita) {
		protectorasFavoritasRepository.save(protectoraFavorita);
	}

	/**
	 * M�todo encargado de determinar si una protectora ya existe en la lista de
	 * protectoras favoritas de un determinado usuario.
	 * 
	 * @param idProtectora:
	 *            Id de la protectora de la que queremos saber si pertenece o no al
	 *            listado de protectoras favoritas de un determinado usuario.
	 * @param idUsuario:
	 *            Id del usuario del que estamos tomando la lista de protectoras
	 *            favoritas.
	 * @return resultado en booleano.
	 */
	@Override
	public Boolean existeProtectoraFavorita(int idProtectora, int idUsuario) {
		Optional<Protectora> protectora = protectorasRepository.findById(idProtectora);
		ProtectoraFavorita protectoraFavorita = protectorasFavoritasRepository
				.findByProtectoraFavAndIdUsuario(protectora, idUsuario);
		if (protectoraFavorita == null) {
			return false;
		}
		if (((protectoraFavorita.getProtectoraFav()).getIdProtectora() == (idProtectora))
				&& (protectoraFavorita.getIdUsuario() == idUsuario)) {
			return true;
		}
		return false;
	}

	/**
	 * M�todo encargado de buscar el listado de protectoras favoritas por el id de
	 * un determinado usuario.
	 * 
	 * @param idUsuario: Id del usuario del cual se quiere buscar su listado de
	 *         protectoras favoritas.
	 * @return Listado de protectoras favoritas por el id de un determinado usuario.
	 */
	@Override
	public List<ProtectoraFavorita> buscarProtectorasFavoritasPorIdUsuario(int idUsuario) {
		return protectorasFavoritasRepository.findByIdUsuario(idUsuario);
	}

	/**
	 * M�todo encargado de eliminar una protectora favorita en funci�n del id de la
	 * protectora.
	 * 
	 * @param idProtectora:
	 *            Id de la protectora que se quiere eliminar.
	 */
	@Override
	public void eliminarProtectoraFavorita(int idProtectora) {
		protectorasFavoritasRepository.deleteById(idProtectora);
	}

}
