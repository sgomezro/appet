package com.sgomezr.appet.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.ProtectoraFavorita;

public interface ProtectorasFavoritasRepository extends JpaRepository<ProtectoraFavorita, Integer> {

	ProtectoraFavorita findByProtectoraFavAndIdUsuario(Optional<Protectora> protectora, int idUsuario);

	List<ProtectoraFavorita> findByIdUsuario(int idUsuario);
}
