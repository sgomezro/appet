package com.sgomezr.appet.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.repository.ProtectorasRepository;

/**
 * Servicio de protectoras.
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class ProtectoraServiceJPA implements IProtectorasService {

	final static Logger logger = Logger.getLogger(ProtectoraServiceJPA.class);

	@Autowired
	private ProtectorasRepository protectorasRepository;

	/**
	 * M�todo encargado de buscar todas las protectoras en la tabla protectoras de
	 * BD.
	 * 
	 * @return Listado de todas las protectoras almacenadas en BD.
	 */
	@Override
	public List<Protectora> buscarTodasLasProtectoras() {
		return protectorasRepository.findAll();
	}

	/**
	 * M�todo encargado de insertar un objeto protectora en la tabla de protectoras
	 * de BD.
	 * 
	 * @param protectora:
	 *            Objeto protectora que se quiere guardar en BD.
	 */
	@Override
	public void insertar(Protectora protectora) {
		protectorasRepository.save(protectora);
	}

	/**
	 * M�todo encargado de buscar una protectora por su id en BD.
	 * 
	 * @param idProtectora:
	 *            Id de la protectora que estamos buscando
	 * @return Protectora con el id que le hemos pasado en el caso de que exista en
	 *         BD y null en el caso de que no exista.
	 */
	@Override
	public Protectora buscarProtectoraPorId(int idProtectora) {
		Optional<Protectora> protectora = protectorasRepository.findById(idProtectora);
		if (protectora.isPresent()) {
			return protectora.get();
		}
		return null;
	}

	/**
	 * M�todo que se encarga de buscar una protectora por su nombre.
	 * 
	 * @param nombreProtectora:
	 *            nombre de la protectora que estamos buscando en BD.
	 * @return Objeto protectora buscada.
	 */
	@Override
	public Protectora buscarProtectoraPorNombre(String nombreProtectora) {
		Protectora protectora = protectorasRepository.findByNombreProtectora(nombreProtectora);
		return protectora;
	}

	/**
	 * M�todo que se encarga de buscar el id de una protectora con un determinado
	 * nombre.
	 * 
	 * @param nombreProtectora:
	 *            nombre de la protectora que estamos buscando en BD.
	 * @return idProtectora: id de la protectora buscada por su nombre.
	 */
	@Override
	public int buscaIdProtectora(String nombreProtectora) {
		int idProtectora = 0;
		Protectora protectora = protectorasRepository.findByNombreProtectora(nombreProtectora);
		idProtectora = protectora.getIdProtectora();
		return idProtectora;
	}

	/**
	 * M�todo encargado de devolver un listado con todas las localidades de Espa�a.
	 * 
	 * @return Listado con las localidades Espa�olas.
	 */
	@Override
	public List<String> buscarLocalidades() {

		List<String> localidades = new LinkedList<>();
		localidades.add("A Coru�a");
		localidades.add("�lava");
		localidades.add("Albacete");
		localidades.add("Alicante");
		localidades.add("Almer�a");
		localidades.add("Asturias");
		localidades.add("�vila");
		localidades.add("Badajoz");
		localidades.add("Islas Baleares");
		localidades.add("Barcelona");
		localidades.add("Burgos");
		localidades.add("C�ceres");
		localidades.add("C�diz");
		localidades.add("Cantabria");
		localidades.add("Castell�n");
		localidades.add("Ciudad Real");
		localidades.add("C�rdoba");
		localidades.add("Cuenca");
		localidades.add("Girona");
		localidades.add("Granada");
		localidades.add("Guadalajara");
		localidades.add("Guip�zcoa");
		localidades.add("Huelva");
		localidades.add("Huesca");
		localidades.add("Ja�n");
		localidades.add("La Rioja");
		localidades.add("Las Palmas");
		localidades.add("Le�n");
		localidades.add("Lleida");
		localidades.add("Lugo");
		localidades.add("Madrid");
		localidades.add("M�laga");
		localidades.add("Murcia");
		localidades.add("Navarra");
		localidades.add("Orense");
		localidades.add("Palencia");
		localidades.add("Pontevedra");
		localidades.add("Salamanca");
		localidades.add("Segovia");
		localidades.add("Sevilla");
		localidades.add("Soria");
		localidades.add("Tarragona");
		localidades.add("Santa Cruz de Tenerife");
		localidades.add("Teruel");
		localidades.add("Toledo");
		localidades.add("Valencia");
		localidades.add("Valladolid");
		localidades.add("Vizcaya");
		localidades.add("Zamora");
		localidades.add("Zaragoza");
		return localidades;
	}

	/*
	 * M�todo encargado de eliminar una protectora con un determinado id de la BD.
	 * 
	 * @param idProtectora: Id de la protectora que queremos eliminar de BD.
	 */
	@Override
	public void eliminar(int idProtectora) {
		protectorasRepository.deleteById(idProtectora);
	}

	/**
	 * M�todo encargado de buscar todas las protectoras de una determinada p�gina.
	 * 
	 * @param page:
	 *            Objeto pageable.
	 * @return Page
	 */
	@Override
	public Page<Protectora> buscarTodasPaginacion(Pageable page) {
		return protectorasRepository.findAll(page);
	}

	/**
	 * M�todo encargado de generar un objeto protectora a partir de un objeto
	 * Usuario.
	 * 
	 * @param usuario:
	 *            Objeto usuario con cuyos datos se quiere generar un objeto
	 *            Protectora.
	 * @return Objeto protectora generado.
	 */
	@Override
	public Protectora generarObjetoProtectora(Usuario usuario) {

		Protectora protectoraGenerada = new Protectora();
		protectoraGenerada.setNombreProtectora(usuario.getUsername());
		protectoraGenerada.setNombre(usuario.getNombre());
		protectoraGenerada.setApellidos(usuario.getApellidos());
		protectoraGenerada.setEmail(usuario.getEmail());
		protectoraGenerada.setPassword(usuario.getPassword());
		protectoraGenerada.setComprobarPass(usuario.getComprobarPass());
		protectoraGenerada.setFechaNacimiento(usuario.getFechaNacimiento());
		protectoraGenerada.setLocalidad(usuario.getLocalidad());
		protectoraGenerada.setLogoProtectora(usuario.getImagen());
		protectoraGenerada.setFechaCreacion(usuario.getFechaCreacion());
		if (usuario.getEstado() == 1) {
			protectoraGenerada.setEstado("Activa");
		} else {
			protectoraGenerada.setEstado("Inactiva");
		}
		return protectoraGenerada;
	}

	/**
	 * M�todo encargado de validar algunos campos del formulario de crear/editar
	 * protectora.
	 * 
	 * @param result:
	 *            Objeto BindingResult
	 * @param protectora:
	 *            Objeto protectora del cual se validan los campos en el formulario.
	 */
	@Override
	public void validarCampos(BindingResult result, Protectora protectora) {
		if (protectora.getPassword().equals(protectora.getComprobarPass())) {
			logger.info("Sin errores en contrase�as");
		} else {
			logger.info("Con errores en contrase�as");
			ObjectError error = new ObjectError("password", "Contrase�a: Las contrase�as no son iguales");
			result.addError(error);
		}
		if ((protectorasRepository.findByNombreProtectora(protectora.getNombreProtectora())) == null) {
			logger.info("Sin errores en nombreProtectora");
		} else {
			if ((protectora.getNombreProtectora()).equalsIgnoreCase(protectorasRepository
					.findByNombreProtectora(protectora.getNombreProtectora()).getNombreProtectora())) {
				logger.info("errores en nombreProtectora");
				ObjectError error = new ObjectError("nombreProtectora",
						"Nombre de la protectora: Ya existe una protectora con el mismo nombre registrada en ApPet");
				result.addError(error);
			}
		}
		if ((protectorasRepository.findByEmail(protectora.getEmail())) == null) {
			logger.info("Sin errores en email");
		} else {
			if ((protectora.getEmail())
					.equalsIgnoreCase(protectorasRepository.findByEmail(protectora.getEmail()).getEmail())) {
				logger.info("errores en email");
				ObjectError error = new ObjectError("email",
						"Email: El email de la protectora ya se encuentra registrado en ApPet, pruebe con otro");
				result.addError(error);
			}
		}
	}

	/**
	 * M�todo encargado de buscar un listado de objetos protectoras por su
	 * localidad.
	 * 
	 * @param localidad:
	 *            Localidad de b�squeda.
	 * @return Listado de objetos protectoras con una determinada localidad.
	 */
	@Override
	public List<Protectora> buscarProtectoraPorLocalidad(String localidad) {
		List<Protectora> listaProtectorasPorLocalidad = protectorasRepository.findByLocalidad(localidad);

		return listaProtectorasPorLocalidad;
	}
}
