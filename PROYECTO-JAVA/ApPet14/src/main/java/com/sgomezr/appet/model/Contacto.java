package com.sgomezr.appet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Clase modelo Contacto
 * 
 * @author Sandra Gomez Roman
 *
 */
@Entity
@Table(name = "contacto")
public class Contacto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_contacto")
	private int id;

	@Column(name = "nombre_contacto")
	private String nombre;

	@Column(name = "apellidos_contacto")
	private String apellidos;

	@Column(name = "email_contacto")
	private String email;

	@Column(name = "tipos_contacto")
	private String tipos;

	@Column(name = "experiencia_contacto")
	private String experiencia;

	@Column(name = "notificaciones_contacto")
	private String notificaciones;

	@Column(name = "comentarios_contacto")
	private String comentarios;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipos() {
		return tipos;
	}

	public void setTipos(String tipos) {
		this.tipos = tipos;
	}

	public String getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(String experiencia) {
		this.experiencia = experiencia;
	}

	public String getNotificaciones() {
		return notificaciones;
	}

	public void setNotificaciones(String notificaciones) {
		this.notificaciones = notificaciones;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	@Override
	public String toString() {
		return "Contacto [id=" + id + ", nombre=" + nombre + ", apellidos=" + apellidos + ", email=" + email
				+ ", tipos=" + tipos + ", experiencia=" + experiencia + ", notificaciones=" + notificaciones
				+ ", comentarios=" + comentarios + "]";
	}

}
