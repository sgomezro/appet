package com.sgomezr.appet.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Clase modelo Usuario
 * 
 * @author Sandra Gomez Roman
 *
 */

@Entity
@Table(name = "usuarios")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	private int id;

	@Column(name = "username")
	private String username;

	@Column(name = "nombre_usuario")
	private String nombre;

	@Column(name = "apellidos_usuario")
	private String apellidos;

	@Column(name = "email_usuario")
	private String email;

	@Column(name = "password_usuario")
	private String password;

	@Column(name = "comprobarPass_usuario")
	private String comprobarPass;

	@Column(name = "genero_usuario")
	private String genero;

	@Column(name = "fechaNacimiento_usuario")
	private Date fechaNacimiento;

	@Column(name = "edad_usuario")
	private int edad;

	@Column(name = "localidad_usuario")
	private String localidad;

	@Column(name = "imagen_usuario")
	private String imagen;

	@Column(name = "fechaCreacion_usuario")
	private Date fechaCreacion;

	@Column(name = "estado_usuario")
	private int estado;

	public Usuario() {
		this.fechaCreacion = new Date(); // Por default, la fecha del sistema
		this.estado = 1;
		this.imagen = "usuario.png";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getComprobarPass() {
		return comprobarPass;
	}

	public void setComprobarPass(String comprobarPass) {
		this.comprobarPass = comprobarPass;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(Date fechaNacimiento) {
		LocalDate hoy = LocalDate.now();
		LocalDate nacimiento = fechaNacimiento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int edad = (int) ChronoUnit.YEARS.between(nacimiento, hoy);
		this.edad = edad;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", username=" + username + ", nombre=" + nombre + ", apellidos=" + apellidos
				+ ", email=" + email + ", password=" + password + ", comprobarPass=" + comprobarPass + ", genero="
				+ genero + ", fechaNacimiento=" + fechaNacimiento + ", edad=" + edad + ", localidad=" + localidad
				+ ", imagen=" + imagen + ", fechaCreacion=" + fechaCreacion + ", estado=" + estado + "]";
	}

}
