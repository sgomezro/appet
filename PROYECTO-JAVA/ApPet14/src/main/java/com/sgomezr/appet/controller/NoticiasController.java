package com.sgomezr.appet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.service.INoticiasService;
import com.sgomezr.appet.util.Utileria;

/**
 * Noticias Controller: Controlador encargado de la gesti�n del m�dulo de
 * noticias de ApPet, las noticias aqu� gestionados se mostrar�n en la p�gina
 * principal de la aplicaci�n.
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
@RequestMapping("/noticias")
public class NoticiasController {

	final static Logger logger = Logger.getLogger(NoticiasController.class);

	@Autowired
	private INoticiasService serviceNoticias;

	/**
	 * M�todo encargado de renderizar la vista donde se muestra la lista de todas
	 * las noticias publicadas en ApPet por los administradores.
	 * 
	 * @param model:
	 *            Model: Objeto de la interfaz Model, al que se le a�aden los
	 *            objetos que se quieren representar en la vista
	 * @return String ruta
	 */

	@GetMapping("/listaNoticias")
	public String listaNoticias(Model model) {
		List<Noticia> noticias = serviceNoticias.buscarTodasLasNoticias();
		model.addAttribute("noticias", noticias);
		return "noticias/listaNoticias";
	}

	/**
	 * M�todo encargado de renderiar el formulario para crear una nueva noticia en
	 * la aplicaci�n.
	 * 
	 * @param noticia:Objeto
	 *            Noticia, modelAttribute nos permite realizar un binding de los
	 *            datos que tenemos en el formulario de "crear nueva notia" con la
	 *            capa de backend, es decir /crear asignar� un objeto Noticia al
	 *            modelo.
	 * @return String ruta
	 */
	@GetMapping(value = "/crear")
	public String crear(@ModelAttribute Noticia noticia) {
		return "noticias/formNoticia";
	}

	/**
	 * M�todo cuya funci�n principal es guardar los datos introudcidos en el
	 * formulario de nueva noticia en la tabla de noticias de la base de datos. A su
	 * vez, dicho m�todo se encarga de realizar la validaci�n de los campos del
	 * formularo, si estos pasan la validaci�n se proceder� al guardado de los datos
	 * de dicha noticia en BD, si esto no ocurriese, se volver�a a renderizar la
	 * pagina de formulario indicando los errores encontrados y los campos a los que
	 * corresponden. Por otro lado, este m�todo se encarga de setear un nombre a la
	 * imagenes, el cual generado con la combinaci�n de un conjunto de caracteres
	 * aleatorios + el nombre del archivo de imagen subido y guardarla en su carpeta
	 * correspondiente.
	 * 
	 * @param noticia:
	 *            Objeto Noticia
	 * @param result:
	 *            Interfaz BindingResult, define los resultados del dataBinding para
	 *            el objeto noticia que se esta creando (valida que los campos del
	 *            formulario sean del tipo de dato que deben ser o esten definidos
	 *            de forma correcta)
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @param multiPart:
	 *            Objeto MultipartFile, se trata de un archivo de imagen, en este
	 *            caso la imagen de la noticia, si existiese.
	 * @param request:
	 *            Objeto HttpServletRequest.
	 * @return String ruta
	 */
	@PostMapping(value = "/guardar")
	public String guardar(@ModelAttribute Noticia noticia, BindingResult result, RedirectAttributes atributos,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request) {

		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "noticias/formNoticia";
		}
		String folder = "noticias";
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request, folder);
			noticia.setImagen(nombreImagen);
		}
		logger.info("Recibiendo objeto noticia" + noticia);
		logger.info("Elementos en la lista antes de la inserci�n: " + serviceNoticias.buscarTodasLasNoticias().size());
		serviceNoticias.insertarNoticia(noticia);
		logger.info(
				"Elementos en la lista despues de la inserci�n: " + serviceNoticias.buscarTodasLasNoticias().size());
		atributos.addFlashAttribute("mensaje", "El noticia ha sido creado y guardado correctamente");
		return "redirect:/noticias/listaNoticias";
	}

	/**
	 * M�todo encargado de editar un determinada noticia de la base de datos (se
	 * encarga de hacer un update en BD).
	 * 
	 * @param idNoticia:
	 *            Id de la noticia del que se quiere editar sus datos.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @return String ruta.
	 */
	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idNoticia") int idNoticia, Model model) {
		Noticia noticia = serviceNoticias.buscarNoticiaPorId(idNoticia);
		model.addAttribute("noticia", noticia);
		return "noticias/formNoticia";
	}

	/**
	 * M�todo encargado de eliminar una noticia de la BD.
	 * 
	 * @param idNoticia:
	 *            Id de la noticia que se quiere eliminar.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta.
	 */
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idNoticia") int idNoticia, Model model, RedirectAttributes atributos) {
		serviceNoticias.eliminar(idNoticia);
		atributos.addFlashAttribute("mensaje", "La noticia fue eliminada correctamente");
		return "redirect:/noticias/listaNoticias";
	}

}