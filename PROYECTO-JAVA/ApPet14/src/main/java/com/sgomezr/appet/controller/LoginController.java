package com.sgomezr.appet.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Login Controler: Controlador encargado de la gestión del acceso a la
 * aplicación.
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
@RequestMapping("/admin")
public class LoginController {

	final static Logger logger = Logger.getLogger(LoginController.class);

	/**
	 * Método encargado de renderizar la vista principal de la aplicación.
	 * 
	 * @return String ruta
	 */
	@GetMapping(value = "/index")
	public String mostrarPrinciapalAdmin() {
		return "/indexPage";
	}

	/**
	 * Método encargado de realizar el logout de la aplicación.
	 * 
	 * @param request
	 *            Objeto HttpServletRequest
	 * @return String ruta
	 */
	@GetMapping(value = "/logout")
	public String logout(HttpServletRequest request) {
		SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
		logoutHandler.logout(request, null, null);
		return "redirect:/formLogin";
	}
}