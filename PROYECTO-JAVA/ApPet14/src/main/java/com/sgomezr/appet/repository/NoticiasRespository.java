package com.sgomezr.appet.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgomezr.appet.model.Noticia2;

@Repository
// public interface NoticiasRespository extends CrudRepository<Noticia2,
// Integer> {
public interface NoticiasRespository extends JpaRepository<Noticia2, Integer> {

	// select * from Noticias
	List<Noticia2> findBy();

	// El nombre del metodo tiene que ser findBy + el nombre del atributo en la
	// clase noticia
	// y el atributo () el nombre del atributo en la clase noticia

	// select * from Noticias where estado = ?
	List<Noticia2> findByEstado(String estado);

	// select * from Noticias where fechaPublicacion = ?
	List<Noticia2> findByFechaPublicacion(Date fechaPublicacion);

	// select * from Noticias where estado = ? and fechaPublicacion = ?
	// Cuando usamos la palabra reservada AND, los dos criterios se tienen que
	// cumplir
	// Si queremos buscar por un 3� criterio solo tendriamosa que a�adir otro And
	List<Noticia2> findByEstadoAndFechaPublicacion(String estado, Date fechaPublicacion);

	// select * from Noticias where estado = ? or fechaPublicacion = ?
	// Cuando usamos la palabra reservada OR, nos va a regresar registros aunque
	// solo se cumpla una condicion
	// Si queremos buscar por un 3� criterio solo tendriamosa que a�adir otro Or
	List<Noticia2> findByEstadoOrFechaPublicacion(String estado, Date fechaPublicacion);

	// select * from Noticias where fechaPublicacion between ? and ?
	// Buscamos los registros entre 2 fechas de publicacion (ambos incluidos)
	List<Noticia2> findByFechaPublicacionBetween(Date fechaPublicacion1, Date fechaPublicacion2);

	// select * from Noticias where id between ? and ?
	// Buscamos los registros entre 2 ids (ambos incluidos)
	List<Noticia2> findByIdBetween(int n1, int n2);

}
