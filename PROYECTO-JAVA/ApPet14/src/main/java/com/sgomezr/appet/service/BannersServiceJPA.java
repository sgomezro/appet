package com.sgomezr.appet.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.repository.BannersRepository;

/**
 * Servicio de Banners
 * 
 * @author Sandra Gomez Roman
 *
 */
@Service
public class BannersServiceJPA implements IBannersService {

	final static Logger logger = Logger.getLogger(BannersServiceJPA.class);

	@Autowired
	private BannersRepository bannersRepository;

	/**
	 * M�todo que se encarga de guardar un banner el base de datos.
	 * 
	 * @param banner:
	 *            Objeto banner que se quiere guardar.
	 */
	@Override
	public void insertar(Banner banner) {
		bannersRepository.save(banner);
	}

	/**
	 * M�todo encargado de buscar un banner por su id en BD.
	 * 
	 * @param idBanner:
	 *            Id del banner que se esta buscando.
	 * @return: Objeto banner que se esta buscando en el caso de que exista, si no
	 *          devolver� null.
	 */
	@Override
	public Banner buscarBannerPorId(int idBanner) {
		Optional<Banner> banner = bannersRepository.findById(idBanner);
		if (banner.isPresent()) {
			return banner.get();
		}
		return null;
	}

	/**
	 * M�todo que se encarga de buscar todos los banners guardados en BD.
	 * 
	 * @return: Lista de con todos los banners.
	 */
	@Override
	public List<Banner> buscarTodos() {
		return bannersRepository.findAll();
	}

	/**
	 * M�todo que se encarga de eliminar un determinado banner por su id.
	 * 
	 * @param idBanner:
	 *            id del banner que se quiere eliminar de BD.
	 */
	@Override
	public void eliminar(int idBanner) {
		bannersRepository.deleteById(idBanner);
	}

	/**
	 * M�todo que se encarga de buscar todos los banners que se muestran en una
	 * determinada p�gina.
	 * 
	 * @param page:
	 *            Objeto Pageable.
	 * @return: Listado de todos los banners que se encuentran en una p�gina.
	 */
	@Override
	public Page<Banner> buscarTodasPaginacion(Pageable page) {
		return bannersRepository.findAll(page);
	}
}