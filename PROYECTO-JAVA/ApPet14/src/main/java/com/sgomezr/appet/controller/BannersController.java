package com.sgomezr.appet.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.service.IBannersService;
import com.sgomezr.appet.util.Utileria;

/**
 * Banners Controller: Controlador encargado de la gesti�n del m�dulo de banners
 * de ApPet, los banners aqu� gestionados son los que se mostrar�n en el slider
 * de la p�gina principal de la aplicaci�n.
 * 
 * @author Sandra Gomez Roman
 */

@Controller
@RequestMapping("/banners")
public class BannersController {

	final static Logger logger = Logger.getLogger(BannersController.class);

	@Autowired
	private IBannersService serviceBanners;

	/**
	 * M�todo encargado de renderizar la vista donde se muestra la lista de todos
	 * los banners publicados en ApPet.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @return String ruta
	 */
	@GetMapping("/listaBanners")
	public String mostrarIndex(Model model) {
		List<Banner> banners = serviceBanners.buscarTodos();
		model.addAttribute("banners", banners);
		return "banners/listaBanners";
	}

	/**
	 * M�todo encargado de renderizar el formulario para crear un nuevo banner para
	 * el slider principal de la aplicaci�n
	 * 
	 * @param banner:
	 *            Objeto Banner
	 * 
	 * @return String ruta
	 */
	@GetMapping("/crear")
	public String crear(@ModelAttribute Banner banner) {
		return "banners/formBanner";
	}

	/**
	 * M�todo cuya funci�n principal es guardar los datos introudcidos en el
	 * formulario de nuevo banner en la tabla de banners de la base de datos. A su
	 * vez, dicho m�todo se encarga de realizar la validaci�n de los campos del
	 * formularo, si estos pasan la validaci�n se proceder� al guardado de los datos
	 * de dicho banner en BD, si esto no ocurriese, se volver�a a renderizar la
	 * pagina de formulario indicando los errores encontrados y los campos a los que
	 * corresponden. Por otro lado, este m�todo se encarga de setear un nombre a la
	 * imagenes, el cual generado con la combinaci�n de un conjunto de caracteres
	 * aleatorios + el nombre del archivo de imagen subido y guardarla en su carpeta
	 * correspondiente.
	 * 
	 * @param banner:
	 *            Objeto Banner
	 * @param result:
	 *            Interfaz BindingResult, define los resultados del dataBinding para
	 *            el objeto banner que se esta creando (valida que los campos del
	 *            formulario sean del tipo de dato que deben ser o esten definidos
	 *            de forma correcta)
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @param multiPart:
	 *            Objeto MultipartFile, se trata de un archivo de imagen, en este
	 *            caso la imagen del banner.
	 * @param request:
	 *            objeto HttpServletRequest.
	 * @return String ruta
	 */
	@PostMapping("/guardar")
	public String guardar(@ModelAttribute Banner banner, BindingResult result, RedirectAttributes atributos,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request) {

		if (result.hasErrors()) {
			logger.info("Existieron errores");
			return "banners/formBanner";
		}
		String folder = "banners";
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request, folder);
			banner.setArchivo(nombreImagen);
		}
		logger.info("Recibiendo objeto banner" + banner);
		logger.info("Elementos en la lista antes de la inserci�n: " + serviceBanners.buscarTodos().size());
		serviceBanners.insertar(banner);
		logger.info("Elementos en la lista despues de la inserci�n: " + serviceBanners.buscarTodos().size());
		atributos.addFlashAttribute("mensaje", "El banner ha sido creado y guardado correctamente");
		return "redirect:/banners/listaBanners";
	}

	/**
	 * M�todo encargado de editar un determinado banner de la base de datos (se
	 * encarga de hacer un update en BD)
	 * 
	 * @param idBanner:
	 *            id del banner del que se quieren editar sus datos
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @return String ruta
	 */
	@GetMapping(value = "/editar")
	public String editar(@RequestParam("idBanner") int idBanner, Model model) {
		Banner banner = serviceBanners.buscarBannerPorId(idBanner);
		model.addAttribute("banner", banner);
		return "banners/formBanner";
	}

	/**
	 * M�todo encargado de eliminar un banner de la BD
	 * 
	 * @param idBanner:
	 *            id del banner que se quiere eliminar.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta
	 */
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idBanner") int idBanner, Model model, RedirectAttributes atributos) {
		serviceBanners.eliminar(idBanner);
		atributos.addFlashAttribute("mensaje", "El banner fue eliminado correctamente");
		return "redirect:/banners/listaBanners";
	}
}
