package com.sgomezr.appet.service;

import java.util.List;

import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Protectora;

public interface IPerfilesService {

	void guardar(Perfil perfil);

	Perfil buscarIdPerfilPorUsername(String username);

	List<Perfil> buscarPerfilPorRol(String perfil);

	void eliminar(int idPerfil);

	Perfil generarObjetoPerfil(Protectora protectora, int idPerfil);

}
