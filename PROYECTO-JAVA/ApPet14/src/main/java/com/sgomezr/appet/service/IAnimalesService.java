package com.sgomezr.appet.service;

import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;

import java.util.List;

import org.springframework.data.domain.Page;

import com.sgomezr.appet.model.Animal;


public interface IAnimalesService {
	
	void insertar (Animal animal);
	
	List<Animal> buscarTodosLosAnimales();

	Animal buscarAnimalPorId (int idAnimal);
	
	List<Animal> buscarAnimalPorProtectora (int idProtectoraAnimal);
	
	List<String> buscarTipologias();
	
	List<String> buscarLocalidades();
	
	void eliminar(int idAnimal);
	
	Page<Animal> buscarTodasPaginacionAnimales(Pageable page);
	
	void validarCampos(BindingResult result, Animal animal);
	
}
