package com.sgomezr.appet.util;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

/**
 * Clase utiler�a: Contiene un conjunto de m�todos que facilitan las tareas.
 * 
 * @author Sandra Gomez Roman
 *
 */
public class Utileria {

	// Implementamos el logger
	final static Logger logger = Logger.getLogger(Utileria.class);

	/**
	 * M�todo encargado de guardar las imagenes a�adidas a distintos formularios en
	 * carpetas del servidor. A su vez, este m�todo se encarga de establecer un
	 * nombre con una cadena aleatoria a cada imagen, para evitar de esta forma que
	 * los nombres de las imagenes se repitan.
	 * 
	 * @param multiPart:
	 *            Objeto MultipartFile, es decir la imagen que estamos guardando.
	 * @param request:
	 *            Objeto HttpServletRequest.
	 * @param folder:
	 *            Carpeta del servidor en la que se guardar� la imagen.
	 * @return devuelve el nombre final de la imagen (string).
	 */
	public static String guardarImagen(MultipartFile multiPart, HttpServletRequest request, String folder) {
		// Obtenemos el nombre original del archivo
		String nombreOriginal = multiPart.getOriginalFilename();
		// Reemplazamos en el nombre de archivo los espacios por guiones
		nombreOriginal = nombreOriginal.replace(" ", "-");
		// Agregamos al nombre del archivo 8 caracteres aleatorios para evitar
		// duplicados.
		String nombreFinal = randomAlphaNumeric(8) + nombreOriginal;
		// Obtenemos la ruta ABSOLUTA del directorio images
		String rutaFinal = request.getServletContext().getRealPath("/resources/images/");
		if (folder.equals("animales")) {
			rutaFinal = request.getServletContext().getRealPath("/resources/images/animales/");
		} else if (folder.equals("protectoras")) {
			rutaFinal = request.getServletContext().getRealPath("/resources/images/protectoras/");
		} else if (folder.equals("banners")) {
			rutaFinal = request.getServletContext().getRealPath("/resources/images/banners/");
		} else if (folder.equals("noticias")) {
			rutaFinal = request.getServletContext().getRealPath("/resources/images/noticias/");
		} else {
			rutaFinal = request.getServletContext().getRealPath("/resources/images/usuarios/");
		}
		try {
			// Formamos el nombre del archivo para guardarlo en el disco duro
			File imageFile = new File(rutaFinal + nombreFinal);
			logger.info(imageFile.getAbsolutePath());
			// Aqui se guarda fisicamente el archivo en el disco duro
			multiPart.transferTo(imageFile);
			return nombreFinal;
		} catch (IOException e) {
			logger.info("Error " + e.getMessage());
			return null;
		}
	}

	/**
	 * Metodo para generar una cadena de longitud N de caracteres aleatorios.
	 * 
	 * @param count:
	 *            contador.
	 * @return cadena de longitud N de caracteres aleatorios.
	 */
	public static String randomAlphaNumeric(int count) {
		String CARACTERES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * CARACTERES.length());
			builder.append(CARACTERES.charAt(character));
		}
		return builder.toString();
	}

	/**
	 * M�todo usado para saber si un valor String es o no num�rico.
	 * 
	 * @param cadena:
	 *            cadena de la que queremos saber si es o no num�rica.
	 * @return resultado en booleano
	 */
	public static boolean isNumeric(String cadena) {
		boolean resultado;
		try {
			Integer.parseInt(cadena);
			resultado = true;
		} catch (NumberFormatException excepcion) {
			resultado = false;
		}
		return resultado;
	}
}
