package com.sgomezr.appet.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gestiones")
/**
 * Gestiones Controller: Controlador encargado de la gesti�n del m�dulo de
 * gestiones (adopciones, apadrinamientos y donaciones) de ApPet. Note:
 * FUNCIONALIDAD NO DESARROLLADA PARA ESTE PMV (PRODUCTO MINIMO VIABLE) DE
 * APPET.
 * 
 * @author Sandra Gomez Roman
 *
 */
public class GestionesController {

	final static Logger logger = Logger.getLogger(GestionesController.class);

	/**
	 * M�todo encargado de renderizar la vista donde se muestra el listado de los
	 * animales adoptados asociado a un usuario de ApPet.
	 * 
	 * @return String ruta
	 */
	@GetMapping("/listaAdopciones")
	public String listaAdopciones() {
		return "gestiones/listaAdopciones";
	}

	/**
	 * M�todo encargado de renderizar la vista donde se muestra el listado de los
	 * animales apadrinados asociado a un usuario de ApPet.
	 * 
	 * @return String ruta
	 */
	@GetMapping("/listaApadrinamientos")
	public String listaApadrinamientos() {
		return "gestiones/listaApadrinamientos";
	}
	
	/**
	 * M�todo encargado de renderizar la vista donde se muestra el listado de las donaciones
	 * asociados a un usuario de ApPet.
	 * 
	 * @return String ruta
	 */
	@GetMapping("/listaDonaciones")
	public String listaDonaciones() {
		return "gestiones/listaDonaciones";
	}
}
