package com.sgomezr.appet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Favorito;
import com.sgomezr.appet.model.ProtectoraFavorita;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IFavoritosService;
import com.sgomezr.appet.service.IProtectorasFavoritasService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.service.IUsuariosService;

/**
 * Favoritos Controller: Controlador encargado de la gesti�n del m�dulo de
 * favoritos de la aplicaci�n, tanto de animales como de protectoras.
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
@RequestMapping("/favoritos")
public class FavoritosController {

	final static Logger logger = Logger.getLogger(FavoritosController.class);

	@Autowired
	private IAnimalesService serviceAnimales;

	@Autowired
	private IProtectorasService serviceProtectoras;

	@Autowired
	private IUsuariosService serviceUsuarios;

	@Autowired
	private IFavoritosService serviceFavoritos;

	@Autowired
	private IProtectorasFavoritasService serviceProtectorasFavoritas;

	/**
	 * M�todo encargado de renderizar la vista donde se muestra el listado de los
	 * animales favoritos asociado a un usuario de ApPet.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @return String ruta
	 */
	@GetMapping("/listaAnimalesFavoritos")
	public String listaAnimalesFavoritos(Model model, Authentication authentication) {
		int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
		List<Favorito> listaFavoritos = serviceFavoritos.buscarFavoritosPorIdUsuario(idUsuario);

		model.addAttribute("favoritos", listaFavoritos);

		return "favoritos/listaAnimalesFavoritos";
	}

	/**
	 * M�todo encargado de renderizar la vista donde se muestra el listado de las
	 * protectoras favoritas asociado a un usuario de ApPet.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @return String ruta.
	 */
	@GetMapping("/listaProtectorasFavoritas")
	public String listaProtectorasFavoritas(Model model, Authentication authentication) {
		int idUsuario = (serviceUsuarios.buscarUsuarioPorUsername(authentication.getName()).getId());
		List<ProtectoraFavorita> listaProtectorasFavoritas = serviceProtectorasFavoritas
				.buscarProtectorasFavoritasPorIdUsuario(idUsuario);

		model.addAttribute("listaProtectorasFavoritas", listaProtectorasFavoritas);
		return "favoritos/listaProtectorasFavoritas";
	}

	/**
	 * M�todo cuya funci�n principal es agregar un objeto animal a la lista de
	 * favoritos de un determinado usuario, comportandose de manera diferente
	 * dependiendo si dicho animal existe o no en dicha lista.
	 * 
	 * @param idAnimal:
	 *            Id del animal que se va a a�adir a la lista.
	 * @param username:
	 *            Nombre del usuario a cuya lista se a�adira el animal seleccionado.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @param request:
	 *            Objeto HttpServletRequest
	 * @return String ruta
	 */
	@GetMapping("/agregar")
	public String agregar(@RequestParam("idAnimal") int idAnimal, @RequestParam("username") String username,
			RedirectAttributes atributos, Authentication authentication, HttpServletRequest request) {
		if (authentication != null) {
			for (GrantedAuthority rol : authentication.getAuthorities()) {
				HttpSession session = request.getSession();
				session.setAttribute("prf", rol.getAuthority());
			}
			Favorito favorito = new Favorito();
			favorito.setAnimal(serviceAnimales.buscarAnimalPorId(idAnimal));
			favorito.setIdUsuario((serviceUsuarios.buscarUsuarioPorUsername(username)).getId());
			if (serviceFavoritos.existeFavorito(idAnimal, (serviceUsuarios.buscarUsuarioPorUsername(username)).getId())
					.equals(true)) {
				atributos.addFlashAttribute("mensaje",
						"El animal seleccionado ya se encuentra en tu listado de favoritos");
			} else {
				serviceFavoritos.agregar(favorito);
				atributos.addFlashAttribute("mensaje", "El animal ha sido a�adido a su lista de favoritos");
			}
		}
		return "redirect:/favoritos/listaAnimalesFavoritos";
	}

	/**
	 * M�todo cuya funci�n principal es agregar un objeto protectora a la lista de
	 * protectoras favoritas de un determinado usuario, comportandose de manera
	 * diferente dependiendo si dicha protectora existe o no en dicha lista.
	 * 
	 * @param idProtectora:
	 *            Id de la protectora que se va a a�adir a la lista.
	 * @param username:
	 *            Nombre del usuario a cuya lista se a�adira el animal seleccionado.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta
	 */
	@GetMapping("/agregarProtectoraFav")
	public String agregarProtectoraFav(@RequestParam("idProtectora") int idProtectora,
			@RequestParam("username") String username, RedirectAttributes atributos) {

		ProtectoraFavorita protectoraFavorita = new ProtectoraFavorita();
		protectoraFavorita.setProtectoraFav(serviceProtectoras.buscarProtectoraPorId(idProtectora));
		protectoraFavorita.setIdUsuario((serviceUsuarios.buscarUsuarioPorUsername(username)).getId());
		if (serviceProtectorasFavoritas
				.existeProtectoraFavorita(idProtectora, (serviceUsuarios.buscarUsuarioPorUsername(username)).getId())
				.equals(true)) {
			atributos.addFlashAttribute("mensaje",
					"La protectora seleccionada ya se encuentra en tu listado de protectoras favoritas");
		} else {
			serviceProtectorasFavoritas.agregar(protectoraFavorita);
			atributos.addFlashAttribute("mensaje", "La protectora ha sido a�adida a su lista de protectoras favoritas");
		}
		return "redirect:/favoritos/listaProtectorasFavoritas";
	}

	/**
	 * M�todo encargado de eliminar un animal favorito de la lista de animales
	 * favoritos asociada a un usuario determinado.
	 * 
	 * @param idFavorito:
	 *            Id del animal favorito que se quiere eliminar de la lista
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta
	 */
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idFavorito") int idFavorito, Model model, RedirectAttributes atributos) {
		serviceFavoritos.eliminar(idFavorito);
		atributos.addFlashAttribute("mensaje", "El animal fue eliminado correctamente de su lista de favoritos");
		return "redirect:/favoritos/listaAnimalesFavoritos";
	}

	/**
	 * M�todo encargado de eliminar una protectora favorita de la lista de
	 * protectoras favoritas asociada a un usuario determinado.
	 * 
	 * @param idProtectoraFav:
	 *            Id de la protectora favorita que se quiere eliminar de la lista
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta
	 */
	@GetMapping(value = "/eliminarProtectoraFav")
	public String eliminarProtectoraFav(@RequestParam("idProtectoraFav") int idProtectoraFav, Model model,
			RedirectAttributes atributos) {
		serviceProtectorasFavoritas.eliminarProtectoraFavorita(idProtectoraFav);
		atributos.addFlashAttribute("mensaje",
				"La protectora fue eliminada correctamente de su lista de protectoras favoritas");
		return "redirect:/favoritos/listaProtectorasFavoritas";
	}
}
