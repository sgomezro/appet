package com.sgomezr.appet.service;

import java.util.List;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;

public interface IFavoritosService {

	void agregar(Favorito favorito);
	List<Favorito> buscarFavoritosPorIdUsuario (int idUsuario);
	Boolean existeFavorito(int idAnimal, int idUsuario);
	public void eliminar(int idFavorito);
}
