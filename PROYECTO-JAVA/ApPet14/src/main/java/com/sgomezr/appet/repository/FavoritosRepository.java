package com.sgomezr.appet.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Favorito;

public interface FavoritosRepository extends JpaRepository<Favorito, Integer> {

	Favorito findByAnimalAndIdUsuario(Optional<Animal> animal, int idUsuario);

	List<Favorito> findByIdUsuario(int idUsuario);
}
