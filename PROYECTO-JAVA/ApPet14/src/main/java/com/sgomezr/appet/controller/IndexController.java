package com.sgomezr.appet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IBannersService;
import com.sgomezr.appet.service.INoticiasService;
import com.sgomezr.appet.service.IPerfilesService;
import com.sgomezr.appet.service.IUsuariosService;

/**
 * Index Controller: Controlador encargado de la gesti�n de la funcionalidad de
 * la p�gina PRINCIPAL o index de la aplicaci�n
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
@RequestMapping("/index")
public class IndexController {
	@Autowired
	private INoticiasService serviceNoticias;

	@Autowired
	private IBannersService serviceBanners;

	@Autowired
	private IUsuariosService serviceUsuarios;

	@Autowired
	private IPerfilesService servicePerfiles;

	final static Logger logger = Logger.getLogger(IndexController.class);

	/**
	 * M�todo encargado de renderizar la vista principal de la aplicaci�n mostrando
	 * las noticias almacenadas en BD, a su vez, si existe usuario autenticado en
	 * dicha p�gina, este m�todo se encargar� de almacenar sus datos principales
	 * (nombre de usuario y rol) en sesi�n, para poder usarlos en cualquier otro
	 * p�gina de la aplicaci�n.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder al
	 *            los datos del usuario autenticado.
	 * @param request:
	 *            Objeto HttpServletRequest
	 * @return String ruta.
	 */
	@GetMapping(value = "/welcome")
	public String mostrarNoticias(Model model, Authentication authentication, HttpServletRequest request) {
		List<Noticia> noticias = serviceNoticias.buscarTodasLasNoticias();
		List<Banner> banners = serviceBanners.buscarTodos();
		model.addAttribute("noticias", noticias);
		model.addAttribute("banners", banners);

		// Recuperamos el nombre de usuario y el rol del usuario en el caso de que un
		// usuario se acabe de logear
		if (authentication != null) {
			logger.info("Username: " + authentication.getName());
			for (GrantedAuthority rol : authentication.getAuthorities()) {
				logger.info("Rol: " + rol.getAuthority());
			}
			Usuario usuario = serviceUsuarios.buscarUsuarioPorUsername(authentication.getName());
			Perfil perfilUsuario = servicePerfiles.buscarIdPerfilPorUsername(usuario.getUsername());
			HttpSession session = request.getSession();
			session.setAttribute("usr", usuario);
			session.setAttribute("prf", perfilUsuario);
		}
		return "index/indexPage";
	}
}
