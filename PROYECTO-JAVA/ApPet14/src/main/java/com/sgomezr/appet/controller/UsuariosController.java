package com.sgomezr.appet.controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IPerfilesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.service.IUsuariosService;
import com.sgomezr.appet.util.Utileria;

/**
 * Usuarios Controller: Controlador encargado de la gesti�n del m�dulo de
 * usuarios de ApPet.
 * 
 * @author Sandra Gomez Roman
 *
 */
@Controller
@RequestMapping("/usuarios")
public class UsuariosController {

	final static Logger logger = Logger.getLogger(UsuariosController.class);

	@Autowired
	private IUsuariosService serviceUsuarios;

	@Autowired
	private IPerfilesService servicePerfiles;

	@Autowired
	private IProtectorasService serviceProtectoras;

	@Autowired
	private BCryptPasswordEncoder encoder;

	/**
	 * M�todo encargado de renderizar el fomulario usuario/protectora para indicar
	 * si se quiere hacer un registro de usuario o de protectora.
	 * 
	 * @return String ruta.
	 */
	@GetMapping("/formUsuarioProtectora")
	public String eleccion() {
		return "usuarios/formUsuarioProtectora";
	}

	/**
	 * Metodo encargado de renderizar el formulario de registro de un nuevo usuario.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param usuario:
	 *            Objeto Usuario.
	 * @return String ruta.
	 */
	@GetMapping("/crearUsuario")
	public String crearUsuario(Model model, @ModelAttribute Usuario usuario) {
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		return "usuarios/formUsuario";
	}

	/**
	 * Metodo encargado de renderizar el formulario de registro de una nueva
	 * protectora.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param usuario:
	 *            Objeto usuario.
	 * @return String ruta.
	 */
	@GetMapping("/crearProtectora")
	public String crearProtectora(Model model, @ModelAttribute Usuario usuario) {
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		return "usuarios/formProtectora";
	}

	/**
	 * Metodo encargado de renderizar el formulario de registro de un nuevo
	 * aadministrador
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param usuario:
	 *            Objeto Usuario.
	 * @return String ruta.
	 */
	@GetMapping("/crearAdministrador")
	public String crearAdministrador(Model model, @ModelAttribute Usuario usuario) {
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		return "usuarios/formAdministrador";
	}

	/**
	 * M�todo encargado de renderizar la vista correspondiente tras un registro en
	 * la aplicaci�n.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @return String ruta.
	 */
	@GetMapping("/indexUsuarios")
	public String indexUsuarios(Model model) {
		return "usuarios/indexUsuarios";
	}

	/**
	 * M�todo encargado de renderizar el listado de todos los usuarios registrados
	 * en la aplicaci�n, clasificados por sus Roles.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @return String ruta.
	 */
	@GetMapping("/listaUsuarios")
	public String listaUsuarios(Model model) {
		logger.info("PERFILES CON ROL USUARIO");
		List<Perfil> perfilesUsuario = servicePerfiles.buscarPerfilPorRol("USUARIO");
		List<Usuario> listaUsuarios = new LinkedList<>();
		List<Usuario> listaProtectorasNA = new LinkedList<>();
		List<Usuario> listaProtectoras = new LinkedList<>();
		List<Usuario> listaAdministradores = new LinkedList<>();
		for (Perfil perfilUsuario : perfilesUsuario) {
			listaUsuarios.add(serviceUsuarios.buscarUsuarioPorUsername(perfilUsuario.getUsername()));
		}
		logger.info("PERFILES CON ROL PROTECTORA NA (Not aproved)");
		List<Perfil> perfilesProtectoraNA = servicePerfiles.buscarPerfilPorRol("PROTECTORA-NA");
		for (Perfil perfilProtectoraNA : perfilesProtectoraNA) {
			listaProtectorasNA.add(serviceUsuarios.buscarUsuarioPorUsername(perfilProtectoraNA.getUsername()));
		}
		logger.info("PERFILES CON ROL PROTECTORA A (Aproved)");
		List<Perfil> perfilesProtectora = servicePerfiles.buscarPerfilPorRol("PROTECTORA");
		for (Perfil perfilProtectora : perfilesProtectora) {
			listaProtectoras.add(serviceUsuarios.buscarUsuarioPorUsername(perfilProtectora.getUsername()));
		}

		logger.info("PERFILES CON ROL ADMINISTRADOR");
		List<Perfil> perfilesAdministrador = servicePerfiles.buscarPerfilPorRol("ADMINISTRADOR");
		for (Perfil perfilAdministrador : perfilesAdministrador) {
			listaAdministradores.add(serviceUsuarios.buscarUsuarioPorUsername(perfilAdministrador.getUsername()));
		}

		model.addAttribute("usuarios", listaUsuarios);
		model.addAttribute("protectorasNA", listaProtectorasNA);
		model.addAttribute("protectoras", listaProtectoras);
		model.addAttribute("administradores", listaAdministradores);
		return "usuarios/listaUsuarios";
	}

	/**
	 * M�todo cuya funci�n principal es guardar los datos introudcidos en el
	 * formulario de registro en la tabla de usuarios de la base de datos. A su vez,
	 * dicho m�todo se encarga de realizar la validaci�n de los campos del
	 * formularo, si estos pasan la validaci�n se proceder� al guardado de los datos
	 * de dicho usuario en BD, si esto no ocurriese, se volver�a a renderizar la
	 * pagina de formulario dependiento del rol que este posea, indicando los
	 * errores encontrados y los campos a los que corresponden. Por otro lado, este
	 * m�todo se encarga de setear un nombre a las imagenes, el cual es generado con
	 * la combinaci�n de un conjunto de caracteres aleatorios + el nombre del
	 * archivo de imagen subido y guardarla en su carpeta correspondiente.
	 * 
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param usuario:
	 *            Objeto usuario
	 * @param result:
	 *            Interfaz BindingResult, define los resultados del dataBinding para
	 *            el objeto usuario que se intenta crear(valida que los campos del
	 *            formulario sean del tipo de dato que deben ser o esten definidos
	 *            de forma correcta).
	 * @param perfil:
	 *            Nombre del perfil que se esta creando (usuario, protectora,
	 *            protectora-na o administrador)
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @param multiPart:
	 *            Objeto MultipartFile, se trata de un archivo de imagen, en este
	 *            caso la imagen de perfil del usuario.
	 * @param request:
	 *            Objeto HttpServletRequest.
	 * @return String ruta.
	 */
	@PostMapping("/guardar")
	public String guardar(Model model, @ModelAttribute Usuario usuario, BindingResult result,
			@RequestParam("perfil") String perfil, RedirectAttributes atributos,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request) {

		if (serviceUsuarios.buscarUsuarioPorId(usuario.getId()) == null) {
			// Validacion de campos/errores del formulario
			serviceUsuarios.validarCampos(result, usuario);
		}
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		if (result.hasErrors()) {
			logger.info("Existieron errores");
			if (perfil.equals("USUARIO")) {
				return "usuarios/formUsuario";
			} else if (perfil.equals("PROTECTORA-NA")) {
				return "usuarios/formProtectora";
			} else if (perfil.equals("ADMINISTRADOR")) {
				return "usuarios/formAdministrador";
			}
		}
		String folder = "usuarios";
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request, folder);
			usuario.setImagen(nombreImagen);
		}
		String encriptado = encoder.encode(usuario.getPassword());
		String comprobarEncriptado = encoder.encode(usuario.getComprobarPass());
		usuario.setPassword(encriptado);
		usuario.setComprobarPass(comprobarEncriptado);
		// usuario.setEstado(1);
		serviceUsuarios.guardar(usuario);
		Perfil perfilTmp = new Perfil();
		perfilTmp.setUsername(usuario.getUsername());
		perfilTmp.setPerfil(perfil);
		if (servicePerfiles.buscarIdPerfilPorUsername(usuario.getUsername()) != null) {
			perfilTmp.setIdPerfil(servicePerfiles.buscarIdPerfilPorUsername(usuario.getUsername()).getIdPerfil());
		}
		servicePerfiles.guardar(perfilTmp);

		if (perfil.equals("USUARIO")) {
			atributos.addFlashAttribute("mensaje", "El usuario ha sido guardado correctamente");
		} else if (perfil.equals("PROTECTORA-NA")) {
			atributos.addFlashAttribute("mensaje",
					"Su protectora ha guardada correctamente, para poder acceder a ApPet con todos los permisos del rol PROTECTORA"
							+ " deber� esperar a la aprobaci�n por parte de un administrador, de momento sus permisos son limitados al rol USUARIO");
		}
		return "redirect:/usuarios/indexUsuarios";
	}

	/**
	 * M�todo encargado de editar los datos de un usuario de la base de datos (se
	 * encarga de hacer un update en BD) dependiendo del rol que dicho usuario
	 * posea.
	 * 
	 * @param idUsuario:
	 *            Id del usuario del que se quieren editar sus datos.
	 * @param username:
	 *            Nombre de usuario del cual se quieren editar sus datos.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @param authentication:
	 *            Objeto de la interfaz Authentication que nos permite acceder a
	 *            determinados datos del usuario autenticado.
	 * @return String ruta.
	 */
	@GetMapping(value = "/editar")
	public String editar(@RequestParam("id") int idUsuario, @RequestParam("username") String username, Model model,
			RedirectAttributes atributos, Authentication authentication) {
		Usuario usuario = serviceUsuarios.buscarUsuarioPorId(idUsuario);
		String formToReturn = "usuarios/formUsuario";
		if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("PROTECTORA-NA")) {

			for (GrantedAuthority rol : authentication.getAuthorities()) {
				if (rol.getAuthority().equals("ADMINISTRADOR")) {
					Protectora protectora = serviceProtectoras.generarObjetoProtectora(usuario);
					model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
					model.addAttribute("nombreProtectora",
							(serviceUsuarios.buscarUsuarioPorId(idUsuario)).getUsername());
					protectora.setPassword(null);
					protectora.setComprobarPass(null);
					System.out.println(protectora);
					model.addAttribute("protectora", protectora);
					formToReturn = "protectoras/formProtectora";
				} else {
					model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
					usuario.setPassword(null);
					usuario.setComprobarPass(null);
					model.addAttribute("usuario", usuario);
					formToReturn = "usuarios/formProtectora";
				}
			}
		} else if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("PROTECTORA")) {
			model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
			Protectora protectora = serviceProtectoras.buscarProtectoraPorNombre(username);
			model.addAttribute("protectora", protectora);
			formToReturn = "protectoras/formProtectora";
		} else if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("ADMINISTRADOR")) {
			model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
			usuario.setPassword(null);
			usuario.setComprobarPass(null);
			model.addAttribute("usuario", usuario);
			formToReturn = "usuarios/formAdministrador";
		} else if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("USUARIO")) {
			model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
			usuario.setPassword(null);
			usuario.setComprobarPass(null);
			model.addAttribute("usuario", usuario);
			formToReturn = "usuarios/formUsuario";
		}
		return formToReturn;
	}

	/**
	 * M�todo encargado de eliminar un usuario de la BD.
	 * 
	 * @param idUsuario:
	 *            Id del usuario que se quiere eliminar.
	 * @param username:
	 *            Nombre de usuario que se quiere eliminar.
	 * @param model:
	 *            Objeto de la interfaz Model, al que se le a�aden los objetos que
	 *            se quieren representar en la vista.
	 * @param atributos:
	 *            Objeto RedirectAttributes para indicar mensajes de inter�s al
	 *            usuario.
	 * @return String ruta.
	 */
	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("id") int idUsuario, @RequestParam("username") String username, Model model,
			RedirectAttributes atributos) {
		// Buscamos el id del perfil corresponiente al usuario y lo eliminamos de BBDD
		servicePerfiles.eliminar(servicePerfiles.buscarIdPerfilPorUsername(username).getIdPerfil());
		// Eliminamos el usuario de BBDD
		serviceUsuarios.eliminar(idUsuario);
		atributos.addFlashAttribute("mensaje", "El usuario fue eliminado correctamente");
		return "redirect:/usuarios/listaUsuarios";
	}

	/**
	 * M�todo de prueba para testear contrase�as encriptadas.
	 * 
	 * @return String ruta.
	 */
	@GetMapping("/demo-bcrypt")
	public String pruebaBcrypt() {
		String password = "sandra123";
		String encriptado = encoder.encode(password);
		logger.info("Pass encriptada: " + encriptado);
		return "usuarios/demo";
	}
}