package pruebasjparepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

//Ahora estamos usando la intefaz JpaRepository ne vez de la de CrudRepository
public class AppPaging {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		// EJEMPLO 1 (Solo paginacion)
		// ===========================

		// Obtener todas las entidades por paginacion
		// Las paginas empiezan en 0, por lo que aqui estariamos recuperando la 1�
		// pagina con 5 registros
		// De esta forma recuperamos los 5 primeros registros
		// Page<Noticia2> page = repo.findAll(PageRequest.of(0, 5));

		// Si queremos recuperar los registros del 6 al 10(es decir los de la 2� pagina)
		// Page<Noticia2> page = repo.findAll(PageRequest.of(1, 5));
		/*
		 * for(Noticia2 n: page) { System.out.println(n); }
		 */

		// EJEMPLO 2 (Paginacion con ordenamiento)
		// =======================================

		Page<Noticia2> page = repo.findAll(PageRequest.of(0, 10, Sort.by("titulo")));

		for (Noticia2 n : page) {
			System.out.println(n);
		}

		context.close();

	}
}
