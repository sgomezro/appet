package pruebasjparepo;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Sort;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

//Ahora estamos usando la intefaz JpaRepository ne vez de la de CrudRepository
public class AppSorting {

	public static void main(String[] args) {

		// JpaRepository internamente extiende de PagindAndSortingRepository
		// por lo que vamos a tener los metodos para recuperar las entidades por
		// paginacion y ordenamiento
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);

		// EJEMPLO 1
		// ==========

		// Ordenamos las entidades por el campo titulo alfabeticamente de forma
		// ascendente
		// List<Noticia2> lista = repo.findAll(Sort.by("titulo"));

		// Si queremos ordenarlos de manera descendente:
		// List<Noticia2> lista = repo.findAll(Sort.by("titulo").descending());

		/*
		 * for(Noticia2 n : lista) { System.out.println(n); }
		 */

		// EJEMPLO 2
		// ==========

		// En el caso de que queramos realizar el ordenamiento por 2 o mas atributos:
		// Si queremos ordenar por un 3� atributo tendriamos que poner otro .and
		List<Noticia2> lista = repo
				.findAll(Sort.by("fechaPublicacion").descending().and(Sort.by("titulo").ascending()));

		for (Noticia2 n : lista) {
			System.out.println(n);
		}
		context.close();

	}
}
