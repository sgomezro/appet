package pruebasquery;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

public class AppKeywordFindBy {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);

		// Ejemplo Keyword findBy

		// EJEMPLO 1: Buscando por estado
		// ==============================

		// Si queremos buscar por algun campo en concreto, por ejemplo el estado, activo
		// o inactivo, tenemos
		// que empezar por crear nuevos metodos en nuestro
		// repositorio.(NoticiasRepository.java)
		/*
		 * List<Noticia2> lista = repo.findByEstado("Activa");
		 * 
		 * for (Noticia2 n : lista) { System.out.println(n); }
		 */

		// EJEMPLO 2: Buscando por fecha de publicacion
		// =============================================

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); // Formato en el que MySQL maneja las fechas
		List<Noticia2> lista = null;

		try {
			lista = repo.findByFechaPublicacion(format.parse("2017-09-01"));

		} catch (ParseException e) {
			e.printStackTrace();
		}

		for (Noticia2 n : lista) {
			System.out.println(n);
		}

		context.close();

	}
}
