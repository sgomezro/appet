package pruebasjparepo;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

//Ahora estamos usando la intefaz JpaRepository ne vez de la de CrudRepository
public class AppDeleteAllInBatch {
	
	public static void main(String[] args) {

		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		
		/* Metodo deleteAllInBatch de la interfaz JpaRepository -> (es mas eficiente)
		 * Una sola instruccion sql para borrar todos los registros.
		 * Seria mejor utilizar esta si nuestra tabla tiene muchos registros
		delete from Noticias */
		
		/* Metodo deleteAll de la interfaz CrudRepository -> (No muy eficiente)
		 * Varias instrucciones sql para borrar todos los registros
		 * delete from Noticias where id=?
		 * delete from Noticias where id=?
		 * delete from Noticias where id=?
		 */
		
		repo.deleteAllInBatch();
		context.close();

	}
}
