package pruebascrudrepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

public class AppFindAll {
	
	public static void main(String[] args) {

		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		
		//Recuperar todos los registros (metodo findAll del repositorio)
		//Nos devolvera una coleccion con todas las noticias, se puede recorrer con un foreach
		Iterable<Noticia2> it = repo.findAll();
		for(Noticia2 n : it) {
			System.out.println(n);
		}
		context.close();

	}
}
