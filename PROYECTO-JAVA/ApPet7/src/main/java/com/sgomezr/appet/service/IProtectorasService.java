package com.sgomezr.appet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;

import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;


public interface IProtectorasService {

	List<Protectora> buscarTodasLasProtectoras();
	
	void insertar (Protectora protectora);
	
	Protectora buscarProtectoraPorId (int idProtectora);
	
	int buscaIdProtectora(String nombreProtectora);
	
	List<String> buscarLocalidades();
	
	void eliminar(int idProtectora);
	
	Page<Protectora> buscarTodasPaginacion(Pageable page);
	
	Protectora generarObjetoProtectora(Usuario usuario);	
	
	void validarCampos(BindingResult result, Protectora protectora);

}
