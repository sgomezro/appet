package com.sgomezr.appet.controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sgomezr.appet.model.Contacto;
import com.sgomezr.appet.service.IAnimalesService;


@Controller
@RequestMapping("/contacto")
public class ContactoController {
	
	@Autowired
	IAnimalesService serviceAnimales;
	
	@GetMapping(value="/crear")
	public String crear(@ModelAttribute Contacto contacto, Model model){
		model.addAttribute("tipos", serviceAnimales.buscarTipologias());
		model.addAttribute("tiposNotificaciones", tipoNotificaciones());
		return "contacto/formContacto";
	}
	
	@PostMapping(value="/guardar")
	public String guardar(@ModelAttribute Contacto contacto, BindingResult result) {
		System.out.println(contacto);
		if (result.hasErrors()) {
			System.out.println("Existieron errores");
			return "contacto/formContacto";
		}
		return "redirect:/contacto/crear";
	}	
	
	private List<String> tipoNotificaciones(){
		List<String> tipos = new LinkedList<>();
		tipos.add("Noticias");
		tipos.add("Nuevos Animales");
		tipos.add("Animales adoptados");
		tipos.add("Nuevas protectoras");
		return tipos;
		
	}
	
	

}
