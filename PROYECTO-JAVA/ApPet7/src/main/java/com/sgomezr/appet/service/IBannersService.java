package com.sgomezr.appet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Banner;

public interface IBannersService {

	void insertar(Banner banner); 
	Banner buscarBannerPorId(int idBanner);
	List<Banner> buscarTodos();
	void eliminar(int idBanner);
	Page<Banner> buscarTodasPaginacion(Pageable page);
	
}
