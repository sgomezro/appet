package com.sgomezr.appet.controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Perfil;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IPerfilesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.service.IUsuariosService;
import com.sgomezr.appet.util.Utileria;

@Controller
@RequestMapping("/usuarios")
public class UsuariosController {

	@Autowired
	private IUsuariosService serviceUsuarios;

	@Autowired
	private IPerfilesService servicePerfiles;

	@Autowired
	private IProtectorasService serviceProtectoras;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@GetMapping("/formUsuarioProtectora")
	public String eleccion() {
		return "usuarios/formUsuarioProtectora";
	}

	@GetMapping("/crearUsuario")
	public String crearUsuario(Model model, @ModelAttribute Usuario usuario) {
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		return "usuarios/formUsuario";
	}

	@GetMapping("/crearProtectora")
	public String crearProtectora(Model model, @ModelAttribute Usuario usuario) {
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		return "usuarios/formProtectora";
	}

	@GetMapping("/crearAdministrador")
	public String crearAdministrador(Model model, @ModelAttribute Usuario usuario) {
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		return "usuarios/formAdministrador";
	}
	
	@GetMapping("/indexUsuarios")
	public String indexUsuarios(Model model) {
	
		return "usuarios/indexUsuarios";
	}

	@GetMapping("/listaUsuarios")
	public String listaUsuarios(Model model) {
		System.out.println("PERFILES CON ROL USUARIO");
		List<Perfil> perfilesUsuario = servicePerfiles.buscarPerfilPorRol("USUARIO");
		List<Usuario> listaUsuarios = new LinkedList<>();
		List<Usuario> listaProtectorasNA = new LinkedList<>();
		List<Usuario> listaProtectoras = new LinkedList<>();
		List<Usuario> listaAdministradores = new LinkedList<>();
		for (Perfil perfilUsuario : perfilesUsuario) {
			listaUsuarios.add(serviceUsuarios.buscarUsuarioPorUsername(perfilUsuario.getUsername()));
		}
		System.out.println("PERFILES CON ROL PROTECTORA NA (Not aproved)");
		List<Perfil> perfilesProtectoraNA = servicePerfiles.buscarPerfilPorRol("PROTECTORA-NA");
		for (Perfil perfilProtectoraNA : perfilesProtectoraNA) {
			listaProtectorasNA.add(serviceUsuarios.buscarUsuarioPorUsername(perfilProtectoraNA.getUsername()));
		}
		System.out.println("PERFILES CON ROL PROTECTORA A (Aproved)");
		List<Perfil> perfilesProtectora = servicePerfiles.buscarPerfilPorRol("PROTECTORA");
		for (Perfil perfilProtectora : perfilesProtectora) {
			listaProtectoras.add(serviceUsuarios.buscarUsuarioPorUsername(perfilProtectora.getUsername()));
		}

		System.out.println("PERFILES CON ROL ADMINISTRADOR");
		List<Perfil> perfilesAdministrador = servicePerfiles.buscarPerfilPorRol("ADMINISTRADOR");
		for (Perfil perfilAdministrador : perfilesAdministrador) {
			listaAdministradores.add(serviceUsuarios.buscarUsuarioPorUsername(perfilAdministrador.getUsername()));
		}

		model.addAttribute("usuarios", listaUsuarios);
		model.addAttribute("protectorasNA", listaProtectorasNA);
		model.addAttribute("protectoras", listaProtectoras);
		model.addAttribute("administradores", listaAdministradores);
		return "usuarios/listaUsuarios";
	}

	@PostMapping("/guardar")
	public String guardar(Model model, @ModelAttribute Usuario usuario, BindingResult result,
			@RequestParam("perfil") String perfil, RedirectAttributes atributos) {
		
		if(serviceUsuarios.buscarUsuarioPorId(usuario.getId()) == null) {
		//Validacion de campos/errores del formulario
		serviceUsuarios.validarCampos(result, usuario);
		}
		model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
		if (result.hasErrors()) {
			System.out.println("Errores");
			if (perfil.equals("USUARIO")) {
				return "usuarios/formUsuario";
			} else if (perfil.equals("PROTECTORA-NA")) {
				return "usuarios/formProtectora";
			} else if (perfil.equals("ADMINISTRADOR")) {
				return "usuarios/formAdministrador";
			}
		}
		System.out.println(usuario);
		String encriptado = encoder.encode(usuario.getPassword());
		String comprobarEncriptado = encoder.encode(usuario.getComprobarPass());
		usuario.setPassword(encriptado);
		usuario.setComprobarPass(comprobarEncriptado);
		usuario.setEstado(1);
		serviceUsuarios.guardar(usuario);
		Perfil perfilTmp = new Perfil();
		perfilTmp.setUsername(usuario.getUsername());
		perfilTmp.setPerfil(perfil);
		if(servicePerfiles.buscarIdPerfilPorUsername(usuario.getUsername()) != null) {
		perfilTmp.setIdPerfil(servicePerfiles.buscarIdPerfilPorUsername(usuario.getUsername()).getIdPerfil());
		}
		servicePerfiles.guardar(perfilTmp);

		if (perfil.equals("USUARIO")) {
			atributos.addFlashAttribute("mensaje", "El usuario ha sido creado y guardado correctamente");
		} else if (perfil.equals("PROTECTORA-NA")) {
			atributos.addFlashAttribute("mensaje",
					"Su protectora ha sido creada y guardada correctamente, para poder acceder a ApPet con todos los permisos del rol PROTECTORA"
					+ " deber� esperar a la aprobaci�n por parte de un administrador, de momento sus permisos son limitados al rol USUARIO");
		}
		return "redirect:/usuarios/indexUsuarios";
	}

	@GetMapping(value = "/editar")
	public String editar(@RequestParam("id") int idUsuario, @RequestParam("username") String username, Model model,RedirectAttributes atributos) {
		Usuario usuario = serviceUsuarios.buscarUsuarioPorId(idUsuario);
		String formToReturn = "usuarios/formUsuario";
		if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("PROTECTORA-NA")) {
			Protectora protectora = serviceProtectoras.generarObjetoProtectora(usuario);
			model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
			model.addAttribute("nombreProtectora", (serviceUsuarios.buscarUsuarioPorId(idUsuario)).getUsername());
			protectora.setPassword(null);
			protectora.setComprobarPass(null);
			model.addAttribute("protectora", protectora);
			formToReturn = "protectoras/formProtectora";
		
		} else if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("ADMINISTRADOR")) {
			model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
			usuario.setPassword(null);
			usuario.setComprobarPass(null);
			model.addAttribute("usuario", usuario);
			formToReturn = "usuarios/formAdministrador";
		} else if ((servicePerfiles.buscarIdPerfilPorUsername(username).getPerfil()).equals("USUARIO")) {
			model.addAttribute("localidades", serviceUsuarios.buscarLocalidades());
			usuario.setPassword(null);
			usuario.setComprobarPass(null);
			model.addAttribute("usuario", usuario);
			System.out.println("=============");
			System.out.println(usuario);
			formToReturn = "usuarios/formUsuario";
		}
		return formToReturn;
	}

	@GetMapping(value = "/eliminar")
	public String eliminar(@RequestParam("id") int idUsuario, @RequestParam("username") String username, Model model,
			RedirectAttributes atributos) {
		// Buscamos el id del perfil corresponiente al usuario y lo eliminamos de BBDD
		servicePerfiles.eliminar(servicePerfiles.buscarIdPerfilPorUsername(username).getIdPerfil());
		// Eliminamos el usuario de BBDD
		serviceUsuarios.eliminar(idUsuario);
		atributos.addFlashAttribute("mensaje", "El usuario fue eliminado correctamente");
		return "redirect:/usuarios/listaUsuarios";
	}

	@GetMapping("/demo-bcrypt")
	public String pruebaBcrypt() {
		String password = "oscar123";
		String encriptado = encoder.encode(password);
		System.out.println("Pass encriptada: " + encriptado);
		return "usuarios/demo";

	}
}
