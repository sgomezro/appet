package com.sgomezr.appet.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Banner;
import com.sgomezr.appet.model.Noticia;
import com.sgomezr.appet.model.Usuario;
import com.sgomezr.appet.service.IBannersService;
import com.sgomezr.appet.service.INoticiasService;
import com.sgomezr.appet.service.IUsuariosService;

@Controller
@RequestMapping("/index")
public class IndexController {
	@Autowired
	private INoticiasService serviceNoticias;
	
	@Autowired
	private IBannersService serviceBanners;
	
	@Autowired
	private IUsuariosService serviceUsuarios;
	

	@GetMapping(value = "/welcome")
	public String mostrarNoticias(Model model, Authentication authentication,HttpServletRequest request, HttpServletResponse response) {
		List<Noticia> noticias = serviceNoticias.buscarTodasLasNoticias();
		List<Banner> banners = serviceBanners.buscarTodos();
		model.addAttribute("noticias",noticias);
		model.addAttribute("banners",banners);
		//Recuperamos el nombre de usuario y el rol del usuario que se acaba de logear
		/* En el jsp <sec:authentication property="principal.username"/>,-*/
		/*System.out.println("Username:" + authentication.getName());
		for(GrantedAuthority rol: authentication.getAuthorities()) {
			System.out.println("Rol: " + rol.getAuthority());
		}
		Usuario usuario = serviceUsuarios.buscarUsuarioPorUsername(authentication.getName());
		HttpSession session = request.getSession();
		session.setAttribute("usr", usuario);*/

		
		return "index/indexPage";
	
	}
	
	

}
