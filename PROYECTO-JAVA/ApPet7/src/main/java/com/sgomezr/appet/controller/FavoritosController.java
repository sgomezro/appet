package com.sgomezr.appet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/favoritos")
public class FavoritosController {
	
	@GetMapping("/listaAnimalesFavoritos")
	public String listaAnimalesFavoritos() {
		return "favoritos/listaAnimalesFavoritos";
	}
	
	@GetMapping("/listaProtectorasFavoritas")
	public String listaProtectorasFavoritas() {
		return "favoritos/listaProtectorasFavoritas";
	}


	

}
