<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Listado de protectoras</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/protectoras" var="urlProtectoras"></spring:url>
<spring:url value="/protectoras/crear" var="urlCreate"></spring:url>
<spring:url value="/protectoras/editar" var="urlEdit"></spring:url>
<spring:url value="/protectoras/eliminar" var="urlDelete"></spring:url>
<spring:url value="/animales" var="urlAnimales"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Bootstrap JS CDN-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container" style="margin-bottom: 40px;">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/banner7.jpg)">

				<h1 class="display-3" style="color: white">Listado de protectoras</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">Protectoras disponibles</h3>
			<div class="separator"
				style="border: 0 solid #2F4F4F; clear: both; position: relative; z-index: 11; border-color: #2F4F4F; border-top-width: 3px; margin-top: 0px; margin-bottom: 71px; width: 100%; max-width: 370px;"></div>

			<c:if test="${mensaje!=null }">
				<div class="alert alert-success" role="alert">${mensaje}</div>
			</c:if>
	
			<a href="${urlCreate}" style="color:white !important; background-color: #0F1626;"
				class="btn btn-success" role="button" title="Nueva protectora">Nueva
				protectora</a><br> <br>
		
			<div class="table-responsive">
				<table style="font-size: 0.9rem;"
					class="table table-hover table-striped table-bordered">
					<thead class="thead-light ">
						<tr>
							<th>Nombre</th>
							<th>Email</th>
							<th>Telefono</th>
							<th>Localidad</th>
							<th>Estado</th>
							<th>Video</th>
							<th>Animales</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<c:forEach var="protectora" items="${protectoras}">
						<tr>
							<th scope="row">${protectora.nombreProtectora}</th>
							<td>${protectora.email}</td>
							<td>${protectora.telefonoProtectora}</td>
							<td>${protectora.localidad}</td>
						

							<c:choose>
								<c:when test="${protectora.estado=='Activa'}">
									<td><span style="color: #8FC33A; font-weight: 600;"
										class="label label-success">${protectora.estado}</span></td>
								</c:when>
								<c:otherwise>
									<td><span style="color: #E24E42; font-weight: 600;"
										style="background-color: #E24E42" class="label label-danger">${protectora.estado}</span></td>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${protectora.urlVideoProtectora =='Sin video'}">
									<td>${protectora.urlVideoProtectora}</td>
								</c:when>
								<c:otherwise>
									<td>Con video</td>
								</c:otherwise>
							</c:choose>
							<td><a href="${urlAnimales}/listaAnimalesProtectora?idProtectora=${protectora.idProtectora }"
								class="btn btn-info btn-sm" role="button" title="Animales"><span
									style="color: white !important;"> Animales</span></a>
								</td>
							
							<td><a href="${urlEdit}?idProtectora=${protectora.idProtectora}"
								class="btn btn-success btn-sm" role="button" title="Edit"><span
									style="color: white !important;" class="fas fa-pencil-alt"></span></a>
								<a href="${urlDelete}?idProtectora=${protectora.idProtectora}"
								onclick=' return confirm("�Estas seguro de eliminar la protectora?") '
								class="btn btn-danger btn-sm" role="button" title="Eliminar"><span
									style="color: white !important;" class="fas fa-trash-alt"></span></a></td>
						</tr>
					</c:forEach>
				</table>
				
			</div>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>

</body>
</html>