<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Detalle protectora</title>

<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/animales" var="urlAnimales"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="includes/menuLateral.jsp" />
		<!-- Page Content -->


		<!-- Page Content -->
		<div class="container">

			<div class="row" style="margin-top: 70px; margin-bottom: 50px">

				<!-- Post Content Column -->

				<div class="col-lg-8">
					<!-- Title -->
					<h3>Detalle de la protectora</h3>

					<!-- Author -->
					<p class="lead">
						<strong> <a>Protectora de:
								${protectora.nombreProtectora} </a></strong>
					</p>

					<hr>
					<div class="row"
						style="box-shadow: 1px 1px 10px #999; text-align: center; padding-top: 10px; padding-bottom: 10px; background-color: #F3EDE8">
						<div class="col-md-2 col-xs-2 col-sm-2 col-lg-2"
							style="margin-left: 15px">
							<img class="img-responsive"
								src="${urlPublic}/images/${protectora.logoProtectora}"
								style="width: 100%">
						</div>
						<div class="col-md-9 col-xs-9 col-sm-9 col-lg-9">
							<a style="text-decoration: none; color: black" href="#video">
								<p>�Quieres saber un poco m�s sobre nosotros?</p>
								<h3 style="text-align: center">Conoce m�s sobre la protectora de
									${protectora.localidad}!</h3>
							</a>
						</div>
					</div>
					<hr>

					<!-- Container for the image gallery -->

<c:choose>
						<c:when test="${protectora.imagenProtectora !='Sin imagen'}">
					<div class="container">
						<div class="mySlides">
							<div class="numbertext">1 / 6</div>
							<img src="${urlPublic}/images/${protectora.imagenProtectora}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">2 / 6</div>
							<img src="${urlPublic}/images/${protectora.imagenProtectora}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">3 / 6</div>
							<img src="${urlPublic}/images/${protectora.imagenProtectora}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">4 / 6</div>
							<img src="${urlPublic}/images/${protectora.imagenProtectora}"
								style="width: 100%">
						</div>
						<div class="mySlides">
							<div class="numbertext">5 / 6</div>
							<img src="${urlPublic}/images/${protectora.imagenProtectora}"
								style="width: 100%">
						</div>
						<div class="mySlides">
							<div class="numbertext">6 / 6</div>
							<img src="${urlPublic}/images/${protectora.imagenProtectora}"
								style="width: 100%">
						</div>



						<a class="prev" onclick="plusSlides(-1)">-</a> <a class="next"
							onclick="plusSlides(1)">-</a>

						<div class="caption-container">
							<p id="caption"></p>
						</div>

						<div class="row" style="margin-left: 0px; margin-right: 0px">
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${protectora.imagenProtectora}"
									style="width: 100%" onclick="currentSlide(1)" alt="Imagen 1">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${protectora.imagenProtectora}"
									style="width: 100%" onclick="currentSlide(2)" alt="Imagen 2">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${protectora.imagenProtectora}"
									style="width: 100%" onclick="currentSlide(3)" alt="Imagen 3">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${protectora.imagenProtectora}"
									style="width: 100%" onclick="currentSlide(4)" alt="Imagen 4">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${protectora.imagenProtectora}"
									style="width: 100%" onclick="currentSlide(4)" alt="Imagen 5">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${protectora.imagenProtectora}"
									style="width: 100%" onclick="currentSlide(4)" alt="Imagen 6">
							</div>

						</div>
					</div>
					<hr>
					</c:when>

					</c:choose>

					<!-- Post Content -->
					<h3 class="mt-4" id="descripcion" style="margin-bottom: 15px">�Quienes
						somos?</h3>
					<p class="lead">${protectora.descripcionProtectora}</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Ut, tenetur natus doloremque laborum quos iste ipsum rerum
						obcaecati impedit odit illo dolorum ab tempora nihil dicta earum
						fugiat. Temporibus, voluptatibus.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Eos, doloribus, dolorem iusto blanditiis unde eius illum
						consequuntur neque dicta incidunt ullam ea hic porro optio ratione
						repellat perspiciatis. Enim, iure!</p>

					<blockquote class="blockquote">
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. Integer posuere erat a ante.</p>
						<footer class="blockquote-footer">
							Someone famous in <cite title="Source Title">Source Title</cite>
						</footer>
					</blockquote>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora
						commodi nihil ullam alias modi dicta saepe minima ab quo
						voluptatem obcaecati?</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione
						tempore quidem voluptates cupiditate voluptas illo saepe quaerat
						numquam recusandae? Qui, necessitatibus, est!</p>



					<!-- 16:9 aspect ratio -->

					<c:choose>
						<c:when test="${protectora.urlVideoProtectora !='Sin video'}">
							<hr>
							<h3 class="mt-4" style="margin-bottom: 15px">Con�cenos
								mejor:</h3>
							<!-- 16:9 aspect ratio -->
							<div id="video" class="embed-responsive embed-responsive-16by9"
								style="margin-bottom: 35px">
								<iframe class="embed-responsive-item"
									src="${protectora.urlVideoProtectora}"></iframe>
							</div>
						</c:when>

					</c:choose>
					<hr>

					<h3 class="mt-4" id="descripcion" style="margin-bottom: 15px">�Como
						adoptar en la protectora de ${protectora.nombreProtectora}?</h3>
					<p class="lead">
						${protectora.descripcionProcedimientoProtectora}</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Ut, tenetur natus doloremque laborum quos iste ipsum rerum
						obcaecati impedit odit illo dolorum ab tempora nihil dicta earum
						fugiat. Temporibus, voluptatibus.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Eos, doloribus, dolorem iusto blanditiis unde eius illum
						consequuntur neque dicta incidunt ullam ea hic porro optio ratione
						repellat perspiciatis. Enim, iure!</p>

					<blockquote class="blockquote">
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. Integer posuere erat a ante.</p>
						<footer class="blockquote-footer">
							Someone famous in <cite title="Source Title">Source Title</cite>
						</footer>
					</blockquote>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora
						commodi nihil ullam alias modi dicta saepe minima ab quo
						voluptatem obcaecati?</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione
						tempore quidem voluptates cupiditate voluptas illo saepe quaerat
						numquam recusandae? Qui, necessitatibus, est!</p>

					<hr>
				</div>

				<!-- Sidebar Widgets Column -->
				<div class="col-md-4">

					<!-- Categories Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							y contacto</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Localidad:</strong>
												${protectora.localidad}</a></li>
										<li><a><strong>Direcci�n: </strong>${protectora.direccionProtectora}</a></li>
										<li><a><strong>Telefono: </strong>${protectora.telefonoProtectora}</a>
										</li>
										<li><a><strong>Email: </strong>${protectora.email}</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>


					<c:choose>
						<c:when test="${protectora.latitudProtectora != 'Sin latitud'}">
								<!-- Categories Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Como llegar:</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<div id="googleMap" style="width: 100%; height: 400px;"></div>
								</div>
							</div>
						</div>
					</div>


					<hr>
						</c:when>

					</c:choose>
				
					<p>
						�Quieres conocer a nuestros animales? <strong>Haz click!</strong>
					</p>
					<a href="${urlAnimales}/listaAnimalesProtectora?idProtectora=${protectora.idProtectora }"><button type="button" class="btn btn-primary">Conoce a
						nuestros animales</button></a>
					<p></p>
					<hr>
					<p>
						<strong>�Quieres ayudar a nuestros animales </strong>pero no
						puedes encargarte de ellos? <strong></strong>
					</p>
					<button type="button" class="btn btn-info">Donativos a la
						protectora ${protectora.nombreProtectora}</button>
					<p></p>
					<hr>


				</div>

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->




		<!-- /container -->
	</div>
	<!-- Footer -->
	<jsp:include page="includes/footer.jsp"></jsp:include>


	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/jquery/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!--JS -->
	<script type="text/javascript" src="${urlPublic}/js/slider.js"></script>
	<!--Google Map JS -->
	<script type="text/javascript" src="${urlPublic}/js/googleMap.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBU3I_y3JAvwOGWBeCdDEzA3_Gz6SybxYI&callback=myMap"></script>

</body>
</html>