<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>Detalle protectora</title>

<spring:url value="/resources" var="urlPublic"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="includes/menuLateral.jsp" />
		<!-- Page Content -->


		<!-- Page Content -->
		<div class="container">

			<div class="row" style="margin-top: 70px; margin-bottom: 50px">

				<!-- Post Content Column -->

				<div class="col-lg-8">
					<!-- Title -->
					<h3>Detalle del animal</h3>

					<!-- Author -->
					<p class="lead">
						<strong>${animal.nombre}: <a
							href="detalleProtectora?idProtectora=${animal.protectora.idProtectora}&idAnimal=${animal.id }">
								${animal.protectora.nombreProtectora}</a></strong>
					</p>

					<hr>
					<div
						style="box-shadow: 1px 1px 10px #999; text-align: center; padding-top: 10px; padding-bottom: 10px; background-color: #F3EDE8">
						<a style="text-decoration: none; color: black" href="#video">
							<p>�Quieres saber un poco mas sobre mi?</p>
							<h3 style="text-align: center">Conoce mejor a
								${animal.nombre}!</h3>
						</a>
					</div>
					<hr>

					<!-- Container for the image gallery -->


					<div class="container">
						<div class="mySlides">
							<div class="numbertext">1 / 6</div>
							<img src="${urlPublic}/images/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">2 / 6</div>
							<img src="${urlPublic}/images/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">3 / 6</div>
							<img src="${urlPublic}/images/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">4 / 6</div>
							<img src="${urlPublic}/images/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">5 / 6</div>
							<img src="${urlPublic}/images/${animal.imagen}"
								style="width: 100%">
						</div>

						<div class="mySlides">
							<div class="numbertext">6 / 6</div>
							<img src="${urlPublic}/images/${animal.imagen}"
								style="width: 100%">
						</div>


						<a class="prev" onclick="plusSlides(-1)">-</a> <a class="next"
							onclick="plusSlides(1)">-</a>

						<div class="caption-container">
							<p id="caption"></p>
						</div>

						<div class="row" style="margin-left: 0px; margin-right: 0px">
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${animal.imagen}" style="width: 100%"
									onclick="currentSlide(1)" alt="Imagen 1">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${animal.imagen}" style="width: 100%"
									onclick="currentSlide(2)" alt="Imagen 2">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${animal.imagen}" style="width: 100%"
									onclick="currentSlide(3)" alt="Imagen 3">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${animal.imagen}" style="width: 100%"
									onclick="currentSlide(4)" alt="Imagen 4">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${animal.imagen}" style="width: 100%"
									onclick="currentSlide(5)" alt="Imagen 5 ">
							</div>
							<div class="column">
								<img class="demo cursor"
									src="${urlPublic}/images/${animal.imagen}" style="width: 100%"
									onclick="currentSlide(6)" alt="Imagen 6">
							</div>
						</div>
					</div>
					<hr>

					<!-- Post Content -->
					<h3 class="mt-4" id="descripcion" style="margin-bottom: 15px">Descripci�n</h3>
					<p class="lead">${animal.descripcion}</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Ut, tenetur natus doloremque laborum quos iste ipsum rerum
						obcaecati impedit odit illo dolorum ab tempora nihil dicta earum
						fugiat. Temporibus, voluptatibus.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Eos, doloribus, dolorem iusto blanditiis unde eius illum
						consequuntur neque dicta incidunt ullam ea hic porro optio ratione
						repellat perspiciatis. Enim, iure!</p>

					<blockquote class="blockquote">
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. Integer posuere erat a ante.</p>
						<footer class="blockquote-footer">
							Someone famous in <cite title="Source Title">Source Title</cite>
						</footer>
					</blockquote>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora
						commodi nihil ullam alias modi dicta saepe minima ab quo
						voluptatem obcaecati?</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione
						tempore quidem voluptates cupiditate voluptas illo saepe quaerat
						numquam recusandae? Qui, necessitatibus, est!</p>

					<hr>
					<c:choose>
						<c:when test="${animal.urlVideo !='Sin video'}">
							<h3 class="mt-4" style="margin-bottom: 15px">As� soy yo:</h3>
							<!-- 16:9 aspect ratio -->
							<div id="video" class="embed-responsive embed-responsive-16by9"
								style="margin-bottom: 35px">
								<iframe class="embed-responsive-item" src="${animal.urlVideo}"></iframe>
							</div>
						</c:when>

					</c:choose>
				</div>

				<!-- Sidebar Widgets Column -->
				<div class="col-md-4">

					<!-- Categories Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							de ${animal.nombre}</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Tipolog�a:</strong> ${animal.tipo}</a></li>
										<li><a><strong>Raza:</strong> ${animal.raza}</a></li>
										<li><a><strong>Sexo:</strong> ${animal.sexo}</a></li>
										<li><a><strong>Fecha de nacimiento:</strong>
												<fmt:formatDate pattern="dd-MM-yyyy" value="${animal.fechaNacimiento}" /></a></li>
										<li><a><strong>Edad:</strong>
												${animal.edad} <span>a�os</span></a></li>
										<li><a><strong>�Tiene microchip?:</strong>
												${animal.tieneMicrochip}</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<!-- Side Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Comportamiento</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a>${animal.comportamiento}</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- Side Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							de la protectora</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Nombre: </strong>${animal.protectora.nombreProtectora}</a>
										</li>
										<li><a><strong>Localidad: </strong>${animal.protectora.localidad}</a></li>
									</ul>
									<a
										href="detalleProtectora?idProtectora=${animal.protectora.idProtectora}&idAnimal=${animal.id }">
										<button type="button" style="margin-top: 10px;"
											class="btn btn-success">Mas informaci�n sobre la
											protectora</button>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Side Widget -->
					<div class="card my-4">
						<h5 class="card-header"
							style="background-color: #424F65; color: white">Informaci�n
							de adopciones</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<ul class="list-unstyled mb-0">
										<li><a><strong>Estado:</strong> ${animal.estado}</a></li>
										<li><a><strong>Envio:</strong> ${animal.envio}</a></li>
										<li><a><strong>Tasa de adopci�n:</strong>
												${animal.tasaAdopcion} euros</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<p>
						�Quieres adoptar a ${animal.nombre}? <strong>Haz click!</strong>
					</p>
					<button type="button" class="btn btn-primary">Ad�ptame</button>
					<p></p>
					<hr>
					<p>
						�Quieres ayudar a ${animal.nombre} pero no puedes adoptarlo? <strong>Apadrinalo!</strong>
					</p>
					<button type="button" class="btn btn-info">Apadriname</button>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /container -->
	</div>

	<!-- Footer -->
	<jsp:include page="includes/footer.jsp"></jsp:include>

	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/jquery/jquery.min.js"></script>
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!--JS -->
	<script type="text/javascript" src="${urlPublic}/js/slider.js"></script>