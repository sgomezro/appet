<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">
<title>Contacto</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/contacto/guardar" var="urlForm"></spring:url>
<!--Favicon-->
<link rel="icon" type="image/png"
	href="${urlPublic}/images/iconos/favicon.ico" />
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
		<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/Banner14.jpg)">

			<h1 class="display-3" style="color: #2F4F4F">Formulario<br/> de contacto</h1>
			
			<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">Contacta con nosotros</h3>
			<div class="separator"
				style="border: 0 solid #2F4F4F; clear: both; position: relative; z-index: 11; border-color: #2F4F4F; border-top-width: 3px; margin-top: 0px; margin-bottom: 50px; width: 100%; max-width: 365px;"></div>
			

			<spring:hasBindErrors name="contacto">
				<div class='alert alert-danger' role='alert'>
				Por favor corrija los siguientes errores:
				<ul>
					<c:forEach var="error" items="${errors.allErrors}">
						<li><spring:message message="${error}" /></li>
					</c:forEach>
				</ul>
				</div>
			</spring:hasBindErrors>
			
			<form:form action="${urlForm}" method="post" modelAttribute="contacto">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="badgetForm" for="nombre">Nombre</label>
							 <form:input type="text"
								class="form-control" path="nombre" id="nombre"
								required="required" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="badgetForm" for="apellidos">Apellidos</label>
							 <form:input type="text"
								class="form-control" path="apellidos" id="apellidos" />
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="email">Email</label> 
							<form:input type="text"
								class="form-control" path="email" id="email" placeholder="Email" />
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="tipo">�Qu� tipologia de animal le gustaria adoptar?</label>
							 <form:select id="tipo"
								path="tipos" multiple="multiple" class="form-control" items="${tipos }" required="required"/>
						</div>
					</div>
					
						<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="experiencia">�Como ha sido tu experiencia en ApPet?</label>
							 <div class="col-sm-10">
								<label><form:radiobutton path="experiencia" value="1" />Muy buena</label>
								<label><form:radiobutton path="experiencia" value="2" />Buena</label>
								<label><form:radiobutton path="experiencia" value="3" />Regular</label>
								<label><form:radiobutton path="experiencia" value="4" />Mala</label>
								<label><form:radiobutton path="experiencia" value="5" />Muy mala</label>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="notificaciones">�Te gustaria recibil notificaciones? Si es as�, indicanos de cual</label>
							  <div class="col-sm-10">
							  <form:checkboxes items="${tiposNotificaciones }" path="notificaciones"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="comentarios">Comentarios</label>
							<form:textarea class="form-control" path="comentarios" id="comentarios"
								rows="10"></form:textarea>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-primary" style="background-color:#0F1626; margin-top: 30px;margin-bottom: 50px;" >Guardar</button>
			</form:form>
		
			</div>
		<!-- /container -->
	</div>
	
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
	<script>
		// Configuracion de la barra de heramientas
		// https://www.tinymce.com/docs/get-started/basic-setup/
		tinymce
				.init({
					selector : '#detalle',
					plugins : "textcolor, table lists code",
					toolbar : " undo redo | bold italic | alignleft aligncenter alignright alignjustify \n\
                    | bullist numlist outdent indent | forecolor backcolor table code"
				});
	</script>
</body>
</html>