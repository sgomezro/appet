<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">
<title>Nuevo banner</title>
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/banners/guardar" var="urlForm"></spring:url>
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/Banner8.jpg)">

				<h1 class="display-3" style="color: white">
					Nuevo <br /> banner
				</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">
				<span class="label label-success">Datos del banner</span>
			</h3>
			<div class="separator"
				style="border: 0 solid #2F4F4F; clear: both; position: relative; z-index: 11; border-color: #2F4F4F; border-top-width: 3px; margin-top: 0px; margin-bottom: 71px; width: 100%; max-width: 260px;"></div>

			<spring:hasBindErrors name="banner">
				<div class='alert alert-danger' role='alert'>
					Por favor corrija los siguientes errores:
					<ul>
						<c:forEach var="error" items="${errors.allErrors}">
							<li><spring:message message="${error}" /></li>
						</c:forEach>
					</ul>
				</div>
			</spring:hasBindErrors>

			<form:form action="${urlForm}" method="post"
				enctype="multipart/form-data" modelAttribute="banner">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="nombre">Titulo:</label>
							<form:input placeholder="Titulo del banner" type="text"
								class="form-control" path="titulo" id="titulo"
								required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="imagen">Imagen:</label><input
								type="file" id="archivoImagen" name="archivoImagen"
								required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="estado">Estado:</label>
							<form:select id="estado" path="estado" class="form-control">
								<form:option value="Activo">Activo</form:option>
								<form:option value="Inactivo">Inactivo</form:option>
							</form:select>
						</div>
					</div>
				</div>
				<div class="container" style="text-align: right">
					<button type="submit" class="btn btn-primary"
						style="background-color: #0F1626; margin-top: 50px; margin-bottom: 50px;">Guardar</button>
				</div>
			</form:form>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->
	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>