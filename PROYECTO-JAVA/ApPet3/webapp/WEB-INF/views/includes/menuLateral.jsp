<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<spring:url value="/resources" var="urlPublic"></spring:url>
<spring:url value="/" var="urlRoot"></spring:url>

</head>
<!-- Sidebar -->
		<div  style="text-align: center" class="pt-5 sidebar navbar-nav">
		<img style="height: 130px; margin-top:20px" class="img-fluid img-profile rounded-circle mx-auto mb-2 img-responsive"
							src=${urlPublic}/images/usuario.png alt="Imagen Perfil">
		<ul style="list-style:none">
		
		<li class="nav-item"><a class="nav-link"
				href="${urlRoot}index/welcome"> <i class="fas fa-dog"></i> <span>Inicio</span>
			</a></li>
			<li class="nav-item active"><a class="nav-link"
				href=""> <i class="fas fa-user"></i> <span>Mi perfil</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="${urlRoot}">
					<i class="fas fa-paw"></i> <span>Animales en adopcion</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="${urlRoot}protectoras/listaProtectoras">
					<i class="fas fa-hospital-alt"></i> <span>Protectoras</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="${urlRoot}animales/crear">
					<i class="fas fa-bone "></i> <span>Nuevo animal</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="${urlRoot}protectoras/crear">
					<i class="fas fa-plus "></i> <span>Nueva protectora</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="${urlRoot}animales/listaAnimales">
					<i class="fas fa-list-ul "></i> <span>Listado de animales</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="${urlRoot}noticias/crear">
					<i class="fas fa-book"></i> <span>Nueva noticia</span>
			</a>
			</li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-heart"></i> <span>Mis favoritos</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-home"></i> <span>Mis adopciones</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-child"></i> <span>Mis apadrinamientos</span>
			</a></li>
				<li class="nav-item"><a class="nav-link" href="">
					<i class="fas fa-donate"></i> <span>Donaciones</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="${urlRoot}banners/crear">
					<i class="fas fa-images"></i> <span>Gestion de banners</span>
			</a></li>
			
		</ul>
</div>