<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta charset="utf-8">

<title>ApPet - Nueva protectora</title>
<!-- Tag de Spring para agregar recursos estaticos -->
<spring:url value="../resources" var="urlPublic"></spring:url>
<spring:url value="/protectoras/guardar" var="urlForm"></spring:url>
<!-- STYLES -->
<link rel="stylesheet" href="${urlPublic}/css/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="${urlPublic}/css/sb-admin.css"
	rel="stylesheet">
<!-- Bootstrap core CSS-->
<link href="${urlPublic}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${urlPublic}/fonts/css/all.min.css" rel="stylesheet"
	type="text/css">
<link href="${urlPublic}/bootstrap/css/bootstrap-grid.css"
	rel="stylesheet">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- FontAwesome CDN for icons -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<!-- DatePicker script -->
<script>
	$(document).ready(
			function() {
				var date_input = $('input[name="fechaNacimiento"]'); //our date input has the name "date"
				var container = $('.bootstrap-iso form').length > 0 ? $(
						'.bootstrap-iso form').parent() : "body";
				var options = {
					format : 'dd-mm-yyyy',
					container : container,
					todayHighlight : true,
					autoclose : true,
				};
				date_input.datepicker(options);
			})
</script>
</head>

<body id="page-top">
	<!-- Menu superior -->
	<jsp:include page="../includes/menuSuperior.jsp" />
	<div id="wrapper">
		<!-- Menu Lateral -->
		<jsp:include page="../includes/menuLateral.jsp" />
		<!-- Page Content -->
		<div class="container">
			<!-- Jumbotron Header -->
			<header class="jumbotron my-5"
				style="background-image:url(${urlPublic}/images/Banner3.jpg)">

				<h1 class="display-3" style="color: white">Nueva protectora</h1>
				<h2 class="lead" style="font-size: 2.25rem !important; color: white">ApPet</h2>
			</header>
			<h3 class="blog-title">
				<span class="label label-success">Datos de la protectora</span>
			</h3>
			<div class="separator"
				style="border: 0 solid #2F4F4F; clear: both; position: relative; z-index: 11; border-color: #2F4F4F; border-top-width: 3px; margin-top: 0px; margin-bottom: 20px; width: 100%; max-width: 352px;"></div>


			<spring:hasBindErrors name="protectora">
				<div class='alert alert-danger' role='alert'>
					Por favor corrija los siguientes errores:
					<ul>
						<c:forEach var="error" items="${errors.allErrors}">
							<li><spring:message message="${error}" /></li>
						</c:forEach>
					</ul>
				</div>
			</spring:hasBindErrors>
			<form:form action="${urlForm}" method="post"
				enctype="multipart/form-data" modelAttribute="protectora">
				<div class="row">

					<div class="col-sm-4">
						<div class="form-group">
							<img style="width: 250px; height: 175px;" class="card-img-top"
								src=${urlPublic}/images/${protectora.logoProtectora
								} title="Imagen actual de la protectora">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="nombreProtectora">Nombre:</label>
							<form:hidden path="idProtectora" />
							<form:input placeholder="Nombre de la protectora" type="text"
								class="form-control" path="nombreProtectora"
								id="nombreProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="emailProtectora">Email:</label>
							<form:input placeholder="Email de la protectora" type="text"
								class="form-control" path="emailProtectora" id="emailProtectora"
								required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="contraseñaProtectora">Contraseña:</label>
							<form:input placeholder="Contraseña de la protectora" type="text"
								class="form-control" path="contraseñaProtectora"
								id="contraseñaProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="telefonoProtectora">Teléfono:</label>
							<form:input placeholder="Telefono de la protectora" type="text"
								class="form-control" path="telefonoProtectora"
								id="telefonoProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="localidadProtectora">Localidad:</label>
							<form:select placeholder="Localidad de la protectora."
								class="form-control" path="localidadProtectora" required="required"
								items="${localidades }" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="localidadProtectora">Dirección:</label>
							<form:input placeholder="Dirección de la protectora" type="text"
								class="form-control" path="direccionProtectora"
								id="direccionProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="localidadProtectora">Añade las cordenadas de su posición si desea que aparezca un mapa de
							situacion:</label>
							<form:input placeholder="Latitud de la protectora" type="text"
								class="form-control" path="latitudProtectora"
								id="latitudProtectora"/>
								
								<form:input placeholder="Altitud de la protectora" type="text"
								class="form-control" path="altitudProtectora"
								id="altitudProtectora" required="required" />
						</div>
					</div>
					<div class="col-sm-4">
						<label class="badgetForm" for="imagen">Fecha de fundacion
							de la protectora:</label>
						<div class="form-group has-feedback">
							<i class="far fa-calendar form-control-feedback"></i>
							<form:input type="text" class="form-controlIcon"
								id="fechaCreacionProtectora" path="fechaCreacionProtectora"
								placeholder="dd-mm-yyyy" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="badgetForm" for="estadoProtectora">Estado:</label>
							<form:select id="estadoProtectora" path="estadoProtectora"
								class="form-control" required="required">
								<form:option value="Activa">Activa</form:option>
								<form:option value="Inactiva">Inactiva</form:option>
							</form:select>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm" for="descripcionProtectora">Descripción
								de la protectora</label>
							<form:textarea class="form-control" path="descripcionProtectora"
								id="descripcionProtectora" rows="10" required="required"></form:textarea>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="badgetForm"
								for="descripcionProcedimientoProtectora">Descripción del
								procedimiento de adopción de la protectora</label>
							<form:textarea class="form-control"
								path="descripcionProcedimientoProtectora"
								id="descripcionProcedimientoProtectora" rows="10" required="required"></form:textarea>
						</div>
					</div>
					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="logoProtectora">Logo de la protectora:</label>
							<form:hidden path="logoProtectora" />
							<input type="file" id="archivoImagenLogo" name="archivoImagenLogo">
						</div>
						
					</div>
					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="imagenProtectora">Imagen:</label>
							<form:hidden path="imagenProtectora" />
							<input type="file" id="archivoImagen" name="archivoImagen">
						</div>
					</div>

					<div class="col-sm-6" style="margin-top: 20px">
						<div class="form-group">
							<label class="badgetForm" for="urlVideoProtectora">URL
								del video (youtube)</label>
							<form:input placeholder="URL del video (youtube)" type="text"
								class="form-control" path="urlVideoProtectora"
								id="urlVideoProtectora" required="required" />
						</div>
					</div>
				</div>
				<div class="container" style="text-align: right">

					<button type="submit" class="btn btn-primary"
						style="background-color: #0F1626; margin-top: 50px; margin-bottom: 50px;">Guardar</button>
				</div>
			</form:form>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../includes/footer.jsp"></jsp:include>
	<!-- Bootstrap core JavaScript -->

	<script src="${urlPublic}/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="${urlPublic}/tinymce/tinymce.min.js"></script>
	<script>
		// Configuracion de la barra de heramientas
		// https://www.tinymce.com/docs/get-started/basic-setup/
		tinymce
				.init({
					selector : '#detalle',
					plugins : "textcolor, table lists code",
					toolbar : " undo redo | bold italic | alignleft aligncenter alignright alignjustify \n\
                    | bullist numlist outdent indent | forecolor backcolor table code"
				});
	</script>
</body>