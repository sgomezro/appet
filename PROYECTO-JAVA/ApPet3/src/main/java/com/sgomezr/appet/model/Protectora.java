package com.sgomezr.appet.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Protectora")
public class Protectora {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_protectora")
	int idProtectora;
	
	@Column(name="nombre_protectora")
	private String nombreProtectora;
	
	@Column(name="email_protectora")
	private String emailProtectora;
	
	@Column(name="contraseña_protectora")
	private String contraseñaProtectora;
	
	@Column(name="telefono_protectora")
	private String telefonoProtectora;
	
	@Column(name="localidad_protectora")
	private String localidadProtectora;
	
	@Column(name="direccion_protectora")
	private String direccionProtectora;
	
	@Column(name="imagen_protectora")
	private String imagenProtectora;
	
	@Column(name="logo_protectora")
	private String logoProtectora;
	
	@Column(name="fechaCreacion_protectora")
	private Date fechaCreacionProtectora;
	
	@Column(name="estado_protectora")
	private String estadoProtectora;
	
	@Column(name="perfil_protectora")
	private String perfilProtectora;
	
	@Column(name="descripcion_protectora")
	private String descripcionProtectora;
	
	@Column(name="urlVideo_protectora")
	private String urlVideoProtectora = "Sin video";
	
	@Column(name="descripcionProcedimiento_protectora")
	private String descripcionProcedimientoProtectora;
	
	@Column(name="latitud_protectora")
	private String latitudProtectora = "Sin latitud";
	
	@Column(name="altitud_protectora")
	private String altitudProtectora = "Sin altitud";

	public Protectora(int idProtectora, String nombreProtectora, String emailProtectora, String contraseñaProtectora,
			String telefonoProtectora, String localidadProtectora, String direccionProtectora, String imagenProtectora,
			String logoProtectora, Date fechaCreacionProtectora, String estadoProtectora, String perfilProtectora,
			String descripcionProtectora, String urlVideoProtectora, String descripcionProcedimientoProtectora,
			String latitudProtectora, String altitudProtectora) {
		super();
		this.idProtectora = idProtectora;
		this.nombreProtectora = nombreProtectora;
		this.emailProtectora = emailProtectora;
		this.contraseñaProtectora = contraseñaProtectora;
		this.telefonoProtectora = telefonoProtectora;
		this.localidadProtectora = localidadProtectora;
		this.direccionProtectora = direccionProtectora;
		this.imagenProtectora = imagenProtectora;
		this.logoProtectora = logoProtectora;
		this.fechaCreacionProtectora = fechaCreacionProtectora;
		this.estadoProtectora = estadoProtectora;
		this.perfilProtectora = perfilProtectora;
		this.descripcionProtectora = descripcionProtectora;
		this.urlVideoProtectora = urlVideoProtectora;
		this.descripcionProcedimientoProtectora = descripcionProcedimientoProtectora;
		this.latitudProtectora = latitudProtectora;
		this.altitudProtectora = altitudProtectora;
	}

	public Protectora() {
		System.out.println("Constructor de Protectora");
		this.fechaCreacionProtectora = new Date(); // Por default, la fecha del sistema
		this.imagenProtectora = "Sin imagen";
		this.logoProtectora = "protectoraImg.jpg";
		this.perfilProtectora = "Protectora";

	}

	public int getIdProtectora() {
		return idProtectora;
	}

	public void setIdProtectora(int idProtectora) {
		this.idProtectora = idProtectora;
	}

	public String getNombreProtectora() {
		return nombreProtectora;
	}

	public void setNombreProtectora(String nombreProtectora) {
		this.nombreProtectora = nombreProtectora;
	}

	public String getEmailProtectora() {
		return emailProtectora;
	}

	public void setEmailProtectora(String emailProtectora) {
		this.emailProtectora = emailProtectora;
	}

	public String getContraseñaProtectora() {
		return contraseñaProtectora;
	}

	public void setContraseñaProtectora(String contraseñaProtectora) {
		this.contraseñaProtectora = contraseñaProtectora;
	}

	public String getTelefonoProtectora() {
		return telefonoProtectora;
	}

	public void setTelefonoProtectora(String telefonoProtectora) {
		this.telefonoProtectora = telefonoProtectora;
	}

	public String getLocalidadProtectora() {
		return localidadProtectora;
	}

	public void setLocalidadProtectora(String localidadProtectora) {
		this.localidadProtectora = localidadProtectora;
	}

	public String getDireccionProtectora() {
		return direccionProtectora;
	}

	public void setDireccionProtectora(String direccionProtectora) {
		this.direccionProtectora = direccionProtectora;
	}

	public String getImagenProtectora() {
		return imagenProtectora;
	}

	public void setImagenProtectora(String imagenProtectora) {
		this.imagenProtectora = imagenProtectora;
	}
	
	public String getLogoProtectora() {
		return logoProtectora;
	}

	public void setLogoProtectora(String logoProtectora) {
		this.logoProtectora = logoProtectora;
	}

	public Date getFechaCreacionProtectora() {
		return fechaCreacionProtectora;
	}

	public void setFechaCreacionProtectora(Date fechaCreacionProtectora) {
		this.fechaCreacionProtectora = fechaCreacionProtectora;
	}

	public String getEstadoProtectora() {
		return estadoProtectora;
	}

	public void setEstadoProtectora(String estadoProtectora) {
		this.estadoProtectora = estadoProtectora;
	}

	public String getPerfilProtectora() {
		return perfilProtectora;
	}

	public void setPerfilProtectora(String perfilProtectora) {
		this.perfilProtectora = perfilProtectora;
	}

	public String getDescripcionProtectora() {
		return descripcionProtectora;
	}

	public void setDescripcionProtectora(String descripcionProtectora) {
		this.descripcionProtectora = descripcionProtectora;
	}

	public String getUrlVideoProtectora() {
		return urlVideoProtectora;
	}

	public void setUrlVideoProtectora(String urlVideoProtectora) {
		this.urlVideoProtectora = urlVideoProtectora;
	}

	public String getDescripcionProcedimientoProtectora() {
		return descripcionProcedimientoProtectora;
	}

	public void setDescripcionProcedimientoProtectora(String descripcionProcedimientoProtectora) {
		this.descripcionProcedimientoProtectora = descripcionProcedimientoProtectora;
	}

	public String getLatitudProtectora() {
		return latitudProtectora;
	}

	public void setLatitudProtectora(String latitudProtectora) {
		this.latitudProtectora = latitudProtectora;
	}

	public String getAltitudProtectora() {
		return altitudProtectora;
	}

	public void setAltitudProtectora(String altitudProtectora) {
		this.altitudProtectora = altitudProtectora;
	}

	@Override
	public String toString() {
		return "Protectora [idProtectora=" + idProtectora + ", nombreProtectora=" + nombreProtectora
				+ ", emailProtectora=" + emailProtectora + ", contraseñaProtectora=" + contraseñaProtectora
				+ ", telefonoProtectora=" + telefonoProtectora + ", localidadProtectora=" + localidadProtectora
				+ ", direccionProtectora=" + direccionProtectora + ", imagenProtectora=" + imagenProtectora
				+ ", logoProtectora=" + logoProtectora + ", fechaCreacionProtectora=" + fechaCreacionProtectora
				+ ", estadoProtectora=" + estadoProtectora + ", perfilProtectora=" + perfilProtectora
				+ ", descripcionProtectora=" + descripcionProtectora + ", urlVideoProtectora=" + urlVideoProtectora
				+ ", descripcionProcedimientoProtectora=" + descripcionProcedimientoProtectora + ", latitudProtectora="
				+ latitudProtectora + ", altitudProtectora=" + altitudProtectora + "]";
	}

	

}
