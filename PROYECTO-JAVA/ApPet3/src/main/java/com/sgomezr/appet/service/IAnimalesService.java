package com.sgomezr.appet.service;

import java.util.List;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Protectora;

public interface IAnimalesService {
	
	void insertar (Animal animal);
	
	List<Animal> buscarTodosLosAnimales();

	Animal buscarAnimalPorId (int idAnimal);
	
	List<Animal> buscarAnimalPorProtectora (String protectoraAnimal);
	
	List<String> buscarTipologias();

	
}
