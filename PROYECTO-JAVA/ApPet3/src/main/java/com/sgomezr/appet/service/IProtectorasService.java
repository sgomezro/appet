package com.sgomezr.appet.service;

import java.util.List;


import com.sgomezr.appet.model.Protectora;


public interface IProtectorasService {

	List<Protectora> buscarTodasLasProtectoras();
	
	void insertar (Protectora protectora);
	
	Protectora buscarProtectoraPorNombre (String nombreProtectora);
	
	List<String> buscarLocalidades();
	

}
