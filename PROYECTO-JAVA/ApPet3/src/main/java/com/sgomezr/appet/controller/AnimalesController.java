package com.sgomezr.appet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sgomezr.appet.model.Animal;
import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.model.Protectora;
import com.sgomezr.appet.service.IAnimalesService;
import com.sgomezr.appet.service.IProtectorasService;
import com.sgomezr.appet.util.Utileria;

@Controller
@RequestMapping("/animales")
public class AnimalesController {
	
	@Autowired
	private IAnimalesService serviceAnimales;
	
	@Autowired
	private IProtectorasService serviceProtectoras;
	
	@GetMapping("/listaAnimales")
	public String mostrarIndex(Model model) {
		List<Animal> listaAnimales = serviceAnimales.buscarTodosLosAnimales();
		model.addAttribute("animales",listaAnimales);
		return "animales/listaAnimales";
	}
	
	@GetMapping("/crear")
	public String crear(Model model, @ModelAttribute Animal animal) {
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		
		List <String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras",protectoras);
		
		model.addAttribute("tipos", serviceAnimales.buscarTipologias());

		return "animales/formAnimal";
		
	}
	
	@PostMapping("/guardar")
	public String guardar(Model model,  @ModelAttribute Animal animal,@ModelAttribute Protectora protectora, BindingResult result, RedirectAttributes atributos, @RequestParam ("archivoImagen") MultipartFile multiPart, HttpServletRequest request ) {
		
		List<Protectora> listaProtectoras = serviceProtectoras.buscarTodasLasProtectoras();
		List <String> protectoras = new LinkedList<>();
		for (Protectora p : listaProtectoras) {
			protectoras.add(p.getNombreProtectora());
		}
		model.addAttribute("protectoras",listaProtectoras);
		
		if(result.hasErrors()) {
			System.out.println("Errores");
			return "animales/formAnimal";
		}
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart,request);
			animal.setImagen(nombreImagen);
		}
		
		System.out.println("Recibiendo objeto animal" + animal);
		System.out.println("Recibiendo objeto animal de protectora: " + protectora.getNombreProtectora());
		System.out.println("Elementos en la lista antes de la inserción: " + serviceAnimales.buscarTodosLosAnimales().size());
		serviceAnimales.insertar(animal);
		System.out.println("Elementos en la lista despues de la inserción: " + serviceAnimales.buscarTodosLosAnimales().size());
		atributos.addFlashAttribute("mensaje","El animal ha sido creado y guardado correctamente");
		return "redirect:/animales/listaAnimales";
	}
		
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat ("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	


}
