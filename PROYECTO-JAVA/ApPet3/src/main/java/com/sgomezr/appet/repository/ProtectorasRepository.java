package com.sgomezr.appet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgomezr.appet.model.Protectora;

@Repository
public interface ProtectorasRepository extends JpaRepository<Protectora, Integer> {

}
