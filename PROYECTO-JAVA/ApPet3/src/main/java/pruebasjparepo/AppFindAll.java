package pruebasjparepo;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sgomezr.appet.model.Noticia2;
import com.sgomezr.appet.repository.NoticiasRespository;

//Ahora estamos usando la intefaz JpaRepository ne vez de la de CrudRepository
public class AppFindAll {
	
	public static void main(String[] args) {

		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NoticiasRespository repo = context.getBean("noticiasRespository", NoticiasRespository.class);
		
		//Recuperar todas las entidades (metodo findAll)
		
		List<Noticia2> lista = repo.findAll();
		for (Noticia2 n : lista) {
			System.out.println(n);
			
		}
		context.close();

	}
}
