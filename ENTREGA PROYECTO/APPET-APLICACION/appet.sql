-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dbappet
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adopciones`
--

DROP TABLE IF EXISTS `adopciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adopciones` (
  `id_adopcion` int(11) NOT NULL AUTO_INCREMENT,
  `estado_adopcion` varchar(45) CHARACTER SET latin1 NOT NULL,
  `Protectoras_id_protectora` int(11) NOT NULL,
  `usuarios_username` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_adopcion`),
  KEY `fk_Adopciones_Protectoras1_idx` (`Protectoras_id_protectora`),
  KEY `fk_Adopciones_usuarios1_idx` (`usuarios_username`),
  CONSTRAINT `fk_Adopciones_Protectoras1` FOREIGN KEY (`Protectoras_id_protectora`) REFERENCES `protectoras` (`id_protectora`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Adopciones_usuarios1` FOREIGN KEY (`usuarios_username`) REFERENCES `usuarios` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adopciones`
--

LOCK TABLES `adopciones` WRITE;
/*!40000 ALTER TABLE `adopciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `adopciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animales`
--

DROP TABLE IF EXISTS `animales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animales` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_animal` varchar(45) CHARACTER SET latin1 NOT NULL,
  `tipo_animal` varchar(45) CHARACTER SET latin1 NOT NULL,
  `raza_animal` varchar(45) CHARACTER SET latin1 NOT NULL,
  `sexo_animal` varchar(45) CHARACTER SET latin1 NOT NULL,
  `fechaNacimiento_Animal` date DEFAULT NULL,
  `imagen_animal` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `tieneMicrochip_animal` enum('Si','No') CHARACTER SET latin1 NOT NULL,
  `descripcion_animal` varchar(10000) CHARACTER SET latin1 NOT NULL,
  `fechaPublicacion_animal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `estado_animal` enum('En adopcion','En apadrinamiento','Adoptado','Apadrinado') CHARACTER SET latin1 NOT NULL,
  `comportamiento_animal` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `envio_animal` varchar(45) CHARACTER SET latin1 NOT NULL,
  `tasaAdopcion_animal` int(11) NOT NULL,
  `urlVideo_animal` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Adopciones_id_adopcion` int(11) DEFAULT NULL,
  `Protectoras_id_protectora` int(11) NOT NULL,
  `edad_animal` int(11) NOT NULL,
  PRIMARY KEY (`id_animal`),
  UNIQUE KEY `id_animal_UNIQUE` (`id_animal`),
  KEY `fk_Animales_Adopciones1_idx` (`Adopciones_id_adopcion`),
  KEY `fk_Animales_Protectoras1_idx` (`Protectoras_id_protectora`),
  CONSTRAINT `fk_Animales_Adopciones1` FOREIGN KEY (`Adopciones_id_adopcion`) REFERENCES `adopciones` (`id_adopcion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animales_Protectoras1` FOREIGN KEY (`Protectoras_id_protectora`) REFERENCES `protectoras` (`id_protectora`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animales`
--

LOCK TABLES `animales` WRITE;
/*!40000 ALTER TABLE `animales` DISABLE KEYS */;
INSERT INTO `animales` VALUES (1,'Sayker','Perro','Labrador Retrevier','Macho','2016-03-08','perros012.jpg','Si','<p><strong>Sayker, un labrador retrevier grande y noblote.</strong></p>\r\n<p>De car&aacute;cter tranquilo y acostumbrado a estar con personas mayores que por edad y enfermedad no pueden seguir cuidando de &eacute;l. Es urgente adopci&oacute;n, ya que sus due&ntilde;os debido a su alta edad ya ni siquiera pueden sacarle a la calle y Sayker necesita hacer ejercicio diario y moverse de continuo, es un labrador muy nervioso, cuyo nervio viene asociado a su corta edad.</p>\r\n<ul>\r\n<li>Es muy cari&ntilde;oso y reclama contacto cuando te conoce.</li>\r\n<li>Es super sociable con otros perros, gatos y ni&ntilde;os, le encanta jugar con todos los perros y se comunica a la perfecci&oacute;n con ellos.</li>\r\n</ul>\r\n<p><strong><em>&iquest;Quieres adoptar a Sayker? No te arrepentir&aacute;s!</em></strong></p>','2019-02-09 16:47:14','En adopcion','Es bueno con otros animales,Es bueno con niños,Es cariñoso,Es amigable','A toda España',120,'https://www.youtube.com/embed/WY8VG0QcGqs',NULL,1,2),(4,'Zeus','Perro','Sin determinar','Macho','2010-09-12','animal.jpg','No','<p><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Zeus es un perro sensible, manifestando esa debilidad mental en forma de energ&iacute;a. Pasea bien con correa pero los estimulos r&aacute;pidos le pueden agobiar en ocasiones, solo necesita confianza y rutina de entorno tranquilo.&nbsp;</span><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Aunque estamos trabajando con &eacute;l y su actitudes es muy positiva, porque tiene muchas ganas de aprender, necesita encontrar una casa de entorno tranquilo, evitando &aacute;reas excesivamente ruidosas y con continuidad veremos el fantastico perrete que es en realidad.&nbsp;</span><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Esta es la consecuencia de malas experiencias en su &eacute;poca de abandono pero es un perro joven y seguro que su pasado quedar&aacute; olvidado en cuanto alguien le de una oportunidad.&nbsp;</span><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Su nivel de actividad es media y <strong>le encanta el contacto cuando te conoce.</strong></span></p>\r\n<p><span style=\"font-size: 16px;\"><em><span style=\"color: #800000; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif;\">*Nota: De momento no poseemos ni fotos ni videos de Zeus ya que se ha incorporado a la protectora recientemente, pero las a&ntilde;adiremos a su ficha con la mayor brevedad posible.</span></em></span></p>','2019-02-08 21:04:12','En apadrinamiento','Es bueno con otros animales,Es bueno con niños,Es cariñoso,Es amigable','Córdoba',0,'Sin video',NULL,10,8),(5,'Miau Miau','Gato','Sin determinar','Hembra','2016-12-03','gatos003.jpg','Si','<p>Miau Miau es una gatita que se quedo huerfana hace un tiempo.</p>\r\n<p>Despu&eacute;s de tener una vida c&oacute;moda y tranquila se vio en la calle sola, perseguida y amenazada.</p>\r\n<p>Gracias al equipo de rescate de BurgosProtect, pudimos rescatarla y le dimos un refugio, pero este no es lugar, no confia ya en los humanos, el tiempo que paso en la calle la hizo desconfiar completamente de ellos.</p>\r\n<p>Necesita alguien que entienda su pasado y la pesadilla vivio.<strong> Estamos seguros de que en un ambiente tranquilo donde la dejen ser ella misma recuperar&aacute; la confianza y sera al fin feliz.</strong></p>','2019-02-08 21:09:14','En apadrinamiento','Muerde,Araña','A toda España',50,'Sin video',NULL,10,2),(6,'Babe','Cerdo','Vietnamita','Macho','2018-03-21','cerdos001.jpg','No','<p>Babe es un cerdito encantador pero muestra desconfianza hacia las personas cuando no las conoce, fue abandonado hace no mucho tiempo y desde entonces no ha vuelto a ser el mismo. \r Necesita adoptantes con experiencia en granja y que vivan en un entorno muy tranquilo, cerca del campo y de otros animales de granja.</p> <p>\r Es cariñoso, amigable y se lleva muy bien con niños y personas de todas las edades.</p> <strong>Si quieres hacerte su amigo de forma rapida...¡¡Le encanta comer galletas!! </strong>','2019-02-08 20:13:37','En adopcion','Es bueno con otros animales,Es bueno con niños,Es cariñoso,Es amigable','Córdoba',50,'https://www.youtube.com/embed/zdlfzcGKBl8',NULL,10,0),(9,'Jimmy','Gato','Sin determinar','Macho','2018-08-29','gatos001.jpg','Si','<p><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Jimmy es un gatito muy tranquilo y sociable. Puede convivir con perros y con todo tipo de animalesperfectamente.&nbsp;</span><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Actualmente est&aacute; en una casa de acogida pero buscamos para &eacute;l un hogar definitivo.</span></p>','2019-02-08 22:40:23','En adopcion','Es bueno con otros animales,Es bueno con niños,Es cariñoso,Es amigable','Alicante',65,'https://www.youtube.com/embed/Dp9X8m7W20k',NULL,12,0),(10,'Filemón','Perro','Sin determinar','Macho','2011-03-08','perros009.jpg','No','<p><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px; background-color: #fafafa;\">Filem&oacute;n es un peque&ntilde;ajo encantador, sociable con otros perros, gatos y cualquier tipo de animal y al que le gusta el contacto con las personas.</span></p>\r\n<p><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Es bastante equilibrado, su energ&iacute;a es media y pasea bastante bien con la correa.</span></p>\r\n<p><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Le encanta jugar con sus juguetes y esperar a que le tiren la pelota una y ota vez. Perfecto para vivir con ni&ntilde;os.</span></p>\r\n<p>&nbsp;</p>','2019-02-08 22:40:23','En apadrinamiento','Es bueno con otros animales,Es bueno con niños,Es cariñoso,Es amigable','Madrid',20,'Sin video',NULL,11,7),(11,'Charlie','Gato','Gato Negro','Macho','2013-02-13','gatos002.jpg','No','<p><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Charlie es un gato encantador, que tuvo que ser entregado por sus due&ntilde;os debido a un problema de alergia.</span><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Se trata un gato destinado a aquellas personas que quieren compartir su vida con la naturaleza felina en estado puro.</span><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Es un gato grande, poderoso, especialista en juegos de caza. Sin embargo necesita la compa&ntilde;&iacute;a de los humanos, es muy afectuoso, cari&ntilde;oso y agradable</span><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><br style=\"box-sizing: inherit; color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\" /><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif; font-size: 16px;\">Si te atre el esp&iacute;ritu y la belleza de los grandes felinos y sabes respetar su forma de ver el mundo, entonces este es tu gato.</span></p>\r\n<p><span style=\"font-size: 16px; color: #ff0000;\"><strong><span style=\"color: #333333; font-family: \'Open Sans\', verdana, Tahoma, arial, sans-serif;\">*Nota: Charlie acaba de ser recientemente ADOPTADO!, enhorabuena Charlie, esperamos que disfrutes de tu nueva vida.</span></strong></span></p>','2019-02-08 22:48:21','Adoptado','Es bueno con otros animales,Es bueno con niños','Madrid',45,'https://www.youtube.com/embed/YG1g7JUVBbA',NULL,11,5),(12,'Lolita','Tortuga','Tortuga enana','Hembra','2018-09-03','tortugas001.jpg','No','<p>Lolita es una tortuga enana, no crece.</p>\r\n<p>A veces muerde, pero es muy tranquila.</p>','2019-02-08 22:48:21','En adopcion','Muerde','Lugo',10,'https://www.youtube.com/embed/WMw8eDczanU',NULL,13,0);
/*!40000 ALTER TABLE `animales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animalesfavoritos`
--

DROP TABLE IF EXISTS `animalesfavoritos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animalesfavoritos` (
  `id_favorito` int(11) NOT NULL AUTO_INCREMENT,
  `Animales_id_animal` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_favorito`),
  KEY `fk_AnimalesFavoritos_Animales1_idx` (`Animales_id_animal`),
  CONSTRAINT `fk_AnimalesFavoritos_Animales1` FOREIGN KEY (`Animales_id_animal`) REFERENCES `animales` (`id_animal`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animalesfavoritos`
--

LOCK TABLES `animalesfavoritos` WRITE;
/*!40000 ALTER TABLE `animalesfavoritos` DISABLE KEYS */;
INSERT INTO `animalesfavoritos` VALUES (17,4,78),(22,1,77),(23,6,77),(24,5,77),(26,1,78);
/*!40000 ALTER TABLE `animalesfavoritos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animalesfavoritos_has_usuario`
--

DROP TABLE IF EXISTS `animalesfavoritos_has_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animalesfavoritos_has_usuario` (
  `usuarios_username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `AnimalesFavoritos_id_favorito` int(11) NOT NULL,
  PRIMARY KEY (`usuarios_username`,`AnimalesFavoritos_id_favorito`),
  KEY `fk_AnimalesFavoritos_has_Usuario_usuarios1_idx` (`usuarios_username`),
  KEY `fk_AnimalesFavoritos_has_Usuario_AnimalesFavoritos1_idx` (`AnimalesFavoritos_id_favorito`),
  CONSTRAINT `fk_AnimalesFavoritos_has_Usuario_AnimalesFavoritos1` FOREIGN KEY (`AnimalesFavoritos_id_favorito`) REFERENCES `animalesfavoritos` (`id_favorito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_AnimalesFavoritos_has_Usuario_usuarios1` FOREIGN KEY (`usuarios_username`) REFERENCES `usuarios` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animalesfavoritos_has_usuario`
--

LOCK TABLES `animalesfavoritos_has_usuario` WRITE;
/*!40000 ALTER TABLE `animalesfavoritos_has_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `animalesfavoritos_has_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apadrinamientos`
--

DROP TABLE IF EXISTS `apadrinamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apadrinamientos` (
  `id_apadrinamiento` int(11) NOT NULL,
  `estado_apadrinamiento` varchar(45) CHARACTER SET ascii NOT NULL,
  `Animales_id_animal` int(11) NOT NULL,
  `Protectoras_id_protectora` int(11) NOT NULL,
  PRIMARY KEY (`id_apadrinamiento`),
  KEY `fk_Apadrinamientos_Animales1_idx` (`Animales_id_animal`),
  KEY `fk_Apadrinamientos_Protectoras1_idx` (`Protectoras_id_protectora`),
  CONSTRAINT `fk_Apadrinamientos_Animales1` FOREIGN KEY (`Animales_id_animal`) REFERENCES `animales` (`id_animal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Apadrinamientos_Protectoras1` FOREIGN KEY (`Protectoras_id_protectora`) REFERENCES `protectoras` (`id_protectora`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apadrinamientos`
--

LOCK TABLES `apadrinamientos` WRITE;
/*!40000 ALTER TABLE `apadrinamientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `apadrinamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_banner` varchar(45) CHARACTER SET latin1 NOT NULL,
  `fecha_banner` date DEFAULT NULL,
  `archivo_banner` varchar(150) CHARACTER SET latin1 NOT NULL,
  `estado_banner` enum('Activo','Inactivo') CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_banner`),
  UNIQUE KEY `id_banner_UNIQUE` (`id_banner`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'Slide1','2018-12-16','inicio003.jpg','Activo'),(2,'Slide 2','2018-12-16','inicio002.jpg','Activo'),(3,'Slide 3','2018-12-16','inicio001.jpg','Activo'),(4,'Slide 4','2018-12-16','inicio004.jpg','Inactivo');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacto`
--

DROP TABLE IF EXISTS `contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacto` (
  `id_contacto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_contacto` varchar(45) CHARACTER SET latin1 NOT NULL,
  `apellidos_contacto` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email_contacto` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `tipos_contacto` varchar(300) CHARACTER SET latin1 NOT NULL,
  `experiencia_contacto` varchar(45) NOT NULL,
  `notificaciones_contacto` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `comentarios_contacto` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id_contacto`),
  UNIQUE KEY `id_contacto_UNIQUE` (`id_contacto`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacto`
--

LOCK TABLES `contacto` WRITE;
/*!40000 ALTER TABLE `contacto` DISABLE KEYS */;
INSERT INTO `contacto` VALUES (1,'Sandra','Gomez Roman','sgomezro922@gmail.com','Perro','1','Noticias,Nuevos Animales','Hola');
/*!40000 ALTER TABLE `contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donaciones`
--

DROP TABLE IF EXISTS `donaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donaciones` (
  `id_donacion` int(11) NOT NULL AUTO_INCREMENT,
  `estado_donacion` varchar(45) CHARACTER SET latin1 NOT NULL,
  `Protectoras_id_protectora` int(11) NOT NULL,
  `usuarios_username` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_donacion`),
  KEY `fk_Donaciones_Protectoras1_idx` (`Protectoras_id_protectora`),
  KEY `fk_Donaciones_usuarios1_idx` (`usuarios_username`),
  CONSTRAINT `fk_Donaciones_Protectoras1` FOREIGN KEY (`Protectoras_id_protectora`) REFERENCES `protectoras` (`id_protectora`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Donaciones_usuarios1` FOREIGN KEY (`usuarios_username`) REFERENCES `usuarios` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donaciones`
--

LOCK TABLES `donaciones` WRITE;
/*!40000 ALTER TABLE `donaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `donaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_noticia` varchar(250) CHARACTER SET latin1 NOT NULL,
  `imagen_noticia` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `fechaPublicacion_noticia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `detalle_noticia` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `estado_noticia` enum('Activa','Inactiva') CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_noticia`),
  UNIQUE KEY `id_noticia_UNIQUE` (`id_noticia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (1,'Recogida de alimentos para nuestros peludos',NULL,'2018-12-16 19:28:59','<p><span style=\"color: #993300;\"><strong>Os presentamos una nueva iniciativa solidaria de la mano de ApPet.</strong></span></p>\r\n<p>Desde el 1 de Enero de 2019 y hasta el dia 31 de Febrero se ha puesto en marcha una campa&ntilde;a de donaciones de comida para perros y gatos que ir&aacute;n destinadas a nuestra Protectoras registradas en la aplicacion.</p>','Activa'),(2,'¡LLEVAMOS EL GORDO!',NULL,'2018-12-16 19:28:40','<p>Hola a todos!</p>\r\n<p>Ya tenemos disponible nuestra LOTER&Iacute;A DE NAVIDAD. Pod&eacute;is adquirirla los S&aacute;bados y Domingos de 10 a 14h aprox en nuestro albergue o en los puntos de venta que os indicamos aqui abajo:\"</p>\r\n<ul>\r\n<li>Paseo Marques de Zafra n&ordm;11 (Madrid)</li>\r\n<li>Paseo Marques de Zafra n&ordm;35 (Madrid).</li>\r\n</ul>\r\n<p><span style=\"background-color: #ffff99;\">Precio por papeleta: 5 euros, de los cuales se juegan 4 euros y 1 euro sera donado a nuestras asociaciones</span></p>','Activa');
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfiles` (
  `id_perfil` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(50) CHARACTER SET latin1 NOT NULL,
  `usuarios_username` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_perfil`),
  UNIQUE KEY `usuarios_username_perfil_UNIQUE` (`usuarios_username`,`perfil`),
  KEY `fk_perfiles_usuarios1_idx` (`usuarios_username`),
  CONSTRAINT `fk_perfiles_usuarios1` FOREIGN KEY (`usuarios_username`) REFERENCES `usuarios` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfiles`
--

LOCK TABLES `perfiles` WRITE;
/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;
INSERT INTO `perfiles` VALUES (87,'ADMINISTRADOR','admin'),(99,'PROTECTORA','AguilarProtect'),(104,'PROTECTORA','AlicanteProtect'),(107,'PROTECTORA-NA','AvilaProtect'),(101,'PROTECTORA','BurgosProtect'),(102,'PROTECTORA','CordobaProtect'),(105,'PROTECTORA','LugoProtect'),(103,'PROTECTORA','MadridProtect'),(33,'USUARIO','user');
/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protectoras`
--

DROP TABLE IF EXISTS `protectoras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protectoras` (
  `id_protectora` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_protectora` varchar(45) CHARACTER SET latin1 NOT NULL,
  `nombre_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `apellidos_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `email_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `password_usuario` varchar(120) CHARACTER SET latin1 NOT NULL,
  `comprobarPass_usuario` varchar(120) CHARACTER SET latin1 NOT NULL,
  `fechaNacimiento_usuario` date DEFAULT NULL,
  `localidad_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `logo_protectora` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `fechaCreacion_usuario` date DEFAULT NULL,
  `estado_usuario` enum('Activa','Inactiva') CHARACTER SET latin1 NOT NULL,
  `telefono_protectora` int(11) NOT NULL,
  `direccion_protectora` varchar(150) CHARACTER SET latin1 NOT NULL,
  `imagen_protectora` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `perfil_protectora` varchar(45) CHARACTER SET latin1 NOT NULL,
  `descripcion_protectora` varchar(10000) CHARACTER SET latin1 NOT NULL,
  `urlVideo_protectora` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `descripcionProcedimiento_protectora` varchar(10000) CHARACTER SET latin1 NOT NULL,
  `latitud_protectora` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `altitud_protectora` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id_protectora`),
  UNIQUE KEY `id_protectora_UNIQUE` (`id_protectora`),
  UNIQUE KEY `nombre_protectora_UNIQUE` (`nombre_protectora`),
  UNIQUE KEY `email_usuario_UNIQUE` (`email_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protectoras`
--

LOCK TABLES `protectoras` WRITE;
/*!40000 ALTER TABLE `protectoras` DISABLE KEYS */;
INSERT INTO `protectoras` VALUES (1,'BurgosProtect','Sandra','Gomez Roman','burgosprotect@gmail.com','burgosprotect','burgosprotect','2010-06-02','Burgos','proBurgos.jpg','2019-02-13','Activa',666666666,'Ctra. Quintanadueñas, 2, 09001 Burgos','general017.jpg','Protectora','<p>Nuestra Asociaci&oacute;n desempe&ntilde;a la funci&oacute;n de acogimiento de animales abandonados, perros y gatos, denunciar el abandono y maltrato, y promover la defensa de los mismos, desde hace m&aacute;s de treinta a&ntilde;os.</p>\r\n<p>Nuestros ingresos dependen de donativos particulares, huchas, mercadillos, cuotas de socios, cuotas de padrinos, de la colaboraci&oacute;n de asociaciones en Alemania Suiza , de las aportaciones de amantes de los animales de pa&iacute;ses fundamentalmente europeos. En la historia de esta asociaci&oacute;n ha habido innumerables momentos duros, much&iacute;simos casos impactantes, momentos dif&iacute;ciles por la falta de recursos, y miles de motivos para desistir y perder definitivamente la fe en el ser humano, pero a&uacute;n as&iacute; siempre hemos contado con el apoyo de numerosos colaboradores que nos han dado fuerzas para continuar, y nunca han faltado un lamet&oacute;n o un ronroneo de agradecimiento a nuestro trabajo que nos impulse a seguir luchando por ellos.</p>\r\n<p>Desde esta asociaci&oacute;n se defiende la esterilizaci&oacute;n como &uacute;nico medio digno de controlar la superpoblaci&oacute;n de perros y gatos.</p>\r\n<p>Adem&aacute;s de gestionar nuestro Refugio, realizamos charlas contra el abandono en colegios, centros c&iacute;vicos, salones de actos. Por ello, queremos animar desde aqu&iacute; a todas aquellas personas sensibilizadas con el problema del abandono, para que nos ayuden a fomentar la posibilidad de la adopci&oacute;n de un animal abandonado, como primera opci&oacute;n a considerar a la hora de adquirir un animal de compa&ntilde;&iacute;a. Esta asociaci&oacute;n entiende que es necesario fomentar entre la gente las adopciones de animales abandonados, primeramente, para hacer part&iacute;cipe a la sociedad del grave problema que supone la negligencia humana para con los animales y, en segundo lugar, para concienciar plenamente a los adoptantes de que compartir la vida con un animal comporta una serie de responsabilidades y obligaciones para toda la vida.</p>','https://www.youtube.com/embed/6_QyMOiYr78','<p>Para la protectora de Burgos es muy importante antes de entregar uno de nuestros animales ponernos en contacto con la persona adoptante, para conocerla y asegurarnos de que dejamos a nuestros animales en buenas manos. Ponte en contacto con nostros v&iacute;a telefono o email si estas interesado en adoptar o apadrinar en nuestra protectora</p>\r\n<p><strong>En BurgosProtect nos aseguraremos de que antes de adoptar un animal debes tener en cuenta que:</strong></p>\r\n<p>- Tendr&aacute;s una serie de gastos en comida, veterinarios, accesorios?</p>\r\n<p>- Deber&aacute;s dedicarle tiempo cada d&iacute;a, darle atenciones, pasearlo?.</p>\r\n<p>- Tu casa y/o jard&iacute;n puede sufrir algunos destrozos.</p>\r\n<p>- En vacaciones o fines de semana, deber&aacute;s llevarlo a una residencia.</p>\r\n<p><strong>Antes de la adopci&oacute;n, y para conocer el estado f&iacute;sico de tu nuevo compa&ntilde;ero, aseg&uacute;rate de que te entregan el animal:</strong></p>\r\n<p>- Con el microchip implantado</p>\r\n<p>- Desparasitado</p>\r\n<p>- Esterilizado</p>\r\n<p>Cada animal adoptado deja un espacio en el Refugio, para que otro que lo necesite, pueda ocupar su lugar.</p>','42.3602324','-3.7108882572561415'),(10,'CordobaProtect','Oscar','Arjona Ramirez','cordobaprotect@cordobaprotect.com','cordobaprotect','cordobaprotect','2019-01-01','Córdoba','protectoraImg.png','2019-02-08','Activa',666812788,' Calle Ronda del Marrubial, 19, 14007 Córdoba','general009.jpg','Protectora','<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">La protectora de animales de C&oacute;rdoba o tambien conocida como <strong>\"El Arca de No&eacute;\"</strong> es una asociaci&oacute;n sin &aacute;nimo de lucro que naci&oacute; el 11 de junio de 1997 y se dedica principalmente a la protecci&oacute;n de animales de C&oacute;rdoba y alrededores.</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">Est&aacute; formada por voluntarios que dedican su tiempo a rescatar, recuperar y cuidar animales, con el objetivo de proporcionarles una vida digna y encontrarles una familia definitiva que re&uacute;na las condiciones adecuadas.</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">La Asociaci&oacute;n se financia con la ayuda de socios y padrinos (unos 300 actualmente) y donativos particulares. No cuenta con subvenciones ni ayudas p&uacute;blicas. Todo el dinero recaudado se destina &iacute;ntegramente al cuidado de los animales de la Asociaci&oacute;n.</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">El Arca de No&eacute; de C&oacute;rdoba ha pasado hace poco por el desalojo de su anterior albergue, y tras una larga y costosa lucha, ha conseguido construir la primera fase de unas nuevas instalaciones en C&oacute;rdoba, inaugurada a comienzos del 2016.</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">Actualmente se encuentra recaudando nuevos fondos para continuar con la contrucci&oacute;n del albergue (quedan pendientes dos fases).</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</div>\r\n<div style=\"box-sizing: border-box; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">Se puede colaborar con la Asociaci&oacute;n de muchas formas: acogiendo, adoptando, haciendo donativos o simplemente difundiendo su animales. Hay muchas maneras y todas se agradecen.&nbsp;</div>','https://www.youtube.com/embed/4MxRmiiWHik','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 10pt;\"><span style=\"box-sizing: border-box; font-weight: bold;\">&iquest;Est&aacute;s&nbsp;interesado/a en adoptar? Entonces, por favor, lee con atenci&oacute;n</span></span><span style=\"box-sizing: border-box; font-size: 10pt;\"><span style=\"box-sizing: border-box; font-weight: bold;\">.<br style=\"box-sizing: border-box;\" /></span></span><em style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">&nbsp;</span></em><br style=\"box-sizing: border-box;\" /><em style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">Ten en cuenta que la mayor&iacute;a de animales disponibles para adoptar precisan de unas necesidades m&iacute;nimas: alimentaci&oacute;n, dedicaci&oacute;n, veterinario... Si crees que no puedes d&aacute;rselo por falta de tiempo o recursos, por favor, no contactes.</span></em><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-size: 10pt;\"><br style=\"box-sizing: border-box;\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 10pt;\"><span style=\"box-sizing: border-box; font-weight: bold;\">&iquest;Que condiciones m&iacute;nimas necesito para adoptar un animal?</span><br style=\"box-sizing: border-box;\" />Debes tener presente que debes ser mayor de edad o, en su defecto, que un adulto&nbsp;(padre, madre o tutor legal) realice el procedimiento.<br style=\"box-sizing: border-box;\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">Enviamos a los animales por transporte animalista a cualquier lugar de Espa&ntilde;a.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-weight: bold;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">&iquest;Que protocolos hay?</span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">Es imprescindible pasar por el primer filtro: un cuestionario&nbsp;pre-adopci&oacute;n. Si lo realizas y resultas seleccionado como posible adoptante, deber&aacute;s tener una reuni&oacute;n con alg&uacute;n representante de la Asociaci&oacute;n para concretar detalles, conocerte mejor y conocer el posible futuro entorno del animal que quieras adoptar.<br style=\"box-sizing: border-box;\" />Si consigues pasar todos los filtros, habr&aacute; una etapa de adaptaci&oacute;n del animal, que ser&aacute; supervisada en todo momento por un miembro de la Asociaci&oacute;n. Si la adaptaci&oacute;n no se realiza con &eacute;xito, se interrumpir&aacute; la adopci&oacute;n, seleccionando a un nuevo adoptante.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-weight: bold;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">Si decido adoptar, &iquest;que debo hacer?</span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">Debes solicitar el cuestionario pre-adopci&oacute;n, que deber&aacute;s rellenar con los datos del animal que quieres adoptar, tus datos y responder a todas las preguntas para valorar si eres apto para adoptar al animal que solicitas.<br style=\"box-sizing: border-box;\" />Si tardamos mas de 24h en responderte, no desesperes, quiz&aacute;s haya m&aacute;s personas interesadas, las cuales debemos valorar para seleccionar a los que creemos los mejores adoptantes para ese animal. Seas seleccionado o no, ser&aacute;s notificado.<br style=\"box-sizing: border-box;\" />Una vez rellenado y enviado el cuestionario pre-adopci&oacute;n, ser&aacute;s informado de los protocolos siguientes para seguir con la adopci&oacute;n, en caso de ser seleccionado.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">Este cuestionario tambi&eacute;n te ayudar&aacute; a plantearte situaciones que pueden suceder una vez tengas el animal en casa y que en ocasiones pueden ser problem&aacute;ticas. T&oacute;mate tu tiempo para rellenarlo y reflexiona bien acerca de ellas.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-weight: bold;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">&iquest;Hay tasa de adopci&oacute;n?</span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">Si, siempre. Es algo que debes tener muy presente. Todos los animales que pasan por nuestra asociaci&oacute;n siguen un protocolo sanitario para descartar enfermedades u otros problemas de salud, poniendo remedio m&eacute;dico si es necesario. Algunos llegan en estados cr&iacute;ticos, algunos muy j&oacute;venes, con necesidades especiales, otros muy viejitos con problemas de salud de altos costes veterinarios...&nbsp;<br style=\"box-sizing: border-box;\" />Con la tasa de adopci&oacute;n no cubrimos estos gastos, pero nos ayudas a respirar un poquito.<em style=\"box-sizing: border-box;\"><br style=\"box-sizing: border-box;\" /></em><br style=\"box-sizing: border-box;\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-weight: bold;\"><span style=\"box-sizing: border-box; font-size: 10pt;\">&iquest;Que valoraciones hacemos para aceptar o denegar una adopci&oacute;n?</span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Rubik, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; font-size: 13.3333px;\">Nosotros no solo buscamos un adoptante acorde a nuestros rescatados, si no que tambi&eacute;n buscamos que nuestros rescatados sean aptos para los adoptantes.<br style=\"box-sizing: border-box;\" />Quiz&aacute;s eres un buen adoptante, pero el animal que solicitas no sea apto para ti. Por eso es tan importante el cuestionario pre-adopci&oacute;n y, si resultas seleccionado, la entrevista siguiente.<br style=\"box-sizing: border-box;\" />Nosotros conocemos a nuestros rescatados, sus necesidades, car&aacute;cter, posibles traumas... Nos falta conocerte a ti y valorar si sois compatibles.</span></p>','37.8916485','-4.8195048'),(11,'MadridProtect','Marta','Roman Talamillo','madridprotect@gmail.com','madridprotect','madridprotect','1992-02-04','Madrid','protectoraMadrid.png','2019-02-08','Activa',916672036,'Calle de Ochagavia, 34, 28039 Madrid','general007.jpg','Protectora','<div class=\"wpb_text_column wpb_content_element \" style=\"background: 0px 0px; border: 0px; margin: 0px 0px 35px; padding: 0px; vertical-align: baseline; outline: 0px;\">\r\n<div class=\"wpb_wrapper\" style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">\r\n<p style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px; text-align: justify;\"><span style=\"color: #000000;\">La&nbsp;<span style=\"color: #ff6600;\"><strong style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">Asociaci&oacute;n Nacional Amigos de los Animales&nbsp;<span style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">(<span class=\"anaa\" style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">ANAA</span>)</span></strong></span>&nbsp;es una entidad sin &aacute;nimo de lucro que se fund&oacute; en Madrid (Espa&ntilde;a) en 1992, como respuesta al elevado n&uacute;mero de animales que son abandonados y maltratados en nuestro pa&iacute;s y a la deficiente atenci&oacute;n de que son objeto por parte de la administraci&oacute;n, que hasta el momento y salvo unas pocas excepciones, se ha limitado a recogerlos y eliminarlos, sin resolver el problema de una manera humanitaria y efectiva.</span></p>\r\n<p style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px;\"><span style=\"color: #000000;\">&nbsp;</span></p>\r\n<p style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px; text-align: justify;\"><span style=\"color: #000000;\"><span style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #ff6600;\"><strong style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">ANAA</strong></span>&nbsp;est&aacute; legalmente constituida y debidamente inscrita en el:</span></p>\r\n<ul style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; list-style-position: inside;\">\r\n<li style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\"><span style=\"color: #000000;\">Registro Nacional de Asociaciones del Ministerio del Interior n&ordm; 112.523.</span></li>\r\n<li style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\"><span style=\"color: #000000;\">Registro Provincial de Asociaciones de la Delegaci&oacute;n del Gobierno n&ordm; 12.653.</span></li>\r\n<li style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\"><span style=\"color: #000000;\">Registro de Asociaciones de la Comunidad de Madrid n&ordm; 27.976.</span></li>\r\n<li style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\"><span style=\"color: #000000;\">T&iacute;tulo de Entidad Colaboradora de la Comunidad de Madrid, n&ordm; CM-004.</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"vc_empty_space\" style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; height: 42px;\">\r\n<p style=\"background: 0px 0px #ffffff; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px; font-family: \'Open Sans\', sans-serif; text-align: justify;\"><span style=\"color: #000000; background-color: #ffffff;\"><span style=\"background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\"><span style=\"color: #ff6600;\"><strong style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\"><span class=\"anaa\" style=\"background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">ANAA</span></strong></span>&nbsp;</span>es miembro del&nbsp;<span style=\"color: #ff6600;\"><strong style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">Consejo de Protecci&oacute;n y Bienestar Animal de la Comunidad de Madrid.</strong></span></span></p>\r\n</div>','https://www.youtube.com/embed/oAplYJDHOrY','<div class=\"wpb_text_column wpb_content_element \" style=\"background: 0px 0px; border: 0px; margin: 0px 0px 35px; padding: 0px; vertical-align: baseline; outline: 0px;\">\r\n<div class=\"wpb_wrapper\" style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px;\">\r\n<h3 style=\"letter-spacing: 0px; font-weight: 400; color: #414141; background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; font-size: 25px; line-height: 35px; font-family: \'Josefin Sans\', sans-serif; text-align: justify;\"><span style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #ff6600;\">Proceso de adopci&oacute;n</span></h3>\r\n<p style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px; text-align: justify;\"><span style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #000000;\">Una vez tomada la importante decisi&oacute;n de adoptar uno de los animales de ANAA comienza la fase de entrevista y primer contacto con nuestros animales. Es importante que todos aquellos que vayan a convivir con el animal acudan al Centro de Adopci&oacute;n para as&iacute; confirmar que todos est&aacute;n de acuerdo con la adopci&oacute;n.</span></p>\r\n<p style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px; text-align: justify;\"><span style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #000000;\">Una vez en el Centro de Adopci&oacute;n uno de los voluntarios realizar&aacute; una peque&ntilde;a entrevista para conocer la experiencia de la familia con otros animales, el entorno de su vivienda o las condiciones en las que se encontrar&aacute; el nuevo miembro, entre otras cuestiones. Adem&aacute;s, se informar&aacute; a la familia sobre las condiciones del Contrato de Adopci&oacute;n y se le ayudar&aacute; a escoger al animal perfecto entre todos los que se encuentran en el Albergue, con el fin de hacer posible una mejor adaptaci&oacute;n y una adopci&oacute;n de &eacute;xito.</span></p>\r\n<p style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px; text-align: justify;\"><span style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #000000;\">El tiempo que ocupa el total del proceso puede variar en funci&oacute;n del n&uacute;mero de personal disponible, la cantidad de afluencia de posibles adoptantes y la duraci&oacute;n de las propias entrevistas.</span></p>\r\n<p style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #9e9e9e; font-size: 15px; line-height: 25px; text-align: justify;\"><span style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; color: #000000;\">En caso de tener un perro que vaya a convivir con el nuevo animal adoptado es importante que tambi&eacute;n acuda al Centro de Adopci&oacute;n. As&iacute; se podr&aacute; comprobar la compatibilidad de caracteres entre ambos y su adaptaci&oacute;n ser&aacute; m&aacute;s f&aacute;cil.</span></p>\r\n</div>\r\n</div>\r\n<div class=\"vc_empty_space\" style=\"background: 0px 0px; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; outline: 0px; height: 42px;\">&nbsp;</div>','40.4545928','-3.6944306'),(12,'AlicanteProtect','Jesús','González Ramirez','alicanteprotect@gmail.com','alicanteprotect','alicanteprotect','1995-02-27','Alicante','protectoraAlicante.jpg','2019-02-08','Activa',965960224,'Camino Elche Viejo, s/n, 03114 Alicante','general14.JPG','Protectora','<p style=\"box-sizing: border-box; margin: 0px 0px 1.421em; color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">La <span style=\"color: #800080;\"><strong>Sociedad Protectora de Animales y Plantas de Alicante</strong></span> es una entidad sin &aacute;nimo de lucro, fundada en 1968, cuya finalidad es promover el respeto y el cari&ntilde;o hacia los animales.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.421em; color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">Para el cumplimiento de estos fines, llevamos a cabo distintos programas de actuaci&oacute;n que en l&iacute;neas generales se dividir&iacute;an en dos, por un lado aquellos cuyo objetivo es la concienciaci&oacute;n o prevenci&oacute;n, y por otro, aquellos destinados a intentar paliar los efectos de uno de los peores problemas que siguen padeciendo los animales de compa&ntilde;&iacute;a y que no es otro que el abandono en la v&iacute;a p&uacute;blica. Por eso, desde el albergue ofrecemos un refugio a aquellos animales de compa&ntilde;&iacute;a que por desgracia han sido abandonados o maltratados.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">A estas dos tareas se une otra muy importante, que es la labor que a nivel social desde el albergue de animales y a trav&eacute;s de la Sociedad Protectora de Animales y Plantas de Alicante se desarrolla ayudando a los animales y ayudando tambi&eacute;n a las personas con problemas.</p>','https://www.youtube.com/embed/is78xRFutbQ','<p><span style=\"font-size: 19px; color: #800080;\"><em><strong><span style=\"font-family: \'Source Sans Pro\'; text-align: justify;\">&iquest;Qu&eacute; condiciones debe reunir un adoptante?</span></strong></em></span></p>\r\n<p><span style=\"color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">Para adoptar a un animal del albergue es necesario por encima de todo, querer a los animales y ser responsable para tenerlo.&nbsp;</span><strong style=\"box-sizing: border-box; color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">Una adopci&oacute;n hoy no puede acabar en un abandono ma&ntilde;ana&hellip;</strong><span style=\"color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">&nbsp;Nosotros no buscamos que nuestros animales sean un regalo para nadie, queremos que las personas que los adopten sean un regalo para nuestros animales.&nbsp;</span></p>\r\n<p><span style=\"color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">Pongase en contacto con nosotros via tel&eacute;fono, email o atrav&eacute;s de ApPet si quiere adoptar o apadrinar a alguno de nuestros animales.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.421em; color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">Las condiciones en las que se entrega un animal en el albergue dependen sustancialmente del estado en el que se encuentra el mismo. Si est&aacute; bien f&iacute;sicamente, si nos ha llegado herido, atropellado o enfermo y, por supuesto, de la edad del mismo seg&uacute;n sea un cachorro o no.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.421em; color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\"><span style=\"color: #800080;\"><em><strong>&iquest;En que condiciones se entregan a los animales del albergue?</strong></em></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.421em; color: #384554; font-family: \'Source Sans Pro\'; font-size: 19px; text-align: justify;\">En cualquier caso,&nbsp;<strong style=\"box-sizing: border-box;\">nuestro protocolo de adopci&oacute;n incluye siempre la vacunaci&oacute;n, la desparasitaci&oacute;n, la identificaci&oacute;n y la esterilizaci&oacute;n, que el adoptante debe de estar dispuesto a asumir,</strong>&nbsp;siempre que sea posible en el mismo momento en el que el animal sale del albergue y, si ello no es posible por las causas explicadas anteriormente, en las semanas posteriores a la adopci&oacute;n, para lo que se firma un contrato. La adopci&oacute;n no se considera finalizada hasta que se cumplimente todo lo anterior</p>','38.3578356','-0.5075437'),(13,'LugoProtect','Lucia','Gil Hernández','lugoprotect@gmail.com','lugoprotect','lugoprotect','2019-03-08','Lugo','protectoraLugo.jpg','2019-02-08','Activa',600548849,'Codeside, s/n, 27192 Santa Mª Muxa, Lugo','general003.jpg','Protectora','<p style=\"margin-top: 0px; margin-bottom: 10px; color: #555555; font-family: Helvetica, Arial, sans-serif; font-size: 13px; text-align: justify;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-size: 14px;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-family: arial, helvetica, sans-serif;\">La&nbsp;<strong><em>Sociedad Protectora de Animales y Plantas de Lugo</em></strong>&nbsp;fue inscrita en el Registro Provincial de Asociaciones, con fecha 5 de Febrero de 1982, momento en el que inici&oacute; su actividad bajo la direcci&oacute;n de&nbsp;<strong>D. Juan Jos&eacute; Fern&aacute;ndez Medina</strong>, presidente de la Junta Gestora de la Sociedad.</span></span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 10px; color: #555555; font-family: Helvetica, Arial, sans-serif; font-size: 13px; text-align: justify;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-size: 14px;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-family: arial, helvetica, sans-serif;\"><br />Semanas despu&eacute;s de su constituci&oacute;n, el 15 de marzo, celebra su primera Junta General Ordinaria de la que sale elegida la primera Junta Directiva de la&nbsp;<em><strong>Sociedad</strong></em>&nbsp;que ser&aacute; presidida por&nbsp;<strong>D. Jes&uacute;s Benito Rego Cobo</strong>&nbsp;quien se dirigi&oacute; a la Diputaci&oacute;n Provincial en solicitud de cesi&oacute;n de terrenos en los que ubicar sus primeras instalaciones de acogida.</span></span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 10px; color: #555555; font-family: Helvetica, Arial, sans-serif; font-size: 13px; text-align: justify;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-size: 14px;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-family: arial, helvetica, sans-serif;\"><br />Aquellas gestiones dieron como resultado la cesi&oacute;n de una parcela, de unos 3.000 metros cuadrados, ubicada en la finca denominada Codeside o Codesido, pr&oacute;xima a As G&aacute;ndaras (Muxa), a unos 7 kil&oacute;metros del centro de Lugo, lugar en las que aun se mantienen sus instalaciones. Desde sus comienzos, las distintas juntas directivas han ido construyendo los cheniles (o caniles), que hoy conforman las actuales instalaciones, con una capacidad de acogida m&aacute;xima de 120 animales, n&uacute;mero siempre superado por las circunstancias de cada momento.</span></span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 10px; color: #555555; font-family: Helvetica, Arial, sans-serif; font-size: 13px; text-align: justify;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-size: 14px;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-family: arial, helvetica, sans-serif;\">&nbsp;<img style=\"background: transparent; border: 0pt none; font-style: inherit; font-weight: inherit; margin: 0px 10px; padding: 0px; outline: 0px; vertical-align: baseline; float: left;\" src=\"http://www.protectoralugo.org/images/protectora/articulos/cachorros.jpg\" align=\"left\" border=\"0\" />Las actividades desarrolladas, desde sus inicios hasta hoy, han estado ligadas a la vida de los animales -principalmente perros abandonados y gatos-, y, en un plano secundario, a las plantas, aunque en sus inicios&nbsp; muchos de sus esfuerzos estuvieron dirigidos a la defensa del acebo, especie hoy protegida gracias a las voces de quienes, entonces, reg&iacute;an los destinos de nuestra&nbsp;<strong><em>Sociedad</em></strong>&nbsp;que, junto a otras organizaciones de&nbsp;<strong><em>Defensa de la Naturaleza</em></strong>, lograron su protecci&oacute;n.</span></span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 10px; color: #555555; font-family: Helvetica, Arial, sans-serif; font-size: 13px; text-align: justify;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-size: 14px;\"><span style=\"background: transparent; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; font-family: arial, helvetica, sans-serif;\">&nbsp; Calculamos que, en los veintinueve a&ntilde;os de vida de la&nbsp;<em><strong>Sociedad,</strong></em>&nbsp;nuestras instalaciones han dado cobijo a m&aacute;s 25.000 perros, un elevado n&uacute;mero de gatos,&nbsp; caballos, ovejas, cabras y burros; conejos, hurones o lagartos abandonados, y ha venido colaborando en la recuperaci&oacute;n de un sin fin de aves de distintas especies, aves que hoy desv&iacute;a hac&iacute;a el&nbsp;<strong><em>Centro de Recuperaci&oacute;n de Fauna Silvestre \"O Veral\"</em></strong>, dependiente de la&nbsp;<strong>Xunta de Galicia.</strong></span></span></p>','https://www.youtube.com/embed/ksxFr7ATTuM','<p style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 16px; overflow-wrap: break-word; color: #777777; line-height: 1.66em; font-family: \'Open Sans\';\"><span style=\"color: #000000;\">Todos l@s interesad@s tendr&aacute;n que cubrir un cuestionario.</span></p>\r\n<p style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 16px; overflow-wrap: break-word; color: #777777; line-height: 1.66em; font-family: \'Open Sans\';\"><span style=\"color: #000000;\">No es un examen, sino el modo que tenemos de saber si el gat@ o perr@ que le interesa a la persona es el adecuado para su modo de vida o, si no se fij&oacute; en ninguno en concreto, poder discernir cu&aacute;l ser&aacute; el id&oacute;neo.</span></p>\r\n<p style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 16px; overflow-wrap: break-word; color: #777777; line-height: 1.66em; font-family: \'Open Sans\';\"><span style=\"color: #000000;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-weight: bold;\">NO</span>&nbsp;entregamos perros para guardia, ni gatos para cazar ratones, ni animales que vayan a estar cerrados en lugares peque&ntilde;os (sea en casa o piso) ni mucho menos atados. En los casos de animales que vayan a estar en el exterior o con acceso al exterior valoramos individualmente cual de nuestros gatos o perros puede adaptarse o incluso ser id&oacute;neo para esa situaci&oacute;n (gatos independientes, perros muy activos&hellip;). No entregamos un animal en adopci&oacute;n si no est&aacute;n de acuerdo con &eacute;l todos los habitantes del lugar donde vaya vivir.</span></p>','43.0547155','-7.5325888');
/*!40000 ALTER TABLE `protectoras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protectorasfavoritas`
--

DROP TABLE IF EXISTS `protectorasfavoritas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protectorasfavoritas` (
  `id_favorito` int(11) NOT NULL AUTO_INCREMENT,
  `Protectoras_id_protectora` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_favorito`),
  KEY `fk_ProtectorasFavoritas_Protectoras1_idx` (`Protectoras_id_protectora`),
  CONSTRAINT `fk_ProtectorasFavoritas_Protectoras1` FOREIGN KEY (`Protectoras_id_protectora`) REFERENCES `protectoras` (`id_protectora`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protectorasfavoritas`
--

LOCK TABLES `protectorasfavoritas` WRITE;
/*!40000 ALTER TABLE `protectorasfavoritas` DISABLE KEYS */;
INSERT INTO `protectorasfavoritas` VALUES (6,1,77),(7,10,77),(8,10,78),(9,1,78);
/*!40000 ALTER TABLE `protectorasfavoritas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protectorasfavoritas_has_usuario`
--

DROP TABLE IF EXISTS `protectorasfavoritas_has_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protectorasfavoritas_has_usuario` (
  `ProtectorasFavoritas_id_favorito` int(11) NOT NULL,
  `usuarios_username` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ProtectorasFavoritas_id_favorito`,`usuarios_username`),
  KEY `fk_ProtectorasFavoritas_has_Usuario_ProtectorasFavoritas1_idx` (`ProtectorasFavoritas_id_favorito`),
  KEY `fk_ProtectorasFavoritas_has_Usuario_usuarios1_idx` (`usuarios_username`),
  CONSTRAINT `fk_ProtectorasFavoritas_has_Usuario_ProtectorasFavoritas1` FOREIGN KEY (`ProtectorasFavoritas_id_favorito`) REFERENCES `protectorasfavoritas` (`id_favorito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ProtectorasFavoritas_has_Usuario_usuarios1` FOREIGN KEY (`usuarios_username`) REFERENCES `usuarios` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protectorasfavoritas_has_usuario`
--

LOCK TABLES `protectorasfavoritas_has_usuario` WRITE;
/*!40000 ALTER TABLE `protectorasfavoritas_has_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `protectorasfavoritas_has_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_has_apadrinamientos`
--

DROP TABLE IF EXISTS `usuario_has_apadrinamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_has_apadrinamientos` (
  `usuarios_username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Apadrinamientos_id_apadrinamiento` int(11) NOT NULL,
  PRIMARY KEY (`usuarios_username`,`Apadrinamientos_id_apadrinamiento`),
  KEY `fk_Usuario_has_Apadrinamientos_usuarios1_idx` (`usuarios_username`),
  KEY `fk_Usuario_has_Apadrinamientos_Apadrinamientos1_idx` (`Apadrinamientos_id_apadrinamiento`),
  CONSTRAINT `fk_Usuario_has_Apadrinamientos_Apadrinamientos1` FOREIGN KEY (`Apadrinamientos_id_apadrinamiento`) REFERENCES `apadrinamientos` (`id_apadrinamiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_has_Apadrinamientos_usuarios1` FOREIGN KEY (`usuarios_username`) REFERENCES `usuarios` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_has_apadrinamientos`
--

LOCK TABLES `usuario_has_apadrinamientos` WRITE;
/*!40000 ALTER TABLE `usuario_has_apadrinamientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_has_apadrinamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `nombre_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `apellidos_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `email_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `password_usuario` varchar(120) CHARACTER SET latin1 NOT NULL,
  `comprobarPass_usuario` varchar(120) CHARACTER SET latin1 NOT NULL,
  `genero_usuario` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `fechaNacimiento_usuario` date DEFAULT NULL,
  `edad_usuario` int(11) NOT NULL,
  `localidad_usuario` varchar(45) CHARACTER SET latin1 NOT NULL,
  `imagen_usuario` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `fechaCreacion_usuario` datetime NOT NULL,
  `estado_usuario` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `id_usuario_UNIQUE` (`id_usuario`),
  UNIQUE KEY `nick_usuario_UNIQUE` (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_usuario_UNIQUE` (`email_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (77,'user','user','user','user@user.com','$2a$10$EtWP4EdMfMeib15a5MW9fuCZs3QJ1eiIwYIzRbIjct0Q/jbIg9Cx2','$2a$10$KP70NKbGpPieDCXd8k.MEOkiCC2cpCp7EcOfmaRdf1PoKB4KpxC1K','Masculino','2018-08-10',0,'A Coruña','user01.jpg','2019-01-19 13:57:12',1),(78,'admin','admin','admin','admin@admin.com','$2a$10$TXNNweq0tNGYothmiPkt/uq1uJ7mpt7nR.TkGZTYXu/ad7gGxPrr6','$2a$10$ffceOvz9IrCMxFuAI8mnE.Q5BV8jktl8Y.iU5MZxjOVyqjTyHFj.e','Masculino','1992-12-06',26,'Burgos','admin01.jpg','2019-02-13 15:46:52',1),(89,'AguilarProtect','AguilarProtect','AguilarProtect','AguilarProtect@AguilarProtect.com','$2a$10$oRj4phUN52rGjWzs649AwORn.vYpbDZdWAA.hDTVnKZ3pmZLKADzi','$2a$10$EGoPg4/SlHtzc3YBaXTmMuIPngx8I.9UYQqaZnRhPS4DQ8Nh8Jt0.',NULL,'2019-01-01',0,'A Coruña','protectoraImg.png','2019-02-03 18:24:43',1),(91,'BurgosProtect','Sandra','Gomez Roman','burgosprotect@gmail.com','$2a$10$jPcAWeboHVJB6onSLbaHkO2130eNyplK2/K96QHuOabpmIZ/AcWoS','$2a$10$A8hKqhGx7BAwe4Q7EpJK/e65s0prZVp0vWaiLnGbyWYSNBKbJnRdK',NULL,'2010-06-02',8,'Burgos','proBurgos.jpg','2019-02-13 16:30:02',1),(92,'CordobaProtect','Oscar','Arjona Ramirez','cordobaprotect@cordobaprotect.com','$2a$10$ntCSp2./Yq3zmfuGjJfCLuvGa7Sr1JgEtLe4u1AGHnG3fgqdEF6He','$2a$10$HeIGuf5eFsqdIkZh8aX8sOKUqZ30trmRxN5CpEmX58VMAGzyWXb2K',NULL,'2019-01-01',0,'Córdoba','protectoraImg.png','2019-02-08 22:25:27',1),(93,'MadridProtect','Marta','Roman Talamillo','madridprotect@gmail.com','$2a$10$nkZs.8lvojt3UaD3Jesjg.sgSrt8BZtvgho5BZQDqsM/7qwVY9HOi','$2a$10$R3StrDCm/xNNp2qVwYsQ5eQcOjMLPupi.lLJGcGD8IP61KJBWhcqi',NULL,'1992-02-04',27,'Madrid','protectoraMadrid.png','2019-02-08 23:02:27',1),(94,'AlicanteProtect','Jesús','González Ramirez','alicanteprotect@gmail.com','$2a$10$0NzbOiqa0BchdhFRyVdp.uHtjuBEQJlk6iDsmz1sXPjS0tgHBw2CC','$2a$10$P08mPo/ftn5sMXmhq5XUJex7.pt01s1QzCMPoQrGu45VL63yCs4Se',NULL,'1995-02-27',23,'Alicante','protectoraAlicante.jpg','2019-02-08 23:04:28',1),(95,'LugoProtect','Lucia','Gil Hernández','lugoprotect@gmail.com','$2a$10$v2G9hix.VxuTUJcNWoMxLeyMi0DGxW7Vvpg7D3oGpa6mFZ4kQYfsO','$2a$10$dz.XVYDF4wq4dc0otnqdWuXDephnG.VvEUliKT9JuY5cCqdpfCMXW',NULL,'2019-03-08',0,'Lugo','protectoraLugo.jpg','2019-02-08 23:18:37',1),(97,'AvilaProtect','Maria','Gomez Gomez','avilaprotect@gmail.com','$2a$10$A2RoWkfEyLF/0QEqC0f4BO/PR.kpRu2RarXblP4OgJbqTcr7NEz6.','$2a$10$ZksehcZOIHFKv3cxLCo//OFKZGD5iVqd6CJJhsNe77Edvd6TzEqei',NULL,'2011-10-06',7,'Ávila','XXKUEBVTprotectoraAvila.png','2019-02-13 16:18:36',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-13 16:35:38
